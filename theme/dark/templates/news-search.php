<?php echo $header; ?>

<script>
    document.title = "Search Results | KCES";
</script>

<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
    <div class="container">
    <div class="page-header page-img">
<!--        <h1> Products </h1>-->
        <img width="1200" height="320" src="<?php echo BASE_URL; ?>/images/SEARCH RESULTS.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="<?php echo BASE_URL; ?>/images/SEARCH RESULTS.jpg 1200w, <?php echo BASE_URL; ?>/images/SEARCH RESULTS.jpg 300w, <?php echo BASE_URL; ?>/images/SEARCH RESULTS.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>        
    </div>
</div><!--titleclass-->

<div id="content" class="container">
    
    <div class="main" id="ktmain" role="main">
        
        <div class='col-md-12 col-sm-12' style='padding:0;'>
            <div class='navigation pagination' style='display:block'>
            <div class='newsletter'>
            </div>
            <div class='newsletter_btn'><a href='<?php echo base_url();?>newsletter'>Newsletter</a></div>
            </div>
        </div>
        
        <div id="kad-blog-grid" class="clearfix">
                  
        
        <?php
        // if(count($search_news) > 0) 
        // {
            foreach ($search_news as $s)
            {
            
//            $nid = $u['news_id'];
//            $photo = $u['news_image'];
//            $news_image = base_url().'uploads/news_events/'.$u['news_image'];
//            $news_title = $u['news_title'];
//            $short_content = $u['short_content'];
//            $news_desc = $u['news_desc'];
      
        ?>
        
<!--        <div class='col-sm-6 col-md-6 mrg-btm40' style='padding:0'>

            <div class='image-left'><img style='height:173px; width:200px' src="<?php //echo base_url().'uploads/news_events/'.$s['news_image'];?>" class='attachment-medium size-medium wp-post-image' alt='<?php //echo base_url().'uploads/news_events/'.$s['news_image'];?>' srcset='<?php //echo base_url().'uploads/news_events/'.$s['news_image'];?> 165w, <?php //echo base_url().'uploads/news_events/'.$s['news_image'];?> 143w' sizes='(max-width: 165px) 100vw, 165px'></div>
                <div class='content-right'>
                    <a href='<?php //echo base_url();?>news_events/<?php //echo base64_encode($s['news_id']);?>' title="<?php //echo $s['news_title'];?>">
                    <h2 class='heading-inner-page'><?php //echo $s['news_title'];?></h2></a>
                    <p><p><?php //echo $s['short_content'];?></p></p>
                    <div class='read-more'> <a href='<?php //echo base_url();?>news_events/<?php //echo base64_encode($s['news_id']);?>'>READ MORE</a>
                </div>
            </div>

        </div>-->
   <div class="row new-row">
<div class="tcol-md-12 tcol-sm-12 tcol-xs-12 tcol-ss-12 search_item">
<div id="post-1882" class="blog_item grid_item">

                      <div class="col-md-4 col-sm-4 col-xs-12">
                      <div class="imghoverclass img-margin-center">
                                    <a href="<?php echo base_url();?>news_events/<?php echo base64_encode($s['news_id']);?>" title="<?php echo $s['news_title'];?>">
                             
                              <img style="height:200px; width:250px;" alt="<?php echo $s['news_title'];?>" class="attachment-post-thumbnail wp-post-image" src="<?php echo base_url().'uploads/news_events/'.$s['news_image'];?>">
                                                             </a> 
                               </div>
                        </div>  
                        <div class="col-md-8 col-sm-8 col-xs-12">    
                      <div class="postcontent">
                          <header>
                              <a href="<?php echo base_url();?>news_events/<?php echo base64_encode($s['news_id']);?>"><h5 class="entry-title"><?php echo $s['news_title'];?></h5></a>
                                <div class="subhead color_gray">
                                  <span class="postauthortop author vcard" rel="tooltip" data-placement="top" data-original-title="KCES_ADMIN">
                                    <i class="icon-user"></i>
                                  </span>
                                  <span class="kad-hidepostauthortop"> | </span>
                                    
                                <span class="postcommentscount" rel="tooltip" data-placement="top" data-original-title="0">
                                  <i class="icon-bubbles"></i>
                                </span>
                                <span class="postdatetooltip">|</span>
                                 <span style="margin-left:3px;" class="postdatetooltip updated" rel="tooltip" data-placement="top" data-original-title="2018-05-31">
                                  <i class="icon-calendar"></i>
                                </span>
                              </div>   
                          </header>
                          <div class="entry-content">
                              <p>
                                  <?php echo $s['short_content'];?> <br />
                                  <a style="color:#1fbdf0;" href="<?php echo base_url();?>news_events/<?php echo base64_encode($s['news_id']);?>">Read More</a>
                              </p>
                          </div>
                          <footer>
                                                        </footer>
                        </div><!-- Text size -->
                        </div>
             
</div> <!-- Blog Item -->
       </div>  </div> <br /><br />
        <?php
            }
        //}
        ?>
           
       
    </div>

    <div class='wp-pagenavi'><?php echo $pagination; ?></div>
    

</div>    
    
        
</div>
    
</div>

<?php echo $footer; ?>
