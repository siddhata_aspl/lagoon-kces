<?php
include_once('header.php');
?>

        <script>
         document.title = "Login | KCES";
        </script>

<style>
    
    .login {
        margin-top: 20%;
    }
    
    .panel-heading {
        background-color: #272973 !important;
        padding: 10px;
    }
    
    .panel-body {
        padding: 8% 18%;
    }
    
    .login-title {
        color: #ffffff;
        text-align: center;
        display: inherit;
        font-weight: 500;
        font-size: 16px;
        padding: 5px;
    }
    
    .btn-submit {
        background-color: #0074C8 !important;
        border: #2d2e74;
        width: 50% !important;
        text-align: center;
        margin: auto;
    }
    
</style>


<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
<!--    <div class="container">
	<div class="page-header page-img">
        <h1> Products </h1>
        <img width="1200" height="320" src="styles/images/about_img.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="styles/images/about_img.jpg 1200w, styles/images/about_img-300x80.jpg 300w, styles/images/about_img-1024x273.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>		
    </div>container-->
</div><!--titleclass-->

<div id="content" class="container">
    <div class="main" id="ktmain" role="main">   
        
        <div class="login">
        
            <div class="col-md-6 col-md-offset-3" style="float: none !important; margin: auto !important;">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <span class="login-title">NEW PASSWORD</span>
                    </div>
                    <div class="panel-body">

<!--                        <form action="" method="post" accept-charset="utf-8">-->
                            <fieldset>                               
                                <div class="form-group">		
                <?php echo form_error('password', '<div class="alert alert-danger">', '</div>'); ?>									                             
					<div class="controls">
						<?php 	$data = array(
						  'name'        => 'password',
						  'id'          => 'password',
						  'class'       => 'form-control',
                                                  'placeholder' => 'New Password',
						  'value'		=> set_value('password'),
                                                  'Required' => 'Required',
						);
			
						echo form_password($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                <div class="form-group">	
                <?php echo form_error('con_password', '<div class="alert alert-danger">', '</div>'); ?>									
					
					<div class="controls">
						<?php 	$data = array(
						  'name'        => 'con_password',
						  'id'          => 'con_password',
						  'class'       => 'form-control',
                                                  'placeholder' => 'Confirm Password',
						  'value'		=> set_value('con_password'),
                                                  'Required' => 'Required',
						);
			
						echo form_password($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->        
                                
                                <br />
                                
                                <div class="form-group">
                                    <input type="submit" name="submit" id="submit" class="btn btn-success btn-block btn-submit" value="Reset">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>


<?php
include_once('footer.php');
?>