<!-- FOOTER
=================================-->
<!--<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-xs-12 pull-left">
                <p><?php echo $settings['siteFooter']; ?></p>
            </div>
            <div class="col-md-5 col-xs-12 pull-right text-right">
                <?php getSocial(); ?>
            </div>
        </div>
    </div>
</div>-->

<!-- /FOOTER ============-->


<!---------- design footer start ----------->


<footer id="containerfooter" class="footerclass" role="contentinfo">
    <div class="container">
        <div class="row">

            <div class="col-md-2 col-sm-2 foot-logo footercol1">
                <div class="widget-1 widget-first footer-widget"><aside id="text-3" class="widget widget_text">         <div class="textwidget"><img src="<?php echo BASE_URL; ?>/uploads/<?php echo $settings['siteFooterLogo']; ?>" alt="<?php echo $settings['siteFooterLogo']; ?>"  /></div>
                    </aside></div>                  </div> 
            <div class="col-md-7 col-sm-9 col-xs-12 foot-address">

                <div class="footercol2">
                    <div class="widget-1 widget-first footer-widget"><aside id="text-2" class="widget widget_text"><h3><?php echo $settings['add1_title']; ?></h3>          <div class="textwidget"><p><?php echo $settings['add1']; ?></p>
                            </div>
                        </aside></div><div class="widget-2 footer-widget"><aside id="text-4" class="widget widget_text">            <div class="textwidget"><div class="cont-dt">
                                    <p><b>PHONE:</b> <?php echo $settings['add1_phone']; ?></p>
                                    <p><b>FAX: </b> <?php echo $settings['add1_fax']; ?></p>
                                </div>
                            </div>
                        </aside></div><div class="widget-3 widget-last footer-widget"><aside id="text-7" class="widget widget_text">            <div class="textwidget"><div class="cont-dt">
                                    <p><b>HOURS:</b> <?php echo $settings['add1_hours']; ?></p>
                                    <p><a href="<?php echo $settings['add1_map']; ?>"  target="_blank"><b>SEE MAP</b></a></div>
                            </div>
                        </aside></div>                  </div> 

                <div class="footercol3">
                    <div class="widget-1 widget-first footer-widget"><aside id="text-5" class="widget widget_text"><h3> <?php echo $settings['add2_title']; ?></h3>         <div class="textwidget"><p><?php echo $settings['add2']; ?></p>
                            </div>
                        </aside></div><div class="widget-2 footer-widget"><aside id="text-6" class="widget widget_text">            <div class="textwidget"><div class="cont-dt"><p><b>PHONE:</b> <?php echo $settings['add2_phone']; ?></p><p><b>FAX:</b> <?php echo $settings['add2_fax']; ?></p></div></div>
                        </aside></div><div class="widget-3 widget-last footer-widget"><aside id="text-8" class="widget widget_text">            <div class="textwidget"><div class="cont-dt"><p><b>HOURS:</b> <?php echo $settings['add2_hours']; ?></p><a href="<?php echo $settings['add2_map']; ?>" target="_blank"><b>SEE MAP</b></a></div></div>
                        </aside></div>                  </div> 
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12 foot-newsletter footercol4">
                <div class="widget-1 widget-first footer-widget">
                    <aside id="newsletterwidget-2" class="widget widget_newsletterwidget">
                        <h3>SIGN UP FOR OUR NEWSLETTER</h3>

<!--                        <script type="text/javascript">
                        //<![CDATA[
                            if (typeof newsletter_check !== "function") {
                                window.newsletter_check = function (f) {
                                    var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                    if (!re.test(f.elements["ne"].value)) {
                                        alert("The email is not correct");
                                        return false;
                                    }
                                    for (var i = 1; i < 20; i++) {
                                        if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                            alert("");
                                            return false;
                                        }
                                    }
                                    if (f.elements["ny"] && !f.elements["ny"].checked) {
                                        alert("You must accept the privacy statement");
                                        return false;
                                    }
                                    return true;
                                }
                            }
                        //]]>
                        </script>-->

                        <div class="newsletter newsletter-widget">

<!--                            <script type="text/javascript">
                            //<![CDATA[
                                if (typeof newsletter_check !== "function") {
                                    window.newsletter_check = function (f) {
                                        var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                        if (!re.test(f.elements["ne"].value)) {
                                            alert("The email is not correct");
                                            return false;
                                        }
                                        for (var i = 1; i < 20; i++) {
                                            if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                                alert("");
                                                return false;
                                            }
                                        }
                                        if (f.elements["ny"] && !f.elements["ny"].checked) {
                                            alert("You must accept the privacy statement");
                                            return false;
                                        }
                                        return true;
                                    }
                                }
                            //]]>
                            </script>-->

<style>
#newsletterwidget-2 form input[type="button"], #newsletterwidget-2 form button {
   position: absolute;
   right: 0px;
   top: 0px;
   border: none;
   background-color: #E6E7E8;
   font-size: 12px;
   color: #282973;
   height: 37px;
   padding: 5px 6px;
   width: 50px;
   line-height: 14px;
   text-transform: uppercase;
}

#newsletterwidget-2 form ::-webkit-input-placeholder { /* WebKit browsers */
    color: #000;
    opacity: 1 !important;
}
</style>

<!--                            <form action="http://kcelectricalsupply.com/?na=s" onsubmit="return newsletter_check(this)" method="post">-->
                           <form action="#" id="newsform" onsubmit="" method="post">
                               <input type="hidden" name="nr" value="widget"/>
                               <p>
                                   <input id="newsmail" class="newsletter-email" required="required" type="email" style="color:#000" name="newsmail" placeholder="Email"/>
<!--                                     <input class="newsletter-email" type="email" style="color:#000" required name="ne" value="Email" onclick="if (this.defaultValue == this.value)
           this.value = ''" onblur="if (this.value == '')
                       this.value = this.defaultValue"/>-->
                               </p>
                               <p><input class="newsletter-submit" type="button" onclick="newsmail_add()" value="Subscribe"/></p>
                           </form>
                       </div>
                   </aside>
               </div>

<script type="text/javascript">

   function newsmail_add() {
       //alert();
           var id = $('#newsmail').val();
           var form = $('#newsform');
           if(id!='')
           {
               $.ajax({
                   url: '<?php echo base_url().'newsletter/add'; ?>',
                   type: "POST",
                   //dataType: "json",
                   data: form.serialize(), // serializes the form's elements.
                   success: function(data){   //alert();
                                   //alert(data); // show response from the php script.
                                   alert("You are subscribed successfully");
                                   location.reload();
                       },
                               error: function(xhr, status, error) {
                   },
               });
           }
           else
           {
               alert("Please enter valid mail id");
           }
       }
   //});
</script>


                        <div class="widget-2 widget-last footer-widget">
                            <aside id="widget_kadence_social-2" class="widget widget_kadence_social">    <div class="virtue_social_widget clearfix">

                            <?php getSocial(); ?>

                            </aside>
                        </div>                  </div> 
        </div> <!-- Row -->        
    </div><!-- container -->
    <div class="footercredits clearfix">
        <div class="container">
            <div class="row">
                <div class="col-sm-8"><div class="footernav clearfix">
<!--                        <ul id="menu-footer-menu" class="footermenu"><li  class="menu-aboutkces menu-item-638"><a href="about.php">About KCES</a></li>
                            <li  class="menu-ourservices menu-item-642"><a href="our-service.php">Our Services</a></li>
                            <li  class="menu-products menu-item-645"><a href="products.php">Products</a></li>
                            <li  class="menu-training menu-item-644"><a href="training.php">Training</a></li>
                            <li  class="menu-resources menu-item-641"><a href="resources.php">Resources</a></li>
                            <li  class="menu-news038events menu-item-640"><a href="news-events.php">News &#038; Events</a></li>
                            <li  class="menu-contactus menu-item-639"><a href="contact-us.php">Contact Us</a></li>
                            <li  class="menu-promos menu-item-969"><a href="promos.php">Promos</a></li>
                            <li  class="menu-termsandconditions menu-item-852"><a href="terms-and-conditions.php">Terms and conditions</a></li>-->
                            <ul id="menu-footer-menu" class="footermenu">
                            <?php hooskNav('footer') ?>
                            </ul>
<!--                            <li  class="menu-kcesemployeeemaillogin menu-item-1453"><a href="https://login.microsoftonline.com/en">KCES Employee Email Login</a></li>-->
                        <!--</ul>-->
                    
                    </div></div>                <div class="col-sm-4"><p><?php echo $settings['siteFooter2']; ?></p></div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <?php echo $settings['siteFooter']; ?>
                </div>
            </div>
        </div>
    </div><!-- credits -->
</footer>


<!---------- design footer complete ----------->

<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?php echo THEME_FOLDER; ?>/templates/js/bootstrap.min.js"></script>
<script type='text/javascript'>
              /* <![CDATA[ */
              var wc_add_to_cart_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "i18n_view_cart": "View Cart", "cart_url": "http:\/\/kcelectricalsupply.com", "is_cart": "", "cart_redirect_after_add": "no"};
              /* ]]> */
</script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/woocommerce/assets/js/frontend/add-to-cart.min91ac.js?ver=2.6.8'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min44fd.js?ver=2.70'></script>
<script type='text/javascript'>
                /* <![CDATA[ */
                var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
                /* ]]> */
</script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/woocommerce/assets/js/frontend/woocommerce.min91ac.js?ver=2.6.8'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/woocommerce/assets/js/jquery-cookie/jquery.cookie.min330a.js?ver=1.4.1'></script>
<script type='text/javascript'>
                /* <![CDATA[ */
                var wc_cart_fragments_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%", "fragment_name": "wc_fragments"};
                /* ]]> */
</script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/woocommerce/assets/js/frontend/cart-fragments.min91ac.js?ver=2.6.8'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/min/plugins-ck8a23.js?ver=277'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/main8a23.js?ver=277'></script>
<script type='text/javascript'>
                /* <![CDATA[ */
                var wc_add_to_cart_variation_params = {"i18n_no_matching_variations_text": "Sorry, no products matched your selection. Please choose a different combination.", "i18n_unavailable_text": "Sorry, this product is unavailable. Please choose a different combination."};
                var wc_add_to_cart_variation_params = {"i18n_no_matching_variations_text": "Sorry, no products matched your selection. Please choose a different combination.", "i18n_make_a_selection_text": "Please select some product options before adding this product to your cart.", "i18n_unavailable_text": "Sorry, this product is unavailable. Please choose a different combination."};
                /* ]]> */
</script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/min/add-to-cart-variation-min1c9b.js?ver=4.6.1'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/includes/js/wp-embed.min1c9b.js?ver=4.6.1'></script>
<?php echo $settings['siteAdditionalJS']; ?>
</body>
</html>
