<?php
include_once('header.php');
?>

        <script>
         document.title = "Login | KCES";
        </script>

<style>
    
    .login {
        margin-top: 20%;
    }
    
    .panel-heading {
        background-color: #272973 !important;
        padding: 10px;
    }
    
    .panel-body {
        padding: 8% 18%;
    }
    
    .login-title {
        color: #ffffff;
        text-align: center;
        display: inherit;
        font-weight: 500;
        font-size: 16px;
        padding: 5px;
    }
    
    .btn-submit {
        background-color: #0074C8 !important;
        border: #2d2e74;
        width: 50% !important;
        text-align: center;
        margin: auto;
    }
    
</style>


<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
<!--    <div class="container">
	<div class="page-header page-img">
        <h1> Products </h1>
        <img width="1200" height="320" src="styles/images/about_img.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="styles/images/about_img.jpg 1200w, styles/images/about_img-300x80.jpg 300w, styles/images/about_img-1024x273.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>		
    </div>container-->
</div><!--titleclass-->

<div id="content" class="container">
    <div class="main" id="ktmain" role="main">
        
        <div class="login">
        
            <div class="col-md-6 col-md-offset-3" style="float: none !important; margin: auto !important;">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <span class="login-title">LOGIN</span>
                    </div>

                    <?php
                        if($this->session->flashdata('success')) {
                        ?> 

                            <div class="box-body">
                                <div class="alert alert-success alert-dismissable" style="margin-bottom:0px">                         
                                    <?php echo $this->session->flashdata('success'); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="top:-10px">x</button>
                                </div>
                            </div>

                        <?php
                        }
                        ?>
                    
                    <div class="panel-body">
                        
                        <?php if (isset($error)){
					if ($error == "1"){
						echo "<div class='alert alert-danger'>".$this->lang->line('login_incorrect')."</div>";
					}
                    if ($error == "2"){
                        echo "<div class='alert alert-danger'>".$this->lang->line('captcha_incorrect')."</div>";
                    }
				} ?>
                        <?php echo form_open(BASE_URL.'/front_login/check'); ?>
                        
<!--                        <form action="" method="post" accept-charset="utf-8">-->
                            <fieldset>
                                <div class="form-group">
                                    <?php     $data = array(
                                                'name'        => 'username',
                                                'id'          => 'username',
                                                'required'    => 'required',                                      
                                                'class'       => 'form-control',
                                                'value'	    => set_value('username'),
                                                'placeholder' => 'USERNAME'
                                              );

                                            echo form_input($data); ?>
                                </div>
                                <div class="form-group">
                                    <?php   $data = array(
                                                'name'        => 'password',
                                                'id'          => 'password',
                                                'required'    => 'required',                                   
                                                'class'       => 'form-control',
                                                'value'	=> set_value('password'),
                                                'placeholder'	=> 'PASSWORD'
                                              );

                                        echo form_password($data); ?>
                                </div>
                                
                                <a style="color:#0074C8; float:right; margin-top:-10px;" href="<?php echo BASE_URL; ?>/forgot">Forgot Password</a>
                                
                                <br /><br />
                                
                                <a style="color:#0074C8; text-align: center; display:inherit;" href="<?php echo BASE_URL; ?>/register">Create An Account</a>
                                
                                <br /><br />

                                <div class="login-captcha" style="text-align: center">
                                   <div class="g-recaptcha" style="display:inline-block;" data-sitekey="6Lcyhm4UAAAAALTANNVA1Sk5APoZtjVcnxOPgizx"></div>
                                </div>

                               <br /><br />
                                
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success btn-block btn-submit" value="Login">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<script>
$("form").submit(function(event) {
          var recaptcha = $("#g-recaptcha-response").val();
          if (recaptcha === "") {
             event.preventDefault();
             alert("Please check the recaptcha");
          }
       });
</script>

<?php
include_once('footer.php');
?>