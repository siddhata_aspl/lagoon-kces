<?php
include_once('header.php');
?>


    <?php
    if($this->uri->segment(2) == '')
    {
    ?>
        <script>
         document.title = "Shop | KCES";
        </script>
    <?php
    }
    if(count($category) > 0)
    {     
        $cat_name = $category[0]['category_name'];
    ?>
        <script>
         document.title = "<?php echo $cat_name;?> | KCES";
        </script>
    <?php     
    }
    if(count($subcategory) > 0)
    {
        $subcat_name = $subcategory[0]['subcat_name'];
    ?>
        <script>
         document.title = "<?php echo $subcat_name;?> | KCES";
        </script>
    <?php
    }
    ?>
        

<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<script src="http://demo.expertphp.in/js/jquery.js"></script>
<style>
    
    .product-category {
        margin-bottom: 20px;
    }
    
    .product-image img {
        max-width: 100%;
        text-align: center;
        height: 100%;
        object-fit: cover;
        width: 100%;
    }
    
    .product-image {
        height: 200px;
    }
    
    .product-page {
        padding-left: 0px !important;
        margin-bottom: 20px !important;
    }
    
    .product-category-menu-dropdown i {
        float: right;
        font-weight: 600;
        margin-top: 5px;
    }
    
    .product-detail .pname, .product-detail .pname a {
        font-weight: 600;
        text-align: center;
        display: inherit;
        color: #808080 !important;
    }
    
    .product-detail .price {
        font-weight: 600;
        text-align: center;
        display: inherit;
        color: #0074C8;
    }
    
    .woocommerce-ordering {
        margin: 0;
    }
    
    .woocommerce-ordering .select2-container {
        background-color: #ededed;
    }
    
    .product-category ul {
        list-style-type: none;
        padding-left: 0;
    }
    
    .product-category h4 {
       margin: 0 !important;
    }
    
    .product-category h4 a {
        color: #555555;
        font-size: 15px;
    }
    
    .product-category h6 {
        margin-top: 0 !important;
    }
    
    .dark-border hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 3px solid #999;
        padding: 0; 
        margin-top: 0 !important;
        margin-bottom: 10px !important;
    }
    
    .light-border hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 2px solid #ddd;
        margin: 0.5em 0;
        padding: 0; 
    }
    
    .sub-menu li a {
        color: #555555;
    }
    
    .backlink {
        color: #444444 !important;
    }
    
    .backlink:hover {
        color: #0074C8 !important;
    }
    
</style>


<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
    <div class="container">
	<div class="page-header page-img">
<!--        <h1> Products </h1>-->
        <img width="1200" height="320" src="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1200w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 300w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>		
    </div><!--container-->
</div><!--titleclass-->

<div id="content" class="container">
    <div class="main" id="ktmain" role="main">
        
<div class="row product-category">
    <div class="col-md-6">
        
        <?php
        if($this->uri->segment(2) == '')
        {
        ?>
            <h4 class="blue-title">All Products</h4>
            <h6><a class="backlink"  href="<?php echo BASE_URL; ?>/shop">Store Products</a><span style="color:#0074C8"> > All Products</span></h6>
        <?php
        }
        if(count($category) > 0)
        {     
            $cat_name = $category[0]['category_name'];
        ?>
            <h4 class="blue-title"><?php echo ucfirst($cat_name); ?></h4>
            <h6><a class="backlink"  href="<?php echo BASE_URL; ?>/shop">Store Products</a><span style="color:#0074C8"> > <?php echo ucfirst($cat_name); ?></span></h6>
        <?php     
        }
        if(count($subcategory) > 0)
        {
            $subcat_name = $subcategory[0]['subcat_name'];
        ?>
            <h4 class="blue-title"><?php echo ucfirst($subcat_name); ?></h4>
            <h6><a class="backlink"  href="<?php echo BASE_URL; ?>/shop">Store Products</a> > <a class="backlink"  href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($categoryname[0]['category_id']); ?>"><?php echo $categoryname[0]['category_name'];?></a><span style="color:#0074C8"> > <?php echo ucfirst($subcat_name); ?></span></h6>
        <?php
        }
        ?>
    </div>
    <div class="col-md-6">
        <div class="sorting-div" style="float: right;">
            <form method="GET">
                <strong>Sort By</strong>

                <?php
                $url1 = $this->uri->segment(1);               
                $url2 = $this->uri->segment(2);
                $url3 = $this->uri->segment(3);
                ?>
                
                <?php 
                if($url3 != '')
                {
                ?>
                    <input type="hidden" id="hidurl3" value="<?php echo $url3; ?>">
                <?php
                }
                if($url2 != '')
                {
                ?>
                    <input type="hidden" id="hidurl2" value="<?php echo $url2; ?>">
                <?php
                }
                ?>
                
                <select name="orderby" <?php if($url3 != '') { ?> class="orderbysubcat" <?php } elseif($url2 != '') { ?> class="orderbycat" <?php } else { ?> class="orderby" <?php } ?> style="opacity:1">
                    <option value="" selected="selected">Select Sort</option>
                    <option value="A-Z">Product A-Z</option>
                    <option value="Z-A">Product Z-A</option>
                    <?php
                    if($this->session->userdata('user_name') != '')
                    {
                    ?>
                    <option value="High-To-Low">Price High To Low</option>
                    <option value="Low-To-High">Price Low To High</option>
                    <?php
                    }
                    ?>
                </select>
                <input type="hidden" name="paged" value="1">
                <input type="hidden" name="add-to-cart" value="649">
            </form>
        </div>
    </div>
</div>    
    
    
<div class="row">
    
    <div class="col-md-3">    
        <div class="product-category-menu-dropdown" style="padding-right: 25px;">
            <span class="dark-border"><hr></span>
            <div class="product-category">
                <ul>
                    <?php
                    $pcount = count($product_category);
                    $i = 0;
                    foreach($product_category as $cat)
                    {   
                        $cid = $cat['category_id'];
                    ?>                       
                    
                    <li>
                        <?php
                        if($cid == $category[0]['category_id'] || $cid == $subcategory[0]['category_id'])
                        {
                        ?>
                            <h4 style="padding-right: 10px; color:#0074C8"><a style="color:#0074C8" href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($cat['category_id']);?>"><?php echo strtoupper($cat['category_name']);?></a> <i class="fa fa-angle-down arrow_slide" id="arrow<?php echo $cid;?>"></i> <i class="fa fa-angle-up arrow_slide" id="arrow<?php echo $cid;?>"></i></h4>
                        <?php
                        }
                        else
                        {
                        ?>
                            <h4 style="padding-right: 10px; color:#555555"><a href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($cat['category_id']);?>"><?php echo strtoupper($cat['category_name']);?></a> <i class="fa fa-angle-down arrow_slide" id="arrow<?php echo $cid;?>"></i> <i class="fa fa-angle-up arrow_slide" id="arrow<?php echo $cid;?>"></i></h4>
                        <?php
                        }
                        ?>
                          
                        <?php
                            if($cid == $subcategory[0]['category_id'])
                            {
                        ?>
                            <ul class="sub-menu" id="sub-menu<?php echo $ctid;?>">
                        <?php
                            }
                            else
                            {
                        ?>
                            <ul class="sub-menu" id="sub-menu<?php echo $ctid;?>" style="display: none;">
                        <?php
                            }
                        ?>
                        <?php
                        foreach($cat['subcategory'] as $subcat)
                        {     
                            $sctid = $subcat['subcat_id'];         
                            $ctid = $subcat['category_id'];
                        ?>
     
                            <?php
                            if($sctid == $subcategory[0]['subcat_id'])
                            {
                            ?>
<!--                                <ul class="sub-menu" id="sub-menu<?php echo $ctid;?>">-->
                                <li><a style="color:#0074C8" href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($subcat['category_id']);?>/<?php echo base64_encode($subcat['subcat_id']);?>"><?php echo $subcat['subcat_name']; ?></a></li>
                            <?php
                            }
                            else
                            {
                            ?>  
                                
                                <li><a href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($subcat['category_id']);?>/<?php echo base64_encode($subcat['subcat_id']);?>"><?php echo $subcat['subcat_name']; ?></a></li>
                            <?php
                            }
                            ?>
                        
                        <?php
                        }
                        ?>
                        </ul>
                    </li>
                    <?php
                    $i++;
                    if($pcount != $i)
                    {
                    ?>
                    <span class="light-border"><hr></span>
                    <?php
                    }
                    }
                    ?>
                    
<!--                    <li><h4><a href="#">MAIN CATEGORY <i class="fa fa-angle-down"></i></h4></a>
                        <ul class="sub-menu">
                            <li><a href="#">Sub Category</a></li>
                            <li><a href="#">Sub Category</a></li>
                            <li><a href="#">Sub Category</a></li>
                            <li><a href="#">Sub Category</a></li>
                            <li><a href="#">Sub Category</a></li>
                        </ul>
                    </li>-->
                </ul>
            </div>
            <span class="dark-border"><hr></span>
        </div>   
    </div>
    
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">
                <div class="row" id="pdata">
                    
                    <?php 
                    
                    if(count($products) > 0) 
                    {
                            
                            foreach($products as $p) 
                            {                          
                                $pid = $p['pid'];
                                $cid = $p['category_id'];
                                $subid = $p['subcat_id'];
                    ?>
                    
                                 
                      <div class="col-md-3 col-sm-3 product-page">
                          <div class="product-image">
                              <?php
                                  if($this->session->userdata('user_name') != '')
                                  {
                                  ?>
                              <a href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($cid);?>/<?php echo base64_encode($subid);?>/<?php echo base64_encode($p['pid']);?>" rel="bookmark" title="<?php echo $p['product_name'];?>">
                                  <?php
                                  } else {
                                  ?>                                          
                              <a href="<?php echo BASE_URL; ?>/shop/login">    
                                  <?php
                                  }
                                  ?>
                                  <?php
                                  if($p['product_image'] != '')
                                  {
                                  ?>
                                  <img src="<?php echo base_url() . 'uploads/products/' . $p['product_image']; ?>"/>
                                  <?php
                                  }
                                  else {
                                  ?>
                                  <img src="<?php echo base_url() . 'uploads/products/blank-product-img.png';?>"/>
                                  <?php
                                  }
                                  ?>
                              </a>
                          </div>
                          <div class="product-detail">
                              <span class="pname">
                                  <?php
                                  if($this->session->userdata('user_name') != '')
                                  {
                                  ?>
                                  <a href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($cid);?>/<?php echo base64_encode($subid);?>/<?php echo base64_encode($p['pid']);?>">                                   
                                  <?php
                                  } else {
                                  ?>                                          
                                  <a href="<?php echo BASE_URL; ?>/shop/login">                                  
                                  <?php
                                  }
                                  ?>
                                      <?php echo $p['product_name'];?>
                                  </a>
                              </span>
                              <?php
                              if($this->session->userdata('user_name') != '')
                              {
                                  if($p['product_unit_price'] != '0') 
                                  {
                                  ?> 
                                        <span class="price">$<?php echo number_format($p['product_unit_price'],2);?></span>
                                  <?php
                                  }
                                  else 
                                  {
                                  ?>
                                        <span class="price">CALL FOR PRICING</span>
                                  <?php
                                  }
                              }
                              ?>
                          </div>
                      </div>
                    
                    <?php
                    
                            }
                    }
                    
                    ?>                    
                    
                </div>
            </div>
        </div>
    </div>
    </div>

</div>
</div>
        
        
</div>
<!-- /.wrap -->


<script type="text/javascript">
    
    $(document).ready(function() {
        $(".fa-angle-up").hide();
        var cid = $('#cid').val();
        var ctid = $('#ctid').val();
        $('.arrow_slide').click(function() {
                var id = $(this).attr('id');
                //$('#sub-menu').slideToggle("fast");
                $("#" + id).closest('li').find('.sub-menu').slideToggle("fast");
                $("#" + id).closest('li').find('.fa-angle-up').toggle();
                $("#" + id).closest('li').find('.fa-angle-down').toggle();
        });
    });

    $(document).ready(function() {
        $('select[name="orderby"]').on('change', function() {
            var sortId = $(this).val();        
           
            var seg2 = $('#hidurl2').val();
            var seg3 = $('#hidurl3').val();
            
            if(sortId) {
                
                if($('select[name="orderby"]').is('.orderbycat'))
                {              
                    $.ajax({   
                        url: '<?php echo base_url().'shop/product/sort/cat/'; ?>'+sortId+'/'+seg2,
                        type: "GET",
                        dataType: "json",
                        success: function(data){   //alert();                            
                                        $("#pdata").html(data);  

                            },
                                    error: function(xhr, status, error) {                                    
                        },          
                    });
                }
                else if($('select[name="orderby"]').is('.orderbysubcat'))
                {             
                    $.ajax({   
                        url: '<?php echo base_url().'shop/product/sort/subcat/'; ?>'+sortId+'/'+seg3,
                        type: "GET",
                        dataType: "json",
                        success: function(data){   //alert();                            
                                        $("#pdata").html(data);  

                            },
                                    error: function(xhr, status, error) {                                    
                        },          
                    });
                }
                else if($('select[name="orderby"]').is('.orderby'))
                {
                    $.ajax({   
                        url: '<?php echo base_url().'shop/product/sort/'; ?>'+sortId,
                        type: "GET",
                        dataType: "json",
                        success: function(data){   //alert();                            
                                        $("#pdata").html(data);  

                            },
                                    error: function(xhr, status, error) {                                    
                        },          
                    });
                }
            }
        });
    });
</script>


<?php
include_once('footer.php');
?>