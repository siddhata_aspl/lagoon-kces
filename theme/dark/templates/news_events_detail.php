<?php echo $header; ?>

<script>
 document.title = "News & Events | KCES";
</script>

<?php //print_r($this->data['news']);die();?>

<div id="content" class="container">
<img class="img-responsive titleimg" src="<?php echo BASE_URL; ?>/images/NEWS &amp; EVENTS.jpg" alt="" id="">
    <div class="row single-article">

    <?php 

    if(count($news) > 0) 
    {
	    foreach($news as $n) 
	    {
	    	?>
        
      <div class="main col-lg-12 col-md-8" id="ktmain" role="main">
                  <article class="post-1781 post type-post status-publish format-standard has-post-thumbnail hentry category-news-events">
              <div class="postmeta updated color_gray">
    <div class="postdate bg-lightgray headerfont">
        <span class="postday">19</span>
        Oct 2017
    </div>       
</div>    <header>
            <h2 class="entry-title heading-inner-page"><?php echo $n['news_title'];?></h2>
        	<!-- <img src="<?php echo BASE_URL.'/uploads/news_events/'.$n['news_image'];?>">	 -->
    </header>
    <div class="entry-content clearfix mrg-btm40">
      <!-- <h4><a href="<?php echo BASE_URL.'/uploads/news_events/'.$n['news_image'];?>" rel="lightbox"><img class="alignright wp-image-1782 size-large" src="<?php echo BASE_URL.'/uploads/news_events/'.$n['news_image'];?>" alt="casino-night-all-empl" srcset="" width="940" height="419"></a><span style="color: #000000;">The KCES team came together at the Argosy Casino Banquet Room early in September to celebrate 90 years.</span></h4>
<h4><span style="color: #000000;">The group enjoyed a fine dinner and a KCES trivia game with prizes.</span></h4>
<p>&nbsp;</p>
<figure id="attachment_1784" class="thumbnail wp-caption alignleft" style="width: 300px"><a href="http://kcelectricalsupply.com/wp-content/uploads/2017/10/THE-GLORIOUS-COMMITTEE-e1508453416620.jpg" rel="lightbox"><img class="wp-image-1784 size-medium" src="http://kcelectricalsupply.com/wp-content/uploads/2017/10/THE-GLORIOUS-COMMITTEE-300x241.jpg" alt="the-glorious-committee" width="300" height="241"></a><figcaption class="caption wp-caption-text">The Entertainment Committee did an outstanding job putting the event together for the enjoyment of all.</figcaption></figure>
<figure id="attachment_1785" class="thumbnail wp-caption aligncenter" style="width: 475px"><a href="http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store.jpg" rel="lightbox"><img class="wp-image-1785" src="http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store-300x196.jpg" alt="mullins-store" srcset="http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store-300x196.jpg 300w, http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store-150x98.jpg 150w, http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store-768x503.jpg 768w, http://kcelectricalsupply.com/wp-content/uploads/2017/10/Mullins-Store-1024x671.jpg 1024w" sizes="(max-width: 475px) 100vw, 475px" width="475" height="310"></a><figcaption class="caption wp-caption-text">KCES Started as an aisle in the Adams &amp; Mullins Grocery.</figcaption></figure>-->
	 

	 <?php echo $n['news_desc'];?>

   <div>
	     <a href="<?php echo BASE_URL;?>/news-events" class="return-to">Return to all News &amp; Articles</a> 
   </div>

    </div>
	</article></div>

		<?php
			}
		}
	?>

   	</div>
</div>


<?php echo $footer; ?>