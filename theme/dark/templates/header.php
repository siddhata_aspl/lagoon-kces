<?php 

$search_text = '';
$search_text = $this->session->userdata('search');

?>


<!DOCTYPE html>

        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title><?php echo $page['pageTitle']; ?> | <?php echo $settings['siteTitle']; ?></title>
        <meta name="description" content="<?php echo $page['pageDescription']; ?>" / >
        <meta name="keywords" content="<?php echo $page['pageKeywords']; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php //if ($settings['siteFavicon']!=""){ ?>
<!--        <link rel="icon" href="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteFavicon']; ?>" />-->
        <?php //} ?>
             
        <link href="<?php echo THEME_FOLDER; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link rel='stylesheet' id='kadence_app-css'  href='<?php echo THEME_FOLDER; ?>/templates/styles/assets/css/virtue8a23.css?ver=277' type='text/css' media='all' />


        <META Http-Equiv="Cache-Control" Content="no-cache">
        <META Http-Equiv="Pragma" Content="no-cache">
        <META Http-Equiv="Expires" Content="0">


        <link href="<?php echo THEME_FOLDER; ?>/css/socicon.css" rel="stylesheet">
        <link href="<?php echo THEME_FOLDER; ?>/templates/css/styles.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Yantramanav:400,500,700,300' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Archivo+Black' rel='stylesheet' type='text/css'>
  
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteFavicon']; ?>" />
  
  <script src="<?php echo THEME_FOLDER; ?>/templates/libs/jquery/1.11.3/jquery.min.js"></script>
<!--  <script src="<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/owl.carousel.js"></script>    
    <script type="text/javascript">

    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 5,
        navigation : true
      });

    });
    </script> -->

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
  
  <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/kcelectricalsupply.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.1"}};
            !function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
        </script>
        <style type="text/css">
img.wp-smiley,
img.emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: 1em !important;
    width: 1em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
</style>
<link rel='stylesheet' id='siteorigin-panels-front-css'  href='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/siteorigin-panels/css/front02fa.css?ver=2.4.18' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/slider/slides/css/settings1dc6.css?ver=4.6.5' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>

<link rel='stylesheet' id='virtue_skin-css'  href='<?php echo THEME_FOLDER; ?>/templates/styles/assets/css/skins/default.css' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato%3A700&amp;ver=1525445792' type='text/css' media='all' />
<link rel='stylesheet' id='wp-paginate-css'  href='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/wp-paginate/wp-paginatee7f0.css?ver=1.3.1' type='text/css' media='screen' />
<script type="text/template" id="tmpl-variation-template">
    <div class="woocommerce-variation-description">
        {{{ data.variation.variation_description }}}
    </div>

    <div class="woocommerce-variation-price">
        {{{ data.variation.price_html }}}
    </div>

    <div class="woocommerce-variation-availability">
        {{{ data.variation.availability_html }}}
    </div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
    <p>Sorry, this product is unavailable. Please choose a different combination.</p>
</script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/includes/js/jquery/jqueryb8ff.js?ver=1.12.4'; ?>'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/themes/dark/includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/themes/dark/styles/template-parts/slider/slides/js/jquery.themepunch.tools.min1dc6.js?ver=4.6.5'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/template-parts/slider/slides/js/jquery.themepunch.revolution.min1dc6.js?ver=4.6.5'></script>
<script type='text/javascript' src='<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/vendor/modernizr-2.7.0.min.js'></script>
<link rel='https://api.w.org/' href='wp-json/index.php' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo THEME_FOLDER; ?>/templates/includes/wlwmanifest.xml" /> 

<link rel="canonical" href="index.php" />
<link rel='shortlink' href='index.php' />
<link rel="alternate" type="application/json+oembed" href="<?php echo THEME_FOLDER; ?>/templates/json/oembed/1.0/embed747f.json?url=http%3A%2F%2Fkcelectricalsupply.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="<?php echo THEME_FOLDER; ?>/templates/json/oembed/1.0/embed240f?url=http%3A%2F%2Fkcelectricalsupply.com%2F&amp;format=xml" />
<script src="<?php echo THEME_FOLDER; ?>/templates/recaptcha/api.js" async defer></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--        <script type="text/javascript">
            jQuery(document).ready(function() {
                // CUSTOM AJAX CONTENT LOADING FUNCTION
                var ajaxRevslider = function(obj) {
                
                    // obj.type : Post Type
                    // obj.id : ID of Content to Load
                    // obj.aspectratio : The Aspect Ratio of the Container / Media
                    // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content
                    
                    var content = "";

                    data = {};
                    
                    data.action = 'revslider_ajax_call_front';
                    data.client_action = 'get_slider_html';
                    data.token = '8365e1366a';
                    data.type = obj.type;
                    data.id = obj.id;
                    data.aspectratio = obj.aspectratio;
                    
                    // SYNC AJAX REQUEST
                    jQuery.ajax({
                        type:"post",
                        url:"http://kcelectricalsupply.com/wp-admin/admin-ajax.php",
                        dataType: 'json',
                        data:data,
                        async:false,
                        success: function(ret, textStatus, XMLHttpRequest) {
                            if(ret.success == true)
                                content = ret.data;                             
                        },
                        error: function(e) {
                            console.log(e);
                        }
                    });
                    
                     // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                     return content;                         
                };
                
                // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
                var ajaxRemoveRevslider = function(obj) {
                    return jQuery(obj.selector+" .rev_slider").revkill();
                };

                // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
                var extendessential = setInterval(function() {
                    if (jQuery.fn.tpessential != undefined) {
                        clearInterval(extendessential);
                        if(typeof(jQuery.fn.tpessential.defaults) !== 'undefined') {
                            jQuery.fn.tpessential.defaults.ajaxTypes.push({type:"revslider",func:ajaxRevslider,killfunc:ajaxRemoveRevslider,openAnimationSpeed:0.3});   
                            // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
                            // func: the Function Name which is Called once the Item with the Post Type has been clicked
                            // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
                            // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)
                        }
                    }
                },30);
            });
        </script>-->
        <script type="text/javascript">var light_error = "The Image could not be loaded.", light_of = "%curr% of %total%", light_load = "Loading...";</script><style type="text/css">#logo {padding-top:0px;}#logo {padding-bottom:15px;}#logo {margin-left:0px;}#logo {margin-right:0px;}#nav-main, .nav-main {margin-top:40px;}#nav-main, .nav-main  {margin-bottom:10px;}.headerfont, .tp-caption, .yith-wcan-list li, .yith-wcan .yith-wcan-reset-navigation, ul.yith-wcan-label li a {font-family:;} 
  .topbarmenu ul li {font-family:;}
  #kadbreadcrumbs {font-family:;}#nav-second ul.sf-menu > li:hover > a, #nav-second ul.sf-menu a:hover, #nav-second ul.sf-menu li.current-menu-item > a, #nav-second ul.sf-menu ul li a:hover {color:#c6c6c6;}.kad-mobile-nav .kad-nav-inner li.current-menu-item>a, .kad-mobile-nav .kad-nav-inner li a:hover, #kad-banner #mobile-nav-trigger a.nav-trigger-case:hover .kad-menu-name, #kad-banner #mobile-nav-trigger a.nav-trigger-case:hover .kad-navbtn {color:#282973;}#nav-main ul.sf-menu a:hover, .nav-main ul.sf-menu a:hover, #nav-main ul.sf-menu li.current-menu-item > a, .nav-main ul.sf-menu li.current-menu-item > a, #nav-main ul.sf-menu ul li a:hover, .nav-main ul.sf-menu ul li a:hover {color:#282973;}.kad-mobile-nav .kad-nav-inner li.current-menu-item>a, .kad-mobile-nav .kad-nav-inner li a:hover, #kad-banner #mobile-nav-trigger a.nav-trigger-case:hover .kad-menu-name, #kad-banner #mobile-nav-trigger a.nav-trigger-case:hover .kad-navbtn  {background:#d8d8d8;}.footerclass {background:#272973    ;}.portfolionav {padding: 10px 0 10px;}.woocommerce-ordering {margin: 16px 0 0;}.product_item .product_details h5 {text-transform: none;}.product_item .product_details h5 {min-height:40px;}#nav-second ul.sf-menu>li {width:16.5%;}.kad-topbar-left, .kad-topbar-left .topbarmenu {float:right;} .kad-topbar-left .topbar_social, .kad-topbar-left .topbarmenu ul, .kad-topbar-left .kad-cart-total,.kad-topbar-right #topbar-search .form-search{float:left}#nav-main .sf-menu ul, .nav-main .sf-menu ul, #nav-second .sf-menu ul, .topbarmenu .sf-menu ul{background: #ffffff;}#nav-main ul.sf-menu ul li a, .nav-main ul.sf-menu ul li a, #nav-second ul.sf-menu ul li a, .topbarmenu ul.sf-menu ul li a {color: #282973;}#nav-main .sf-menu ul li, .nav-main .sf-menu ul li, #nav-second .sf-menu ul li, .topbarmenu .sf-menu ul li,#nav-main .sf-menu ul, .nav-main .sf-menu ul, #nav-second .sf-menu ul, .topbarmenu .sf-menu ul {border-color: ;}.kad-header-style-two .nav-main ul.sf-menu > li {width: 33.333333%;}.kad-hidepostauthortop, .postauthortop {display:none;}.subhead .postedintop, .kad-hidepostedin {display:none;}.postcommentscount {display:none;}.postdate, .kad-hidedate, .postdatetooltip{display:none;}[class*="wp-image"] {-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;border:none;}[class*="wp-image"]:hover {-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;border:none;} .light-dropshaddow {-moz-box-shadow: none;-webkit-box-shadow: none;box-shadow: none;} header .nav-trigger .nav-trigger-case {width: auto;} .nav-trigger-case .kad-menu-name {display:none;} @media (max-width: 767px) {header .nav-trigger .nav-trigger-case {width: auto; top: 0; position: absolute;} #kad-mobile-nav {margin-top:50px;}}</style><style type="text/css" title="dynamic-css" class="options-output">header #logo a.brand,.logofont{line-height:40px;font-weight:400;font-style:normal;font-size:32px;}.kad_tagline{line-height:20px;font-weight:400;font-style:normal;color:#444444;font-size:14px;}.product_item .product_details h5{font-family:Lato;line-height:20px;font-weight:700;font-style:normal;font-size:16px;}h1{line-height:40px;font-weight:400;font-style:normal;font-size:38px;}h2{line-height:40px;font-weight:400;font-style:normal;font-size:32px;}h3{line-height:40px;font-weight:400;font-style:normal;font-size:28px;}h4{line-height:40px;font-weight:400;font-style:normal;font-size:24px;}h5{line-height:24px;font-size:18px;}body{line-height:20px;font-weight:400;font-style:normal;font-size:14px;}#nav-main ul.sf-menu a, .nav-main ul.sf-menu a{line-height:18px;font-weight:400;font-style:normal;color:#8095a9;font-size:15px;}#nav-second ul.sf-menu a{line-height:22px;font-weight:400;font-style:normal;font-size:18px;}.kad-nav-inner .kad-mnav, .kad-mobile-nav .kad-nav-inner li a,.nav-trigger-case{line-height:20px;font-weight:400;font-style:normal;color:#8095a9;font-size:16px;}</style>  
  <!--[if lt IE 9]>
      <script src="http://kcelectricalsupply.com/styles/themes/KC_Electricals/assets/js/vendor/respond.min.js"></script>
    <![endif]-->
<!--<Google Analytic Code-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-78731592-1', 'kcelectricalsupply.com');
  ga('send', 'pageview');
</script>
                </head>
<body>

<!-- <nav class="navbar navbar-inverse">
    <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo BASE_URL; ?>"><img src="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteLogo']; ?>" alt="<?php echo $settings['siteTitle']; ?>"></a>
      </div>
    <div class="collapse navbar-collapse">
<?php hooskNav('header') ?>
</div>
    </div> --><!-- /.container -->
<!-- </nav> --><!-- /.navbar -->


<body class="home page wide notsticky not_ie" data-smooth-scrolling="0" data-smooth-scrolling-hide="0" data-jsselect="1" data-product-tab-scroll="0" data-animate="1" data-sticky="0">
<div id="wrapper" class="container">
  <!--[if lt IE 8]><div class="alert"> You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</div><![endif]-->
    
    <style>
        
    .shop-button {
        background-color: #0074C8;
        color: #fff;
        border-radius: 50px;
        text-transform: capitalize !important;
        font-weight: 500;
        font-size: 14px !important;
        line-height: 0 !important;
        padding: 15px 20px !important;
        border: 2px solid #0074C8;
        float: right;
        margin-right: 5px;
    }
    
    .cart-button {
        background-color: #808080;
        color: #fff;
        border-radius: 50px;
        text-transform: capitalize !important;
        font-weight: 500;
        font-size: 14px !important;
        line-height: 0 !important;
        padding: 8px 20px !important;
        border: 2px solid #808080;
        float: right;
    } 
    
    #nav-main {
        margin-top: 10px;
    }

    .glyphicon-chevron-right {
       margin-right: -40% !important;
   }

   .glyphicon-chevron-left {
       margin-left: -40% !important;
   }

   .btn-default {
       margin-right: 5px !important;
   }

   .btn-default a {
       margin-left: 2px;
       padding: 6px 16px !important;
   }

   .btn-primary li a {
       padding: 6px 20px !important;
       margin: 0 7px !important;
   }

   .btn-primary a.active {
       color: #D33B27 !important;
       background-color: transparent !important;
       border-color: #D33B27;
   }
        
    </style>
  <header id="kad-banner" class="banner headerclass" role="banner" data-header-shrink="0" data-mobile-sticky="0">
  <section id="topbar" class="topclass visible-lg">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 kad-topbar-left">
          <div class="topbarmenu clearfix">
              
              <?php 
              $uname = $this->session->userdata('user_name');
              if($uname) 
              {   
              ?>
              <span style="float:left; margin-top:8px; margin-right:25px;font-size:14px">Welcome <strong style="color:#0074C8;"><?php echo $this->session->userdata('user_name');?></strong> - <a href="<?php echo BASE_URL ; ?>/logout">Logout</a> </span>
              <?php
              }
              ?>
              
            <ul id="menu-top-menu" class="sf-menu">
                  <?php hooskNav('header-top') ?>
            </ul>  
            
          </div>
            
        </div><!-- close col-md-6 -->
        <div class="col-md-6 col-sm-6 kad-topbar-right">
                  </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </section>  
  <div class="container  header-sm">
    <div class="row ">
          <div class="col-md-4 clearfix kad-header-left">
<!--                     <div class="hidden-lg">
            <div id="logo" class="logocase">
          
              <a class="brand logofont" href="index.php">
                       <div id="thelogo" class="sm-logo"><img src="<?php echo THEME_FOLDER; ?>/templates/styles/images/mobile-logo.png" alt="Kansas City Electrical Supply" class="kad-standard-logo" /></div>
              </a>
                         </div> 
           </div>-->
                      <div class="hidden-sm">
            <div id="logo" class="logocase">
              <a class="brand logofont" href="<?php echo BASE_URL; ?>">
                        
                       <div id="thelogo" class="dekstop-logo"><img src="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteLogo']; ?>" alt="<?php echo $settings['siteTitle']; ?>" class="kad-standard-logo" />
                                                 </div>               </a>
                         </div> 
           </div>
                      
                       <!-- Close #logo -->
       </div><!-- close col-md-4 -->
               <div class="col-md-8 kad-header-right">
                 <nav id="nav-main" class="clearfix" role="navigation">
                     
                     <?php 
                    $uname = $this->session->userdata('user_name');
                    if($uname) 
                    {   
                    ?>
                              <button type="button" onclick="window.location.href='<?php echo BASE_URL; ?>/cart'" class="kad_add_to_cart cart-button">CART <?php if($pro_count) { ?>(<?php echo $pro_count;?>) <?php } ?> <i class="fa fa-shopping-cart"></i></button>
               
                     <?php
                    }
                    ?>
                              <button type="button" onclick="window.location.href='<?php echo BASE_URL; ?>/shop'" class="kad_add_to_cart shop-button">SHOP NOW</button>
             
                     <ul id="menu-main-menu" class="sf-menu sf-js-enabled">                 
                        <?php hooskNav('header1') ?>
                     </ul>  </nav>               
               </div>
       </div>
<!--                  <div id="mobile-nav-trigger" class="nav-trigger">
              <a class="nav-trigger-case mobileclass" data-toggle="collapse" rel="nofollow" data-target=".kad-nav-collapse">
                <div class="kad-navbtn clearfix"><i class="icon-menu"></i></div>
                                <div class="kad-menu-name">Menu</div>
              </a>
            </div>-->
<!--            <div id="kad-mobile-nav" class="kad-mobile-nav">
              <div class="kad-nav-inner mobileclass">
                <div id="mobile_menu_collapse" class="kad-nav-collapse collapse mobile_menu_collapse">
                <div class="three-btn">
                             <form role="search" method="get" id="searchform" class="form-search" action="http://kcelectricalsupply.com/">
  <label class="hide" for="s">Search for:</label>
  <input type="text" value="" name="s" id="s" class="search-query" placeholder="Search">
  <button type="submit" id="searchsubmit" class="search-icon"><i class="icon-search"></i></button>
</form>
                    
                    <div class="two-buttons"><a href="https://secure.billtrust.com/KCELECTRICALSUPPLY/ig/signin" target="_blank" class="btn btn-primary">View My Bill</a>
                    <a href="customer-form.php" class="btn btn-primary btn-primary-second">Customer Forms</a></div>
                    <a href="quick-quote.php" class="btn btn-default">Quick Quote</a>
                   
                        </div>
                        <div class="menu-bg">
                 <ul id="menu-main-menu-1" class="kad-mnav"><li  class="menu-aboutkces sf-dropdown menu-item-472"><a href="about/index.php">About Kces</a>
<ul class="sf-dropdown-menu">
    <li  class="menu-ourpeople menu-item-1163"><a href="our-people.php">Our People</a></li>
    <li  class="menu-ourhistory menu-item-481"><a href="our-history.php">Our History</a></li>
</ul>
</li>
<li  class="menu-ourservices menu-item-478"><a href="our-service.php">Our Services</a></li>
<li  class="menu-products sf-dropdown menu-item-534"><a href="products.php">Products</a>
<ul class="sf-dropdown-menu">
    <li  class="menu-ballasts038transformers menu-item-900"><a href="ballasts-transformers.php">Ballasts &#038; Transformers</a></li>
    <li  class="menu-breakerspanelscontrols038gear menu-item-901"><a href="breakerspanelscontrols-gear.php">Breakers, Panels, Controls &#038; Gear</a></li>
    <li  class="menu-closeouts menu-item-902"><a href="closeouts.php">Closeouts</a></li>
    <li  class="menu-companieswerepresent menu-item-903"><a href="companies-we-represent.php">Companies We Represent</a></li>
    <li  class="menu-conduit038fittings menu-item-904"><a href="conduit-fittings.php">Conduit &#038; Fittings</a></li>
    <li  class="menu-dimmersandlightingcontrols menu-item-906"><a href="dimmers-and-lighting-controls.php">Dimmers And Lighting Controls</a></li>
    <li  class="menu-devices menu-item-905"><a href="devices.php">Devices</a></li>
    <li  class="menu-fuses menu-item-907"><a href="fuses.php">Fuses</a></li>
    <li  class="menu-handtools menu-item-908"><a href="hand-tools.php">Hand Tools</a></li>
    <li  class="menu-lamps038lightbulbs menu-item-909"><a href="lamps-light-bulbs.php">Lamps &#038; Light Bulbs</a></li>
    <li  class="menu-lightfixtures menu-item-910"><a href="light-fixtures.php">Light Fixtures</a></li>
    <li  class="menu-sds menu-item-912"><a href="msds.php">SDS</a></li>
    <li  class="menu-wire menu-item-913"><a href="wire.php">Wire</a></li>
    <li  class="menu-wiremanagement menu-item-914"><a href="wire-management.php">Wire Management</a></li>
</ul>
</li>
<li  class="menu-training menu-item-487"><a href="training.php">Training</a></li>
<li  class="menu-resources menu-item-499"><a href="resources.php">Resources</a></li>
<li  class="menu-news038events menu-item-438"><a href="news-events.php">News &#038; Events</a></li>
<li  class="menu-contactus menu-item-443"><a href="contact-us.php">Contact Us</a></li>
<li  class="menu-promos menu-item-484"><a href="promos.php">Promos</a></li>
<li  class="menu-careers menu-item-1171"><a href="careers.php">Careers</a></li>
</ul>                 </div>
                   <div class="header-address-bg">
                    <div class="address1">
                    <div class="add_left">
                    <h5>KANSAS CITY</h5>
                    <p><strong>PHONE: (816) 924-7000</strong></p>
                    <p>HOURS: M-F  7AM - 5PM</p>
                    </div>
                    <div class="add_right">
                    <a href="https://www.google.co.in/maps/place/4451+Troost+Ave,+Kansas+City,+MO+64110,+USA/@39.0454994,-94.5745303,17z/data=!3m1!4b1!4m2!3m1!1s0x87c0eff298993ac7:0x6d7cb8d464ee8e1a?hl=en" target="_blank"><img src="styles/assets/img/btn_map.png" alt="Map" /></a>
                    </div>
                    </div>
                    
                    <div class="address1">
                    <div class="add_left">
                    <h5>LENEXA</h5>
                    <p><strong>PHONE: (913) 563-7000</strong></p>
                    <p>HOURS: M-F  7AM - 5PM</p>
                    </div>
                    <div class="add_right">
                    <a href="https://www.google.co.in/maps/place/14851+W+99th+St,+Lenexa,+KS+66215,+USA/@38.9491861,-94.7601766,17z/data=!3m1!4b1!4m2!3m1!1s0x87c094edf68d109f:0x258a130baa7c5c1f?hl=en" target="_blank"><img src="styles/assets/img/btn_map.png" alt="Map" /></a>
                    </div>
                    </div>
                    
                    <div class="col-sm-12 col-xs-12 foot-newsletter footercol4">
                    <div class="widget-1 widget-first footer-widget"><aside class="widget widget_newsletterwidget" id="newsletterwidget-2"><h3>SIGN UP FOR OUR NEWSLETTER</h3>



<div class="newsletter newsletter-widget">



<form method="post" onsubmit="return newsletter_check(this)" action="http://192.168.88.205/KC_Electricals/?na=s"><input type="hidden" value="widget" name="nr"><input type="email" placeholder="EMAIL ADDRESS" name="ne" required="" class="newsletter-email"><button value="" type="submit" class="newsletter-submit">Sign Up</button></form></div></aside></div>                  </div>
                 </div>
               </div>
            </div>
          </div>   -->
           
          
           <div class="header-bottom">
            <div class="row">
                <div class="col-sm-4">
                    <p class="bottom-tatg"><?php echo $settings['LogoBottomTitle'];?></p>
                </div>
                <div class="col-sm-8 text-right">
                   <!--  <a href="https://secure.billtrust.com/KCELECTRICALSUPPLY/ig/signin" target="_blank" class="btn btn-primary">View My Bill</a>
                    <a href="customer-form.php" class="btn btn-primary">Customer Forms</a>
                    <a href="quick-quote.php" class="btn btn-default">Quick Quote</a> -->
                    
                <ul class="btn btn-primary">
                     <?php hooskNav('red-btn') ?>
                </ul>
                <ul class="btn btn-default">
                     <?php hooskNav('green-btn') ?>
                </ul>
                <div class="search-box btn">
                        <div id="topbar-search" class="topbar-widget">
                            <form action="<?php echo base_url().'news-events/search'; ?>" role="search" method="post" id="" class="form-search">
                                <label class="hide" for="s">Search for:</label>
                                <input type="text"  name="s" id="s" class="search-query" placeholder="Search" value="<?php echo $search_text;?>">
                                <button type="submit" name="searchsubmit" id="searchsubmit" class="search-icon"><i class="icon-search"></i></button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
          </div>
          
  </div>
  
   <!-- Close Container -->
   
      </header>


<!------- Archive section show hide ---------->
<script type="text/javascript">

    $('a[href^="https://"]').attr('target','_blank'); 

    $(document).ready(function() {
    //document.getElementByClass('archive-sub-menu').style.display='none';
    $('.blog-list-archive-list li ul').hide();
    $('.blog-list-archive li a').click(function(){
        $(this).parent().addClass('selected');
        $(this).parent().children('ul').slideDown(250);
        $(this).parent().siblings().children('ul').slideUp(250);
        $(this).parent().siblings().removeClass('selected');
    });
   });
   function myFunction() { 
       $('.blog-list-archive').slideToggle(250)
   }
</script>
<script type="text/javascript">
    if(window.innerWidth <= '980') { 
<!------- Responsive menu section show hide ---------->
$('.kad-mnav .sf-dropdown').prepend('<span class="show">+</span>');
$('.sf-dropdown-menu').hide();
$(document).on('click', '.kad-mnav .show',function(event){

    $(this).siblings('.sf-dropdown-menu').show(); //or whatever show-function you want to use
    $(this).removeClass('show').addClass('hidemenu');
    $(this).text('-');

});
$(document).on('click', '.sf-dropdown .hidemenu',function(event){

    $(this).siblings('.sf-dropdown-menu').hide(); //or whatever show-function you want to use
    $(this).removeClass('hidemenu').addClass('show');
    $(this).text('+');

});
<!------- End Responsive menu section show hide ---------->
}
</script>