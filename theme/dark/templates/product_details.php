<?php
include_once('header.php');
?>

        <script>
         document.title = "<?php echo $pdetails['product_name']; ?> | KCES";
        </script>

<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo THEME_FOLDER; ?>/templates/styles/assets/css/jssor_slider_style.css">

<script src="<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/jssor.slider-27.4.0.min.js" type="text/javascript"></script>
<script src="<?php echo THEME_FOLDER; ?>/templates/styles/assets/js/jssor_slider.js" type="text/javascript"></script>


<style>

    .product-category {
        margin-bottom: 20px;
    }

    .product-category h4 {
        margin: 0 !important;
    } 

    .product-category h6 {
        margin-top: 0 !important;
    }

    .product-title {
        color: #666666;
        font-size: 35px;
        font-weight: 500;
    }

    .price {
        font-weight: 600 !important;
        color: #0074C8 !important;
    }

    .quantity {
        width: auto !important;
        margin-right: 35px !important;
    }

    .quantity .qun {
        font-size: 14px !important;
        font-weight: 500;
    }

    .quantity .per {
        font-size: 12px !important;
    }

    .qun-text {
        width: 115px;
        height: 35px;
    }

    .qunblock {
        padding-right: 10px;
    }

    .add-cart-button {
        background-color: #808080;
        color: #fff;
        border-radius: 26px;
        text-transform: capitalize !important;
        font-weight: 500;
        font-size: 16px !important;
        line-height: 0 !important;
        padding: 8px 18px !important;
        border: 2px solid #808080;
    }

    .estimate-button {
        background-color: transparent;
        color: #999999;
        border-radius: 26px;
        text-transform: capitalize !important;
        font-weight: 500;
        font-size: 16px !important;
        line-height: 0 !important;
        padding: 17px 18px !important;
        border: 2px solid #ccc;
    }

    .other-details .oth {
        font-size: 14px !important;
        font-weight: 500;
    }

    .det-light-border hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 2px solid #ddd;
        margin: 1em 0;
        padding: 0; 
    }

    .button-description {
        margin-bottom: 30px;
    }

    .desc-first {
        padding-right: 40px;
    }

    .ext {
        color: #272973;
    }

    .sds-button {
        float: right;
        margin-top: 5px;
        padding: 3px 15px;
        background-color: #b30000;
        color: #fff;
        border: none;
        border-radius: 3px;
    }

    #jssor_1 {
        width: auto !important;
    }
    
    .backlink {
        color: #444444 !important;
    }
    
    .backlink:hover {
        color: #0074C8 !important;
    }

    .qun-text {
       text-align: center;
       /* color: blue; */
       font-weight: 600 !important;
       color: #0074C8 !important;
       font-size: 16px;
   }

</style>


<div class="wrap contentclass" role="document">

    <div id="pageheader" class="titleclass">
        <div class="container">
            <div class="page-header page-img">
                <!--        <h1> Products </h1>-->
                <img width="1200" height="320" src="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1200w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 300w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
            </div>		
        </div><!--container-->
    </div><!--titleclass-->

    <div id="content" class="container">
        <div class="main" id="ktmain" role="main">
            
            <?php
            if($this->session->flashdata('success')) {
            ?> 
           
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable">                         
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    </div>
                </div>
            
            <?php
            }
            ?>


            <div class="row product-category">
                <div class="col-md-12">
                    <h4 class="blue-title"><?php echo $category[0]['category_name'];?></h4>
                    <h6><a class="backlink" href="<?php echo BASE_URL; ?>/shop">Store Products</a> > <a class="backlink" href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($category[0]['category_id']); ?>"><?php echo $category[0]['category_name'];?></a> ><span style="color:#0074C8"> <?php echo $subcategory[0]['subcat_name']; ?></span></h6>
                </div>
            </div>            
            <div class="row">
                <div class="main col-md-12" role="main">
                    <?php
                    //if (count($pdetails) > 0) {
                        ?>
                        <div id="product-541" class="post-541 product type-product status-publish has-post-thumbnail product_cat-apparel product_cat-tshirts product_cat-womans-tshirts product_tag-brand product_tag-clothing product_tag-t-shirts first instock featured shipping-taxable purchasable product-type-variable has-default-attributes">
                            <div class="row">

                                <div class="col-md-5 product-img-case">
                                    <!--                              <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-5 images kad-light-gallery contain-woo_product_slider_enabled" data-columns="5" style="">
                                                                      <figure class="woocommerce-product-gallery__wrapper woo_product_slider_enabled woo_product_zoom_enabled">
                                                                          <div data-thumb="styles/images/news-detail-page.png" class="woocommerce-product-gallery__image"><a href="styles/images/news-detail-page.png" title="front_tshirt_mountainware_01">
                                                                                  <img src="styles/images/news-detail-page.png" width="456" height="456" srcset="styles/images/news-detail-page.png 456w, styles/images/news-detail-page.png 150w, styles/images/news-detail-page.png 300w, styles/images/news-detail-page.png 1024w, styles/images/news-detail-page.png 180w, styles/images/news-detail-page.png 600w, styles/images/news-detail-page.png 912w, styles/images/news-detail-page.png 1200w" sizes="(max-width: 456px) 100vw, 456px" class="attachment-shop_single shop_single wp-post-image" alt="" title="" data-caption="" data-src="uploads\2013/07/front_tshirt_mountainware_011.jpg" data-large_image="uploads\2013/07/front_tshirt_mountainware_011.jpg" data-large_image_width="1200" data-large_image_height="1200"></a>
                                                                          </div>
                                                                                  <div data-thumb="styles/images/news-detail-page.png" class="woocommerce-product-gallery__image">
                                                                                      <a href="styles/images/news-detail-page.png" data-rel="lightbox[product-gallery]" title="back_tshirt_mountainware_01">
                                                                                          <img src="styles/images/news-detail-page.png" width="456" height="456" srcset="styles/images/news-detail-page.png 912w, styles/images/news-detail-page.png 1200w" sizes="(max-width: 456px) 100vw, 456px" class="attachment-shop_single shop_single wp-post-image" alt="back_tshirt_mountainware_01" title="" data-caption="" data-src="styles/images/news-detail-page.png" data-large_image="styles/images/news-detail-page.png" data-large_image_width="1200" data-large_image_height="1200"></a>
                                                                                  </div>
                                                                      </figure>
                                                                  </div>-->

                                    <!-- #region Jssor Slider Begin -->

                                    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:1220px;overflow:hidden;visibility:hidden;">
                                        <!-- Loading Screen -->
                                        <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                                            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
                                        </div>
                                        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:920px;height:900px;overflow:hidden;">

                                        <?php
                                        $pimage = $pdetails['product_images'];
                                        if(count($pimage) > 0) 
                                        {
                                            for ($i = 0; $i < count($pimage); $i++) {
                                                $img = $pimage[$i]['product_image'];
                                                ?>
                                                    <div>
                                                        <img data-u="image" src="<?php echo base_url() . 'uploads/products/' . $img; ?>" style="border: 2px solid #666;" />
                                                        <div data-u="thumb">
                                                            <img data-u="thumb" src="<?php echo base_url() . 'uploads/products/' . $img; ?>" />
                                                            <!--                    <div class="ti">Slide Description</div>-->
                                                        </div>
                                                    </div>

                                        <?php
                                            }
                                        }
                                        else
                                        {
                                        ?>
                                                    <div>
                                                        <img data-u="image" src="<?php echo base_url() . 'uploads/products/blank-product-img.png'; ?>" style="border: 2px solid #666;" />
                                                        <div data-u="thumb">
                                                            <img data-u="thumb" src="" />
                                                            <!--                    <div class="ti">Slide Description</div>-->
                                                        </div>
                                                    </div>   
                                        <?php
                                        }
                                        ?>
                                            
                                            
                                        </div>
                                        <!-- Thumbnail Navigator -->
                                        <div data-u="thumbnavigator" class="jssort111" style="position:absolute;left:0px;bottom:0px;width:980px;height:280px;cursor:default;" data-autocenter="1" data-scale-bottom="0.75">
                                            <div data-u="slides">
                                                <div data-u="prototype" class="p" style="width:330px !important;" >
                                                    <div data-u="thumbnailtemplate" class="t"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Arrow Navigator -->
                                        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:162px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:-120px;left:0;width:100%;height:100%;">
                                            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
                                            </svg>
                                        </div>
                                        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:162px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                                            <svg viewbox="0 0 16000 16000" style="position:absolute;top:-120px;left:0;width:100%;height:100%;">
                                            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
                                            </svg>
                                        </div>
                                    </div>
                                    <script type="text/javascript">jssor_1_slider_init();</script>

                                </div>

                                <div class="col-md-7 product-summary-case" style="margin-top: -15px !important;">
                                    <div class="summary entry-summary">
                                        <h1 class="product-title"><?php echo $pdetails['product_name']; ?></h1>
                                        <h4 class="blue-title">VEND PROD 980112058875</h4>
                                        <p>
      <!--                                      <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol"></span>-->
                                            <?php
                                            if($pdetails['product_unit_price'] != '0') 
                                            {
                                            ?>
                                                <h2 class="price">$<?php echo number_format($pdetails['product_unit_price'],2); ?></h2>
                                            <?php
                                            }
                                            else 
                                            {
                                            ?>
                                                <h2 class="price">CALL FOR PRICING</h2>
                                            <?php
                                            }
                                            ?>
                                        <!--                                      </span>-->
                                        </p>
                                        <!--                                  <div class="product-description">
                                                                              <p>Aenean tellus sapien, cursus ut ullamcorper eu, congue vestibulum libero. Praesent at semper ipsum. Pellentesque tempus, libero non varius sollicitudin, mi tortor tristique metus, viverra ullamcorper lorem ligula quis tortor.
                                                                              </p>
                                                                          </div>-->
                                        
                                        <?php
                                        if($pdetails['product_unit_price'] != '0') 
                                        {
                                        ?>  
                                        
                                        <div class="button-description">
                                            <form class="variations_form cart" method="post" enctype="multipart/form-data" action="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($category[0]['category_id']); ?>/<?php echo base64_encode($subcategory[0]['subcat_id']); ?>/<?php echo base64_encode($pdetails['product_id']); ?>/addcart">
                                                <div class="quantity">    
                                                    <table>
                                                        <tr>
                                                            <td class="qunblock">
                                                                <span class="qun">Quantity(M)</span><br />
                                                                <span class="per">Per thousand</span>
                                                            </td>
                                                            <td>
                                                                <input type="number" name="quantity" class="qun-text input-text qty text" required="required" min="1" max="" oninput="validity.valid||(value='');" >
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <input type="hidden" name="product_id" value="<?php echo $pdetails['product_id']; ?>">
                                                <input type="hidden" name="product_name" value="<?php echo $pdetails['product_name']; ?>">
                                                <input type="hidden" name="product_price" value="<?php echo $pdetails['product_unit_price']; ?>">
                                               
                                                <div class="shop-buttons">  
                                                
                                                    <button type="submit" class="add-cart-button">Add To Cart <i class="fa fa-shopping-cart"></i></button>
                                                    <!--                                      <button type="submit" class="kad_add_to_cart estimate-button">Add To Estimate</button>-->
                                                
                                                </div>
                                               
                                            </form>
                                        </div>
                                        
                                        <?php
                                            }    
                                        ?>

                                        <span class="det-light-border"><hr></span>   

                                        <div class="other-details">    
                                            <table>
                                                <?php 
                                                $location_data = $pdetails['location_data'];
                                                
                                                if (count($location_data) > 0) 
                                                {
                                                    for ($l = 0; $l < count($location_data); $l++) {
                                                        $location_name = $location_data[$l]['location_name'];
                                                        $location_quantity = $location_data[$l]['location_quantity'];
                                                ?>
                                                
                                                <tr>
                                                    <td class="desc-first">
                                                        <span class="oth">Location: <span style="color: #272973"><?php echo $location_name; ?></span></span>
                                                    </td>
                                                    <td>
                                                        <span class="oth">Qty At Location: <span style="color: #272973"><?php echo $location_quantity; ?></span></span>
                                                    </td>
                                                </tr>
                                                
                                                

                                                <?php       }?>
                                                <button type="button" class="sds-button" onclick="window.open('<?php echo $pdetails['sds_link']; ?>')">SDS</button>
                                                <?php
                                                        } 
                                                    else { 
                                                ?>
                                                
                                                <tr>
                                                    <td class="desc-first">
                                                        <span class="oth">Location: <span style="color: #272973"></span></span>
                                                    </td>
                                                    <td>
                                                        <span class="oth">Qty At Location: <span style="color: #272973"></span></span>
                                                    </td>
                                                </tr>
                                                
                                                <button type="button" style="margin-top: -3px;" class="sds-button" onclick="window.open('<?php echo $pdetails['sds_link']; ?>')">SDS</button>
                                                
                                                <?php
                                                    }
                                                ?>
                                            </table>

                                            

                                        </div>                                 

                                        <span class="det-light-border"><hr></span>

                                        <span class="ext"> <?php echo $pdetails['product_desc']; ?> </span>

    <!--                                  <span class="ext"> TRIPLEX VOLUTA PE </span><br /><br />

    <span class="ext"> CABLE </span>-->

                                        <span class="det-light-border"><hr></span>

                                        <!--                                      <div class="single_variation_wrap_kad single_variation_wrap" style="display:block;">
                                                                                  <div class="single_variation headerfont">
                                                                                      
                                                                                  </div>
                                                                                  <div class="woocommerce-variation-add-to-cart variations_button">
                                                                                      <div class="quantity">
                                                                                          <label class="screen-reader-text" for="quantity_5b76ab9c88f8c">Quantity</label> 
                                                                                          <input type="number" name="quantity">
                                                                                          <input type="number" id="quantity_5b76ab9c88f8c" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="">
                                                                                      </div> 
                                                                                      <button type="submit" class="kad_add_to_cart headerfont kad-btn kad-btn-primary single_add_to_cart_button button alt">Add to cart</button> 
                                                                                      <input type="hidden" name="add-to-cart" value="541"> 
                                                                                      <input type="hidden" name="product_id" value="541"> 
                                                                                      <input type="hidden" name="variation_id" class="variation_id" value="0">
                                                                                  </div>
                                                                              </div>-->
                                        </form>
                                        <!--                                  <div class="product_meta"> 
                                                                              <span class="sku_wrapper">SKU: <span class="sku">N/A</span></span> 
                                                                              <span class="posted_in">Categories: <a href="..\..\product-category\apparel\index.htm" rel="tag">Apparel</a>, 
                                                                                  <a href="..\..\product-category\tshirts\index.htm" rel="tag">T-Shirts</a>, 
                                                                                  <a href="..\..\product-category\tshirts\womans-tshirts\index.htm" rel="tag">Womans</a>
                                                                              </span> 
                                                                              <span class="tagged_as">Tags: <a href="..\..\product-tag\brand\index.htm" rel="tag">brand</a>, 
                                                                                  <a href="..\..\product-tag\clothing\index.htm" rel="tag">clothing</a>, 
                                                                                  <a href="..\..\product-tag\t-shirts\index.htm" rel="tag">T-shirts</a>
                                                                              </span>
                                                                          </div>-->
                                    </div>
                                </div>
                            </div>
                            <!--                      <div class="woocommerce-tabs wc-tabs-wrapper">
                                                      <ul class="tabs wc-tabs" role="tablist">
                                                          <li class="description_tab" id="tab-title-description" role="tab" aria-controls="tab-description"> 
                                                          </li>
                                                      </ul>
                                                  </div>-->
                        </div><br /><br />
                    </div>
                </div>


    <?php
//                            }
//}
?>


        </div>
    </div>



</div>


<script>

   //disable dot(.) in input
   $(".qty").on("keypress", function(evt) {
       var keycode = evt.charCode || evt.keyCode;
       if (keycode == 46) {
         return false;
       }
   });

</script>



<?php
include_once('footer.php');
?>