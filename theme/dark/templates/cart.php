<?php
include_once('header.php');
?>

        <script>
         document.title = "Cart | KCES";
        </script>
<script src="http://demo.expertphp.in/js/jquery.js"></script>
<style>
    thead {
        background-color: #888 !important;
        color: #fff;
    }   
    
    table {
        border: 1px solid #888 !important;
    }
    
    .wc-backward {
        color: #D33B27;
        background-color: #fff;
        border: 1px solid #D33B27;
        font-weight: 600;
        padding: 8px 18px !important;
        font-size: 14px;
    }
    
    .wc-backward:hover {
        background-color: #D33B27;
        color: #fff;
    }
    
    .wc-forward {
        float: right;
        background-color: #0074C8;
        border: 1px solid #0074C8;
        color: #fff;
        padding: 8px 18px !important;
        font-weight: 600;
    }
    
    .wc-forward:hover {
        background-color: #fff;
        color: #0074C8;
    }
    
    .shop_table .product-price {
        width: 5% !important;
    }
    
    .btn-increment-decrement {
        display: inline-block;
        padding: 5px 0px;
        background: #e2e2e2;
        width: 30px;
        text-align: center;
        cursor: pointer;
    }
    
    .woocommerce .quantity input.qty {
        float: none !important;
    }
    
    .shop_table .product-quantity .quantity {
        float: none !important;
        text-align: center !important;
        margin: auto !important;
        width: 110px !important;
    }
    
/*    .input-quantity {
        border: 0px;
        width: 30px;
        display: inline-block;
        margin: 0;
        box-sizing: border-box;
        text-align: center;
    }
    
    .cart-info.quantity {
        width: 90px;
        border: #ccc 1px solid;
    }*/
</style>

<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
    <div class="container">
    <div class="page-header page-img">
<!--        <h1> Products </h1>-->
        <img width="1200" height="320" src="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="<?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1200w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 300w, <?php echo THEME_FOLDER; ?>/templates/styles/images/store_products.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>        
    </div><!--container-->
</div><!--titleclass-->

<div id="content" class="container">
    <div class="main" id="ktmain" role="main">              
 
     
 
<?php 

    if(count($cart_items) > 0) 
    {
                        
?>
    
    <div class="row">
        <div class="main col-md-12" id="ktmain" role="main">
                        <div class="entry-content" itemprop="mainContentOfPage">
                <div class="woocommerce">
                                    
                                    <div class="main col-md-7">
                                    
<form class="woocommerce-cart-form" action="#" method="post">
    <h2 style="font-size: 28px;">MY CART(<?php echo $pro_count;?>)</h2>
    <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
        <thead>
            <tr>
                <th class="product-remove">&nbsp;</th>
<!--                <th class="product-thumbnail">&nbsp;</th>-->
                <th class="product-name">Product</th>
                <th class="product-price" style="width: 5% !important">Price</th>
                <th class="product-quantity" style="width: 15%; text-align:center">Quantity</th>
                                <th class="product-subtotal" style="width: 13%">Total</th>
            </tr>
        </thead>
        <tbody>
                                <?php
                                //print_r($cart_items);die;
                                $total = 0;
                                
                                $count = 0;
                                foreach($cart_items as $items)
                                {
                                    $count++;
                                ?>
                    <tr class="woocommerce-cart-form__cart-item cart_item">

                        <td class="product-remove">
                                                    <a href="<?php echo base_url() . 'cart/remove/'. base64_encode($items['cart_id']);?>" class="remove" onclick="return deletechecked();" aria-label="Remove this item" data-product_id="649" data-product_sku="">&times;</a>                      
                                                </td>

<!--                        <td class="product-thumbnail">
                        <a href="#"><img width="300" height="300" src="#" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt=""></a>
                                                </td>-->

                        <td class="product-name" data-title="Product">
                                                    <a href="<?php echo BASE_URL; ?>/shop/<?php echo base64_encode($items['category_id']);?>/<?php echo base64_encode($items['subcat_id']);?>/<?php echo base64_encode($items['product_id']);?>"><?php echo $items['product_name'];?></a>
                            </td>

                        <td class="product-price" data-title="Price">
                                                    <span class="woocommerce-Price-currencySymbol">$</span><span class="woocommerce-Price-amount amount"><?php echo number_format($items['product_price'],2);?></span>          
                        </td>

                        <td class="product-quantity" data-title="Quantity" style="text-align:center">
                                                <div class="quantity" style="text-align:center">
                                                        <span class="btn-increment-decrement" onclick="decrement_quantity(<?php echo $items["cart_id"]; ?>, '<?php echo $items["product_price"]; ?>')">-</span>
                                                        <input type="number" id="input-quantity-<?php echo $items["cart_id"]; ?>" onblur="changeprice(this.value, '<?php echo $items["cart_id"]; ?>','<?php echo $items["product_price"]; ?>')" class="input-text qty text" min="1" max="" oninput="validity.valid||(value='');" name="newquantity" value="<?php echo $items['product_quantity'];?>" title="Quantity" size="4">
                                                        <span class="btn-increment-decrement" onclick="increment_quantity(<?php echo $items["cart_id"]; ?>, '<?php echo $items["product_price"]; ?>')">+</span>
                                                </div>
<!--                                                    <span class="woocommerce-Price-amount amount"><?php echo $items['product_quantity'];?></span>-->
<!--                                                    <div class="cart-info quantity">
                                                        <div class="btn-increment-decrement" onclick="decrement_quantity('3DcAM01')">-</div><input class="input-quantity" id="input-quantity-3DcAM01" value="5"><div class="btn-increment-decrement" onclick="increment_quantity('3DcAM01')">+</div>
                                                    </div>-->
                        </td>

                        <td class="product-subtotal" data-title="Total">
                                                    <input type="hidden" name="pricehidden" class="pricehidden" id="pricehidden-<?php echo $items["cart_id"]; ?>" value="<?php echo $items['product_total'];?>">
                                                    <span class="woocommerce-Price-currencySymbol" style="float:left">$</span>
                                                    <span class="woocommerce-Price-amount amount" id="cart-price-<?php echo $items["cart_id"]; ?>" style="float:right">
                                                        <?php echo number_format($items['product_total'],2);?>
                                                    </span>         
                        </td>

                    </tr>
                                        
                                <?php
                                $total = $total+$items['product_total'];
                                }
                                ?>          
            
<!--            <tr>
                <td colspan="6" class="actions">

                                        <div class="coupon">
                                            <label for="coupon_code">Coupon:</label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="Coupon code"> <button type="submit" class="button" name="apply_coupon" value="Apply coupon">Apply coupon</button>
                    </div> 
                                    
                                        <div class="coupon">
                                            <button type="submit" class="button" name="apply_coupon" value="Apply coupon">Clear Cart</button>
                    </div> 
                    
                    <button type="submit" class="button" name="update_cart" value="Update cart">Update Cart</button>

                    
                    <input type="hidden" id="woocommerce-cart-nonce" name="woocommerce-cart-nonce" value="304ce7518c"><input type="hidden" name="_wp_http_referer" value="/virtue-premium/cart/?remove_item=55b37c5c270e5d84c793e486d798c01d">              </td>
            </tr>-->

                    </tbody>
    </table>
    </form>
                                    </div>

                                    <div class="main col-md-5">
        <div class="cart-collaterals">
            
    <div class="cart_totals " style="width:100%">
            
            
<!--<form class="variations_form cart" method="post" enctype="multipart/form-data" action="<?php echo base_url() . 'product/cart/checkout'; ?>">-->
    
    <h2 style="font-size: 28px;">PRICE DETAILS</h2>

    <table cellspacing="0" class="shop_table shop_table_responsive">

<!--        <tr class="cart-subtotal">
            <th>Subtotal</th>
            <td data-title="Subtotal"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>639.68</span></td>
        </tr>       
            
        <tr class="shipping">
            <th>Shipping</th>
            <td data-title="Shipping"> ---- </td>
        </tr>       -->
        
        <tr class="order-total">
                    <th style="width: 71%">PRICE <?php if($pro_count) { ?>(<span id="total-quantity"><?php echo $pro_count;?></span> items) <?php } ?></th>
                        <td data-title="Total"><strong><span class="woocommerce-Price-currencySymbol" style="float:left">$</span><span class="woocommerce-Price-amount amount" id="total-price" style="float:right"><?php echo number_format($total,2);?></span></strong> </td>
        </tr>
                
<!--                <tr>
                    <td>
                        <a href="<?php echo base_url() . 'cart/checkout'; ?>" class="checkout-button button alt wc-forward">
    Proceed to checkout</a>
                    </td>
                    <td>
                        <p class="return-to-shop">
                                        <a class="button wc-backward" href="<?php echo base_url() . 'shop'; ?>"> Return to shop     </a>
                                    </p>
                    </td>
                </tr>-->

        
    </table>
        
        

    <div class="wc-proceed-to-checkout">
            
<!--         <p class="return-to-shop">-->
             <a class="button wc-backward" href="<?php echo base_url() . 'shop'; ?>"> CONTINUE SHOPPING</a>
<!--                                    </p>-->
        
                <a href="<?php echo base_url() . 'cart/checkout'; ?>" style="float:right" class="checkout-button button alt wc-forward">
                    PROCEED TO CHECKOUT
                </a>
    </div>

    
</div>
</div>
                                    </div>

</div>
            </div>
    </div><!-- /.main -->

</div><!-- /.row-->

<?php
    }
    else
    {
?>

            <?php
            if($this->session->flashdata('success')) {
            ?> 
           
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable">                         
                        <?php echo $this->session->flashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    </div>
                </div>
            
            <?php
            }
            ?>


<br />
    <div class="row">
        <div class="main col-md-12" id="ktmain" role="main">
            <div class="entry-content" itemprop="mainContentOfPage">
                <div class="woocommerce">
                                    <p class="cart-empty">Your cart is empty.</p>   
                                    <p class="return-to-shop">
                                        <a class="button wc-backward" href="<?php echo base_url() . 'shop'; ?>"> Return to shop     </a>
                                    </p>
</div>
            </div>
                    </div><!-- /.main -->
            </div><!-- /.row-->
 <br />                       
 <?php
    }
 ?>
 <br />       
        
    </div>
</div>
</div>

<script>
    
    //alert on delete product
    function deletechecked()
    {
        if(confirm("Are you sure you want to delete this product?"))
        {
            return true;
        }
        else
        {
            return false;  
        } 
    } 
    
    //disable dot(.) in input
    $(".qty").on("keypress", function(evt) {
        var keycode = evt.charCode || evt.keyCode;
        if (keycode == 46) {
          return false;
        }
    });
    
    //disable 0 in input
    $(".qty").on("input", function() {
        if (/^0/.test(this.value)) {
          this.value = this.value.replace(/^0/, "")
        }
    })
    
    //disable 0 and set 1 in input
    const numInputs = document.querySelectorAll('input[type=number]')

    numInputs.forEach(function (input) {
      input.addEventListener('change', function (e) {
        if (e.target.value == '') {
          e.target.value = 1
        }
      })
    })
      
    //increase quantity 
    function increment_quantity(cart_id, price) {       
        var inputQuantityElement = $("#input-quantity-"+cart_id);      
        var new_quantity = parseInt($(inputQuantityElement).val())+1;      
        var new_price = new_quantity * price;
        save_to_db(cart_id, new_quantity, new_price);
    }
    
    //decrease quantity
    function decrement_quantity(cart_id, price) {
        var inputQuantityElement = $("#input-quantity-"+cart_id);
        if($(inputQuantityElement).val() > 1) 
        {
            var new_quantity = parseInt($(inputQuantityElement).val())-1;
            var new_price = new_quantity * price;
            save_to_db(cart_id, new_quantity, new_price);
        }
    }
    
    //input type quantity update
    function changeprice(qty, cart_id, price) {      
        var new_quantity = qty;
        var new_price = new_quantity * price;
        save_to_db(cart_id, new_quantity, new_price);
    }

    //update values and database table
    function save_to_db(cart_id, new_quantity, new_price) {
            var inputQuantityElement = $("#input-quantity-"+cart_id);
            var priceElement = $("#cart-price-"+cart_id);
            var hidprice = $("#pricehidden-"+cart_id);
            $.ajax({
                    url : "<?php echo base_url().'cart/quantity/update'; ?>",
                    data : "cart_id="+cart_id+"&new_quantity="+new_quantity+"&new_price="+new_price,
                    type : 'post',
                    success : function(response) {
                            $(inputQuantityElement).val(new_quantity);
                                if(isNaN(new_price))
                                {
                                    $(priceElement).text(0);
                                }
                                else
                                {
                                    $(priceElement).text(addCommas(new_price.toFixed(2)));
                                    $(hidprice).val(new_price.toFixed(2));
                                }
                                                      
//                                var totalQuantity = 0;
//                                $("input[id*='input-quantity-']").each(function() {
//                                    var cart_quantity = $(this).val();
//                                    totalQuantity = parseInt(totalQuantity) + parseInt(cart_quantity);
//                                    finaltotalQuantity = totalQuantity.toFixed(2);
//                                });
//                                $("#total-quantity").text(finaltotalQuantity);
                                var totalItemPrice = 0;
                                $(".pricehidden").each(function() {
                                    var cart_price = $(this).val();
                                    //var cart_price = $(this).text();
                                    //var cart_price = parseInt(new_quantity) * parseInt(new_price);
                                    totalItemPrice = parseFloat(totalItemPrice) + parseFloat(cart_price);
                                });
                                $("#total-price").text(addCommas(parseFloat(totalItemPrice.toFixed(2))));
                    }
            });
    }
    
    //add comma in price value
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    
</script>


<?php
include_once('footer.php');
?>