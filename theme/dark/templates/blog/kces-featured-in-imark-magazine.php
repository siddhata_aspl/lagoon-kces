<?php
include_once('header.php');
?>



 
  <div class="wrap contentclass" role="document">

                      <div class="titleclass" id="pageheader">
		<div class="container">
        <div class="page-header page-img">
  <h1> News &amp; Events      </h1>
  <img width="1200" height="320" src="styles/images/history_page_img.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="history_page_img" srcset="styles/images/history_page_img.jpg 1200w, styles/images/history_page_img-300x80.jpg 300w, styles/images/history_page_img-1024x273.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
  
  
   </div>   
   <div class="navigation pagination mar-btm10">
			<div class="newsletter">
			<!--<div class="navigation"><span><img src="/styles/images/arrow-prew-first.png"></span><a href="/KC_Electricals/news-events/"><img src="/styles/images/2016/02/prew.png"></a><span class="current">1</span><a class="inactive" href="/news-events/page/2/">2</a><a class="inactive" href="/news-events/page/3/">3</a><a class="inactive" href="/news-events/page/4/">4</a><a class="inactive" href="/news-events/page/5/">5</a><a href="/news-events/page/5/"><img src="/styles/images/next.png"></a><span><img src="/styles/images/2016/02/arrow-next-last.png"></span></div>-->
</div>
<div class="newsletter_btn"><a href="../newsletter/index.php">Newsletter</a></div>
</div>     
		</div><!--container-->
	</div>
      
    <div id="content" class="container">
    <div class="row single-article">
        
      <div class="main col-md-12" id="ktmain" role="main">
                  <article class="post-1683 post type-post status-publish format-standard has-post-thumbnail hentry category-news-events">
          					  <div class="middle-part">
					  <div class="row">
                       <div class="news-event-detail-left"> 
                       <div class="imghoverclass post-single-img border-blue"><!--<a href="styles/images/imark-logo-with-margin-e1508453126229.jpg" rel="lightbox" class="">--><img src="styles/images/imark-logo-with-margin-e1508453126229.jpg"alt="KCES featured in IMARK magazine" /></a></div> 
                       </div>
                    <div class="news-event-detail-right">   
                                  <div class="postmeta updated color_gray">
    <div class="postdate bg-lightgray headerfont">
        <span class="postday">20</span>
        Apr 2017
    </div>       
</div>    <header>
            <h2 class="entry-title heading-inner-page">KCES featured in IMARK magazine</h2>
        		
    </header>
    <div class="entry-content clearfix">
      <p>When Jay and Harriett Mullins opened a grocery store that included an aisle of electrical supplies in 1927, they had no way of knowing that Kansas City Electrical Supply would be a thriving electrical distribution equipment and switchgear supplier run by their great-granddaughter nearly a century later.</p>
<p>Company President Kaylin Crain is the fourth generation of the Mullins family and oversees 47 employees in two locations. The company serves light industrial, commercial and residential<br />
electrical contractors with manufacturing, maintenance and repair needs.</p>
<p>The company operates on its original motto, “<strong><em>Every time we make a friend, we grow a little</em></strong>.”</p>
<p>Read more here: <strong><a href="styles/images/IMARK-NOW-Winter-2017-KCES-Profile.pdf" target="_blank">IMARK NOW article</a> </strong></p>
<p><a href="../styles/images/2017/04/Mullins-Store.bmp" rel="lightbox" ><img class="aligncenter size-full wp-image-1704" src="styles/images/Mullins-Store.bmp" alt="mullins-store" width="2048" height="1341" /></a></p>
	 <a href="news-events.php" class="return-to">Return to all News & Articles</a>
    </div>
	</div>
   				 </div>
                 </div>
          </article>
</div>        
            </div><!-- /.row-->
    </div><!-- /.content -->
  </div><!-- /.wrap -->





<?php
include_once('footer.php');
?>