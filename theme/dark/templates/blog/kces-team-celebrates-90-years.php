<?php
include_once('header.php');
?>



 
  <div class="wrap contentclass" role="document">

                      <div class="titleclass" id="pageheader">
		<div class="container">
        <div class="page-header page-img">
  <h1> News &amp; Events      </h1>
  <img width="1200" height="320" src="styles/images/history_page_img.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="history_page_img" srcset="styles/images/history_page_img.jpg 1200w, styles/images/history_page_img-300x80.jpg 300w, styles/images/history_page_img-1024x273.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
  
  
   </div>   
   <div class="navigation pagination mar-btm10">
			<div class="newsletter">
			<!--<div class="navigation"><span><img src="/styles/images/arrow-prew-first.png"></span><a href="/KC_Electricals/news-events/"><img src="/styles/images/prew.png"></a><span class="current">1</span><a class="inactive" href="/news-events/page/2/">2</a><a class="inactive" href="/news-events/page/3/">3</a><a class="inactive" href="/news-events/page/4/">4</a><a class="inactive" href="/news-events/page/5/">5</a><a href="/news-events/page/5/"><img src="/styles/images/next.png"></a><span><img src="/styles/images/arrow-next-last.png"></span></div>-->
</div>
<div class="newsletter_btn"><a href="../newsletter/index.php">Newsletter</a></div>
</div>     
		</div><!--container-->
	</div>
      
    <div id="content" class="container">
    <div class="row single-article">
        
      <div class="main col-lg-9 col-md-8" id="ktmain" role="main">
                  <article class="post-1781 post type-post status-publish format-standard has-post-thumbnail hentry category-news-events">
              <div class="postmeta updated color_gray">
    <div class="postdate bg-lightgray headerfont">
        <span class="postday">19</span>
        Oct 2017
    </div>       
</div>    <header>
            <h2 class="entry-title heading-inner-page">KCES Team Celebrates 90 years!</h2>
        		
    </header>
    <div class="entry-content clearfix">
      <h4><a href="styles/images/CASINO-NIGHT-ALL-EMPL.jpg" rel="lightbox" ><img class="alignright wp-image-1782 size-large" src="styles/images/CASINO-NIGHT-ALL-EMPL-1024x456.jpg" alt="casino-night-all-empl" width="940" height="419" srcset="styles/images/CASINO-NIGHT-ALL-EMPL-1024x456.jpg 1024w, styles/images/CASINO-NIGHT-ALL-EMPL-150x67.jpg 150w, styles/images/CASINO-NIGHT-ALL-EMPL-300x134.jpg 300w, styles/images/CASINO-NIGHT-ALL-EMPL-768x342.jpg 768w" sizes="(max-width: 940px) 100vw, 940px" /></a><span style="color: #000000;">The KCES team came together at the Argosy Casino Banquet Room early in September to celebrate 90 years.</span></h4>
<h4><span style="color: #000000;">The group enjoyed a fine dinner and a KCES trivia game with prizes.</span></h4>
<p>&nbsp;</p>
<figure id="attachment_1784" class="thumbnail wp-caption alignleft" style="width: 300px"><a href="styles/images/THE-GLORIOUS-COMMITTEE-e1508453416620.jpg" rel="lightbox" ><img class="wp-image-1784 size-medium" src="styles/images/THE-GLORIOUS-COMMITTEE-300x241.jpg" alt="the-glorious-committee" width="300" height="241" /></a><figcaption class="caption wp-caption-text">The Entertainment Committee did an outstanding job putting the event together for the enjoyment of all.</figcaption></figure>
<figure id="attachment_1785" class="thumbnail wp-caption aligncenter" style="width: 475px"><a href="styles/images/Mullins-Store.jpg" rel="lightbox" ><img class="wp-image-1785" src="styles/images/Mullins-Store-300x196.jpg" alt="mullins-store" width="475" height="310" srcset="styles/images/Mullins-Store-300x196.jpg 300w, styles/images/Mullins-Store-150x98.jpg 150w, styles/images/Mullins-Store-768x503.jpg 768w, styles/images/Mullins-Store-1024x671.jpg 1024w" sizes="(max-width: 475px) 100vw, 475px" /></a><figcaption class="caption wp-caption-text">KCES Started as an aisle in the Adams &amp; Mullins Grocery.</figcaption></figure>
	 <a href="news-events.php" class="return-to">Return to all News & Articles</a>
    </div>
	</div>
   				 </div>
                 </div>
          </article>
</div>        
            <aside id="ktsidebar" class="col-lg-3 col-md-4 kad-sidebar desktopsidebar" role="complementary">
        <div class="sidebar">
          <section id="wpb_widget-2" class="widget-1 widget-first widget widget_wpb_widget"><div class="widget-inner"><div class="mobilesidebar" onclick="myFunction()"><h3>Archives</h3></div><div class="desktopsidebar"><h3>Archives</h3></div><div class="desktopsidebar blog-list-archive">
      <div class="blog-list-archive-list" > <li ><a class="title" href="JavaScript:void()">2018</a>
                <ul class="archive-sub-menu">

                                </ul>
            </li>
            </div>
        <div class="blog-list-archive-list-selected" > <li class="selected"><a class="title" href="JavaScript:void()">2017</a>
                <ul class="archive-sub-menu-top" style="display:block">

                                    <li class="last-select"><a href="blog/2017/10/index.php">October</a>
                    </li>
                    
                                        <li ><a href="blog/2017/04/index.php">April</a>
                    </li>
                    
                                    </ul>
            </li>
            </div>
        <div class="blog-list-archive-list" > <li ><a class="title" href="JavaScript:void()">2016</a>
                <ul class="archive-sub-menu">

                                    <li ><a href="blog/2016/05/index.php">May</a>
                    </li>
                    
                                        <li ><a href="blog/2016/03/index.php">March</a>
                    </li>
                    
                                    </ul>
            </li>
            </div>
        </div>
</div></section>        </div><!-- /.sidebar -->
      </aside><!-- /aside -->
            </div><!-- /.row-->
    </div><!-- /.content -->
  </div><!-- /.wrap -->





<?php
include_once('footer.php');
?>