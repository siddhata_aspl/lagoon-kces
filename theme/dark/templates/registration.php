<?php
include_once('header.php');
?>

        <script>
         document.title = "Registration | KCES";
        </script>

<style>
    
    .login {
        margin-top: 20%;
    }
    
    .panel-heading {
        background-color: #272973 !important;
        padding: 10px;
    }
    
    .panel-body {
        padding: 8% 18%;
    }
    
    .login-title {
        color: #ffffff;
        text-align: center;
        display: inherit;
        font-weight: 500;
        font-size: 16px;
        padding: 5px;
    }
    
    .btn-submit {
        background-color: #0074C8 !important;
        border: #2d2e74;
        width: 50% !important;
        text-align: center;
        margin: auto;
    }
    
</style>


<div class="wrap contentclass" role="document">
        
<div id="pageheader" class="titleclass">
<!--    <div class="container">
	<div class="page-header page-img">
        <h1> Products </h1>
        <img width="1200" height="320" src="styles/images/about_img.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="about_img" srcset="styles/images/about_img.jpg 1200w, styles/images/about_img-300x80.jpg 300w, styles/images/about_img-1024x273.jpg 1024w" sizes="(max-width: 1200px) 100vw, 1200px"> 
      </div>		
    </div>container-->
</div><!--titleclass-->

<div id="content" class="container">
    <div class="main" id="ktmain" role="main">
        
        <div class="login">
        
            <div class="col-md-6 col-md-offset-3" style="float: none !important; margin: auto !important;">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <span class="login-title">REGISTER</span>
                    </div>
                    <div class="panel-body">
                        
                        <?php echo form_open_multipart(BASE_URL.'/register/user'); ?>
                        
<!--                        <form action="" method="post" accept-charset="utf-8">-->
                            <fieldset>
                                
                                <div class="form-group">		
                                <?php echo form_error('username', '<div class="alert alert-danger">', '</div>'); ?>														
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'username',
						  'id'          => 'username',
                                                  'required'    => 'required',  
						  'class'       => 'form-control',
                                                  'placeholder'	=> 'USERNAME*',                                                  
						  'value'		=> set_value('username', '', FALSE)
						);
			
						echo form_input($data); ?>

						
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                <div class="form-group">
                                    <?php echo form_error('firstname', '<div class="alert alert-danger">', '</div>'); ?>									
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'firstname',
						  'id'          => 'firstname',
						  'class'       => 'form-control',
                                                  'placeholder'	=> 'FIRST NAME',
						  'value'		=> set_value('firstname', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->	
                                </div>
                                <div class="form-group">
                                    <?php echo form_error('lastname', '<div class="alert alert-danger">', '</div>'); ?>														
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'lastname',
						  'id'          => 'lastname',
						  'class'       => 'form-control',
                                                  'placeholder'	=> 'LAST NAME',
						  'value'		=> set_value('lastname', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->	
                                </div>
                                
                                <div class="form-group">		
                <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>														
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'email',
						  'id'          => 'email',
                                                  'required'    => 'required',  
						  'class'       => 'form-control',
                                                  'placeholder'	=> 'EMAIL*',
						  'value'		=> set_value('email', '', FALSE)
						);
			
						echo form_input($data); ?>
                                            <!--<p class="help-block">&nbsp;</p>-->
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                <div class="form-group">		
                <?php echo form_error('phone', '<div class="alert alert-danger">', '</div>'); ?>													
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'phone',
						  'id'          => 'phone',
						  'class'       => 'form-control',
                          'placeholder'	=> 'PHONE(123-456-7890)',
						  'value'		=> set_value('phone', '', FALSE)
						);
			
						echo form_input($data); ?> 
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                <div class="form-group">
            		<?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('user_new_profilepic'); ?></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
						<!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input type="file" name="file_upload" value=""/>
					</div> <!-- /controls -->
				</div> <!-- /form-group -->   
                                
                                <br />
                                
                                <div class="form-group">
                                    <input type="submit" name="submit" id="submit" class="btn btn-success btn-block btn-submit" value="Submit">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<script>

document.getElementById('phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

</script>

<?php
include_once('footer.php');
?>