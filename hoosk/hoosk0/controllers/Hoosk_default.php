<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hoosk_default extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->load->model('News_events_model');
        $this->load->model('Front_clients_model');
        $this->load->model('Hoosk_model');
        $this->load->helper('hoosk_page_helper');
        $this->load->library('session');
        $this->load->helper("url");
        define('LANG', $this->Front_clients_model->getLang());
        $this->lang->load('admin', LANG);       
        define('SITE_NAME', $this->data['settings']['siteTitle']);
        define('THEME', $this->data['settings']['siteTheme']);
        define('DOMAIN', $this->Hoosk_model->getDomain());
        define('API_KEY', $this->Hoosk_model->getAPIKey());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        $this->maintenanceMode = $this->data['settings']['siteMaintenance'];
        if (($this->maintenanceMode) && ($this->session->userdata('logged_in'))) {
            $this->maintenanceMode = false;
        }
    }

    public function index() {
        
        if (!$this->maintenanceMode) {
            $totSegments = $this->uri->total_segments();
            if (!is_numeric($this->uri->segment($totSegments))) {
                $pageURL = $this->uri->segment($totSegments);
            } else if (is_numeric($this->uri->segment($totSegments))) {
                $pageURL = $this->uri->segment($totSegments - 1);
            }
            if ($pageURL == "") {
                $pageURL = "home";
            }
            $this->data['page'] = $this->Hoosk_page_model->getPage($pageURL);
//                        print_r($this->data['page']) ;
//                        if(substr_count($this->data['page']['pageContentHTML'],"%htmlfortemplate=")>0)
//                        {
//                            $this->data['page']['pageContentHTML'] = str_replace("%htmlfortemplate=55%","Peter",$this->data['page']['pageContentHTML']);
//                        }

            if (substr_count($this->data['page']['pageContentHTML'], "%#shortcodeKces=") > 0) {
                $pageContentHTML = $this->data['page']['pageContentHTML'];
                $shortcode_array = explode('%#', $pageContentHTML);
                for ($i = 0; $i < count($shortcode_array); $i++) {
                    if ($i % 2 == 0) {
                        // do nothing
                    } else {
                        $short_code = $shortcode_array[$i];
                        $res = $this->get_method($short_code);
                        $pageContentHTML = str_replace('%#' . $short_code . '%#', $res, $pageContentHTML);
                    }
                }
                $this->data['page']['pageContentHTML'] = $pageContentHTML;
            } else {
                
            }
            $this->Hoosk_page_model->getOurPeople();
            //$pro_count = $this->Front_clients_model->get_total_cart_products($uid);
               // echo $pro_count;die;
            if ($this->data['page']['pageTemplate'] != "") {
                
                $this->data['header'] = $this->load->view('templates/header', $this->data, true);
                $this->data['footer'] = $this->load->view('templates/footer', '', true);
                $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
            } else {
                $this->error();
            }
        } else {
            $this->maintenance();
        }
        
    }

    public function viewnews_events() {
        //Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $news_id = base64_decode($this->uri->segment(2));
        //echo $news_id;die();
        $this->load->helper('directory');
        $this->data['themesdir'] = directory_map($_SERVER["DOCUMENT_ROOT"] . '/theme/', 1);
        $this->data['langdir'] = directory_map(APPPATH . '/language/', 1);

        //Get user details from database
        $this->data['news'] = $this->News_events_model->getnews_eventss($news_id);
        //print_r($this->data['newsevents']);die();
        //Load the view
        $this->data['header'] = $this->load->view('templates/header', $this->data, true);
        $this->data['footer'] = $this->load->view('templates/footer', '', true);
        $this->load->view('templates/news_events_detail', $this->data);
    }

    public function searchlist_news_events() {       

        if (isset($_REQUEST['s'])) {           
             $search_text = $this->input->post('s');             
             $this->session->set_userdata('search', $search_text);
        } else {
             $search_text = $this->session->userdata('search');
        }
        
        $this->load->library("pagination");

        $config = array();

        if ($search_text == '') {
            $this->session->unset_userdata('search');
        }
        
        if ($this->session->userdata('search') == "") {
            $search_text = $this->input->post('s');
        } else {
            $search_text = $this->session->userdata('search');
        }

        $allnewscount = $this->News_events_model->count_news_events_search($search_text);
        $config["base_url"] = base_url() . "news-events/search/";
        $config["use_page_numbers"] = FALSE;
        $config["total_rows"] = $allnewscount;
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $limit = $config["per_page"];

        $start = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $this->data['search_news'] = $this->News_events_model->get_news_events_search($limit, $start, $search_text);
        $this->data['pagination'] = $this->pagination->create_links();
        
        $this->data['header'] = $this->load->view('templates/header', $this->data, true);
        $this->data['footer'] = $this->load->view('templates/footer', '', true);
        $this->load->view('templates/news-search', $this->data);
    }

    public function view_newsletter_page() {        
        $this->data['header'] = $this->load->view('templates/header', $this->data, true);
        $this->data['footer'] = $this->load->view('templates/footer', '', true);
        $this->load->view('templates/newsletter', $this->data);
    }

    public function newsletter_mail() {

       $this->Front_clients_model->add_newsletter_mail();
   }

    function get_method($short_code) {
//        return $short_code;
        $method_data = explode('=', $short_code)[1];
        $method_id = explode(',', $method_data)[0];
        $sorting = explode(',', $method_data)[1];
        $limit = explode(',', $method_data)[2];

        $res = $this->Hoosk_page_model->getMethodName($method_id, $sorting, $limit);

        return $res;
        die;
    }

    public function category() {
        if (!$this->maintenanceMode) {
            $catSlug = $this->uri->segment(2);
            $this->data['page'] = $this->Hoosk_page_model->getCategory($catSlug);
            if ($this->data['page']['categoryID'] != "") {
                $this->data['header'] = $this->load->view('templates/header', $this->data, true);
                $this->data['footer'] = $this->load->view('templates/footer', '', true);
                $this->load->view('templates/category', $this->data);
            } else {
                $this->error();
            }
        } else {
            $this->maintenance();
        }
    }

    public function article() {
        if (!$this->maintenanceMode) {
            $articleURL = $this->uri->segment(2);
            $this->data['page'] = $this->Hoosk_page_model->getArticle($articleURL);
            if ($this->data['page']['postID'] != "") {
                $this->data['header'] = $this->load->view('templates/header', $this->data, true);
                $this->data['footer'] = $this->load->view('templates/footer', '', true);
                $this->load->view('templates/article', $this->data);
            } else {
                $this->error();
            }
        } else {
            $this->maintenance();
        }
    }

    public function error() {
        $this->data['page']['pageTitle'] = "Oops, Error";
        $this->data['page']['pageDescription'] = "Oops, Error";
        $this->data['page']['pageKeywords'] = "Oops, Error";
        $this->data['page']['pageID'] = "0";
        $this->data['page']['pageTemplate'] = "error";
        $this->data['header'] = $this->load->view('templates/header', $this->data, true);
        $this->data['footer'] = $this->load->view('templates/footer', '', true);
        $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
    }

    public function maintenance() {
        $this->data['page']['pageTitle'] = "Maintenance Mode";
        $this->data['page']['pageDescription'] = "Maintenance Mode";
        $this->data['page']['pageKeywords'] = "Maintenance Mode";
        $this->data['page']['pageID'] = "0";
        $this->data['page']['pageTemplate'] = "maintenance";
        $this->data['header'] = $this->load->view('templates/header', $this->data, true);
        $this->data['footer'] = $this->load->view('templates/footer', '', true);
        $this->load->view('templates/' . $this->data['page']['pageTemplate'], $this->data);
    }

    public function feed() {

        if (($this->uri->segment(2) == "atom") || ($this->uri->segment(2) == "rss")) {
            $posts = getFeedPosts();
            $this->load->library('feed');
            $feed = new Feed();
            $feed->title = SITE_NAME;
            $feed->description = SITE_NAME;
            $feed->link = BASE_URL;
            $feed->pubdate = date("m/d/y H:i:s", $posts[0]['unixStamp']);
            foreach ($posts as $post) {
                $feed->add($post['postTitle'], BASE_URL . '/article/' . $post['postURL'], date("m/d/y H:i:s", $post['unixStamp']), $post['postExcerpt']);
            }
            $feed->render($this->uri->segment(2));
            } else if ($this->uri->segment(2) == "json") {
            $posts = getFeedPosts();
            $json_posts = array();
            foreach ($posts as $post) {
                $single_post = array(
                    'postTitle' => $post['postTitle'],
                    'postExcerpt' => $post['postExcerpt'],
                    'postDate' => date("m/d/y H:i:s", $post['unixStamp']),
                    'postURL' => BASE_URL . '/article/' . $post['postURL'],
                    'postContentHTML' => $post['postContentHTML'],
                    'postContentJSON' => json_decode($post['postContent']),
                );
                array_push($json_posts, $single_post);
            }
            $response = array('status' => 'OK');

            $this->output
                    ->set_status_header(200)
                    ->set_content_type('application/json', 'utf-8')
                    ->set_output(json_encode($json_posts, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                    ->_display();
            exit;
        } else {
            $this->error();
        }
    }
    
    

    /*     * *************************** */
    /*     * ** Front Shop System ************ */
    /*     * *************************** */
    
    
    public function register() {
        
        $this->load->view('templates/registration', $this->data);
    }
    
    public function confirm_user_register()
    {
		
		//Load the form validation library
		$this->load->library('form_validation');
		//Set validation rules
		$this->form_validation->set_rules('firstname', 'Firstname', 'trim|alpha');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|alpha');
               $this->form_validation->set_rules('username', 'Username', 'trim|alpha_dash|alpha_numeric|required|is_unique[hoosk_user.userName]');
        //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_user.email]');
               $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_user.email]|is_unique[hoosk_company.email]|is_unique[employees.emp_email]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
//		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[32]');
//		$this->form_validation->set_rules('con_password', 'confirm password','trim|required|matches[password]');
                // if (empty($_FILES['file_upload']['name']))
                // {
                //     $this->form_validation->set_rules('file_upload', 'profile picture', 'required');
                // }


		if($this->form_validation->run() == FALSE) {
			//Validation failed
			$this->register();
		}  else  {
			//Validation passed
			//Add the user
			//echo "hii";die;
			if (!empty($_FILES['file_upload']['name']))
			{
                        $config['upload_path']          = './uploads/users/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['max_size']             = 1000;
                        $config['max_width']            = 1024;
                        $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        $this->upload->do_upload('file_upload');
                        $upload_data = array('upload_data' => $this->upload->data());
                        $filename = $upload_data['upload_data']['file_name'];
			}
			else
			{
			            $filename = '';
			}
                        
                            
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $this->Front_clients_model->registerUser($filename);
                            
                                        $email= $this->input->post('email');
                                        $this->load->helper('string');
                                        $rs= random_string('alnum', 12);
                                        $data = array(
                                        'rs' => $rs
                                        );
                                        $forgotmail_user = $this->Front_clients_model->update_user_for_forgotmail($data,$email);


                                        //now we will send an email
                                        $config['protocol'] = 'sendmail';
                                        $config['mailpath'] = '/usr/sbin/sendmail';
                                        $config['charset'] = 'iso-8859-1';
                                        $config['wordwrap'] = TRUE;

                                        $this->load->library('email', $config);

                                        $mail_template = $this->Front_clients_model->getmail_template(1);
                                        //print_r($mail_template);die;
                                        if(isset($mail_template[0]['email_template_title']))
                                        {   
                                        	//echo "hii";die;
                                            $link= "\r\n".BASE_URL.'/kces/register/'.$rs;
                                            $message = $mail_template[0]['email_template_desc'];
                                            $subject = $mail_template[0]['email_template_subject'];
                                            $message = str_replace("%firstname%",$forgotmail_user[0]['User_firstname'],$message);
                                            $message = str_replace("%lastname%",$forgotmail_user[0]['User_lastname'],$message);
                                            $message = str_replace("%email%",$forgotmail_user[0]['email'],$message);
                                            $message = str_replace("%link%",$link,$message);

                                            //$this->load->library('email', $config);

                    //                         $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                    //                         $this->email->to($email);

                    //                         $this->email->subject($subject);
                    //                         $this->email->message($message );
                    // //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    // //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                    //                         $this->email->send();

                                            $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);

                                            //echo "hii";die;
                                        }
                            
                            $this->session->set_flashdata('success', 'A confirmation email has been sent to registered email address. Please click on a confirmation link in the email to activate your account.');
                            //Return to user list
                            redirect(BASE_URL.'/shop/login', 'refresh');
                        // }
                        // else
                        // {
                        //     $this->form_validation->set_rules('file_upload', 'profile picture', 'callback_file_upload_error');
                        //     $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                        //     if($this->form_validation->run() == FALSE) {
                        //         $this->addUser();
                        //     }
                        // }
			
	  	}
    }   
    
    public function new_register()
    {
                $rs = $this->uri->segment(3);
		$query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                if ($query->num_rows() == 0)
                {
//                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                $this->load->view('templates/error1', $this->data);

                }
                else
                {
                                $this->load->database();
                                $this->load->helper('url');
                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                                $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                                if ($this->form_validation->run() == FALSE)
                                {
                                        echo form_open();
//                                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                        $this->load->view('templates/newpassform', $this->data);
                                }
                                else
                                {
                                        $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                                        if ($query->num_rows() == 0)
                                        {
                                                show_error('Sorry!!! Invalid Request!');
                                        }
                                        else
                                        {
                                                $data = array(
                                                'password' => base64_encode($this->input->post('password')),
                                                'rs' => '',
                                                'user_status'=>'Enabled',
                                                );
                                                $where=$this->db->where('rs', $rs);
                                                $where->update('hoosk_user',$data);
//                                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                                $this->load->view('templates/newpass', $this->data);
                                        }
                                }
                        }
    }

    public function login() {

        $username = $this->session->userdata('user_name') == '' ? '' : $this->session->userdata('user_name');
        if ($this->session->userdata('user_name') != '') {
            redirect(BASE_URL . '/shop', 'refresh');
        } else {
            $this->load->view('templates/login', $this->data);
        }
    }

    // public function loginCheck() {
    //     //setcookie('PHPSESSID', '', time() - 86400, '/');
    //     $username = $this->input->post('username');
    //     //$password=md5($this->input->post('password').SALT);
    //     $password = base64_encode($this->input->post('password'));
    //     $result = $this->Front_clients_model->login($username, $password);
    //     if ($result) {
    //         redirect(BASE_URL . '/shop', 'refresh');
    //     } else {
    //         $this->data['error'] = "1";
    //         $this->login();
    //     }
    // }

    public function loginCheck() {
       //setcookie('PHPSESSID', '', time() - 86400, '/');

       if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
               $secret = '6Lcyhm4UAAAAAKORUT3Vs4FnUpPyPBJxbfJKylFX';
               //get verify response data
               $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
               $responseData = json_decode($verifyResponse);

               if($responseData->success){

                       $username = $this->input->post('username');
                       $password = base64_encode($this->input->post('password'));
                       $result = $this->Front_clients_model->login($username, $password);

                       if ($result) {

                           redirect(BASE_URL . '/shop', 'refresh');

               }
               else
               {
                   $this->data['error'] = "1";
                   $this->login();
               }
       }
       else {
               $this->data['error'] = "2";
               $this->login();
            }
       }
       else {
               $this->data['error'] = "2";
               $this->login();
       }
   }

    function ajaxLogin() {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password') . SALT);
        $result = $this->Front_clients_model->login($username, $password);
        if ($result) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function forgot() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
        if ($this->form_validation->run() == FALSE) {
//            $this->data['header'] = $this->load->view('templates/header', $this->data, true);
//            $this->data['footer'] = $this->load->view('templates/footer', '', true);
            $this->load->view('templates/email_check', $this->data);
        } else {
            $email = $this->input->post('email');
            $this->load->helper('string');
            $rs = random_string('alnum', 12);
            $data = array(
                'rs' => $rs
            );
            $forgotmail_user = $this->Front_clients_model->update_user_for_forgotmail($data, $email);


            //now we will send an email
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $forgotmail_template = $this->Front_clients_model->getmail_template(2);

            if (isset($forgotmail_template[0]['email_template_title'])) {
                $link = "\r\n" . BASE_URL . '/kces/reset/' . $rs;
                $message = $forgotmail_template[0]['email_template_desc'];
                $subject = $forgotmail_template[0]['email_template_subject'];
                $message = str_replace("%firstname%", $forgotmail_user[0]['User_firstname'], $message);
                $message = str_replace("%lastname%", $forgotmail_user[0]['User_lastname'], $message);
                $message = str_replace("%email%", $forgotmail_user[0]['email'], $message);
                $message = str_replace("%link%", $link, $message);

                // $this->load->library('email', $config);

                // $this->email->from('password@' . EMAIL_URL, SITE_NAME);
                // $this->email->to($email);

                // $this->email->subject($subject);
                // $this->email->message($message);
                // //			$this->email->subject($this->lang->line('email_reset_subject'));
                // //			$this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                // $this->email->send();

                $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
//                $this->data['header'] = $this->load->view('templates/header', $this->data, true);
//                $this->data['footer'] = $this->load->view('templates/footer', '', true);
                $this->load->view('templates/check', $this->data);
            } else {
//                $this->data['header'] = $this->load->view('templates/header', $this->data, true);
//                $this->data['footer'] = $this->load->view('templates/footer', '', true);
                $this->load->view('templates/check', $this->data);
            }
        }
    }

    public function email_check($str) {
        $query = $this->db->get_where('hoosk_user', array('email' => $str), 1);
        if ($query->num_rows() == 1) {
            return true;
        } else {
            $this->form_validation->set_message('email_check', $this->lang->line('email_check'));
            return false;
        }
    }

    public function getPassword()
    {
        $rs = $this->uri->segment(3);
        $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);

       if ($query->num_rows() == 0)
       {
//                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                $this->data['footer'] = $this->load->view('admin/footer', '', true);
               $this->load->view('templates/error', $this->data);

       }
       else
       {
               $this->load->database();
               $this->load->helper('url');
               $this->load->library('form_validation');
               $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
               $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
               if ($this->form_validation->run() == FALSE)
               {
                   echo form_open();
//                    $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                    $this->data['footer'] = $this->load->view('admin/footer', '', true);
                   $this->load->view('templates/resetform', $this->data);
               }
               else
               {
                   $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                   if ($query->num_rows() == 0)
                   {
                       show_error('Sorry!!! Invalid Request!');
                   }
                   else
                   {
                       $data = array(
                       'password' => base64_encode($this->input->post('password')),
                       'rs' => ''
                       );
                       $where=$this->db->where('rs', $rs);
                       $where->update('hoosk_user',$data);
//                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
                       $this->load->view('templates/reset', $this->data);
                   }
               }
        }
   }
    
    public function logout() {
        //echo "hiii";die;
        $data = array(
            'user_id'   => '',
            'user_name' => '',
            'user_email' => '',
            //'logged_in'	=> '',
        );
//                
                //echo $this->session->userdata('user_name');
        
               $this->session->unset_userdata('user_id');
               $this->session->unset_userdata('user_name');
               $this->session->unset_userdata('user_email');
               //$this->session->unset_userdata('logged_in');
               // $this->load->driver('cache');
               // $this->session->sess_destroy();
               // $this->cache->clean();
    		//	ob_clean();

    			
               $this->session->unset_userdata($data);
                //$this->session->unset_userdata('logged_in');
                //$domain = base_url();
                //setcookie('PHPSESSID', '', time() - 86400, '/');
		//$this->session->unset_userdata($data);
		//$this->session->sess_destroy();
                //session_destroy();
                //print_r($_SESSION);
		//$this->login();
        
        //setcookie( config_item('kces_cookie'), '', time() - 3600 );
        //delete_cookie( config_item('kces_cookie') );
        
		        $user_data = $this->session->all_userdata();
            
                foreach ($user_data as $key => $value) {
                if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                        $this->session->unset_userdata($key);
                    }
                }
            
         $this->session->sess_destroy();

         redirect(BASE_URL . '/shop/login', 'refresh');

    }

    public function all_products_view() {     
        
        $cat_id = base64_decode($this->uri->segment(2));
        
        //$subcat_id = base64_decode($this->uri->segment(4));
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        //if ($this->session->userdata('user_name') != '') {
            
            $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
            //echo $pro_count;die;
            //$this->data['pro_count'] = $pro_count;
            //$this->data['header'] = $this->load->view('templates/header', $this->data, true);
            
            $this->data['product_category'] = $this->Front_clients_model->get_product_category_list();
            
            //for product subcategory listing
            $category = $this->data['product_category'];
            $i = 0;
            foreach($category as $c)
            {
                $category_id = $c['category_id'];
                //echo $category_id;
                $this->data['product_category'][$i]['subcategory'] = $this->Front_clients_model->get_subcategory_list_by_catid($category_id);
                $i++;
            }

            
            if($cat_id != '')
            {
                $this->data['products'] = $this->Front_clients_model->get_category_product_list($cat_id);
                $this->data['category'] = $this->Front_clients_model->get_catname_by_catid($cat_id);
            }
//            else if($subcat_id != '')
//            {
//                echo "hii";die;
//                $this->data['products'] = $this->Front_clients_model->get_subcategory_product_list($subcat_id);
//                //$this->data['category'] = $this->Front_clients_model->get_subcatname_by_catid($cat_id);
//                $this->data['category'] = $this->Front_clients_model->get_catname_by_catid($subcat_id);
//            }
            else
            {              
                $this->data['products'] = $this->Front_clients_model->get_product_list();
            }
            
            $this->load->view('templates/shop', $this->data);
        // } else {
        //     $this->load->view('templates/login', $this->data);
        // }
    }
    
    public function category_products_view() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $cat_id = base64_decode($this->uri->segment(2));
        
        $subcat_id = base64_decode($this->uri->segment(3));
        
        //if ($this->session->userdata('user_name') != '') {
            
            $this->data['product_category'] = $this->Front_clients_model->get_product_category_list();  
            
            //for product subcategory listing
            $category = $this->data['product_category'];
            $i = 0;
            foreach($category as $c)
            {
                $category_id = $c['category_id'];
                //echo $category_id;
                $this->data['product_category'][$i]['subcategory'] = $this->Front_clients_model->get_subcategory_list_by_catid($category_id);
                $i++;
            }
            
            if($subcat_id != '')
            {             
                $this->data['products'] = $this->Front_clients_model->get_subcategory_product_list($subcat_id);              
                $this->data['subcategory'] = $this->Front_clients_model->get_subcatname_by_subcatid($subcat_id);
                $this->data['categoryname'] = $this->Front_clients_model->get_catname_by_catid($cat_id);
                //print_r($this->data['subcategory']);die;
            }        
            
            $this->load->view('templates/shop', $this->data);
        // } else {
        //     $this->load->view('templates/login', $this->data);
        // }
    }
    
    public function products_view_by_sort() {
        //echo "hii";die;
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $sort_id = $this->uri->segment(4);
        
        $result = $this->Front_clients_model->get_product_list_by_sort($sort_id);
        
        echo json_encode($result);    
    }

    public function cat_products_view_by_sort() {
        //echo "hii";die;
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $sort_id = $this->uri->segment(5);
        $cat_id = base64_decode($this->uri->segment(6));
        
        $result = $this->Front_clients_model->get_cat_product_list_by_sort($sort_id,$cat_id);
        
        echo json_encode($result);    
    }
    
    public function subcat_products_view_by_sort() {
        //echo "hii";die;
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $sort_id = $this->uri->segment(5);
        $subcat_id = base64_decode($this->uri->segment(6));
        
        $result = $this->Front_clients_model->get_subcat_product_list_by_sort($sort_id,$subcat_id);
        
        echo json_encode($result);    
    }

    public function product_details_view() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);

        $pro_id = base64_decode($this->uri->segment(4));
        
        $cid = base64_decode($this->uri->segment(2));
        $subid = base64_decode($this->uri->segment(3));
        // get product detail by product id
        $product_data = $this->Front_clients_model->get_product_details($pro_id);
        
        if ($this->session->userdata('user_name') != '') {

            if (count($product_data) > 0) { // check data available for given product id or not
                
                $product_data[0]['product_images'] = $this->Front_clients_model->getImagesByProductId($pro_id); // get all images by product id
                
                $product_data[0]['location_data'] = $this->Front_clients_model->getLocationsByProductId($pro_id); // get all location by product id
                
                $this->data['pdetails'] = $product_data[0];
                
                $this->data['category'] = $this->Front_clients_model->get_catname_by_catid($cid);
                
                $this->data['subcategory'] = $this->Front_clients_model->get_subcatname_by_subcatid($subid);
                
            } 
        }
        else {
                redirect(BASE_URL . '/shop/login', 'refresh');
        }

        $this->load->view('templates/product_details', $this->data);
    }
    
    public function product_add_cart() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $addcart = $this->Front_clients_model->add_product_in_cart($uid);   
        
        $cid = $this->uri->segment(2);
      
        $subid = $this->uri->segment(3);
        
        $pro_id = base64_encode($this->input->post('product_id'));        
        
        $this->session->set_flashdata('success', "Item added successfully in cart."); 
        
        //echo $this->session->flashdata('success');die;
                
        redirect(BASE_URL . '/shop/' . $cid . '/' . $subid . '/'. $pro_id , 'refresh');
 
    }
    
    public function product_cart_view() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);        
        
        $this->data['cart_items'] = $this->Front_clients_model->get_product_cart_details($uid);
        
        $this->load->view('templates/cart', $this->data);
    }

    public function cart_quantity_update() {

       $uid = $this->session->userdata('user_id');

       $this->Front_clients_model->update_cart_data($uid);
    }
    
    public function remove_cart_product() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);
        
        $cid = base64_decode($this->uri->segment(3)); 
        
        $this->Front_clients_model->remove_cart_data($cid,$uid);
        
        redirect(BASE_URL . '/cart', 'refresh');
    }
      
    public function cart_checkout() {
        
        $uid = $this->session->userdata('user_id');        
        $this->data['pro_count'] = $this->Front_clients_model->get_total_cart_products($uid);

        $this->Front_clients_model->add_order($uid);
        
        $uname = $this->session->userdata('user_name');

        //generate pdf of shopping products

    //     $carts = $this->Front_clients_model->get_product_cart_details($uid);

        
    //     $cart_block = "<table class='shop_table shop_table_responsive cart woocommerce-cart-form__contents' cellspacing='0'>
    //     <thead>
    //         <tr>
    //             <th class='product-name'>Product</th>
    //             <th class='product-price'>Price</th>
    //             <th class='product-quantity'>Quantity</th>
    //             <th class='product-subtotal'>Total</th>
    //         </tr>
    //     </thead>
    //     <tbody>";

    //     $count = 0;
    //     foreach($carts as $cart)
    //     {
    //         $count++;
    //         $pname = $cart['product_name'];
    //         $pprice = $cart['product_price'];
    //         $pqty = $cart['product_quantity'];
    //         $ptotal = $cart['product_total'];
                                
    //         $cart_block .= "<tr class='woocommerce-cart-form__cart-item cart_item'>

    //                     <td class='product-name' data-title='Product'>
    //                     $pname
    //                     </td>

    //                     <td class='product-price' data-title='Price'>
    //                     $$pprice                                      
    //                     </td>

    //                     <td class='product-quantity' data-title='Quantity'>
    //                     $pqty
    //                     </td>

    //                     <td class='product-subtotal' data-title='Total'>
    //                     $$ptotal        
    //                     </td>

    //                 </tr>";

    //         $total = $total+$ptotal;
                               
    //     }

    //     $cart_block .= "</tbody>
    // </table><div class='cart-collaterals'>
    //                 <div class='cart_totals '>
                            
    //                 <h2>Cart totals</h2>

    //                 <table cellspacing='0' class='shop_table shop_table_responsive'>
                        
    //                     <tr class='order-total'>
    //                         <th>Total</th>
    //                         <td data-title='Total'><strong><span class='woocommerce-Price-amount amount'><span class='woocommerce-Price-currencySymbol'>$</span>$total</span></strong> </td>
    //                     </tr>
                        
    //                 </table>
                    
    //             </div>
    //             </div>";
    //     //echo $cart_block;die;       
    //     $time = time();
    //     $filename = 'SHOP-'.$uname.'-'.$time.'-PDF';

    //     $this->load->library('pdf');
    //     $pdf = $this->pdf->load();
    //     $pdf->SetFooter('KCES'.'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure
    //     $pdf->WriteHTML($cart_block); // write the HTML into the PDF
        //$pdf->Output('/var/www/html/kces/uploads/shop_pdf/'.$filename, 'F'); // save to file because we can


        //send successful mail
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;    

        $this->load->library('email', $config);
        
        $umail = $this->session->userdata('user_email');
        
        $this->email->from($umail.EMAIL_URL, SITE_NAME); 
        $this->email->to($umail);
        $this->email->subject('Shopping in KCES'); 
        $this->email->message('Thank you for shopping in KCES'); 
        
        $this->email->send();  

        $this->db->delete('cart_products', array('login_id' => $uid)); 
        
        $this->session->set_flashdata('success', "Thank you for shopping in KCES."); 
        
        redirect(BASE_URL . '/cart', 'refresh');
    }

}
