<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_template extends CI_Controller {

	function __construct()
	{
            
                parent::__construct();
		define("HOOSK_ADMIN",1);
		$this->load->model('Hoosk_model');
                $this->load->model('Email_template_model');
		$this->load->helper(array('admincontrol', 'url', 'form'));
		$this->load->library('session');
		define ('LANG', $this->Hoosk_model->getLang());
		$this->lang->load('admin', LANG);
		//Define what page we are on for nav
                $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
		$this->data['current'] = $this->uri->segment(2);
		define ('SITE_NAME', $this->Hoosk_model->getSiteName());
		define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
		define('THEME', $this->Hoosk_model->getTheme());
		define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
	}
        

        public function index()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/email_template/';
                $config['total_rows'] = $this->Email_template_model->countemail_template();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

		//Get users from database
		$this->data['email_template'] = $this->Email_template_model->get_email_template($result_per_page, $this->uri->segment(3));
                //Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/email_template', $this->data);
	}
        
        public function editEmail_template()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $email_template_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$mail_data = $this->Email_template_model->getemail_template($email_template_id);
		
		if(count($mail_data) > 0) {
		  
    		$this->data['email_tempalate'] = $mail_data;
    		//echo "hii";die();
    		//Load the view
    		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/email_template_edit', $this->data);
		}
		else
		{
		    $this->session->set_flashdata('message', 'Record with specified id does not exist');
            redirect(BASE_URL.'/admin/email_template', 'refresh');
		}
	}

	public function edited()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                if($this->uri->segment(4) != '')
                {
                    //Load the form validation library
                    $this->load->library('form_validation');
                    //Set validation rules
                    $this->form_validation->set_rules('title', 'Title', 'required');
                    $this->form_validation->set_rules('subject', 'Subject', 'required');
                    $this->form_validation->set_rules('editor1', 'Description', 'required');

                    if($this->form_validation->run() == FALSE) {
                            //Validation failed
                            $this->editEmail_template();
                    }  else  {

                            $title = strip_tags($this->input->post('title'));
                            $subject = strip_tags($this->input->post('subject'));
                            $desc = $this->input->post('editor1');
                            $submited_data = array('email_template_title' => $title, 'email_template_desc' => $desc, 'email_template_subject'=>$subject);
                            $this->Email_template_model->updateemail_template(base64_decode($this->uri->segment(4)),$submited_data);
                            $this->session->set_flashdata('success', 'Email Template Is Successfully Updated.');
                            redirect(BASE_URL.'/admin/email_template', 'refresh');

                    }
                }
	}
        function email_templateSearch()
	{
                
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
		$this->Email_template_model->email_templateSearch($this->input->post('term'),$result_per_page,0);
	}

}
