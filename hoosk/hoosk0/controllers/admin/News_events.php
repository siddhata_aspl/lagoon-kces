<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_events extends CI_Controller {

    function __construct()
    {
            //echo "hii";exit();
        parent::__construct();
        define("HOOSK_ADMIN",1);
        $this->load->model('Hoosk_model');
                $this->load->model('News_events_model');
        $this->load->helper(array('admincontrol', 'url', 'form'));
        $this->load->library('session');
        define ('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('admin', LANG);
                
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define ('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME', $this->Hoosk_model->getTheme());
        define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
                
    }
        

    public function index()
    {
//            echo $this->uri->segment(1);die();
            
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/news_events/';
                $config['total_rows'] = $this->News_events_model->countnews_events();//echo "hii";exit();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

        //Get banners from database
        $this->data['news_events'] = $this->News_events_model->getnews_events($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/news_events', $this->data);
    }
        
    public function get_all_data()
    {
        //echo "hi";exit();
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        echo $this->News_events_model->get_news_events_datatable_data('news_events',$_REQUEST);
                
    }

    public function addnews_events()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/news_events_new', $this->data);
    }

    public function confirm()
    {
           
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//                echo $_FILES['file_upload']['name'];
//                exit();
        //Load the form validation library
        $this->load->library('form_validation'); 
        //Set validation rules
        $this->form_validation->set_rules('news_title', 'News events title', 'trim|required');
        $this->form_validation->set_rules('short_content', 'Short content', 'trim');
        $this->form_validation->set_rules('news_desc', 'News description', 'trim');
                
                if (empty($_FILES['file_upload']['name']))
                {
                    $this->form_validation->set_rules('file_upload', 'News events image', 'required');
                }


        if($this->form_validation->run() == FALSE) {
            //Validation failed
                    
            $this->addnews_events();
        }  else  {
            //Validation passed
            //Add the user
                        $config['upload_path']          = './uploads/news_events/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        // $config['max_size']             = 1000;
                        // $config['max_width']            = 1024;
                        // $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('file_upload'))
                        {
                            $upload_data = array('upload_data' => $this->upload->data());
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $this->News_events_model->createnews_events($upload_data['upload_data']['file_name']);
                            $this->session->set_flashdata('success', 'News Events Is Successfully Added.');
                            //Return to user list
                            redirect(BASE_URL.'/admin/news_events', 'refresh');
                        }
                        else
                        {
                            $this->form_validation->set_rules('file_upload', 'News events image', 'callback_file_upload_error');
                            $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                            if($this->form_validation->run() == FALSE) {
                                $this->addnews_events();
                            }
                        }
            
        }
    }
        
        
        public function file_upload_error()
        {
            return FALSE;
        }
        
        
        public function change_status()
        {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $newsID = $this->input->post('news_id');

                    $data = array(
                        'news_status' => $status,
                    );
                    //echo $this->db->last_query();die();
                    if ($this->News_events_model->changenews_events_status($data, (int) $newsID)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/admin/news_events', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/admin/news_events', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/admin/news_events', 'refresh');
                }
        }
        

    public function editnews_events()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $news_id = base64_decode($this->uri->segment(4));
                  
        //Get user details from database
        $news_events_data = $this->News_events_model->getnews_eventss($news_id);
        
        if(count($news_events_data) > 0) {
        
            $this->data['news_events'] = $news_events_data;
            
            //Load the view
            $this->data['header'] = $this->load->view('admin/header', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/news_events_edit', $this->data);
        }
        else {
            $this->session->set_flashdata('message', 'Record with specified id does not exist');
            redirect(BASE_URL.'/admin/news_events', 'refresh');
        }
    }

    public function edited()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('news_title', 'News events title', 'required');
        $this->form_validation->set_rules('short_content', 'Short content', 'trim');
        $this->form_validation->set_rules('news_desc', 'News description', 'trim');
                
        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editnews_events();
        }  else  {
                        //usre want to change profile picture
                        if (!empty($_FILES['file_upload']['name']))
                        {
                            $config['upload_path']          = './uploads/news_events/';
                            $config['allowed_types']        = 'gif|jpg|png';
                            // $config['max_size']             = 1000;
                            // $config['max_width']            = 1024;
                            // $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if ($this->upload->do_upload('file_upload'))
                            {
                                $upload_data = array('upload_data' => $this->upload->data());
                                $this->News_events_model->updatenews_events(base64_decode($this->uri->segment(4)),$upload_data['upload_data']['file_name']);
                                $this->session->set_flashdata('success', 'News Events Is Successfully Updated.');
                                redirect(BASE_URL.'/admin/news_events', 'refresh');
                            }
                            else
                            {
                                $this->form_validation->set_rules('file_upload', 'News events image', 'callback_file_upload_error');
                                $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                                if($this->form_validation->run() == FALSE) {
                                    $this->editnews_events();
                                }
                            }
                            //Validation passed
                            //Update the user
                            
                        }
                        else
                        {
                            //echo "hiii";die();
                            //Validation passed
                            //Update the user
                            $this->News_events_model->updatenews_events(base64_decode($this->uri->segment(4)));
                            $this->session->set_flashdata('success', 'News Events Is Successfully Updated.');
                            redirect(BASE_URL.'/admin/news_events', 'refresh');
                        }
            //Return to user list
            
        }
    }


    function delete()
    {
            
        //echo "hii";die();
        // Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        // if($this->input->post('deleteid')):
        //  $this->Hoosk_model->removeBanners($this->input->post('deleteid'));
        //  redirect(BASE_URL.'/admin/banners');
        // else:
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        if($this->input->post('deleteid')):
            $this->News_events_model->removenews_events($this->input->post('deleteid'));
            redirect(BASE_URL.'/admin/news_events');
        else:
            $delete_id = base64_decode($this->uri->segment(4));
            $this->data['form']=$this->News_events_model->getnews_eventss($delete_id);
            $this->load->view('admin/news_events_delete.php', $this->data );
        endif;
    }

    
}
