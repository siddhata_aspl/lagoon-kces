<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Project extends CI_Controller {

    function __construct()
    {

        parent::__construct();
        define("HOOSK_ADMIN", 1);
        $this->load->model('Hoosk_model');
        $this->load->helper(array('admincontrol', 'url', 'form'));
        $this->load->library('session');
        define('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('THEME', $this->Hoosk_model->getTheme());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
    }

    public function index()
    {

       

        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('pagination');
        $result_per_page_result = $this->Hoosk_model->resultperpage();
        $result_per_page = $result_per_page_result[0][resultperpage];

        //for add page pdf D
        $pdfId = $this->session->userdata('pdfId');

        if($pdfId != NULL) {
           $this->session->unset_userdata('pdfId');
        } 

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->data['pdfid'] =  $pdfId;

        $this->load->view('admin/project', $this->data);
 

    }
    public function get_all_data()
    {
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        echo $this->Hoosk_model->get_project_datatable_data($_REQUEST);
            
    }
    public function addProject()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->data['company'] = $this->Hoosk_model->get_enabled_company();
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/project_new', $this->data);
    }
    
    public function get_product_ajax()
    {
        //echo 'sassadsa';exit();
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $productname = $this->input->post('productname');
        $rowid = $this->input->post('rowid');
        $type = $this->input->post('type');
        $result = $this->Hoosk_model->get_product($productname);
        $u = '';
        if(count($result)>0)
        {
            $u .= ' <ul id="country-list"> ';
            foreach($result as $country) {
                if($type != 'desc'){
                    $u .= "<li onClick=selectCountry('".$country[product_id]."','".$rowid."')> ". $country["product_name"] ." </li> ";            
                }else{
                    $u .= "<li onClick=selectCountry('".$country[product_id]."','".$rowid."')> ". $country["product_desc"] ." </li> ";
                }
            }
            $u .= ' </ul> ';
        }
        else
        {
            $u .= ' <ul id="country-list"> ';
            $u .= "<li onClick=selectCountry('','".$rowid."')> No Product Found </li> "; 
            $u .= ' </ul> ';
        }
        echo $u;
        
    }
    public function get_company_byid_ajax()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $companyid = $this->input->post('companyid');
        $result = $this->Hoosk_model->get_company_by_id($companyid);
        echo json_encode($result);
    }
    public function get_product_byid_ajax()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $productid = $this->input->post('productid');
        $result = $this->Hoosk_model->get_product_by_id($productid);
        echo json_encode($result);
    }
    public function confirm()
    {
        
//        print_r($_REQUEST);exit();
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('po_name', 'Name', 'trim|required');
       
        $this->form_validation->set_rules('po_number', 'Number', 'trim|required');
        $this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        $this->form_validation->set_rules('po_age', 'Age', 'trim|numeric');
        $this->form_validation->set_rules('po_site_name', 'Name', 'trim');
        $this->form_validation->set_rules('po_site_email', 'Email', 'trim');
        $this->form_validation->set_rules('po_office_phone', 'Office Phone', 'trim|min_length[12]');
        $this->form_validation->set_rules('po_cell_phone', 'Cell Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->addProject();
                
        }else  {
                 if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
                        $secret = '6Lcyhm4UAAAAAKORUT3Vs4FnUpPyPBJxbfJKylFX';
                        //get verify response data
                        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                        $responseData = json_decode($verifyResponse);
                        if($responseData->success){
                            $po_name = strip_tags($this->input->post('po_name'));
                            $po_number = $this->input->post('po_number');
                            $po_status = $this->input->post('po_status');
                            if($po_status != 1 || isset($_POST['submit']))
                            {
                                $po_status1 = 'Pending';
                            }
                            else
                            {
                                $po_status1 = 'Approved';
                            }
                            $po_manager = $this->input->post('po_manager');
                            $po_age = strip_tags($this->input->post('po_age'));
                            $po_site_name = strip_tags($this->input->post('po_site_name'));
                            $po_site_email = strip_tags($this->input->post('po_site_email'));
                            $po_office_phone = strip_tags($this->input->post('po_office_phone'));
                            $po_cell_phone = strip_tags($this->input->post('po_cell_phone'));
                            $po_state = strip_tags($this->input->post('po_state'));
                            $product_rows = $this->input->post('product_rows');
                            $custom_rows = $this->input->post('custom_rows');
                            $data = array(
                                'po_number'=>$po_number,
                                'po_name'=>$po_name,
                                'po_company'=>$po_manager,
                                'po_age'=>$po_age,
                                'po_state'=>$po_state,
                                'po_site_contact_name'=>$po_site_name,
                                'po_site_contact_email'=>$po_site_email,
                                'po_site_contact_office_phone'=>$po_office_phone,
                                'po_site_contact_cell_phone'=>$po_cell_phone,
                                'po_status'=>$po_status1,
                                'created_date'=> date("Y-m-d H:i:s"),
                                'modified_date'=> date("Y-m-d H:i:s"),
                            );
                            $confirm_project_id = $this->Hoosk_model->confirm_project($data);
                            if($confirm_project_id != '')
                            {
                                if(isset($_POST['submit']))
                                {
                                    
                                    
                                    for($i=1;$i<=$product_rows;$i++)
                                    {
                                        $product_check = $this->input->post('po_row'.$i.'_product_id');
                                        if(isset($product_check))
                                        {
                                            $product_id = $this->input->post('po_row'.$i.'_product_id');
                                            $product_name = $this->input->post('po_row'.$i.'_product');
                                            $product_product_desc = $this->input->post('po_row'.$i.'_product_desc');
                                            $product_product_quntity = $this->input->post('po_row'.$i.'_product_quntity');
                                            $product_order_quntity = $this->input->post('po_row'.$i.'_order_quntity');
                                            $product_product_price = $this->input->post('po_row'.$i.'_product_price');
                                            $product_total = $this->input->post('po_row'.$i.'_total');
                                            $product_comment = $this->input->post('po_row'.$i.'_comment');
                                            if($product_product_quntity >= $product_order_quntity)
                                            {

                                            }
                                            else
                                            {
                                                $product_order_quntity = 0;
                                                $product_total = 0;
                                            }
                                            $is_product_exits = $this->Hoosk_model->is_product_exist_po($product_id,$confirm_project_id);
                                            if(count($is_product_exits)>0)
                                            {
                                                $data = array(
                                                    'quantity'=>$is_product_exits[0]['quantity'] + $product_product_quntity,
                                                    'po_draft_order'=>$is_product_exits[0]['po_draft_order'] + $product_order_quntity,
                                                    'total'=>$is_product_exits[0]['total'] + $product_total,
                                                );
                                                $p_id = $is_product_exits[0]['po_product_id'];
                                                $this->Hoosk_model->confirm_update_product($data,$is_product_exits[0]['po_product_id']);
                                            }
                                            else
                                            {
                                                //product for po
                                                $data = array(
                                                    'po_id'=>$confirm_project_id,
                                                    'product_id'=>$product_id,
                                                    'quantity'=>$product_product_quntity,
                                                    'po_draft_order'=>$product_order_quntity,
                                                    'total'=>$product_total,
                                                    'comment'=>$product_comment,
                                                );
                                                $p_id = $this->Hoosk_model->confirm_product($data);
                                            }
                                        }
                                    }
                                    for($i=1;$i<=$custom_rows;$i++)
                                    {
                                        $custom_check = $this->input->post('po_row'.$i.'_custom_item');
                                        if($custom_check != '')
                                        {
                                            $product_custom_item = $this->input->post('po_row'.$i.'_custom_item');
                                            $product_custom_desc = $this->input->post('po_row'.$i.'_custom_desc');
                                            $product_custom_quantity = $this->input->post('po_row'.$i.'_custom_quantity');
                                            $product_custom_price = $this->input->post('po_row'.$i.'_custom_price');
                                            $product_custom_total = $this->input->post('po_row'.$i.'_custom_total');
                                            $product_custom_comment = $this->input->post('po_row'.$i.'_custom_comment');
                                            $data = array(
                                                'po_id'=>$confirm_project_id,
                                                'po_custom_title'=>$product_custom_item,
                                                'po_custom_desc'=>$product_custom_desc,
                                                'po_custom_quantity'=>$product_custom_quantity,
                                                'po_custom_price'=>$product_custom_price,
                                                'total'=>$product_custom_total,
                                                'comment'=>$product_custom_comment,
                                            );
                                            $c_id = $this->Hoosk_model->confirm_custom_product($data);
                                        }
                                    }

                                    if($po_status != 1)
                                    {

                                        //$this->pdfgen($confirm_project_id);
                                        $this->session->set_userdata('pdfId', $confirm_project_id);

                                        //header('Location: ' . base_url() . 'admin/project');

                                    }
                                    //exit;
                                    $this->session->set_flashdata('success', 'Project inserted successfully.');
                                   
                                    redirect(BASE_URL.'/admin/project', 'refresh');
                                   
                                }
                                else
                                {
                                    $data = array(
                                        'po_id'=>$confirm_project_id,
                                        'order_status'=>'Approved',
                                        'created_date'=> date("Y-m-d H:i:s"),
                                    );
                                    $confirm_order_id = $this->Hoosk_model->confirm_order($data);

                                    for($i=1;$i<=$product_rows;$i++)
                                    {
                                        $product_check = $this->input->post('po_row'.$i.'_product_id');
                                        if(isset($product_check))
                                        {
                                            $product_id = $this->input->post('po_row'.$i.'_product_id');
                                            $product_name = $this->input->post('po_row'.$i.'_product');
                                            $product_product_desc = $this->input->post('po_row'.$i.'_product_desc');
                                            $product_product_quntity = $this->input->post('po_row'.$i.'_product_quntity');
                                            $product_order_quntity = $this->input->post('po_row'.$i.'_order_quntity');
                                            $product_product_price = $this->input->post('po_row'.$i.'_product_price');
                                            $product_total = $this->input->post('po_row'.$i.'_total');
                                            $product_comment = $this->input->post('po_row'.$i.'_comment');
                                            if($product_product_quntity >= $product_order_quntity)
                                            {

                                            }
                                            else
                                            {
                                                $product_order_quntity = 0;
                                                $product_total = 0;
                                            }
                                            $is_product_exits = $this->Hoosk_model->is_product_exist_po($product_id,$confirm_project_id);
                                            
                                            if(count($is_product_exits)>0)
                                            {
                
                                                $data = array(
                                                    'quantity'=>$is_product_exits[0]['quantity'] + $product_product_quntity,
                                                    'po_draft_order'=>0,
                                                    'total'=>$is_product_exits[0]['total'] + $product_total,
                                                );
                                                $p_id = $is_product_exits[0]['po_product_id'];
                                                $this->Hoosk_model->confirm_update_product($data,$is_product_exits[0]['po_product_id']);

                                                //$is_product_exits = $this->Hoosk_model->is_product_exist_po($product_id,$confirm_project_id);
                                            }
                                            else
                                            {
                                                //product for po
                                                $data = array(
                                                    'po_id'=>$confirm_project_id,
                                                    'product_id'=>$product_id,
                                                    'quantity'=>$product_product_quntity,
                                                    'po_draft_order'=>0,
                                                    'total'=>$product_total,
                                                    'comment'=>$product_comment,
                                                );
                                                $p_id = $this->Hoosk_model->confirm_product($data);
                                            }

                                            //product for order
                                            $data = array(
                                                'order_id'=>$confirm_order_id,
                                                'product_id'=>$p_id,
                                                'quantity'=>$product_order_quntity,
                                            );
                                            $this->Hoosk_model->confirm_order_products($data);
                                        }
                                    }
                                    for($i=1;$i<=$custom_rows;$i++)
                                    {
                                        $custom_check = $this->input->post('po_row'.$i.'_custom_item');
                                        if($custom_check != '')
                                        {
                                            $product_custom_item = $this->input->post('po_row'.$i.'_custom_item');
                                            $product_custom_desc = $this->input->post('po_row'.$i.'_custom_desc');
                                            $product_order_id = $this->input->post('po_row'.$i.'_order_id');
                                            $product_order_id = isset($product_order_id)?$product_order_id:'';
                                            $product_custom_quantity = $this->input->post('po_row'.$i.'_custom_quantity');
                                            $product_custom_price = $this->input->post('po_row'.$i.'_custom_price');
                                            $product_custom_total = $this->input->post('po_row'.$i.'_custom_total');
                                            $product_custom_comment = $this->input->post('po_row'.$i.'_custom_comment');
                                            if($product_order_id != '')
                                            {
                                                $data = array(

                                                    'po_custom_quantity'=>$product_custom_quantity,
                                                    'total'=>$product_custom_total,
                                                    'comment'=>$product_custom_comment,

                                                );
                                                $this->Hoosk_model->confirm_update_order($data,$product_order_id);
                                                $p_id = $product_order_id;
                                            }
                                            else
                                            {
                                                $data = array(
                                                    'po_id'=>$confirm_project_id,
                                                    'po_custom_title'=>$product_custom_item,
                                                    'po_custom_desc'=>$product_custom_desc,
                                                    'po_custom_quantity'=>$product_custom_quantity,
                                                    'po_custom_price'=>$product_custom_price,
                                                    'total'=>$product_custom_total,
                                                    'comment'=>$product_custom_comment,
                                                );
                                                $c_id = $this->Hoosk_model->confirm_custom_product($data);
                                                $data = array(
                                                    'order_id'=>$confirm_order_id,
                                                    'product_id'=>$c_id,
                                                    'quantity'=> 0,
                                                );
                                                $this->Hoosk_model->confirm_order_products($data);
                                            }
                                                
                                        }
                                    }
//                                            echo "adsdsa";
//                                            echo $confirm_project_id;exit();
                                    if($po_status != 1)
                                    {
//                                                echo $confirm_project_id;exit();
                                        $this->pdfgen($confirm_project_id);
                                    }
                                    $this->session->set_flashdata('success', 'Project inserted successfully.');
                                    redirect(BASE_URL.'/admin/project', 'refresh');
                                }
                                
                            }
                            else
                            {
                                $this->addProject();
                            }
                        }
                        else
                        {
                            $this->session->set_flashdata('message', 'Project Not Added Due to Captcha.');
                            redirect(BASE_URL.'/admin/project', 'refresh');
                        }
                    }
                    else
                    {
                        $this->session->set_flashdata('message', 'Project Not Added Due to Captcha.');
                        redirect(BASE_URL.'/admin/project', 'refresh');
                    }
        }
        
        
    }
    
    public function editProject()
    {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $project_id = base64_decode($this->uri->segment(4));
                $remaining = $this->Hoosk_model->get_remaining_for_project($project_id);
                
                foreach ($remaining as $key => $value)
                {
                    $remaining_pro_id = $value['product_id'];
                    if(!isset($remaining_pro[$remaining_pro_id]))
                    {
                        $remaining_pro[] = $remaining_pro_id;
                        $remaining_pro[$remaining_pro_id] =  $value['quantity'];
                    }
                    else
                    {
                        $remaining_pro[$remaining_pro_id] =  $remaining_pro[$remaining_pro_id] + $value['quantity'];
                    }
                    
                }
                
		//Get user details from database
		$project_data = $this->Hoosk_model->getProjectById($project_id);
	
		if(count($project_data) > 0) {
    			
    		//Load the view
    		$this->data['project'] = $project_data;
                 $this->data['remaining'] = $remaining_pro;
                $this->data['company'] = $this->Hoosk_model->get_enabled_company();
    		$this->data['products'] = $this->Hoosk_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
    		$this->data['customs'] = $this->Hoosk_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
                $this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/project_edit', $this->data);
		}
		else {
		     //$this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/admin/project', 'refresh');
		}
    }
    
    public function edited()
    {
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('po_name', 'Name', 'trim|required');
       
        $this->form_validation->set_rules('po_number', 'Number', 'trim|required');
        $this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        $this->form_validation->set_rules('po_age', 'Age', 'trim|numeric');
        $this->form_validation->set_rules('po_site_name', 'Name', 'trim');
        $this->form_validation->set_rules('po_site_email', 'Email', 'trim');
        $this->form_validation->set_rules('po_office_phone', 'Office Phone', 'trim|min_length[12]');
        $this->form_validation->set_rules('po_cell_phone', 'Cell Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->editProject();
                
        }  else  { 
            if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
            $secret = '6Lcyhm4UAAAAAKORUT3Vs4FnUpPyPBJxbfJKylFX';
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
                $po_name = strip_tags($this->input->post('po_name'));
                $po_number = $this->input->post('po_number');
                $po_manager = $this->input->post('po_manager');
                $po_age = strip_tags($this->input->post('po_age'));
                $po_status = strip_tags($this->input->post('po_status'));
                if($po_status != 1 || isset($_POST['submit']))
                {
                    $po_status1 = 'Pending';
                }
                else
                {
                    $po_status1 = 'Approved';
                }
                $po_state = strip_tags($this->input->post('po_state'));
                $po_site_name = strip_tags($this->input->post('po_site_name'));
                $po_site_email = strip_tags($this->input->post('po_site_email'));
                $po_office_phone = strip_tags($this->input->post('po_office_phone'));
                $po_cell_phone = strip_tags($this->input->post('po_cell_phone'));
                $product_rows = $this->input->post('product_rows');
                $custom_rows = $this->input->post('custom_rows');
                $confirm_project_id = base64_decode($this->uri->segment(4));
                $data = array(
                    'po_number'=>$po_number,
                    'po_name'=>$po_name,
                    'po_company'=>$po_manager,
                    'po_age'=>$po_age,
                    'po_state'=>$po_state,
                    'po_site_contact_name'=>$po_site_name,
                    'po_site_contact_email'=>$po_site_email,
                    'po_site_contact_office_phone'=>$po_office_phone,
                    'po_status'=>$po_status1,
                    'po_site_contact_cell_phone'=>$po_cell_phone,
                    'modified_date'=> date("Y-m-d H:i:s"),
                );
                if($this->Hoosk_model->update_project($data,$confirm_project_id))
                {
                   if(isset($_POST['submit']))
                   {  
                        if($confirm_project_id != '')
                        {
                            for($i=1;$i<=$product_rows;$i++)
                            {
                                $product_check = $this->input->post('po_row'.$i.'_product_id');
                                if(isset($product_check))
                                {
                                    $product_id = $this->input->post('po_row'.$i.'_product_id');
                                    $product_name = $this->input->post('po_row'.$i.'_product');
                                    $product_product_desc = $this->input->post('po_row'.$i.'_product_desc');
                                    $product_product_quntity = $this->input->post('po_row'.$i.'_product_quntity');
                                    $product_order_quntity = $this->input->post('po_row'.$i.'_order_quntity');
                                    $product_order_po_id = $this->input->post('po_row'.$i.'_order_po_id');
                                    $product_order_po_id = isset($product_order_po_id)?$product_order_po_id:'';
                                    $product_product_price = $this->input->post('po_row'.$i.'_product_price');
                                    $product_total = $this->input->post('po_row'.$i.'_total');
                                    $product_comment = $this->input->post('po_row'.$i.'_comment');
                                    $product_order_quntity = isset($product_order_quntity)?$product_order_quntity:0;


                                    if($product_product_quntity >= $product_order_quntity)
                                    {

                                    }
                                    else
                                    {
                                        $product_order_quntity = 0;
                                        $product_total = 0;
                                    }

                                    if($product_order_po_id != '')
                                    {
                                        $data = array(

                                            'quantity'=>$product_product_quntity,
                                            'po_draft_order'=>$product_order_quntity,
                                            'total'=>$product_total,
                                            'comment'=>$product_comment,

                                        );
                                        $this->Hoosk_model->confirm_update_product($data,$product_order_po_id);
                                        $p_id = $product_order_po_id;
                                    }
                                    else
                                    {
                                        $is_product_exits = $this->Hoosk_model->is_product_exist_po($product_id,$confirm_project_id);
                                        if(count($is_product_exits)>0)
                                        {
                                            $data = array(
                                                'quantity'=>$is_product_exits[0]['quantity'] + $product_product_quntity,
                                                'po_draft_order'=>$is_product_exits[0]['po_draft_order'] + $product_order_quntity,
                                                'total'=>$is_product_exits[0]['total'] + $product_total,
                                            );
                                            $p_id = $is_product_exits[0]['po_product_id'];
                                            $this->Hoosk_model->confirm_update_product($data,$is_product_exits[0]['po_product_id']);
                                        }
                                        else
                                        {
                                            $data = array(
                                                'po_id'=>$confirm_project_id,
                                                'product_id'=>$product_id,
                                                'quantity'=>$product_product_quntity,
                                                'po_draft_order'=>$product_order_quntity,
                                                'total'=>$product_total,
                                                'comment'=>$product_comment,
                                            );
                                            $p_id = $this->Hoosk_model->confirm_product($data);

                                        }

                                    }

                                }
                            }
                            //$this->Hoosk_model->delete_custom_related($confirm_project_id);  
                            for($i=1;$i<=$custom_rows;$i++)
                            {
                                $custom_check = $this->input->post('po_row'.$i.'_custom_item');
                                if(isset($custom_check))
                                {
                                    $product_custom_item = $this->input->post('po_row'.$i.'_custom_item');
                                    $product_order_id = $this->input->post('po_row'.$i.'_order_id');
                                    $product_order_id = isset($product_order_id)?$product_order_id:'';
                                    $product_custom_desc = $this->input->post('po_row'.$i.'_custom_desc');
                                    $product_custom_quantity = $this->input->post('po_row'.$i.'_custom_quantity');
                                    $product_custom_price = $this->input->post('po_row'.$i.'_custom_price');
                                    $product_custom_total = $this->input->post('po_row'.$i.'_custom_total');
                                    $product_custom_comment = $this->input->post('po_row'.$i.'_custom_comment');
                                    if($product_order_id != '')
                                    {
                                        $data = array(

                                            'po_custom_quantity'=>$product_custom_quantity,
                                            'total'=>$product_custom_total,
                                            'comment'=>$product_custom_comment,

                                        );
                                        $this->Hoosk_model->confirm_update_order($data,$product_order_id);
                                        $p_id = $product_order_id;
                                    }
                                    else
                                    {
                                        $data = array(

                                            'po_id'=>$confirm_project_id,
                                            'po_custom_title'=>$product_custom_item,
                                            'po_custom_desc'=>$product_custom_desc,
                                            'po_custom_quantity'=>$product_custom_quantity,
                                            'po_custom_price'=>$product_custom_price,
                                            'total'=>$product_custom_total,
                                            'comment'=>$product_custom_comment,

                                        );
                                        $p_id =  $this->Hoosk_model->confirm_custom_product($data);
                                        $data = array(
                                            'order_id'=>$confirm_order_id,
                                            'product_id'=>$p_id,
                                            'quantity'=>$product_custom_quantity,
                                        );
                                        $this->Hoosk_model->confirm_order_products($data);
                                    }
                                }
                            }
                            if($po_status != 1)
                            {
                                // $this->pdfgen($confirm_project_id);
                                 $this->session->set_userdata('pdfId', $confirm_project_id);

                                 //header('Location: ' . base_url() . 'admin/project');
                            }
                            $this->session->set_flashdata('success', 'Project updated successfully.');
                            redirect(BASE_URL.'/admin/project', 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('message', 'Project is not found.');
                            redirect(BASE_URL.'/admin/project', 'refresh');
                        }
                   }
                   else
                   {
                        if($confirm_project_id != '')
                        {
                            $data = array(
                                'po_id'=>$confirm_project_id,
                                'order_status'=>'Approved',
                                'created_date'=> date("Y-m-d H:i:s"),
                            );
                            $confirm_order_id = $this->Hoosk_model->confirm_order($data);


                            //$this->Hoosk_model->delete_product_related($confirm_project_id);     
                            for($i=1;$i<=$product_rows;$i++)
                            {
                                $product_check = $this->input->post('po_row'.$i.'_product_id');
                                if(isset($product_check))
                                {
                                    $product_id = $this->input->post('po_row'.$i.'_product_id');
                                    $product_name = $this->input->post('po_row'.$i.'_product');
                                    $product_product_desc = $this->input->post('po_row'.$i.'_product_desc');
                                    $product_product_quntity = $this->input->post('po_row'.$i.'_product_quntity');
                                    $product_order_quntity = $this->input->post('po_row'.$i.'_order_quntity');
                                    $product_order_po_id = $this->input->post('po_row'.$i.'_order_po_id');
                                    $product_order_po_id = isset($product_order_po_id)?$product_order_po_id:'';
                                    $product_product_price = $this->input->post('po_row'.$i.'_product_price');
                                    $product_total = $this->input->post('po_row'.$i.'_total');
                                    $product_comment = $this->input->post('po_row'.$i.'_comment');
                                    $product_order_quntity = isset($product_order_quntity)?$product_order_quntity:0;


                                    if($product_product_quntity >= $product_order_quntity)
                                    {

                                    }
                                    else
                                    {
                                        $product_order_quntity = 0;
                                        $product_total = 0;
                                    }

                                    if($product_order_po_id != '')
                                    {
                                        $data = array(

                                            'quantity'=>$product_product_quntity,
                                            'po_draft_order'=>0,
                                            'total'=>$product_total,
                                            'comment'=>$product_comment,

                                        );
                                        $this->Hoosk_model->confirm_update_product($data,$product_order_po_id);
                                        $p_id = $product_order_po_id;
                                    }
                                    else
                                    {
                                        $is_product_exits = $this->Hoosk_model->is_product_exist_po($product_id,$confirm_project_id);
                                        if(count($is_product_exits)>0)
                                        {
                                            $data = array(
                                                'quantity'=>$is_product_exits[0]['quantity'] + $product_product_quntity,
                                                'po_draft_order'=>0,
                                                'total'=>$is_product_exits[0]['total'] + $product_total,
                                            );
                                            $p_id = $is_product_exits[0]['po_product_id'];
                                            $this->Hoosk_model->confirm_update_product($data,$is_product_exits[0]['po_product_id']);
                                        }
                                        else
                                        {
                                            $data = array(
                                                'po_id'=>$confirm_project_id,
                                                'product_id'=>$product_id,
                                                'quantity'=>$product_product_quntity,
                                                'po_draft_order'=>0,
                                                'total'=>$product_total,
                                                'comment'=>$product_comment,
                                            );
                                            $p_id = $this->Hoosk_model->confirm_product($data);

                                        }

                                    }
                                    $data = array(
                                        'order_id'=>$confirm_order_id,
                                        'product_id'=>$p_id,
                                        'quantity'=>$product_order_quntity,
                                    );
                                    $this->Hoosk_model->confirm_order_products($data);

                                }
                            }
                            //$this->Hoosk_model->delete_custom_related($confirm_project_id);  
                            for($i=1;$i<=$custom_rows;$i++)
                            {
                                $custom_check = $this->input->post('po_row'.$i.'_custom_item');
                                if(isset($custom_check))
                                {
                                    $product_custom_item = $this->input->post('po_row'.$i.'_custom_item');
                                    $product_order_id = $this->input->post('po_row'.$i.'_order_id');
                                    $product_order_id = isset($product_order_id)?$product_order_id:'';
                                    $product_custom_desc = $this->input->post('po_row'.$i.'_custom_desc');
                                    $product_custom_quantity = $this->input->post('po_row'.$i.'_custom_quantity');
                                    $product_custom_price = $this->input->post('po_row'.$i.'_custom_price');
                                    $product_custom_total = $this->input->post('po_row'.$i.'_custom_total');
                                    $product_custom_comment = $this->input->post('po_row'.$i.'_custom_comment');
                                    if($product_order_id != '')
                                    {
                                        $data = array(

                                            'po_custom_quantity'=>$product_custom_quantity,
                                            'total'=>$product_custom_total,
                                            'comment'=>$product_custom_comment,

                                        );
                                        $this->Hoosk_model->confirm_update_order($data,$product_order_id);
                                        $p_id = $product_order_id;
                                    }
                                    else
                                    {
                                        $data = array(

                                            'po_id'=>$confirm_project_id,
                                            'po_custom_title'=>$product_custom_item,
                                            'po_custom_desc'=>$product_custom_desc,
                                            'po_custom_quantity'=>$product_custom_quantity,
                                            'po_custom_price'=>$product_custom_price,
                                            'total'=>$product_custom_total,
                                            'comment'=>$product_custom_comment,

                                        );
                                        $p_id =  $this->Hoosk_model->confirm_custom_product($data);
                                        $data = array(
                                            'order_id'=>$confirm_order_id,
                                            'product_id'=>$p_id,
                                            'quantity'=>$product_custom_quantity,
                                        );
                                        $this->Hoosk_model->confirm_order_products($data);
                                    }
                                }
                            }
                            if($po_status != 1)
                            {
                                $this->pdfgen($confirm_project_id);
                            }
                            $this->session->set_flashdata('success', 'Project updated successfully.');
                            redirect(BASE_URL.'/admin/project', 'refresh');
                        }
                        else
                        {
                            $this->session->set_flashdata('message', 'Project is not found.');
                            redirect(BASE_URL.'/admin/project', 'refresh');
                        }
                   }
                }
                else
                {
                    $this->session->set_flashdata('message', 'Project is not found.');
                    redirect(BASE_URL.'/admin/project', 'refresh');
                }
            }else
                {
                    $this->session->set_flashdata('message', 'Project Not Added Due to Captcha.');
                    redirect(BASE_URL.'/admin/project', 'refresh');
                }
            }else
            {
                $this->session->set_flashdata('message', 'Project Not Added Due to Captcha.');
                redirect(BASE_URL.'/admin/project', 'refresh');
            }
        }
    }

    public function pdfgen($project_id = '')
    {
        if($project_id == '')
        {
            $project_id = base64_decode($this->uri->segment(4));
        }
        //echo $project_id;
        $project_data = $this->Hoosk_model->getProjectById($project_id,'company');

        if(count($project_data) > 0) 
        {
            //print_r($project_data[0]);exit();
            $project = $project_data[0];
            $company = $this->Hoosk_model->get_enabled_company();
            $products = $this->Hoosk_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
            $customs = $this->Hoosk_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
            $fullhtml = $this->load->view('admin/project_print', '', true);
//            echo $fullhtml;exit();
            $project_block = '';
            $product_grand_total = 0;
            //print_r($products);die;
            foreach ($products as $u1) 
            {

                $ptotal = $u1['quantity'] * $u1['product_unit_price'];

                 $project_block .= "<table class='table row0' data-id='0' style='max-width: 100%;width: 100%;border-collapse: collapse;
    border-spacing: 0;'>
                            <th style='display:table-row-group;'>
                                <tr style='background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;border-color:#a9ceec !important;'>
                                    <th class='col-md-2' style='border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;'>PRODUCT #</th>
                                    <th class='col-md-3' style='border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 25%;text-align: left;font-weight: bold;
    color: #302a75;'> DESCRIPTION</th>
                                    <th class='col-md-2' colspan='2' style='border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 25%;text-align: left;font-weight: bold;
    color: #302a75;'>
                                        <span style='display: inline-flex;vertical-align: middle;'> PROJECT <br>QTY &amp; PRICE</span></th>

                                    <th class='col-md-1' style='border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 10%;text-align: left;font-weight: bold;
    color: #302a75;'>
                                         REMAINING </th>

                                    <th class='col-md-2' style='border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;'>
                                       TOTAL</th>
                                   
                                </tr>
                            </th>
                            <tbody class='breadcrumb' style='background-color:#2b3031 !important;padding: 8px 15px;margin-bottom: 21px;list-style:none;border-radius:4px;display: table-row-group;vertical-align: middle;box-sizing: border-box;'>
                           
                            <tr style='background-color:#ecf0f1 !important; width: 20%;'> 
                                <td style='padding: 10px; margin-left:10px !important;border:none;border-top: 0;' >
                                    <span style='border-width: 2px;box-shadow: none;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 2.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' id='po_row0_product' autocomplete='off' class='form-control productsearch' ondrop='return false;' onpaste='return false;'>".$u1['product_name']."</span>
                                    <div id='suggesstion-box' class='sugg_row0'></div>
                                   
                                </td>
                                <td style='width: 25%; padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                    <span style='border-width: 2px;box-shadow: none;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' id='po_row0_product_desc' autocomplete='off' class='form-control productsearch' ondrop='return false;' onpaste='return false;'>".$u1['product_desc']."</span>
                                    <div id='suggesstion-box' class='sugg_desc_row0'></div>
                                </td>
                                <td style='width: 10%;padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                    <span style='border-width: 2px;box-shadow: none;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' name='po_row0_product_quntity' value='' id='po_row0_product_quntity' class='form-control projectquantity numberic' ondrop='return false;' onpaste='return false;'>".$u1['quantity']."</span>
                                </td>
                                <td style='width: 15%;padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                    <div class='input-group'>
    <span style='border-width: 2px;box-shadow: none;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' id='po_row0_product_price1' class='form-control productprice1' readonly='readonly' ondrop='return false;' onpaste='return false;'>$ ".number_format($u1['product_unit_price'],2)."</span>
</div>                                    
                                </td>

                                <td style='width: 10%;padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                    <span style='border-width: 2px;box-shadow: none;
width: 100px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' value='' id='po_row0_remaining' class='form-control row_remaining' readonly='readonly' ondrop='return false;' onpaste='return false;'>".$u1['product_available_quantity']."</span>
                                </td>

                                <td style='width: 20%; padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                <div class='input-group'>
    <span style='border-width: 2px;box-shadow: none;
transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' id='po_row0_total1' class='form-control row_total1' readonly='readonly' ondrop='return false;' onpaste='return false;'>$ ".number_format($ptotal,2)."</label>
</div>                                </td>
                               
                            </tr>
                            <tr style='background-color:#ecf0f1 !important;padding: 20px 20px!important;'>
                                <td colspan='8' style='padding: 10px; margin-left:10px !important;border:none;border-top: 0;'>
                                    <div class='input-group'><span class='input-group-addon' style='border-right: 0;border-bottom-right-radius: 0;
    border-top-right-radius: 0;padding: 9px 15px;font-size: 15px;font-weight: 400;line-height: 1;color: #2c3e50;text-align: center;background-color: #ecf0f1;border: 1px solid #dce4ec;
    border-radius: 4px;width: 1%;white-space: nowrap;vertical-align: middle;display: table-cell;font-size: 20px;'>PROJECT NOTES:</span>
    <span style='border-width: 2px;box-shadow: none;
width: 400px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;' cols='50' rows='1' id='po_row0_comment' class='form-control'>".$u1['comment']."</span>
</div>                                </td>
                            </tr>
                            </tbody>
                        </table>";          
                        $product_grand_total = $product_grand_total + $ptotal;
                        $remaining_total = $remaining_total + ($u1['product_available_quantity'] * $u1['product_unit_price']);
            }
            $custom_block = ''; 
            $custom_grand_total = 0;
            foreach ($customs as $u2) 
            {
                    $custom_block .='<table class="table row0" data-id="0" style="max-width: 140%;width: 140%;border-collapse: collapse;
    border-spacing: 0;">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;height:500px;">
                                    <th class="col-md-3" style="border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 16.33333%;text-align: left;font-weight: bold;
    color: #302a75;">ITEM</th>
                                    <th class="col-md-3" style="border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;"> DESCRIPTION</th>
                                  <th class="col-md-2" style="border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;">QTY</th>
                                   <th class="col-md-2" style="border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;">PRICE</th>
                                   
                                      
                                      <th class="col-md-2" style="border: none;vertical-align: bottom;padding: 8px;line-height: 1.42857143;position: static;float: none;display: table-cell;width: 20%;text-align: left;font-weight: bold;
    color: #302a75;">TOTAL</th>
                                    
                                </tr>
                            </thead>
                            <tbody class="breadcrumb" style="height:1500px;padding: 8px 15px;margin-bottom: 21px;list-style: none;background-color: #e9eff1;border-radius: 4px;display: table-row-group;vertical-align: middle;">
                           
                            <tr style="background-color:#ecf0f1 !important;">
                                <td style="padding: 10px; margin-left:10px !important;">
                                    <span style="border-width: 2px;box-shadow: none;
width: 255px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" id="po_row0_product" autocomplete="off" class="form-control productsearch" ondrop="return false;" onpaste="return false;">'.$u2["po_custom_title"].'</span>
                                    <div id="suggesstion-box" class="sugg_row0"></div>
                                   
                                </td>
                                <td style="padding: 10px; margin-left:10px !important;">
                                    <span style="border-width: 2px;box-shadow: none;
width: 327px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" id="po_row0_product_desc" autocomplete="off" class="form-control productsearch" ondrop="return false;" onpaste="return false;">'. $u2['po_custom_desc'].'</span>
                                    <div id="suggesstion-box" class="sugg_desc_row0"></div>
                                </td>
                                <td style="padding: 10px; margin-left:10px !important;">
                                
                                    <span style="border-width: 2px;box-shadow: none;
width: 250px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" name="po_row0_product_quntity" value="" id="po_row0_product_quntity" class="form-control projectquantity numberic" ondrop="return false;" onpaste="return false;">'. $u2['po_custom_quantity'].'</span>
                                 <div id="suggesstion-box" class="sugg_row0"></div>
                              
               
                                </td>
                              
                               <td style="padding: 10px; margin-left:10px !important;">
                               <div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;font-size: 15px;font-weight: 400;line-height: 1;color: #2c3e50;text-align: center;background-color: #ecf0f1;border: 1px solid #dce4ec;
    border-radius: 4px;width: 1%;white-space: nowrap;vertical-align: middle;display: table-cell;font-size: 20px;">$</span>
    <span style="border-width: 2px;box-shadow: none;
width: 195px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" id="po_row0_total1" class="form-control row_total1" readonly="readonly" ondrop="return false;" onpaste="return false;">'. $u2['po_custom_price'].'</span>
</div>                                </td>

 <td style="padding: 10px; margin-left:10px !important;">
                               <div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;font-size: 15px;font-weight: 400;line-height: 1;color: #2c3e50;text-align: center;background-color: #ecf0f1;border: 1px solid #dce4ec;
    border-radius: 4px;width: 1%;white-space: nowrap;vertical-align: middle;display: table-cell;font-size: 20px;">$</span>
    <span style="border-width: 2px;box-shadow: none;
width: 195px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" id="po_row0_total1" class="form-control row_total1" readonly="readonly" ondrop="return false;" onpaste="return false;">'. $u2['total'].'</span>
</div>                                </td>
                               

                                             
                               
                            </tr>
                            <tr style="background-color:#ecf0f1 !important;">
                                <td colspan="5" style="padding: 10px; margin-left:10px !important;">
                                    <div class="input-group"><span class="input-group-addon" style="border-right: 0;border-bottom-right-radius: 0;
    border-top-right-radius: 0;padding: 9px 15px;font-size: 15px;font-weight: 400;line-height: 1;color: #2c3e50;text-align: center;background-color: #ecf0f1;border: 1px solid #dce4ec;
    border-radius: 4px;width: 1%;white-space: nowrap;vertical-align: middle;display: table-cell;font-size: 20px;">PROJECT NOTES:</span>
    <span name="po_row0_comment" style="border-width: 2px;box-shadow: none;
width: 400px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 20px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;" cols="50" rows="1" id="po_row0_comment" class="form-control">'.$u2['comment'].'</label>
</div>                                </td>
                            </tr>
                            </tbody>
                        </table>'; 
                    $custom_grand_total = $custom_grand_total + $u2['total'] ;

            }
            $fullhtml = str_replace("%po_name%",$project['po_name'],$fullhtml);
            $fullhtml = str_replace("%po_number%",$project['po_number'],$fullhtml);
            $fullhtml = str_replace("%company_name%",$project['company_name'],$fullhtml);
            $fullhtml = str_replace("%address%",$project['address'],$fullhtml);
            $fullhtml = str_replace("%city%",$project['city'],$fullhtml);
            $fullhtml = str_replace("%po_age%",$project['po_age'],$fullhtml);
            $fullhtml = str_replace("%po_state%",$project['po_state'],$fullhtml);
            $fullhtml = str_replace("%po_site_contact_name%",$project['po_site_contact_name'],$fullhtml);
            $fullhtml = str_replace("%po_site_contact_email%",$project['po_site_contact_email'],$fullhtml);
            $fullhtml = str_replace("%po_site_contact_office_phone%",$project['po_site_contact_office_phone'],$fullhtml);
            $fullhtml = str_replace("%po_site_contact_cell_phone%",$project['po_site_contact_cell_phone'],$fullhtml);
            $fullhtml = str_replace("%products_block%",$project_block,$fullhtml);
            $fullhtml = str_replace("%customs%",$custom_block,$fullhtml);
            $fullhtml = str_replace("%product_total%",number_format($product_grand_total,2),$fullhtml);
            $fullhtml = str_replace("%custom_total%",$custom_grand_total,$fullhtml);
            $fullhtml = str_replace("%remaining_total%",number_format($remaining_total,2),$fullhtml);
            $fullhtml = str_replace("%t_total%",$custom_grand_total + $product_grand_total ,$fullhtml);
            $filename = 'PO-'.$project['company_name'].'-'.$project['po_name'];
           
            $this->generate_pdf($fullhtml);
            // //ini_set('memory_limit','32M');
            // $this->load->library('pdf');
            // $pdf = $this->pdf->load();
            // $pdf->SetFooter('KCES'.'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
            // $pdf->allow_charset_conversion=true;
            // $pdf->charset_in='UTF-8';
            // //$fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            // // $fullhtml = iconv("cp1252", "UTF-8", $fullhtml);
            // //$fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            // //$fullhtml = "<h1>asdsdadsa</h1>";
            // //echo $fullhtml;exit();
            // // echo phpinfo();exit();
            // $pdf->WriteHTML($fullhtml); // write the HTML into the PDF
            // // $this->pdf->mpdf->WriteHTML($fullhtml);
            // $pdf->Output($filename, 'F'); // save to file because we can
            return;
        }
        else {
             $this->session->set_flashdata('message', 'Record with specified id does not exist');
             redirect(BASE_URL.'/admin/project', 'refresh');
        }
        
        
    }
    public function generate_pdf($html)
        {
            // echo "hi";die;
            $this->load->library('pdf');
            $pdf = $this->pdf->load();
            $pdf->SetFooter('KCES'.'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
            // $pdf->allow_charset_conversion=true;
            // $pdf->charset_in='windows-1252';
            // $fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            // $fullhtml = iconv("cp1252", "UTF-8", $fullhtml);
            // $fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            //echo $html;die;
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($filename, 'D'); // save to file because we can


            // echo '<script type="text/javascript">console.log("success");';
            // echo 'window.location.href="'.BASE_URL.'/admin/project";';

            // echo '</script>';
            //redirect(BASE_URL.'/admin/project', 'refresh');
            
            return;
        }
    function delete()
    {
            Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
            if($this->input->post('deleteid')):
                    $this->Hoosk_model->removeProject($this->input->post('deleteid'));
                    $this->session->set_flashdata('success', 'Project deleted successfully.');
                    redirect(BASE_URL.'/admin/project');
            else:
                    $delete_id = base64_decode($this->uri->segment(4));
                    $this->data['form']=$this->Hoosk_model->getProjectById($delete_id);
                    $this->load->view('admin/project_delete.php', $this->data );
            endif;
    }
    function view()
    {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $project_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$project_data = $this->Hoosk_model->getProjectById($project_id);
	
		if(count($project_data) > 0) {
    			
    		//Load the view
    		$this->data['project'] = $project_data;
                $this->data['company'] = $this->Hoosk_model->get_enabled_company();
    		$this->data['products'] = $this->Hoosk_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
    		$this->data['customs'] = $this->Hoosk_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
                $this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/project_view', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/admin/project', 'refresh');
		}
    }
    function change_status()
    {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $projectID = $this->input->post('projectid');

                    $data = array(
                        'po_status' => $status,
                    );
                    
                    if ($this->Hoosk_model->change_project_status($data, (int) $projectID)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/admin/project', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/admin/project', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/admin/project', 'refresh');
                }
    }
}
