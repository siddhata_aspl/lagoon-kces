<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		define("HOOSK_ADMIN",1);
		$this->load->model('Hoosk_model');
		$this->load->helper(array('admincontrol', 'url', 'form'));
		$this->load->library('session');
		define ('LANG', $this->Hoosk_model->getLang());
		$this->lang->load('admin', LANG);
		//Define what page we are on for nav
                $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
		$this->data['current'] = $this->uri->segment(2);
		define ('SITE_NAME', $this->Hoosk_model->getSiteName());
		define('THEME', $this->Hoosk_model->getTheme());
                define('DOMAIN', $this->Hoosk_model->getDomain());
                define('API_KEY', $this->Hoosk_model->getAPIKey());
		define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
		define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
	}
        
        public function generate_pdf()
        {
            // echo "hi";die;
            $this->load->library('pdf');
            $pdf = $this->pdf->load();
            $pdf->SetFooter('KCES'.'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
            // $pdf->allow_charset_conversion=true;
            // $pdf->charset_in='windows-1252';
            // $fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            // $fullhtml = iconv("cp1252", "UTF-8", $fullhtml);
            // $fullhtml = mb_convert_encoding($fullhtml, 'UTF-8', 'UTF-8');
            $pdf->WriteHTML('<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/bootstrap.min.css" rel="stylesheet"  media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/sb-admin.css" rel="stylesheet" media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/jquery.fancybox-1.3.4.css" rel="stylesheet" media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/jquery.typeahead.css" rel="stylesheet" media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/dataTables.bootstrap.min.css" rel="stylesheet" media="print">
<link href="http://kcelectricalsupply.dev.lagoon.com/theme/admin/css/dataTables.fontAwesome.css" rel="stylesheet" media="print">
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Project
                    </h1>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
<!--                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-pencil fa-fw"></i>
                                 Project Summery
                            </h3>
                        </div>-->
                        <div class="panel-body">                    
                            <h4 style="color: #302a75;font-weight: bold;">Project Information</h4>
                            <div class="form-group">        
                                <label class="control-label" for="name">Name</label>
                                <div class="controls">
                                           Building
                                </div> <!-- /controls -->               
                            </div> <!-- /form-group -->
                            <div class="col-md-12 nopadding">
                                <div class="col-md-5 nopadding" style="width: 40%;display: inline-block;">              
                                    <div class="form-group">                                        
                                        <label class="control-label" for="name">Number</span></label>
                                        <div class="controls">
                                            9138881111
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-7" style="padding-right: 0px;width: 60%;display: inline-block;">              
                                    <div class="form-group">    
                                        <label class="control-label" for="name">Manager</label>
                                        <div class="controls">
                                           DL_TEST4
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>   
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">        
                                        <label class="control-label" for="name">Address</label>
                                        <div class="controls">
                                            asdasdad
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-4" style="padding-right: 0px">              
                                    <div class="form-group">        
                                        <label class="control-label" for="name">City</label>
                                        <div class="controls">
                                            Kansas City
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-2" style="padding-right: 0px">              
                                    <div class="form-group">        
                                        <label class="control-label" for="name">Age</label>
                                        <div class="controls">
                                            0
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>

                            <br>
                            <hr>
                            
                            <h4 style="color: blue;">Project Site Contact</h4>
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">
                                        <label class="control-label" for="po_site_name">Name</label>
                                        <div class="controls">
                                            asdasdad
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">              
                                    <div class="form-group">        
                                        <label class="control-label" for="po_site_email">Email</label>
                                        <div class="controls">
                                            john@mailinator.com
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">        
                                        <label class="control-label" for="po_office_phone">Office Phone</label>
                                        <div class="controls">
                                            9135552525
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">              
                                    <div class="form-group">        
                                        <label class="control-label" for="po_site_email">Cell Phone</label>
                                        <div class="controls">
                                            9135652626
                                        </div> <!-- /controls -->               
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>
                            <br>
                            <hr>
                            <pagebreak></pagebreak>
                            <h4 style="color: blue;">Projects</h4>
                            <div class="col-md-12 nopadding" id="product_data">
                                <div id="copy_product">
                                    <table class="table row1" data-id="1">
                              <thead>
                               <tr class="info">
                                    <th class="col-md-1">PRODUCT #</th>
                                    <th class="col-md-2"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> PROJECT QTY & PRICE</th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart."></i> TOTAL</th>
                                    
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                            
                            <tr>
                                <td>
                                    Breakers 2
                                </td>
                                <td>
                                    Description of Breakers 2
                                </td>
                                <td>
                                    12
                                </td>
                                <td>
                                   1200
                                </td>
                                <td>
                                    0
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <b>Comment </b>: hgkdhgkhdkghdkgsdkgkdshgksdfgkskgskdhgkdgksdhgksdgkskgkghkshgksdhgkshkgskg
                                </td>
                            </tr>
                            </tbody>
                        </table>
                                </div>
                            </div>
                            <br>
                            <br>
                            <hr>
                            <h4 style="color: blue;">Custom Items</h4>
                            <div class="col-md-12 nopadding" id="custom_data">
                                <div id="custom_copy" class="hidden">

                                </div>
                                <div id="set_copy">
                                    <table class="table customrow1">
                            <thead>
                                <tr class="info">
                                    <th class="col-md-2">ITEM</th>
                                    <th class="col-md-6"> DESCRIPTION</th>
                                    <th class="col-md-1">QTY</th>
                                    <th class="col-md-1">PRICE</th>
                                    <th class="col-md-1">TOTAL</th>
                                </tr>
                            </thead>

                            
                            <tbody class="breadcrumb">
                                
                                <tr>
                                    <td>
                                        bulb
                                    </td>
                                    <td>
                                        hfahshashgkahsghahgiuashgiuahighasighiauehgikjhiuth
                                    </td>
                                    <td>
                                        5
                                    </td>
                                    <td>
                                        45
                                    </td>
                                    <td>
                                        225
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td colspan="5">
                                            <b>Comment </b>: gjlkdjgjgldgljlgjosjgoieorgldnbljerynblseoitp
                                    </td>
                                </tr>
                                 
                            </tbody>
                            </table>
                                </div>

                            </div>
                            <div class="col-md-12 nopadding">
                                <div class="table-responsive col-md-offset-6">
                                    <table class="table table-striped" id="invoice"  style="text-align: right;">
                                        <tr>
                                            <th>PRODUCTS TOTAL $</th>
                                            <td class="total_show">0</td>
                                        </tr>
                                        <tr>
                                            <th>CUSTOM ITEM TOTAL $</th>
                                            <td class="total_custom_show">225</td>
                                        </tr>
                                        <tr>
                                            <th>PROJECT OVERALL  ITEM TOTAL $</th>
                                            <td class="total_custom_show">225</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            </div>



                    </div>
                </div>
            </div>
        </div>
    <style>
    #country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
    #country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
    #country-list li:hover{background:#ece3d2;cursor: pointer;}
    </style>
    </div>
</div>'); // write the HTML into the PDF
            $pdf->Output($filename, 'D'); // save to file because we can
            return ;
        }
        public function index()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/users/';
                $config['total_rows'] = $this->Hoosk_model->countUsers();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

		//Get users from database
		$this->data['users'] = $this->Hoosk_model->getUsers($result_per_page, $this->uri->segment(3));

		//Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/users', $this->data);
	}
	public function get_all_data()
        {
                //echo "hi";exit();
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        		//Get users from database
        		echo $this->Hoosk_model->get_datatable_data('hoosk_user',$_REQUEST);
        }
        

	public function addUser()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		//Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/user_new', $this->data);
	}

	public function confirm()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//                echo $_FILES['file_upload']['name'];
//                exit();
		//Load the form validation library
		$this->load->library('form_validation');
		//Set validation rules
		        $this->form_validation->set_rules('firstname', 'firstname', 'trim|alpha_dash');
                $this->form_validation->set_rules('lastname', 'lastname', 'trim|alpha_dash');
                $this->form_validation->set_rules('username', 'Username', 'trim|alpha_dash|required|is_unique[hoosk_user.userName]');
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_user.email]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_user.email]|is_unique[hoosk_company.email]|is_unique[employees.emp_email]');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
//		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[32]');
//		$this->form_validation->set_rules('con_password', 'confirm password','trim|required|matches[password]');
                // if (empty($_FILES['file_upload']['name']))
                // {
                //     $this->form_validation->set_rules('file_upload', 'profile picture', 'required');
                // }


		if($this->form_validation->run() == FALSE) {
			//Validation failed
			$this->addUser();
		}  else  {
			//Validation passed
			//Add the user
			if (!empty($_FILES['file_upload']['name']))
			{
                        $config['upload_path']          = './uploads/users/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['max_size']             = 1000;
                        $config['max_width']            = 1024;
                        $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        $this->upload->do_upload('file_upload');
                        $upload_data = array('upload_data' => $this->upload->data());
                        $filename = $upload_data['upload_data']['file_name'];
			}
			else
			{
			            $filename = '';
			}
                        
                            
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $this->Hoosk_model->createUser($filename);
                            
                                        $email= $this->input->post('email');
                                        $this->load->helper('string');
                                        $rs= random_string('alnum', 12);
                                        $data = array(
                                        'rs' => $rs
                                        );
                                        $forgotmail_user = $this->Hoosk_model->update_user_for_forgotmail($data,$email);


                                        //now we will send an email
                                        $config['protocol'] = 'sendmail';
                                        $config['mailpath'] = '/usr/sbin/sendmail';
                                        $config['charset'] = 'iso-8859-1';
                                        $config['wordwrap'] = TRUE;
                                        $mail_template = $this->Hoosk_model->getmail_template(1);

                                        if(isset($mail_template[0]['email_template_title']))
                                        {   
                                            $link= "\r\n".BASE_URL.'/user/register/'.$rs;
                                            $message = $mail_template[0]['email_template_desc'];
                                            $subject = $mail_template[0]['email_template_subject'];
                                            $message = str_replace("%firstname%",$forgotmail_user[0]['User_firstname'],$message);
                                            $message = str_replace("%lastname%",$forgotmail_user[0]['User_lastname'],$message);
                                            $message = str_replace("%email%",$forgotmail_user[0]['email'],$message);
                                            $message = str_replace("%link%",$link,$message);
                                             $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
                                        }
                            
                            $this->session->set_flashdata('success', 'User Is Successfully Added.');
                            //Return to user list
                            redirect(BASE_URL.'/admin/users', 'refresh');
                        // }
                        // else
                        // {
                        //     $this->form_validation->set_rules('file_upload', 'profile picture', 'callback_file_upload_error');
                        //     $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                        //     if($this->form_validation->run() == FALSE) {
                        //         $this->addUser();
                        //     }
                        // }
			
	  	}
	}
        
        
        public function file_upload_error()
        {
            return FALSE;
        }
        
        
        public function change_status()
        {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $userID = $this->input->post('userid');

                    $data = array(
                        'user_status' => $status,
                    );
                    
                    if ($this->Hoosk_model->change_status($data, (int) $userID)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/admin/users', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/admin/users', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/admin/users', 'refresh');
                }
        }
        

        public function editUser()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $user_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$user_data = $this->Hoosk_model->getUser($user_id);
	
		if(count($user_data) > 0) {
    			
    		//Load the view
    		$this->data['users'] = $user_data;
    		
    		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/user_edit', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
             redirect(BASE_URL.'/admin/users', 'refresh');
		}
	}

	public function edited()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		//Load the form validation library
		$this->load->library('form_validation');
		//Set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
                $this->form_validation->set_rules('firstname', 'First name', 'trim|alpha_dash');
                $this->form_validation->set_rules('lastname', 'Last name', 'trim|alpha_dash');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[12]');
                $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
//		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[32]');
//		$this->form_validation->set_rules('con_password', 'confirm password','trim|required|matches[password]');
                
		if($this->form_validation->run() == FALSE) {
			//Validation failed
			$this->editUser();
		}  else  {
                        //usre want to change profile picture
                        if (!empty($_FILES['file_upload']['name']))
                        {
                            $config['upload_path']          = './uploads/users/';
                            $config['allowed_types']        = 'gif|jpg|png';
                            $config['max_size']             = 1000;
                            $config['max_width']            = 1024;
                            $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if ($this->upload->do_upload('file_upload'))
                            {
                                $upload_data = array('upload_data' => $this->upload->data());
                                $this->Hoosk_model->updateUser(base64_decode($this->uri->segment(4)),$upload_data['upload_data']['file_name']);
                                $this->session->set_flashdata('success', 'User Is Successfully Updated.');
                                redirect(BASE_URL.'/admin/users', 'refresh');
                            }
                            else
                            {
                                $this->form_validation->set_rules('file_upload', 'profile picture', 'callback_file_upload_error');
                                $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                                if($this->form_validation->run() == FALSE) {
                                    $this->editUser();
                                }
                            }
                            //Validation passed
                            //Update the user
                            
                        }
                        else
                        {
                            //Validation passed
                            //Update the user
                            $this->Hoosk_model->updateUser(base64_decode($this->uri->segment(4)));
                            $this->session->set_flashdata('success', 'User Is Successfully Updated.');
                            redirect(BASE_URL.'/admin/users', 'refresh');
                        }
			//Return to user list
			
	  	}
	}


		function delete()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		if($this->input->post('deleteid')):
			$this->Hoosk_model->removeUser($this->input->post('deleteid'));
			redirect(BASE_URL.'/admin/users');
		else:
                        $delete_id = base64_decode($this->uri->segment(4));
			$this->data['form']=$this->Hoosk_model->getUser($delete_id);
			$this->load->view('admin/user_delete.php', $this->data );
		endif;
	}

	/************** Forgotten Password Resets **************/

	public function forgot()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
		if ($this->form_validation->run() == FALSE)
		{
			$this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
			$this->data['footer'] = $this->load->view('admin/footer', '', true);
			$this->load->view('admin/email_check', $this->data);
		}
		else
		{
			$email= $this->input->post('email');
			$this->load->helper('string');
			$rs= random_string('alnum', 12);
			$data = array(
			'rs' => $rs
			);
                        $forgotmail_user = $this->Hoosk_model->check_admin_for_mail($data,$email);
			
            
			//now we will send an email
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$forgotmail_template = $this->Hoosk_model->getmail_template(2);
			
			if(isset($forgotmail_template[0]['email_template_title']))
			{   
                            $link= "\r\n".BASE_URL.'/user/reset/'.$rs;
                            $message = $forgotmail_template[0]['email_template_desc'];
                            $subject = $forgotmail_template[0]['email_template_subject'];
                            $message = str_replace("%firstname%",$forgotmail_user[0]['User_firstname'],$message);
                            $message = str_replace("%lastname%",$forgotmail_user[0]['User_lastname'],$message);
                            $message = str_replace("%email%",$forgotmail_user[0]['email'],$message);
                            $message = str_replace("%link%",$link,$message);
                             $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                //             $this->load->library('email', $config);

                //             $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                //             $this->email->to($email);

                //             $this->email->subject($subject);
                //             $this->email->message($message );
                // //			$this->email->subject($this->lang->line('email_reset_subject'));
                // //			$this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                //             $this->email->send();

                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
                            $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                            $this->data['footer'] = $this->load->view('admin/footer', '', true);
                            $this->load->view('admin/check', $this->data);
			}
			else
			{
			    $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
    			$this->data['footer'] = $this->load->view('admin/footer', '', true);
    			$this->load->view('admin/check', $this->data);
			}
		}
 }

	public function email_check($str)
	{
		$query = $this->db->get_where('admin', array('email' => $str), 1);
		if ($query->num_rows()== 1)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('email_check', $this->lang->line('email_check'));
			return false;
		}
	}
        public function newuser()
        {
                $rs = $this->uri->segment(3);
		$query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                if ($query->num_rows() == 0)
                {
                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                $this->load->view('admin/error1', $this->data);

                }
                else
                {
                                $this->load->database();
                                $this->load->helper('url');
                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                                $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                                if ($this->form_validation->run() == FALSE)
                                {
                                        echo form_open();
                                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                        $this->load->view('admin/newpassform', $this->data);
                                }
                                else
                                {
                                        $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                                        if ($query->num_rows() == 0)
                                        {
                                                show_error('Sorry!!! Invalid Request!');
                                        }
                                        else
                                        {
                                                $data = array(
                                                'password' => base64_encode($this->input->post('password')),
                                                'rs' => '',
                                                'user_status'=>'Enabled',
                                                );
                                                $where=$this->db->where('rs', $rs);
                                                $where->update('hoosk_user',$data);
                                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                                $this->load->view('admin/newpass', $this->data);
                                        }
                                }
                        }
                }
        

	public function getPassword()
	{
		$rs = $this->uri->segment(3);
		$query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);

                if ($query->num_rows() == 0)
                {
                            $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                            $this->data['footer'] = $this->load->view('admin/footer', '', true);
                            $this->load->view('admin/error', $this->data);

                }
                else
                {
                                $this->load->database();
                                $this->load->helper('url');
                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                                $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                                if ($this->form_validation->run() == FALSE)
                                {
                                        echo form_open();
                                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                        $this->load->view('admin/resetform', $this->data);
                                }
                                else
                                {
                                        $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                                        if ($query->num_rows() == 0)
                                        {
                                                show_error('Sorry!!! Invalid Request!');
                                        }
                                        else
                                        {
                                                $data = array(
                                                'password' => base64_encode($this->input->post('password')),
                                                'rs' => ''
                                                );
                                                $where=$this->db->where('rs', $rs);
                                                $where->update('hoosk_user',$data);
                                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                                $this->load->view('admin/reset', $this->data);
                                        }
                                }
		}
	}

}
