<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		define("HOOSK_ADMIN",1);
		$this->load->model('Hoosk_model');
		$this->load->helper(array('admincontrol', 'url', 'form'));
		$this->load->library('session');
		define ('LANG', $this->Hoosk_model->getLang());
		$this->lang->load('admin', LANG);
		//Define what page we are on for nav
		$this->data['current'] = $this->uri->segment(2);
                $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
		define ('SITE_NAME', $this->Hoosk_model->getSiteName());
		define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
		define('THEME', $this->Hoosk_model->getTheme());
		define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
	}
        

        public function index()
	{
//            echo $this->uri->segment(1);die();
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/banners/';
                $config['total_rows'] = $this->Hoosk_model->countBanners();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

		//Get banners from database
		$this->data['banners'] = $this->Hoosk_model->getBanners($result_per_page, $this->uri->segment(3));

		//Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/banners', $this->data);
	}
        
        public function get_all_data()
        {
                //echo "hi";exit();
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		        echo $this->Hoosk_model->get_banner_datatable_data('banners',$_REQUEST);
                
        }

	public function addBanners()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		//Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/banner_new', $this->data);
	}

	
    public function confirm()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//                echo $_FILES['file_upload']['name'];
//                exit();
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('title', 'Banner title', 'trim');
                
                if (empty($_FILES['file_upload']['name']))
                {
                    $this->form_validation->set_rules('file_upload', 'Banner image', 'required');
                }


        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->addBanners();
        }  else  {
                    //echo "hii";die();
            //Validation passed
            //Add the user
                        $config['upload_path']          = './uploads/banners/';
                        $config['allowed_types']        = 'gif|jpg|png';
//                        $config['max_size']             = 1000;
//                        $config['max_width']            = 1024;
//                        $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('file_upload'))
                        {
                            $upload_data = array('upload_data' => $this->upload->data());
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $this->Hoosk_model->createBanners($upload_data['upload_data']['file_name']);
                            $this->session->set_flashdata('success', 'Banners Is Successfully Added.');
                            //Return to user list
                            redirect(BASE_URL.'/admin/banners', 'refresh');
                        }
                        else
                        {
                            $this->form_validation->set_rules('file_upload', 'Banner Image', 'callback_file_upload_error');
                            $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                            if($this->form_validation->run() == FALSE) {
                                $this->addBanners();
                            }
                        }
            
        }
    }
        
        
        public function file_upload_error()
        {
            return FALSE;
        }
        
        
        public function change_status()
        {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $userID = $this->input->post('bannerid');

                    $data = array(
                        'banner_status' => $status,
                    );
                    
                    if ($this->Hoosk_model->changebanner_status($data, (int) $userID)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/admin/banners', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/admin/banners', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/admin/banners', 'refresh');
                }
        }
        

        public function editBanner()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $banner_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$banner_data = $this->Hoosk_model->getBanner($banner_id);
		
		if(count($banner_data) > 0) {
		
    		$this->data['banner'] = $banner_data;
    		
    		//Load the view
    		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/banner_edit', $this->data);
		}
		else {
		    $this->session->set_flashdata('message', 'Record with specified id does not exist');
            redirect(BASE_URL.'/admin/banners', 'refresh');
		}
	}

	public function edited()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        //$this->form_validation->set_rules('title', 'Banner title', 'required');
                
//      if($this->form_validation->run() == FALSE) {
//          //Validation failed
//          $this->editBanner();
//      }  else  {
                        //usre want to change profile picture
                        if (!empty($_FILES['file_upload']['name']))
                        {
                            $config['upload_path']          = './uploads/banners/';
                            $config['allowed_types']        = 'gif|jpg|png';
//                            $config['max_size']             = 1000;
//                            $config['max_width']            = 1024;
//                            $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if ($this->upload->do_upload('file_upload'))
                            {
                                $upload_data = array('upload_data' => $this->upload->data());
                                $this->Hoosk_model->updateBanner(base64_decode($this->uri->segment(4)),$upload_data['upload_data']['file_name']);
                                $this->session->set_flashdata('success', 'Banners Is Successfully Updated.');
                                redirect(BASE_URL.'/admin/banners', 'refresh');
                            }
                            else
                            {
                                $this->form_validation->set_rules('file_upload', 'Banner Image', 'callback_file_upload_error');
                                $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                                if($this->form_validation->run() == FALSE) {
                                    $this->editBanner();
                                }
                            }
                            //Validation passed
                            //Update the user
                            
                        }
                        else
                        {
                            //Validation passed
                            //Update the user
                            $this->Hoosk_model->updateBanner(base64_decode($this->uri->segment(4)));
                            $this->session->set_flashdata('success', 'Banners Is Successfully Updated.');
                            redirect(BASE_URL.'/admin/banners', 'refresh');
                        }
            //Return to user list
            
        //}
    }


	function delete()
	{
            
        //echo "hii";die();
		// Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		// if($this->input->post('deleteid')):
		// 	$this->Hoosk_model->removeBanners($this->input->post('deleteid'));
		// 	redirect(BASE_URL.'/admin/banners');
		// else:
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        if($this->input->post('deleteid')):
            $this->Hoosk_model->removeBanners($this->input->post('deleteid'));
            redirect(BASE_URL.'/admin/banners');
        else:
            $delete_id = base64_decode($this->uri->segment(4));
			$this->data['form']=$this->Hoosk_model->getBanner($delete_id);
			$this->load->view('admin/banner_delete.php', $this->data );
		endif;
	}

	/************** Forgotten Password Resets **************/

	


	

}
