<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_category extends CI_Controller {

    function __construct()
    {
            
        parent::__construct();
        define("HOOSK_ADMIN",1);
        $this->load->model('Hoosk_model');
        $this->load->model('Product_category_model');
        $this->load->helper(array('admincontrol', 'url', 'form'));
        $this->load->library('session');
        define ('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('admin', LANG);
                
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define ('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME', $this->Hoosk_model->getTheme());
        define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
          //echo "hiii";die();      
    }
        

        public function index()
    {
//            echo $this->uri->segment(1);die();
            
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Hoosk_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/product_category/';
                $config['total_rows'] = $this->Product_category_model->countproduct_category();//echo "hii";exit();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

        //Get banners from database
        $this->data['product_category'] = $this->Product_category_model->getproduct_category($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/product_category', $this->data);
    }
        
        public function get_all_data()
        {
                //echo "hi";exit();
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                echo $this->Product_category_model->get_product_category_datatable_data('product_category',$_REQUEST);
                
        }

    public function addproduct_category()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/product_category_new', $this->data);
    }

    public function confirm()
    {
           
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//                echo $_FILES['file_upload']['name'];
//                exit();
        //Load the form validation library
        $this->load->library('form_validation'); 
        //Set validation rules
        $this->form_validation->set_rules('category_name', 'Product category name', 'trim|required');
                
                if (empty($_FILES['file_upload']['name']))
                {
                    $this->form_validation->set_rules('file_upload', 'Product category image', 'required');
                }


        if($this->form_validation->run() == FALSE) {
            //Validation failed
                    
            $this->addproduct_category();
        }  else  {
            //Validation passed
            //Add the user
                        $config['upload_path']          = './uploads/products/';
                        $config['allowed_types']        = 'gif|jpg|png';
                        $config['max_size']             = 1000;
                        $config['max_width']            = 1024;
                        $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload('file_upload'))
                        {
                            $upload_data = array('upload_data' => $this->upload->data());
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $this->Product_category_model->createproduct_category($upload_data['upload_data']['file_name']);
                            $this->session->set_flashdata('success', 'Product Category Is Successfully Added.');
                            //Return to user list
                            redirect(BASE_URL.'/admin/product_category', 'refresh');
                        }
                        else
                        {
                            $this->form_validation->set_rules('file_upload', 'product_category Photo', 'callback_file_upload_error');
                            $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                            if($this->form_validation->run() == FALSE) {
                                $this->addproduct_category();
                            }
                        }
            
        }
    }
        
        
        public function file_upload_error()
        {
            return FALSE;
        }
        
        
//        public function change_status()
//        {
//                $this->load->library('form_validation');
//                $this->form_validation->set_rules('status', 'status', 'required');
//                
//                if ($this->form_validation->run() == TRUE) {
//
//                    $status = $this->input->post('status');
//                    $userID = $this->input->post('bannerid');
//
//                    $data = array(
//                        'banner_status' => $status,
//                    );
//                    
//                    if ($this->Hoosk_model->changebanner_status($data, (int) $userID)) {
//                        $this->session->set_flashdata('success', 'Status updated successfully.');
//                        redirect(BASE_URL.'/admin/banners', 'refresh');
//                    } else {
//                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
//                        redirect(BASE_URL.'/admin/banners', 'refresh');
//                    }
//                } 
//                else {
//                    redirect(BASE_URL.'/admin/banners', 'refresh');
//                }
//        }
        

        public function editproduct_category()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $product_category_id = base64_decode($this->uri->segment(4));
                
        //Get user details from database
        $product_category_data = $this->Product_category_model->getproduct_categorys($product_category_id);
        
        if(count($product_category_data) > 0) {
        
            $this->data['product_category'] = $product_category_data;
            
            //Load the view
            $this->data['header'] = $this->load->view('admin/header', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/product_category_edit', $this->data);
        }
        else {
            $this->session->set_flashdata('message', 'Record with specified id does not exist');
            redirect(BASE_URL.'/admin/product_category', 'refresh');
        }
    }

    public function edited()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('category_name', 'Product category name', 'required');
                
        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editproduct_category();
        }  else  {
                        //usre want to change profile picture
                        if (!empty($_FILES['file_upload']['name']))
                        {
                            $config['upload_path']          = './uploads/products/';
                            $config['allowed_types']        = 'gif|jpg|png';
                            $config['max_size']             = 1000;
                            $config['max_width']            = 1024;
                            $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if ($this->upload->do_upload('file_upload'))
                            {
                                $upload_data = array('upload_data' => $this->upload->data());
                                $this->Product_category_model->updateproduct_category(base64_decode($this->uri->segment(4)),$upload_data['upload_data']['file_name']);
                                $this->session->set_flashdata('success', 'Product category Is Successfully Updated.');
                                redirect(BASE_URL.'/admin/product_category', 'refresh');
                            }
                            else
                            {
                                $this->form_validation->set_rules('file_upload', 'product_category Photo', 'callback_file_upload_error');
                                $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                                if($this->form_validation->run() == FALSE) {
                                    $this->editproduct_category();
                                }
                            }
                            //Validation passed
                            //Update the user
                            
                        }
                        else
                        {
                            //echo "hiii";die();
                            //Validation passed
                            //Update the user
                            $this->Product_category_model->updateproduct_category(base64_decode($this->uri->segment(4)));
                            $this->session->set_flashdata('success', 'Product category Is Successfully Updated.');
                            redirect(BASE_URL.'/admin/product_category', 'refresh');
                        }
            //Return to user list
            
        }
    }


    function delete()
    {
            
        //echo "hii";die();
        // Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        // if($this->input->post('deleteid')):
        //  $this->Hoosk_model->removeBanners($this->input->post('deleteid'));
        //  redirect(BASE_URL.'/admin/banners');
        // else:
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        if($this->input->post('deleteid')):
            $this->Product_category_model->removeproduct_category($this->input->post('deleteid'));
            redirect(BASE_URL.'/admin/product_category');
        else:
            $delete_id = base64_decode($this->uri->segment(4));
            $this->data['form']=$this->Product_category_model->getproduct_categorys($delete_id);
            $this->load->view('admin/product_category_delete.php', $this->data );
        endif;
    }


}
