<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        define("HOOSK_ADMIN", 1);
        $this->load->model('Hoosk_model');
        $this->load->helper(array('admincontrol', 'url', 'form'));
        $this->load->library('session');
        define('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('THEME', $this->Hoosk_model->getTheme());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
    }

    public function index()
    {
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('pagination');
        $result_per_page_result = $this->Hoosk_model->resultperpage();
        $result_per_page = $result_per_page_result[0][resultperpage];
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/order', $this->data);
        
    }
    public function get_all_data()
    {
            
            Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//            echo $this->Hoosk_model->get_order_datatable_data('order',$_REQUEST);
            $po_id = $this->uri->segment(4);
            //$company_id = $this->session->userdata('companyId');
            //Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
            echo $this->Hoosk_model->get_order_datatable_data('order',$_REQUEST,$po_id);
    }
    
    public function orderbyproject()
    {
            
            Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
            $this->load->library('pagination');
            $result_per_page_result = $this->Hoosk_model->resultperpage();
            $result_per_page = $result_per_page_result[0][resultperpage];
            //Load the view
            $order_id = base64_decode($this->uri->segment(4));
            $this->data['project_id'] = $order_id;
            $this->data['header'] = $this->load->view('admin/header', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/order', $this->data);
            
    }
    
    public function addOrder()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->data['company'] = $this->Hoosk_model->get_enabled_company();
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/order_new', $this->data);
    }
    
    function get_po_of_manager()
    {
        //echo 'adssaddsa';exit();
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $companyid = $this->input->post('companyid');
        $result = $this->Hoosk_model->get_pos_from_company($companyid);
        echo json_encode($result);
    }

    public function get_product_ajax()
    {
        //echo 'sassadsa';exit();
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $productname = $this->input->post('productname');
        $rowid = $this->input->post('rowid');
        $result = $this->Hoosk_model->get_product($productname);
        $u = ' <ul id="country-list"> ';
        foreach($result as $country) {
            $u .= "<li onClick=selectCountry('".$country[product_id]."','".$rowid."')> ". $country["product_name"] ." </li> ";            
        }
        $u .= ' </ul> ';
        echo $u;
    }
    public function get_company_byid_ajax()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $companyid = $this->input->post('companyid');
        $result = $this->Hoosk_model->get_company_by_id($companyid);
        echo json_encode($result);
    }
    public function get_product_byid_ajax()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $productid = $this->input->post('productid');
        $result = $this->Hoosk_model->get_product_by_id($productid);
        echo json_encode($result);
    }
    public function confirm()
    {
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('order_po', 'Project', 'trim|required');
        $this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->addOrder();
                
        }  else  {
                
                $po_name = $this->input->post('order_po');
                $po_manager = $this->input->post('po_manager');
                $product_data = $this->input->post('product_data');
                $custom_data = $this->input->post('custom_data');
                $data = array(
                    'po_id'=>$po_name,
                    'order_status'=>'Pending',
                    'created_date'=> date("Y-m-d H:i:s"),
                );
                $confirm_order_id = $this->Hoosk_model->confirm_order($data);
                if($confirm_order_id)
                {
                    for($i=1;$i<=$product_data;$i++)
                    {

                            $order_product_id = $this->input->post('order_product'.$i);
                            $order_product_quantity = $this->input->post('order_product_quantity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Hoosk_model->confirm_order_products($data);

                    }
                    for($i=1;$i<=$custom_data;$i++)
                    {
                            $order_product_id = $this->input->post('order_custom'.$i);
                            $order_product_quantity = $this->input->post('order_custom_quanity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Hoosk_model->confirm_order_products($data);
                    }
                    $this->session->set_flashdata('success', 'Order Inserted successfully.');
                    redirect(BASE_URL.'/admin/order', 'refresh');
                }  
                else
                {
                    $this->session->set_flashdata('message', 'Something went wrong.');
                    redirect(BASE_URL.'/admin/order', 'refresh');
                }
        }
        
        
    }
    
    public function editOrder()
    {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $order_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$order_data = $this->Hoosk_model->getOrderById($order_id);
		$this->data['order_product_data'] = $this->Hoosk_model->get_order_product_data($order_id);
                //print_r($this->data['order_product_data']);exit();
		if(count($order_data) > 0) {
    			
    		//Load the view
//                $project_id = $order_data[0]['po_id'];
                $this->data['company'] = $this->Hoosk_model->get_enabled_company();
    		$this->data['order'] = $order_data;
//    		$this->data['products'] = $this->Hoosk_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
//    		$this->data['customs'] = $this->Hoosk_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
                $this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/order_edit', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/admin/order', 'refresh');
		}
    }
    public function get_po_data()
    {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $project_id = $this->input->post('projectid');
		//Get user details from database
		$project_data = $this->Hoosk_model->getProjectById($project_id);
	
		if(count($project_data) > 0) {
                        
                        $project = $project_data;
                        $products = $this->Hoosk_model->get_products_for_project($project_id);
                        $customs = $this->Hoosk_model->get_custom_for_project($project_id);
                        $remaining = $this->Hoosk_model->get_remaining_for_project($project_id);//print_r($remaining );exit();
                        $data = array(
                          'project'=>$project,
                          'product'=>$products,
                          'custom'=>$customs,
                          'remaining'=>$remaining,
                        );
                        echo json_encode($data);
		}
		else {
                        echo json_encode('');
		}
    }

    public function edited()
    {
        
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('order_po', 'Project', 'trim|required');
        $this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->editOrder();
                
        }  else  {
                
                $po_name = $this->input->post('order_po');
                $po_manager = $this->input->post('po_manager');
                $product_data = $this->input->post('product_data');
                $custom_data = $this->input->post('custom_data');
                $confirm_order_id = base64_decode($this->uri->segment(4));
                if($confirm_order_id)
                {
                    $this->Hoosk_model->order_products_delete($confirm_order_id);
                    for($i=1;$i<=$product_data;$i++)
                    {

                            $order_product_id = $this->input->post('order_product'.$i);
                            $order_product_quantity = $this->input->post('order_product_quantity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Hoosk_model->confirm_order_products($data);

                    }
                    for($i=1;$i<=$custom_data;$i++)
                    {
                            $order_product_id = $this->input->post('order_custom'.$i);
                            $order_product_quantity = $this->input->post('order_custom_quanity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Hoosk_model->confirm_order_products($data);
                    }
                    $this->session->set_flashdata('success', 'Order Updated successfully.');
                    redirect(BASE_URL.'/admin/order', 'refresh');
                }  
                else
                {
                    $this->session->set_flashdata('message', 'Record with specified id does not exist');
                    redirect(BASE_URL.'/admin/order', 'refresh');
                }
        }
    }

    
    function delete()
    {
            Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
            if($this->input->post('deleteid')):
                    $this->Hoosk_model->removeOrder($this->input->post('deleteid'));
                    $this->session->set_flashdata('success', 'Record Deleted Succesfully.');
                    redirect(BASE_URL.'/admin/order');
            else:
                    $delete_id = base64_decode($this->uri->segment(4));
                    $this->data['form']=$this->Hoosk_model->getOrderById($delete_id);
                    $this->load->view('admin/order_delete.php', $this->data );
            endif;
    }
    function view()
    {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $order_id = base64_decode($this->uri->segment(4));
		$order_data = $this->Hoosk_model->getOrderById($order_id);
		$this->data['order_product_data'] = $this->Hoosk_model->get_order_product_data($order_id);
		if(count($order_data) > 0) {
                $this->data['company'] = $this->Hoosk_model->get_enabled_company();
    		$this->data['order'] = $order_data;
                $this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/order_view', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/admin/order', 'refresh');
		}
    }
    function change_status()
    {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $orderid = $this->input->post('orderid');

                    $data = array(
                        'order_status' => $status,
                    );
                    
                    if ($this->Hoosk_model->change_order_status($data, (int) $orderid)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/admin/order', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/admin/order', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/admin/order', 'refresh');
                }
    }
}
