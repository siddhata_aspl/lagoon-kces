<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_orders extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		define("HOOSK_ADMIN",1);
		$this->load->model('Hoosk_model');
                $this->load->model('Site_orders_model');
		$this->load->helper(array('admincontrol', 'url', 'form'));
		$this->load->library('session');
		define ('LANG', $this->Hoosk_model->getLang());
		$this->lang->load('admin', LANG);
		//Define what page we are on for nav
                $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
		$this->data['current'] = $this->uri->segment(2);
		define ('SITE_NAME', $this->Hoosk_model->getSiteName());
		define('THEME', $this->Hoosk_model->getTheme());
		define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
		define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
	}
        

        public function index()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Site_orders_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/admin/site_orders/';
                $config['total_rows'] = $this->Site_orders_model->countSiteOrders();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

		//Get users from database
		$this->data['siteorders'] = $this->Site_orders_model->getSiteOrders($result_per_page, $this->uri->segment(3));

		//Load the view
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/site_orders', $this->data);
	}
        
        public function get_all_data()
        {
                //echo "hi";exit();
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        	//Get site orders from database
        	echo $this->Site_orders_model->get_site_orders_datatable_data('site_orders',$_REQUEST);
        }
        
        function view_site_orders()
        {
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
                $uid = base64_decode($this->uri->segment(4));
                $oid = base64_decode($this->uri->segment(5));
                
		//Get user details from database
		$userdata = $this->Site_orders_model->getUserByOrderUserId($uid);
                $orderanddate = $this->Site_orders_model->getOrderIdDate($oid);
                $orderdata = $this->Site_orders_model->getSiteOrderById($oid);
	
		if(count($orderdata) > 0) {
    			
    		//Load the view
                $this->data['userdata'] = $userdata;
                $this->data['orderanddate'] = $orderanddate;
    		$this->data['orderdata'] = $orderdata;
                
                //$this->data['company'] = $this->Hoosk_model->get_enabled_company();
    		//$this->data['products'] = $this->Hoosk_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
    		//$this->data['customs'] = $this->Hoosk_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
                $this->data['header'] = $this->load->view('admin/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('admin/footer', '', true);
    		$this->load->view('admin/site_order_view', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/admin/site_orders', 'refresh');
		}
        }
}