<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	//	ob_start();
		// $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		// $this->output->set_header('Pragma: no-cache');
		// $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		define("HOOSK_ADMIN",1);
		$this->load->helper(array('admincontrol', 'url', 'hoosk_admin', 'form'));
                $this->load->helper('cookie');
		$this->load->library('session');
		$this->load->model('Hoosk_model');
                $this->load->model('Hoosk_page_model');
                $this->data['settings'] = $this->Hoosk_page_model->getSettings();
		define ('LANG', $this->Hoosk_model->getLang());
		$this->lang->load('admin', LANG);
		define ('SITE_NAME', $this->Hoosk_model->getSiteName());
		define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
		define('THEME', $this->Hoosk_model->getTheme());
		define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
		$this->load->model('Change_password_model');
	}

	public function index()
	{
//            print_r($_SESSION);
//            exit();
            
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->data['current'] = $this->uri->segment(2);
		$this->data['recenltyUpdated'] = $this->Hoosk_model->getUpdatedPages();
                
		if(!RSS_FEED) {
                    
            $this->load->library('rssparser');
            
            $this->rssparser->set_feed_url('http://hoosk.org/feed/rss');
            
            $this->rssparser->set_cache_life(30);
            $this->data['hooskFeed'] = $this->rssparser->getFeed(3);
            
        }
		$this->data['maintenaceActive'] = $this->Hoosk_model->checkMaintenance();
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
               
		$this->load->view('admin/home', $this->data);
	}

	public function upload()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$attachment = $this->input->post('attachment');
		$uploadedFile = $_FILES['attachment']['tmp_name']['file'];

		$path = $_SERVER["DOCUMENT_ROOT"].'/images';
		//$path = BASE_URL.'/images';
		$url = BASE_URL.'/images';

		// create an image name
		$fileName = $attachment['name'];

		// upload the image
		move_uploaded_file($uploadedFile, $path.'/'.$fileName);

		$this->output->set_output(json_encode(array('file' => array(
		'url' => $url . '/' . $fileName,
		'filename' => $fileName
		))),
		200,
		array('Content-Type' => 'application/json')
		);
	}

	public function login()
	{
//            echo CI_VERSION;
//            echo phpinfo();
//            exit();
                $username = $this->session->userdata('userName') ==''?'':$this->session->userdata('userName');
                if($username != '')
                {
                    redirect(BASE_URL.'/admin', 'refresh');
                }
		$this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/login', $this->data);
	}

	public function loginCheck()
 	{
 	    //setcookie('PHPSESSID', '', time() - 86400, '/');
		$username=$this->input->post('username');
		//$password=md5($this->input->post('password').SALT);
                $password= base64_encode($this->input->post('password'));
		$result=$this->Hoosk_model->login($username,$password);
		if($result) {
			redirect(BASE_URL.'/admin', 'refresh');
		}
		else
		{
			$this->data['error'] = "1";
			$this->login();
		}
	}
	
	function ajaxLogin(){
		$username=$this->input->post('username');
		$password=md5($this->input->post('password').SALT);
		$result=$this->Hoosk_model->login($username,$password);
		if($result) {
			echo 1;
		}
		else
		{
			echo 0;
		}
	}

	/************ Siddhata - Change Password *****************/

	public function change_password(){

		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		//Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
               
		$this->load->view('admin/change_password', $this->data);
	}

	public function confirm_changepass(){

        //Admincontrol_helper::is_logged_in($this->session->userdata('userName'));

        //Load the form validation library
		$this->load->library('form_validation');

		//Set validation rules
		$this->form_validation->set_rules('cur_pass', 'Current Password', 'trim|required');
		$this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|min_length[6]|max_length[32]');
		$this->form_validation->set_rules('conf_new_pass', 'Confirm New Password','trim|required|matches[new_pass]');

		if($this->form_validation->run() == FALSE) {
			//Validation failed
			$this->change_password();
		}  else  {
			$res=$this->Change_password_model->checkCurPass();
			if($res)
			{
				$this->Change_password_model->saveNewPass();
                $this->session->set_flashdata('success', 'Password updated successfully.');
                redirect(BASE_URL.'/admin/change_password', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('message', 'Your current password is incorrect.');
                redirect(BASE_URL.'/admin/change_password', 'refresh');
			}
		}
	}

	/**********************************************************/
	
	public function logout()
	{
		$data = array(
				'userID'    => 	'',
				'userName'  => 	'',
                'logged_in'	=>  '',
		);

		$this->session->unset_userdata('userID');
        $this->session->unset_userdata('userName');
        $this->session->unset_userdata('logged_in');
                
        $this->session->unset_userdata($data);
               // $this->load->driver('cache');
               // $this->session->sess_destroy();
               // $this->cache->clean();
    		//	ob_clean();

    			
                //$this->session->unset_userdata('userID');
                //$this->session->unset_userdata('logged_in');
                //$domain = base_url();
                //setcookie('PHPSESSID', '', time() - 86400, '/');
		//$this->session->unset_userdata($data);
		//$this->session->sess_destroy();
                //session_destroy();
                //print_r($_SESSION);
		//$this->login();
        
        //setcookie( config_item('kces_cookie'), '', time() - 3600 );
        //delete_cookie( config_item('kces_cookie') );
        
		$user_data = $this->session->all_userdata();
            
        foreach ($user_data as $key => $value) {
             if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                     $this->session->unset_userdata($key);
                 }
             }
            
         $this->session->sess_destroy();
         
         redirect(BASE_URL.'/admin/login', 'refresh');

	}


	public function settings()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->load->helper('directory');
		$this->data['themesdir'] = directory_map($_SERVER["DOCUMENT_ROOT"].'/theme/', 1);
		$this->data['langdir'] = directory_map(APPPATH.'/language/', 1);
                $this->data1['settings'] = $this->Hoosk_page_model->getSettings();
		$this->data['settings'] = $this->Hoosk_model->getSettings();
		$this->data['current'] = $this->uri->segment(2);
		$this->data['header'] = $this->load->view('admin/header', $this->data1, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/settings', $this->data);
	}

	public function updateSettings()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		
		//Load the form validation library
		$this->load->library('form_validation');

		//Set validation rules
		$this->form_validation->set_rules('siteTitle', 'Site Name', 'required');
		//$this->form_validation->set_rules('domain', 'Domain', 'trim');
		//$this->form_validation->set_rules('api_key', 'API Key', 'trim');
		
		if($this->form_validation->run() == FALSE) {
			//Validation failed
			$this->settings();
		}  else  {
    		$path_upload = $_SERVER["DOCUMENT_ROOT"] . '/uploads/';
    		$path_images = $_SERVER["DOCUMENT_ROOT"] . '/images/';
    		if ($this->input->post('siteLogo') != ""){
    			rename($path_upload . $this->input->post('siteLogo'), $path_images . $this->input->post('siteLogo'));
    		}
    		if ($this->input->post('siteFavicon') != ""){
    			rename($path_upload . $this->input->post('siteFavicon'), $path_images . $this->input->post('siteFavicon'));
    		}
    		 if ($this->input->post('siteFooterLogo') != ""){
    			rename($path_upload . $this->input->post('siteFooterLogo'), $path_images . $this->input->post('siteFooterLogo'));
    		}
    		$this->Hoosk_model->updateSettings();
    		redirect(BASE_URL.'/admin/settings', 'refresh');
		}
	}

	public function uploadLogo()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png|ico';

		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			if ( ! $this->upload->do_upload($key))
			{
					$error = array('error' => $this->upload->display_errors());
					echo 0;
			}
			else
			{
					echo '"'.$this->upload->data('file_name').'"';
			}
		}
	}

		public function social()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));


		$this->data['social'] = $this->Hoosk_model->getSocial();
		$this->data['current'] = $this->uri->segment(2);
		$this->data['header'] = $this->load->view('admin/header', $this->data, true);
		$this->data['footer'] = $this->load->view('admin/footer', '', true);
		$this->load->view('admin/social', $this->data);
	}

	public function updateSocial()
	{
		Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
		$this->Hoosk_model->updateSocial();
		redirect(BASE_URL.'/admin/social', 'refresh');
	}

	public function checkSession()
	{
		if(!$this->session->userdata('logged_in')){
			echo 0;
		} else {
			echo 1;
		}
	}

    public function complete()
    {
        unlink(FCPATH."install/hoosk.sql");
        unlink(FCPATH."install/index.php");
        redirect(BASE_URL.'/admin', 'refresh');
    }
}
