<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company extends CI_Controller {

    function __construct()
    {

        parent::__construct();
        define("HOOSK_ADMIN", 1);
        $this->load->model('Hoosk_model');
        $this->load->helper(array('admincontrol', 'url', 'form'));
        $this->load->library('session');
        define('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('admin', LANG);
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('THEME', $this->Hoosk_model->getTheme());
        define('DOMAIN', $this->Hoosk_model->getDomain());
        define('API_KEY', $this->Hoosk_model->getAPIKey());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
    }

    public
            function index()
    {

        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $this->load->library('pagination');
        //$result_per_page =15;  // the number of result per page
        $result_per_page_result = $this->Hoosk_model->resultperpage();
        $result_per_page = $result_per_page_result[0][resultperpage];
        $config['base_url'] = BASE_URL . '/admin/company/';
        $config['total_rows'] = $this->Hoosk_model->countCompany();
        $config['per_page'] = $result_per_page;

        $this->pagination->initialize($config);

        //Get users from database
        $this->data['company'] = $this->Hoosk_model->getCompany($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/company', $this->data);
    }

    public function addCompany()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the view
        $this->data['header'] = $this->load->view('admin/header', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/footer', '', true);
        $this->load->view('admin/company_new', $this->data);
    }
    
    public function viewCompany()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $company_id = base64_decode($this->uri->segment(4));

        //Get user details from database
        $company_data = $this->Hoosk_model->getCompanyById($company_id);
        if (count($company_data) > 0)
        {
            $this->data['users'] = $company_data;
            //Load the view
            $this->data['header'] = $this->load->view('admin/header', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/company_view', $this->data);
        }
        else
        {
            $this->session->set_flashdata('message', 'Record with specific id does not exist.');
            redirect(BASE_URL . '/admin/company', 'refresh');
        }
    }

    public function confirm()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//                echo "<pre>";print_r($_POST);die;
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('firstname', 'First name', 'trim|alpha_dash');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|alpha_dash');
        $this->form_validation->set_rules('companyname', 'Company name', 'trim|required');
        //this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_company.email]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[hoosk_company.email]|is_unique[hoosk_user.email]|is_unique[employees.emp_email]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
//        $this->form_validation->set_rules('city', 'City', 'trim');
//        $this->form_validation->set_rules('state', 'State', 'trim');
//        $this->form_validation->set_rules('address', 'Address', 'trim');
        $this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|min_length[5]|max_length[7]|numeric');
        
        
        
        


        if ($this->form_validation->run() == FALSE)
        {
            //Validation failed
            $this->addCompany();
        }
        else
        {
            if(!empty($_FILES['file_upload']['name']))
            {
                    //Validation passed
                    $config['upload_path'] = './uploads/company/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('file_upload');
                    $upload_data = array('upload_data' => $this->upload->data());
                    $file_name = $upload_data['upload_data']['file_name'];
            }
            else
            {
                    $file_name = '';
            }

            //print_r($upload_data['upload_data']['file_name']);
            //exit();
            $this->Hoosk_model->createCompany($file_name);

            $email = $this->input->post('email');
            $this->load->helper('string');
            $rs = random_string('alnum', 12);
            $data = array(
                'rs' => $rs
            );
            $forgotmail_company = $this->Hoosk_model->update_company_for_forgotmail($data, $email);


            //now we will send an email
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $mail_template = $this->Hoosk_model->getmail_template(3);

            if (isset($mail_template[0]['email_template_title']))
            {
                $link = "\r\n" . BASE_URL . '/vendor/newcompany/' . $rs;
                $message = $mail_template[0]['email_template_desc'];
                $subject = $mail_template[0]['email_template_subject'];
                $message = str_replace("%firstname%", $forgotmail_company[0]['first_name'], $message);
                $message = str_replace("%lastname%", $forgotmail_company[0]['last_name'], $message);
                $message = str_replace("%email%", $forgotmail_company[0]['email'], $message);
                $message = str_replace("%link%", $link, $message);

                // $this->load->library('email', $config);

                // $this->email->from('password@' . EMAIL_URL, SITE_NAME);
                // $this->email->to($email);

                // $this->email->subject($subject);
                // $this->email->message($message);
                // //                      $this->email->subject($this->lang->line('email_reset_subject'));
                // //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                // $this->email->send();

                $message = str_replace("&nbsp;",' ',$message);
                $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
            }

            $this->session->set_flashdata('success', 'Company Is Successfully Added.');
            //Return to user list
            redirect(BASE_URL . '/admin/company', 'refresh');
        }
    }

    public function newcompany($param)
    {
        $this->session->set_userdata('company', TRUE);

                $rs = $this->uri->segment(3);
		$query=$this->db->get_where('hoosk_company', array('rs' => $rs), 1);
                if ($query->num_rows() == 0)
                {
                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                $this->load->view('admin/error1', $this->data);

                }
                else
                {
                        $this->load->database();
                        $this->load->helper('url');
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                        $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                        if ($this->form_validation->run() == FALSE)
                        {
                                echo form_open();
                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                $this->load->view('admin/newpassform', $this->data);
                        }
                        else
                        {
                                $query=$this->db->get_where('hoosk_company', array('rs' => $rs), 1);
                                if ($query->num_rows() == 0)
                                {
                                        show_error('Sorry!!! Invalid Request!');
                                }
                                else
                                {
                                        $data = array(
                                        'password' => base64_encode($this->input->post('password')),
                                        'rs' => '',
                                        'company_status'=>'Enabled',
                                        );
                                        $where=$this->db->where('rs', $rs);
                                        $where->update('hoosk_company',$data);
                                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
                                        $this->load->view('admin/newpass', $this->data);
                                }
                        }
                }
                
    }
    
    public
            function file_upload_error()
    {
        return FALSE;
    }
    
    public function get_all_data()
    {
        //echo "hi";exit();
                Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        		//Get users from database
        		echo $this->Hoosk_model->get_company_datatable_data('hoosk_company',$_REQUEST);
    }
    public
            function change_status()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('status', 'status', 'required');

        if ($this->form_validation->run() == TRUE)
        {

            $status = $this->input->post('status');
            $companyID = $this->input->post('companyid');

            $data = array(
                'company_status' => $status,
            );

            if ($this->Hoosk_model->change_status_company($data, (int) $companyID))
            {
                $this->session->set_flashdata('success', 'Status updated successfully.');
                redirect(BASE_URL . '/admin/company', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                redirect(BASE_URL . '/admin/company', 'refresh');
            }
        }
        else
        {
            redirect(BASE_URL . '/admin/company', 'refresh');
        }
    }

    public
            function editCompany()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        $company_id = base64_decode($this->uri->segment(4));

        //Get user details from database
        $company_data = $this->Hoosk_model->getCompanyById($company_id);
        if (count($company_data) > 0)
        {
            $this->data['users'] = $company_data;
            //Load the view
            $this->data['header'] = $this->load->view('admin/header', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/company_edit', $this->data);
        }
        else
        {
            $this->session->set_flashdata('message', 'Record with specific id does not exist.');
            redirect(BASE_URL . '/admin/company', 'refresh');
        }
    }

    public function edited()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('firstname', 'First name', 'trim|alpha_dash');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|alpha_dash');
        $this->form_validation->set_rules('companyname', 'Company name', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[12]');
        $this->form_validation->set_message('min_length', 'The %s field must be at least 10 numbers in length.');
        $this->form_validation->set_rules('zipcode', 'Zipcode', 'trim|min_length[5]|max_length[7]|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            //Validation failed
            $this->editCompany();
        }
        else
        {
            $filename = '';
            if (!empty($_FILES['file_upload']['name']))
            {
                $config['upload_path'] = './uploads/company/';
                $config['allowed_types'] = 'gif|jpg|png';

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('file_upload'))
                {
                    $upload_data = array('upload_data' => $this->upload->data());
                    $filename = $upload_data['upload_data']['file_name'];
                }
                else
                {

                    $filename = '';

                }
                $this->Hoosk_model->updateCompany(base64_decode($this->uri->segment(4)), $filename);
                $this->session->set_flashdata('success', 'Record updated successfully.');
                redirect(BASE_URL . '/admin/company', 'refresh');
                //Validation passed
                //Update the user
            }
            else
            {
                //Validation passed
                //Update the user
                $this->Hoosk_model->updateCompany(base64_decode($this->uri->segment(4)), $filename);
                $this->session->set_flashdata('success', 'Record updated successfully.');
                redirect(BASE_URL . '/admin/company', 'refresh');
            }
            //Return to user list
        }
    }

    function delete()
    {
        Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
        if ($this->input->post('deleteid')):
            $this->Hoosk_model->removeCompany($this->input->post('deleteid'));
            $this->session->set_flashdata('success', 'Data removed successfully.');
            redirect(BASE_URL . '/admin/company');
        else:
            $delete_id = base64_decode($this->uri->segment(4));
            $this->data['form'] = $del = $this->Hoosk_model->getCompanyById($delete_id);
//            echo "<pre>";print_r($del);die;
            $this->load->view('admin/company_delete.php', $this->data);
        endif;
    }

    /*     * ************ Forgotten Password Resets ************* */

    public function forgot()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
        if ($this->form_validation->run() == FALSE)
        {
            $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/email_check', $this->data);
        }
        else
        {
            $email = $this->input->post('email');
            $this->load->helper('string');
            $rs = random_string('alnum', 12);
            $data = array(
                'rs' => $rs
            );
            $forgotmail_user = $this->Hoosk_model->update_user_for_forgotmail($data, $email);


            //now we will send an email
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $forgotmail_template = $this->Hoosk_model->getmail_template(2);

            if (isset($forgotmail_template[0]['email_template_title']))
            {
                $link = "\r\n" . BASE_URL . '/admin/reset/' . $rs;
                $message = $forgotmail_template[0]['email_template_desc'];
                $subject = $forgotmail_template[0]['email_template_subject'];
                $message = str_replace("%firstname%", $forgotmail_user[0]['User_firstname'], $message);
                $message = str_replace("%lastname%", $forgotmail_user[0]['User_lastname'], $message);
                $message = str_replace("%email%", $forgotmail_user[0]['email'], $message);
                $message = str_replace("%link%", $link, $message);

                // $this->load->library('email', $config);

                // $this->email->from('password@' . EMAIL_URL, SITE_NAME);
                // $this->email->to($email);

                // $this->email->subject($subject);
                // $this->email->message($message);
                // //			$this->email->subject($this->lang->line('email_reset_subject'));
                // //			$this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                // $this->email->send();

                $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
                                            
                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                $this->load->view('admin/check', $this->data);
            }
            else
            {
                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                $this->load->view('admin/check', $this->data);
            }
        }
    }

    public function email_check($str)
    {
        $query = $this->db->get_where('hoosk_company', array('email' => $str), 1);
        if ($query->num_rows() == 1)
        {
            return true;
        }
        else
        {
            $this->form_validation->set_message('email_check', $this->lang->line('email_check'));
            return false;
        }
    }

    public function addCompanys()
    {
        $rs = $this->uri->segment(3);
        $query = $this->db->get_where('hoosk_company', array('rs' => $rs), 1);
        if ($query->num_rows() == 0)
        {
            $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/error1', $this->data);
        }
        else
        {
            $this->load->database();
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                echo form_open();
                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                $this->load->view('admin/newpassform', $this->data);
            }
            else
            {
                $query = $this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                if ($query->num_rows() == 0)
                {
                    show_error('Sorry!!! Invalid Request!');
                }
                else
                {
                    $data = array(
                        'password' => base64_encode($this->input->post('password')),
                        'rs' => ''
                    );
                    $where = $this->db->where('rs', $rs);
                    $where->update('hoosk_user', $data);
                    $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('admin/footer', '', true);
                    $this->load->view('admin/newpass', $this->data);
                }
            }
        }
    }

    public
            function getPassword()
    {
        $rs = $this->uri->segment(3);
        $query = $this->db->get_where('hoosk_company', array('rs' => $rs), 1);

        if ($query->num_rows() == 0)
        {
            $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
            $this->data['footer'] = $this->load->view('admin/footer', '', true);
            $this->load->view('admin/error', $this->data);
        }
        else
        {
            $this->load->database();
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
            if ($this->form_validation->run() == FALSE)
            {
                echo form_open();
                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                $this->data['footer'] = $this->load->view('admin/footer', '', true);
                $this->load->view('admin/resetform', $this->data);
            }
            else
            {
                $query = $this->db->get_where('hoosk_user', array('rs' => $rs), 1);
                if ($query->num_rows() == 0)
                {
                    show_error('Sorry!!! Invalid Request!');
                }
                else
                {
                    $data = array(
                        'password' => base64_encode($this->input->post('password')),
                        'rs' => ''
                    );
                    $where = $this->db->where('rs', $rs);
                    $where->update('hoosk_user', $data);
                    $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('admin/footer', '', true);
                    $this->load->view('admin/reset', $this->data);
                }
            }
        }
    }

}
