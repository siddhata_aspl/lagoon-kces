<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Employees extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        define("HOOSK_ADMIN",1);
        $this->load->model('Vendor_model');
        $this->load->model('Hoosk_model');
        $this->load->helper(array('vendorcontrol', 'url', 'form'));
        $this->load->library('session');
        define ('LANG', $this->Vendor_model->getLang());
        $this->lang->load('vendor', LANG);
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define ('SITE_NAME', $this->Vendor_model->getSiteName());
        define('THEME', $this->Vendor_model->getTheme());
        define('DOMAIN', $this->Hoosk_model->getDomain());
        define('API_KEY', $this->Hoosk_model->getAPIKey());
        define('FOOTER_LINE', $this->Vendor_model->getSiteFooterLine());
        define ('THEME_FOLDER', BASE_URL.'/theme/'.THEME);
        $this->load->model('Vendor_employee_model');
        
        //for url access permission
        $seg2 = $this->uri->segment(2);
        //echo $seg2;
        $seg3 = $this->uri->segment(3);
        //echo $seg3;die();
        
        $module = $seg2.'_'.$seg3;

        if($seg3 == 'view' || $seg3 == 'new' || $seg3 == 'edit' || $seg3 == 'delete') 
        {
            if(!$this->Default_vendor_model->check_url_permission($module))
            {
                redirect(BASE_URL.'/vendor', 'refresh');
            }
        }
        
        //Vendorcontrol_helper::check_url_permission($seg2,$seg3);
        
    }
        
    public function index()
    { 
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $this->load->library('pagination');
                //$result_per_page =15;  // the number of result per page
                $result_per_page_result = $this->Vendor_employee_model->resultperpage();
                $result_per_page = $result_per_page_result[0][resultperpage];
                $config['base_url'] = BASE_URL. '/vendor/employees/';
                $config['total_rows'] = $this->Vendor_employee_model->countemployees();
                $config['per_page'] = $result_per_page;

                $this->pagination->initialize($config);

        //Get employees from database
        $this->data['employees'] = $this->Vendor_employee_model->getemployees($result_per_page, $this->uri->segment(3));

        //Load the view
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/employees', $this->data);
    }

    public function get_all_data()
    {
        //echo "hi";exit();
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //Get employees from database
        $company_id = $this->session->userdata('companyId');
        echo $this->Vendor_employee_model->get_employee_datatable_data('employees',$_REQUEST,$company_id);
    }
        

    public function addemployee()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //Load the view
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/employee_new', $this->data);
    }

    public function confirm()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
//                echo $_FILES['file_upload']['name'];
//                exit();
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('emp_firstname', 'firstname', 'trim|alpha_dash');
        $this->form_validation->set_rules('emp_lastname', 'lastname', 'trim|alpha_dash'); 
        //$this->form_validation->set_rules('emp_email', 'Email', 'trim|required|valid_email|is_unique[employees.emp_email]');
        $this->form_validation->set_rules('emp_email', 'Email', 'trim|required|valid_email|is_unique[employees.emp_email]|is_unique[hoosk_user.email]|is_unique[hoosk_company.email]');
        


        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->addemployee();
        }  else  {
            
            
                if ($_FILES['file_upload']['name'] != '')
                {                     

                            //public function do_upload(){ 
                            $config['upload_path']          = './uploads/employees/';
                            $config['allowed_types']        = 'gif|jpg|png';
    //                        $config['max_size']             = 1000;
    //                        $config['max_width']            = 1024;
    //                        $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            $this->upload->do_upload('file_upload');
                            $upload_data = array('upload_data' => $this->upload->data());
                            //print_r($upload_data);die();
                            $filename = $upload_data['upload_data']['file_name'];
                           // }
                }
                else
                {
                            $filename = '';
                }
   
                            //print_r($upload_data['upload_data']['file_name']);
                            //exit();
                            $cid = $this->session->userdata('companyId');
                            $this->Vendor_employee_model->createemployee($filename,$cid);
                            
                                         $email= $this->input->post('emp_email');
                                         $this->load->helper('string');
                                         $rs= random_string('alnum', 12);
                                         $data = array(
                                         'rs' => $rs
                                         );
                                         $forgotmail_employee = $this->Vendor_employee_model->update_employee_for_getmail($data,$email);


                                         //now we will send an email
                                         $config['protocol'] = 'sendmail';
                                         $config['mailpath'] = '/usr/sbin/sendmail';
                                         $config['charset'] = 'iso-8859-1';
                                         $config['wordwrap'] = TRUE;
                                         $mail_template = $this->Vendor_employee_model->getmail_template_for_emp_login(4);

                                         if(isset($mail_template[0]['email_template_title']))
                                         {   
                                             $link= "\r\n".BASE_URL.'/vendor/newemployee/'.$rs;
                                             $message = $mail_template[0]['email_template_desc'];
                                             $subject = $mail_template[0]['email_template_subject'];
                                             $message = str_replace("%firstname%",$forgotmail_employee[0]['emp_firstname'],$message);
                                             $message = str_replace("%lastname%",$forgotmail_employee[0]['emp_lastname'],$message);
                                             $message = str_replace("%email%",$forgotmail_employee[0]['emp_email'],$message);
                                             $message = str_replace("%link%",$link,$message);

                                             // $this->load->library('email', $config);

                                             // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                             // $this->email->to($email);

                                             // $this->email->subject($subject);
                                             // $this->email->message($message);
                                             // $this->email->subject($this->lang->line('email_reset_subject'));
                                             // $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/vendor/employees/reset/'.$rs );
                                             
                                             // $this->email->send();

                                             $message = str_replace("&nbsp;",' ',$message);
                                            $message = str_replace("&",'',$message);

                                            // $this->load->library('email', $config);

                                            // $this->email->from('password@'.EMAIL_URL, SITE_NAME);
                                            // $this->email->to($email);

                                            // $this->email->subject($subject);
                                            // $this->email->message($message );
                    //                      $this->email->subject($this->lang->line('email_reset_subject'));
                    //                      $this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );

                                            //$this->email->send();

                                            $to = $email;
                                            $from = FROM_EMAIL;
                                            $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                                            $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);

                                         }
                            
                                $this->session->set_flashdata('success', 'Employee Is Successfully Added.');
                                //Return to employee list
                                redirect(BASE_URL.'/vendor/employees', 'refresh');
                        // }
                        // else
                        // {
                        //     $this->form_validation->set_rules('file_upload', 'profile picture', 'callback_file_upload_error');
                        //     $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
                        //     if($this->form_validation->run() == FALSE) {
                        //         $this->addemployee();
                        //     }
                        // }
            
        }
    }
    
        
        
    public function file_upload_error()
    {
            return FALSE;
    }
        
        
        
        public function change_status()
        {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $emp_id = $this->input->post('emp_id');

                    $data = array(
                        'emp_status' => $status,
                    );
                    
                    if ($this->Vendor_employee_model->change_employeestatus($data, (int) $emp_id)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/vendor/employees', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/vendor/employees', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/vendor/employees', 'refresh');
                }
        }
        

    public function editemployee()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $emp_id = base64_decode($this->uri->segment(4));
                
        //Get employee details from database
        $employee_data = $this->Vendor_employee_model->getemployee($emp_id);
    
        if(count($employee_data) > 0) {
                
            //Load the view
            $this->data['employees'] = $employee_data;
            
            $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
            $this->data['footer'] = $this->load->view('vendor/footer', '', true);
            $this->load->view('vendor/employee_edit', $this->data);
        }
        else {
             $this->session->set_flashdata('message', 'Record with specified id does not exist');
             redirect(BASE_URL.'/vendor/employees', 'refresh');
        }
    }

    public function edited()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        //$this->form_validation->set_rules('employeename', 'employeename', 'required');
                //$this->form_validation->set_rules('emp_email', 'Email', 'trim|required|valid_email|is_unique[employees.emp_email]');
                $this->form_validation->set_rules('emp_firstname', 'firstname', 'trim|alpha_dash');
                $this->form_validation->set_rules('emp_lastname', 'lastname', 'trim|alpha_dash');
                // $this->form_validation->set_rules('phone', 'Phone', 'trim|min_length[7]|max_length[16]');
//      $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[4]|max_length[32]');
//      $this->form_validation->set_rules('con_password', 'confirm password','trim|required|matches[password]');
                
        if($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->editemployee();
        }  else  {
                        //usre want to change profile picture
                        if ($_FILES['file_upload']['name'] != '')
                        {
                            $config['upload_path']          = './uploads/employees/';
                            $config['allowed_types']        = 'gif|jpg|png';
//                            $config['max_size']             = 1000;
//                            $config['max_width']            = 1024;
//                            $config['max_height']           = 768;
                            $this->load->library('upload', $config);
//                            if ($this->upload->do_upload('file_upload'))
//                            {
                                $this->upload->do_upload('file_upload');
                                $upload_data = array('upload_data' => $this->upload->data());
                                //$filename = $upload_data['upload_data']['file_name'];
                                $this->Vendor_employee_model->updateEmployee(base64_decode($this->uri->segment(4)),$upload_data['upload_data']['file_name']);
                                $this->session->set_flashdata('success', 'Employee Is Successfully Updated.');
                                redirect(BASE_URL.'/vendor/employees', 'refresh');
                                
//                            }
//                            else
//                            {
//                                $this->form_validation->set_rules('file_upload', 'profile picture', 'callback_file_upload_error');
//                                $this->form_validation->set_message('file_upload_error', $this->upload->display_errors());
//                                if($this->form_validation->run() == FALSE) {
//                                    $this->editemployee();
//                                }
//                            }
                            //Validation passed
                            //Update the employee
                            
                        }
                        else
                        {
                            //Validation passed
                            //Update the employee
                            $this->Vendor_employee_model->updateEmployee(base64_decode($this->uri->segment(4)));
                            $this->session->set_flashdata('success', 'Employee Is Successfully Updated.');
                            redirect(BASE_URL.'/vendor/employees', 'refresh');
                        }
            //Return to employee list
            
        }
    }


    function delete()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        if($this->input->post('deleteid')):
            $this->Vendor_employee_model->removeemployee($this->input->post('deleteid'));
            redirect(BASE_URL.'/vendor/employees');
        else:
            $delete_id = base64_decode($this->uri->segment(4));
            $this->data['form']=$this->Vendor_employee_model->getemployee($delete_id);
            $this->load->view('vendor/employee_delete.php', $this->data );
        endif;
    }

        public function newemployee()
        {
                 $rs = $this->uri->segment(3);
                 $query=$this->db->get_where('employees', array('rs' => $rs), 1);
                 if ($query->num_rows() == 0)
                 {
                                 $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                                 $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                                 $this->load->view('vendor/error1', $this->data);

                 }
                 else
                 {
                                 $this->load->database();
                                 $this->load->helper('url');
                                 $this->load->library('form_validation');
                                 $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                                 $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                                 if ($this->form_validation->run() == FALSE)
                                 {
                                         echo form_open();
                                         $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                                         $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                                         $this->load->view('vendor/newpassform', $this->data);
                                 }
                                 else
                                 {
                                         $query=$this->db->get_where('employees', array('rs' => $rs), 1);
                                         if ($query->num_rows() == 0)
                                         {
                                                 show_error('Sorry!!! Invalid Request!');
                                         }
                                         else
                                         {
                                                 $data = array(
                                                 'password' => base64_encode($this->input->post('password')),
                                                 'rs' => '',
                                                 'emp_status'=>'Enabled',
                                                 );
                                                 $where=$this->db->where('rs', $rs);
                                                 $where->update('employees',$data);
                                                 $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                                                 $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                                                 $this->load->view('vendor/newpass', $this->data);
                                         }
                                 }
                         }
        }
        

     public function getPassword()
     {
         $rs = $this->uri->segment(4);
         //echo $rs;die();
         $query=$this->db->get_where('employees', array('rs' => $rs), 1);

                 if ($query->num_rows() == 0)
                 {//echo "hii";die();
                             $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                             $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                             $this->load->view('vendor/error', $this->data);

                 }
                 else
                 {

                                 $this->load->database();
                                 $this->load->helper('url');
                                 $this->load->library('form_validation');
                                 $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
                                 $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
                                 if ($this->form_validation->run() == FALSE)
                                 {
                                         echo form_open();
                                         $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                                         $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                                         $this->load->view('vendor/resetform', $this->data);
                                 }
                                 else
                                 {
                                         $query=$this->db->get_where('employees', array('rs' => $rs), 1);
                                         if ($query->num_rows() == 0)
                                         {
                                                 show_error('Sorry!!! Invalid Request!');
                                         }
                                         else
                                         {
                                                 $data = array(
                                                 'password' => base64_encode($this->input->post('password')),
                                                 'rs' => '',
                                                 'emp_status'=>'Enabled',
                                                 );
                                                 $where=$this->db->where('rs', $rs);
                                                $where->update('employees',$data);
                                                 $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                                                 $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                                                 $this->load->view('vendor/reset', $this->data);
                                         }
                                 }
         }
     }

}
