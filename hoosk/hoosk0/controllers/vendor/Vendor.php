<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendor extends CI_Controller {

    function __construct() {

        parent::__construct();
        //ob_start();
        // $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        // $this->output->set_header('Pragma: no-cache');
        // $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        define("HOOSK_ADMIN", 1);
        $this->load->helper(array('vendorcontrol', 'url', 'hoosk_vendor', 'form'));
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->model('Vendor_model');
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        define('LANG', $this->Vendor_model->getLang());
        $this->lang->load('vendor', LANG);
        define('SITE_NAME', $this->Vendor_model->getSiteName());
        define('FOOTER_LINE', $this->Vendor_model->getSiteFooterLine());
        define('THEME', $this->Vendor_model->getTheme());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        $this->load->model('Vendor_change_password_model');
        
    }

    public function index() {
//            print_r($_SESSION);
//            exit();

        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        

        $this->data['current'] = $this->uri->segment(2);
        //$this->data['recenltyUpdated'] = $this->Vendor_model->getUpdatedPages();
        //$this->data['maintenaceActive'] = $this->Hoosk_model->checkMaintenance();
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/home', $this->data);
    }

    public function upload() {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $attachment = $this->input->post('attachment');
        $uploadedFile = $_FILES['attachment']['tmp_name']['file'];

        $path = $_SERVER["DOCUMENT_ROOT"] . '/images';
        $url = BASE_URL . '/images';

        // create an image name
        $fileName = $attachment['name'];

        // upload the image
        move_uploaded_file($uploadedFile, $path . '/' . $fileName);

        $this->output->set_output(json_encode(array('file' => array(
                'url' => $url . '/' . $fileName,
                'filename' => $fileName
    ))), 200, array('Content-Type' => 'application/json')
        );
    }

    public function login() {
//            echo CI_VERSION;
//            echo phpinfo();
//            exit();
        //$username = $this->session->userdata('userName') == '' ? '' : $this->session->userdata('userName');
        if ($email != '') {
            redirect(BASE_URL . '/vendor', 'refresh');
        }
        $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/login', $this->data);
    }

    public function loginCheck() {

        //setcookie('PHPSESSID', '', time() - 86400, '/');
        $email = $this->input->post('email');
        //$password=md5($this->input->post('password').SALT);
        $password = base64_encode(trim($this->input->post('password')));
        $com_result = $this->Vendor_model->company_login($email, $password);
        if ($com_result) {
            // for company login
            redirect(BASE_URL . '/vendor', 'refresh');
        } else {
            // for employee login
            $emp_result = $this->Vendor_model->employee_login($email, $password);
            if ($emp_result) {
                //echo "hii";die();
                redirect(BASE_URL . '/vendor', 'refresh');
            } else {
                $this->data['error'] = "1";
                $this->login();
            }
        }
    }

    function ajaxLogin() {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password') . SALT);
        $com_result = $this->Vendor_model->company_login($email, $password);
        $emp_result = $this->Vendor_model->employee_login($email, $password);
        if ($com_result) {
            echo 1;
        }
        else {
            $emp_result = $this->Vendor_model->employee_login($email, $password);
            if ($emp_result) {
                echo 1;
            } else {
                echo 0;
            }
        }
    }

    public function logout() {
        if($this->session->userdata('is_master') == FALSE)
        {
            $data = array(
                'companyId' => '',
                'company_name' => '',
                'first_name' => '',
                'email' => '',
                'is_master' => '',
                'logged_in' => '',
            );

            $this->session->unset_userdata('companyId');
            $this->session->unset_userdata('company_name');
            $this->session->unset_userdata('first_name');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('is_master');
            $this->session->unset_userdata('logged_in');

            $this->session->unset_userdata($data);
            
            //$this->load->driver('cache');
            //$this->session->sess_destroy();
            //$this->cache->clean();
            //ob_clean();
            
            //$domain = base_url();
            //setcookie('PHPSESSID', '', time() - 86400, '/');
            //$this->session->unset_userdata($data);
            //$this->session->sess_destroy();
            //session_destroy();
            //print_r($_SESSION);
            //$this->login();
            
            $user_data = $this->session->all_userdata();
            
            foreach ($user_data as $key => $value) {
                if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                    $this->session->unset_userdata($key);
                }
            }
            
            $this->session->sess_destroy();
            
            redirect(BASE_URL . '/vendor/login', 'refresh');
        }
        
        if($this->session->userdata('is_master') == TRUE)
        {
            $data = array(
                'companyId' => '',
                'emp_id' => '',
                'first_name' => '',
                'email' => '',
                'is_master' => '',
                'logged_in' => '',
            );

            $this->session->unset_userdata('companyId');
            $this->session->unset_userdata('emp_id');
            $this->session->unset_userdata('first_name');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('is_master');
            $this->session->unset_userdata('logged_in');

            $this->session->unset_userdata($data);
            
            //$this->load->driver('cache');
            //$this->session->sess_destroy();
            //$this->cache->clean();
            //ob_clean();
            
            //$domain = base_url();
            //setcookie('PHPSESSID', '', time() - 86400, '/');
            //$this->session->unset_userdata($data);
            //$this->session->sess_destroy();
            //session_destroy();
            //print_r($_SESSION);
            //$this->login();
        
            $user_data = $this->session->all_userdata();
            
            foreach ($user_data as $key => $value) {
                if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                    $this->session->unset_userdata($key);
                }
            }
            
            $this->session->sess_destroy();
            
            redirect(BASE_URL . '/vendor/login', 'refresh');
        }
    }

    /*     * ************ Forgotten Password Resets ************* */

    public function forgot() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
        //echo "hii";die();
        if ($this->form_validation->run() == FALSE) {
            //echo "hii";die();
            $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
            $this->data['footer'] = $this->load->view('vendor/footer', '', true);
            $this->load->view('vendor/email_check', $this->data);
        } else {
            $email = $this->input->post('email');
            $this->load->helper('string');
            $rs = random_string('alnum', 12);
            $data = array(
                'rs' => $rs
            );

            // echo $this->session->userdata('is_master');exit();
            $cquery = $this->db->get_where('hoosk_company', array('email' => $email), 1);
            if($cquery->num_rows() == 1)
            {
            
                $forgotmail_user = $this->Vendor_model->update_company_for_forgotmail($data, $email);

                //now we will send an email
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $forgotmail_template = $this->Vendor_model->getmail_template(2);

                if (isset($forgotmail_template[0]['email_template_title'])) {
                    $link = "\r\n" . BASE_URL . '/vendor/reset/' . $rs;
                    $message = $forgotmail_template[0]['email_template_desc'];
                    $subject = $forgotmail_template[0]['email_template_subject'];
                    $message = str_replace("%firstname%", $forgotmail_user[0]['first_name'], $message);
                    $message = str_replace("%lastname%", $forgotmail_user[0]['last_name'], $message);
                    $message = str_replace("%email%", $forgotmail_user[0]['email'], $message);
                    $message = str_replace("%link%", $link, $message);
                    $message = str_replace("&nbsp;",' ',$message);
                    $message = str_replace("&",'',$message);

//                    $this->load->library('email', $config);
//
//                    $this->email->from('password@' . EMAIL_URL, SITE_NAME);
//                    $this->email->to($email);
//
//                    $this->email->subject($subject);
//                    $this->email->message($message);
//
//                    //$this->email->subject($this->lang->line('email_reset_subject'));
//                    //$this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );
//
//                    $this->email->send();
                    
                    $to = $email;
                    $from = FROM_EMAIL;
                    $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                    $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
                    
                    
                    $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                    $this->load->view('vendor/check', $this->data);
                } else {
                    $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                    $this->load->view('vendor/check', $this->data);
                }
            }
            else {
                
                $forgotmail_user = $this->Vendor_model->update_employee_for_forgotmail($data, $email);

                //now we will send an email
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $forgotmail_template = $this->Vendor_model->getmail_template(2);

                if (isset($forgotmail_template[0]['email_template_title'])) {
                    $link = "\r\n" . BASE_URL . '/vendor/reset/' . $rs;
                    $message = $forgotmail_template[0]['email_template_desc'];
                    $subject = $forgotmail_template[0]['email_template_subject'];
                    $message = str_replace("%firstname%", $forgotmail_user[0]['emp_firstname'], $message);
                    $message = str_replace("%lastname%", $forgotmail_user[0]['emp_lastname'], $message);
                    $message = str_replace("%email%", $forgotmail_user[0]['emp_email'], $message);
                    $message = str_replace("%link%", $link, $message);
                    $message = str_replace("&nbsp;",' ',$message);
                    $message = str_replace("&",'',$message);

//                    $this->load->library('email', $config);
//
//                    $this->email->from('password@' . EMAIL_URL, SITE_NAME);
//                    $this->email->to($email);
//
//                    $this->email->subject($subject);
//                    $this->email->message($message);
//
//                    //$this->email->subject($this->lang->line('email_reset_subject'));
//                    //$this->email->message($this->lang->line('email_reset_message')."\r\n".BASE_URL.'/admin/reset/'.$rs );
//
//                    $this->email->send();
                    
                    $to = $email;
                    $from = FROM_EMAIL;
                    $from_name = SITE_NAME;
                                            
//                                            $parameters = array('from' => $from,
//                                                'to' => $to,
//                                                'subject' => $subject,
//                                                'text' => $message,
//                                                //'attachment[0]' => curl_file_create($filepath, 'application/pdf', $filename)
//                                                );
                                            
//                                            $filename = __dir__.'../upload/images/'.$file_name;
                    $this->Default_vendor_model->send_mail($to,$from,$subject,$from_name,$message,$filename,$file_to_attach);
                    
                    
                    $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                    $this->load->view('vendor/check', $this->data);
                } else {
                    $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                    $this->load->view('vendor/check', $this->data);
                }
                
            }
            
        }
    }

    public function email_check($str) {
        $cquery = $this->db->get_where('hoosk_company', array('email' => $str), 1);
        if ($cquery->num_rows() == 1) {
            return true;
        } 
        else {
            $equery = $this->db->get_where('employees', array('emp_email' => $str), 1);
            if ($equery->num_rows() == 1)
            {
                return true;
            } else {
                $this->form_validation->set_message('email_check', $this->lang->line('email_check'));
                return false;
            }
        }
    }

//        public function newuser()
//        {
//                $rs = $this->uri->segment(3);
//      $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
//                if ($query->num_rows() == 0)
//                {
//                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
//                                $this->load->view('admin/error1', $this->data);
//
//                }
//                else
//                {
//                                $this->load->database();
//                                $this->load->helper('url');
//                                $this->load->library('form_validation');
//                                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
//                                $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
//                                if ($this->form_validation->run() == FALSE)
//                                {
//                                        echo form_open();
//                                        $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                        $this->data['footer'] = $this->load->view('admin/footer', '', true);
//                                        $this->load->view('admin/newpassform', $this->data);
//                                }
//                                else
//                                {
//                                        $query=$this->db->get_where('hoosk_user', array('rs' => $rs), 1);
//                                        if ($query->num_rows() == 0)
//                                        {
//                                                show_error('Sorry!!! Invalid Request!');
//                                        }
//                                        else
//                                        {
//                                                $data = array(
//                                                'password' => base64_encode($this->input->post('password')),
//                                                'rs' => '',
//                                                'user_status'=>'Enabled',
//                                                );
//                                                $where=$this->db->where('rs', $rs);
//                                                $where->update('hoosk_user',$data);
//                                                $this->data['header'] = $this->load->view('admin/headerlog', $this->data, true);
//                                                $this->data['footer'] = $this->load->view('admin/footer', '', true);
//                                                $this->load->view('admin/newpass', $this->data);
//                                        }
//                                }
//                        }
//                }
//        

    public function getPassword() {
        $rs = $this->uri->segment(3);
        $query = $this->db->get_where('hoosk_company', array('rs' => $rs), 1);

        if ($query->num_rows() == 0) {
                    $this->db->get_where('employees', array('rs' => $rs), 1);
                    if ($query->num_rows() == 0) {
                            $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
            $this->data['footer'] = $this->load->view('vendor/footer', '', true);
            $this->load->view('vendor/error', $this->data);
                    }
                    else
                    {
                        $data = array(
                        'password' => base64_encode($this->input->post('password')),
                        'rs' => ''
                        );
                        $where = $this->db->where('rs', $rs);
                        $where->update('hoosk_company', $data);
                        $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                        $this->load->view('vendor/reset', $this->data);
                    }
            
        } else {
            $this->load->database();
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[20]|matches[con_password]');
            $this->form_validation->set_rules('con_password', 'Confirm Password', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                echo form_open();
                $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                $this->load->view('vendor/resetform', $this->data);
            } else {
                $query = $this->db->get_where('hoosk_company', array('rs' => $rs), 1);
                if ($query->num_rows() == 0) {
                    $this->db->get_where('employees', array('rs' => $rs), 1);
                    if ($query->num_rows() == 0) {
                            show_error('Sorry!!! Invalid Request!');
                    }
                    else
                    {
                        $data = array(
                        'password' => base64_encode($this->input->post('password')),
                        'rs' => ''
                        );
                        $where = $this->db->where('rs', $rs);
                        $where->update('hoosk_company', $data);
                        $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                        $this->load->view('vendor/reset', $this->data);
                    }
                    
                } else {
                    $data = array(
                        'password' => base64_encode($this->input->post('password')),
                        'rs' => ''
                    );
                    $where = $this->db->where('rs', $rs);
                    $where->update('hoosk_company', $data);
                    $this->data['header'] = $this->load->view('vendor/headerlog', $this->data, true);
                    $this->data['footer'] = $this->load->view('vendor/footer', '', true);
                    $this->load->view('vendor/reset', $this->data);
                }
            }
        }
    }

    /*     * *************** Change Password **************** */

    public function change_password() {

        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //Load the view
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);

        $this->load->view('vendor/change_password', $this->data);
    }

    public function confirm_changepass() {

        //Vendorcontrol_helper::is_logged_in($this->session->userdata('userName'));
        //Load the form validation library
        $this->load->library('form_validation');

        //Set validation rules
        $this->form_validation->set_rules('cur_pass', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_pass', 'New Password', 'trim|required|min_length[6]|max_length[32]');
        $this->form_validation->set_rules('conf_new_pass', 'Confirm New Password', 'trim|required|matches[new_pass]');

        if ($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->change_password();
        } else {
            $res = $this->Vendor_change_password_model->checkCurPass();
            if ($res) {
                $this->Vendor_change_password_model->saveNewPass();
                $this->session->set_flashdata('success', 'Password updated successfully.');
                redirect(BASE_URL . '/vendor/change_password', 'refresh');
            } else {
                $this->session->set_flashdata('message', 'Your current password is incorrect.');
                redirect(BASE_URL . '/vendor/change_password', 'refresh');
            }
        }
    }

    /*     * ************************************* */

//  public function settings()
//  {
//      Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//      $this->load->helper('directory');
//      $this->data['themesdir'] = directory_map($_SERVER["DOCUMENT_ROOT"].'/theme/', 1);
//      $this->data['langdir'] = directory_map(APPPATH.'/language/', 1);
//
//      $this->data['settings'] = $this->Hoosk_model->getSettings();
//      $this->data['current'] = $this->uri->segment(2);
//      $this->data['header'] = $this->load->view('admin/header', $this->data, true);
//      $this->data['footer'] = $this->load->view('admin/footer', '', true);
//      $this->load->view('admin/settings', $this->data);
//  }
//
//  public function updateSettings()
//  {
//      Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//      
//      //Load the form validation library
//      $this->load->library('form_validation');
//
//      //Set validation rules
//      $this->form_validation->set_rules('siteTitle', 'Site Name', 'required');
//      
//      if($this->form_validation->run() == FALSE) {
//          //Validation failed
//          $this->settings();
//      }  else  {
//          $path_upload = $_SERVER["DOCUMENT_ROOT"] . '/uploads/';
//          $path_images = $_SERVER["DOCUMENT_ROOT"] . '/images/';
//          if ($this->input->post('siteLogo') != ""){
//              rename($path_upload . $this->input->post('siteLogo'), $path_images . $this->input->post('siteLogo'));
//          }
//          if ($this->input->post('siteFavicon') != ""){
//              rename($path_upload . $this->input->post('siteFavicon'), $path_images . $this->input->post('siteFavicon'));
//          }
//           if ($this->input->post('siteFooterLogo') != ""){
//              rename($path_upload . $this->input->post('siteFooterLogo'), $path_images . $this->input->post('siteFooterLogo'));
//          }
//          $this->Hoosk_model->updateSettings();
//          redirect(BASE_URL.'/admin/settings', 'refresh');
//      }
//  }
//
//  public function uploadLogo()
//  {
//      Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//      $config['upload_path']          = './uploads/';
//      $config['allowed_types']        = 'gif|jpg|png|ico';
//
//      $this->load->library('upload', $config);
//      foreach ($_FILES as $key => $value) {
//          if ( ! $this->upload->do_upload($key))
//          {
//                  $error = array('error' => $this->upload->display_errors());
//                  echo 0;
//          }
//          else
//          {
//                  echo '"'.$this->upload->data('file_name').'"';
//          }
//      }
//  }
//
//      public function social()
//  {
//      Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//
//
//      $this->data['social'] = $this->Hoosk_model->getSocial();
//      $this->data['current'] = $this->uri->segment(2);
//      $this->data['header'] = $this->load->view('admin/header', $this->data, true);
//      $this->data['footer'] = $this->load->view('admin/footer', '', true);
//      $this->load->view('admin/social', $this->data);
//  }
//
//  public function updateSocial()
//  {
//      Admincontrol_helper::is_logged_in($this->session->userdata('userName'));
//      $this->Hoosk_model->updateSocial();
//      redirect(BASE_URL.'/admin/social', 'refresh');
//  }

    public function checkSession() {
        if (!$this->session->userdata('logged_in')) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function complete() {
        unlink(FCPATH . "install/hoosk.sql");
        unlink(FCPATH . "install/index.php");
        redirect(BASE_URL . '/vendor', 'refresh');
    }

}
