<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        define("HOOSK_ADMIN", 1);
        $this->load->model('Hoosk_model');
        $this->load->helper(array('vendorcontrol', 'url', 'form'));
        $this->load->library('session');
        define('LANG', $this->Hoosk_model->getLang());
        $this->lang->load('vendor', LANG);
        //Define what page we are on for nav
        $this->load->model('Hoosk_page_model');
        $this->data['settings'] = $this->Hoosk_page_model->getSettings();
        $this->data['current'] = $this->uri->segment(2);
        define('SITE_NAME', $this->Hoosk_model->getSiteName());
        define('THEME', $this->Hoosk_model->getTheme());
        define('FOOTER_LINE', $this->Hoosk_model->getSiteFooterLine());
        define('THEME_FOLDER', BASE_URL . '/theme/' . THEME);
        $this->load->model('Vendor_order_model');
        
        $this->data['comp_id'] = $this->session->userdata('companyId');
        
        $company_id = $this->session->userdata('companyId');
        
        //Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        
        //for url access permission
        $seg2 = $this->uri->segment(2);
        //echo $seg2;
        $seg3 = $this->uri->segment(3);
        //echo $seg3;die();
        
        $module = $seg2.'_'.$seg3;

        if($seg3 == 'view' || $seg3 == 'new' || $seg3 == 'edit' || $seg3 == 'delete') 
        {
            if(!$this->Default_vendor_model->check_url_permission($module))
            {
                redirect(BASE_URL.'/vendor', 'refresh');
            }
        }
        //Vendorcontrol_helper::check_url_permission($seg2,$seg3);
        
    }

    public function index()
    {
        
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $this->load->library('pagination');
        $result_per_page_result = $this->Vendor_order_model->resultperpage();
        $result_per_page = $result_per_page_result[0][resultperpage];
        //Load the view
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/order', $this->data);
        
    }
    public function get_all_data()
    {
            $po_id = $this->uri->segment(4);
            $company_id = $this->session->userdata('companyId');
            Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
            echo $this->Vendor_order_model->get_order_datatable_data('order',$_REQUEST,$company_id,$po_id);
            
    }
    public function addOrder()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $this->data['company'] = $this->Vendor_order_model->get_enabled_company();
        $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        $this->data['footer'] = $this->load->view('vendor/footer', '', true);
        $this->load->view('vendor/order_new', $this->data);
    }
    
    function get_po_of_manager()
    {
        //echo 'adssaddsa';exit();
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //$companyid = $this->input->post('companyid');
        $company_id = $this->session->userdata('companyId');
        $result = $this->Vendor_order_model->get_pos_from_company($company_id);
        echo json_encode($result);
    }

    public function get_product_ajax()
    {
        //echo 'sassadsa';exit();
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $productname = $this->input->post('productname');
        $rowid = $this->input->post('rowid');
        $result = $this->Vendor_order_model->get_product($productname);
        $u = ' <ul id="country-list"> ';
        foreach($result as $country) {
            $u .= "<li onClick=selectCountry('".$country[product_id]."','".$rowid."')> ". $country["product_name"] ." </li> ";            
        }
        $u .= ' </ul> ';
        echo $u;
    }
    public function get_company_byid_ajax()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        //$companyid = $this->input->post('companyid');
        $company_id = $this->session->userdata('companyId');
        $result = $this->Vendor_order_model->get_company_by_id($company_id);
        echo json_encode($result);
    }
    public function get_product_byid_ajax()
    {
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $productid = $this->input->post('productid');
        $result = $this->Vendor_order_model->get_product_by_id($productid);
        echo json_encode($result);
    }
    public function confirm()
    {
        
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('order_po', 'Project', 'trim|required');
        //$this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->addOrder();
                
        }  else  {
                
                $po_name = $this->input->post('order_po');
                //$po_manager = $this->input->post('po_manager');
                $po_manager = $this->session->userdata('companyId');
                $product_data = $this->input->post('product_data');
                $custom_data = $this->input->post('custom_data');
                $data = array(
                    'po_id'=>$po_name,
                    'order_status'=>'Pending',
                    'created_date'=> date("Y-m-d H:i:s"),
                );
                $confirm_order_id = $this->Vendor_order_model->confirm_order($data);
                if($confirm_order_id)
                {
                    for($i=1;$i<=$product_data;$i++)
                    {

                            $order_product_id = $this->input->post('order_product'.$i);
                            $order_product_quantity = $this->input->post('order_product_quantity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Vendor_order_model->confirm_order_products($data);

                    }
                    for($i=1;$i<=$custom_data;$i++)
                    {
                            $order_product_id = $this->input->post('order_custom'.$i);
                            $order_product_quantity = $this->input->post('order_custom_quanity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Vendor_order_model->confirm_order_products($data);
                    }
                    $this->session->set_flashdata('success', 'Order Inserted successfully.');
                    redirect(BASE_URL.'/vendor/order', 'refresh');
                }  
                else
                {
                    $this->session->set_flashdata('message', 'Something went wrong.');
                    redirect(BASE_URL.'/vendor/order', 'refresh');
                }
        }
        
        
    }
    
    public function orderbyproject()
    {
            Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
            $this->load->library('pagination');
            $result_per_page_result = $this->Vendor_order_model->resultperpage();
            $result_per_page = $result_per_page_result[0][resultperpage];
            //Load the view
            $order_id = base64_decode($this->uri->segment(4));
            $this->data['project_id'] = $order_id;
            $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
            $this->data['footer'] = $this->load->view('vendor/footer', '', true);
            $this->load->view('vendor/order', $this->data);
    }

    public function editOrder()
    {
                Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
                $order_id = base64_decode($this->uri->segment(4));
                
		//Get user details from database
		$order_data = $this->Vendor_order_model->getOrderById($order_id);
		$this->data['order_product_data'] = $this->Vendor_order_model->get_order_product_data($order_id);
                //print_r($this->data['order_product_data']);exit();
		if(count($order_data) > 0) {
    			
    		//Load the view
//                $project_id = $order_data[0]['po_id'];
                $this->data['company'] = $this->Vendor_order_model->get_enabled_company();
    		$this->data['order'] = $order_data;
//    		$this->data['products'] = $this->Vendor_order_model->get_products_for_project($project_id);//print_r($this->data['products']);exit();
//    		$this->data['customs'] = $this->Vendor_order_model->get_custom_for_project($project_id);//print_r($this->data['customs']);exit();
                $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
    		$this->data['footer'] = $this->load->view('vendor/footer', '', true);
    		$this->load->view('vendor/order_edit', $this->data);
		}
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/vendor/order', 'refresh');
		}
    }

    public function get_po_data()
    {
                Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
                $project_id = $this->input->post('projectid');
		//Get user details from database
		$project_data = $this->Vendor_order_model->getProjectById($project_id);
	
		if(count($project_data) > 0) {
                        
                        $project = $project_data;
                        $products = $this->Vendor_order_model->get_products_for_project($project_id);

                        $customs = $this->Vendor_order_model->get_custom_for_project($project_id);
                        $remaining = $this->Vendor_order_model->get_remaining_for_project($project_id);
                       // echo $this->db->last_query();die;
                       // echo "<pre>";print_r($products );die;
                        $data = array(
                          'project'=>$project,
                          'product'=>$products,
                          'custom'=>$customs,
                          'remaining'=>$remaining,
                        );
                        echo json_encode($data);
		}
		else {
                        echo json_encode('');
		}
    }

    public function edited()
    {
        
        Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('order_po', 'Project', 'trim|required');
        //$this->form_validation->set_rules('po_manager', 'Manager', 'trim|required');
        
        if($this->form_validation->run() == FALSE) {
            
                $this->editOrder();
                
        }  else  {
                
                $po_name = $this->input->post('order_po');
                //$po_manager = $this->input->post('po_manager');
                $po_manager = $this->session->userdata('companyId');
                $product_data = $this->input->post('product_data');
                $custom_data = $this->input->post('custom_data');
                $confirm_order_id = base64_decode($this->uri->segment(4));
                if($confirm_order_id)
                {
                    $this->Vendor_order_model->order_products_delete($confirm_order_id);
                    for($i=1;$i<=$product_data;$i++)
                    {

                            $order_product_id = $this->input->post('order_product'.$i);
                            $order_product_quantity = $this->input->post('order_product_quantity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Vendor_order_model->confirm_order_products($data);

                    }
                    for($i=1;$i<=$custom_data;$i++)
                    {
                            $order_product_id = $this->input->post('order_custom'.$i);
                            $order_product_quantity = $this->input->post('order_custom_quanity'.$i);

                            $data = array(
                                'order_id'=>$confirm_order_id,
                                'product_id'=>$order_product_id,
                                'quantity'=>$order_product_quantity,
                            );
                            $this->Vendor_order_model->confirm_order_products($data);
                    }
                    $this->session->set_flashdata('success', 'Order Updated successfully.');
                    redirect(BASE_URL.'/vendor/order', 'refresh');
                }  
                else
                {
                    $this->session->set_flashdata('message', 'Record with specified id does not exist');
                    redirect(BASE_URL.'/vendor/order', 'refresh');
                }
        }
    }

    
    function delete()
    {
            Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
            if($this->input->post('deleteid')):
                    $this->Vendor_order_model->removeOrder($this->input->post('deleteid'));
                    $this->session->set_flashdata('success', 'Record Deleted Succesfully.');
                    redirect(BASE_URL.'/vendor/order');
            else:
                    $delete_id = base64_decode($this->uri->segment(4));
                    $this->data['form']=$this->Vendor_order_model->getOrderById($delete_id);
                    $this->load->view('vendor/order_delete.php', $this->data );
            endif;
    }
    function view()
    {
            Vendorcontrol_helper::is_logged_in($this->session->userdata('email'));
            $order_id = base64_decode($this->uri->segment(4));
    		$order_data = $this->Vendor_order_model->getOrderById($order_id);
    		$this->data['order_product_data'] = $this->Vendor_order_model->get_order_product_data($order_id);
            // echo "<pre>";print_r($this->data['order_product_data']);die;
    		if(count($order_data) > 0) {
                $this->data['company'] = $this->Vendor_order_model->get_enabled_company();
        		$this->data['order'] = $order_data;
                $this->data['header'] = $this->load->view('vendor/header', $this->data, true);
        		$this->data['footer'] = $this->load->view('vendor/footer', '', true);
        		$this->load->view('vendor/order_view', $this->data);
           }
		else {
		     $this->session->set_flashdata('message', 'Record with specified id does not exist');
                     redirect(BASE_URL.'/vendor/order', 'refresh');
		}
    }
    function change_status()
    {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('status', 'status', 'required');
                
                if ($this->form_validation->run() == TRUE) {

                    $status = $this->input->post('status');
                    $orderid = $this->input->post('orderid');

                    $data = array(
                        'order_status' => $status,
                    );
                    
                    if ($this->Vendor_order_model->change_order_status($data, (int) $orderid)) {
                        $this->session->set_flashdata('success', 'Status updated successfully.');
                        redirect(BASE_URL.'/vendor/order', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', 'Something went wrong. Please try again');
                        redirect(BASE_URL.'/vendor/order', 'refresh');
                    }
                } 
                else {
                    redirect(BASE_URL.'/vendor/order', 'refresh');
                }
    }
}
