<?php echo $header; ?>


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-10">
                <h1 class="page-header">
                    <?php echo $this->lang->line('project_header'); ?>
                </h1>
            </div>
            <div class="col-md-2">
                    <div class="page-header pull-right">
                        <a href="<?php echo BASE_URL; ?>/vendor/project/new" class="btn btn-small btn-primary"><i class="fa fa-plus"></i> Add</a> 
                        </div>
                         <a style="display:none;" href='<?php echo BASE_URL."/vendor/project/pdf/".base64_encode($vpdfid); ?>'  id='pdfdata'><i class="fa fa-fw fa-file-text-o"></i></a>
                    </div>
            </div>
        </div>
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/vendor"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                <i class="fa  fa-file-text-o"></i>
                	<?php echo $this->lang->line('project_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="container-fluid">
  	<div class="row">
      	<div class="col-md-12">
        <?php
            if ($this->session->flashdata('message')) {
                ?>
                <!--  start message-red -->
                <div class="box-body">
                    <div class=" alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <!--  end message-red -->
            <?php } ?>
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <!--  start message-green -->
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <!--  end message-green -->
            <?php } ?>
              <table class="table table-striped table-bordered" id="record_table">
                <thead>
                  <tr>
<!--                    <th> <?php echo $this->lang->line('company_name'); ?> </th>-->
                    <th> <?php echo $this->lang->line('po_name'); ?> </th>
                    <th> <?php echo $this->lang->line('po_number'); ?> </th>
                    <th> <?php echo $this->lang->line('company_city'); ?> </th>
                    <!--<th> <?php // echo $this->lang->line('po_status'); ?> </th>-->
                    <?php if(($this->Default_vendor_model->check_permission('project_edit')) || ($this->Default_vendor_model->check_permission('project_view')) || ($this->Default_vendor_model->check_permission('project_delete'))) {  ?>
                    <th class="td-actions"> <?php echo $this->lang->line('po_action'); ?> </th>
                    <?php } ?>
                  </tr>
                </thead>
                
              </table>
              
        	</div>
      </div>
 </div>
<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Change Status</h4>
            </div>
            <form action="<?php echo base_url("vendor/project/change_status"); ?>" method="post">
                <div class="modal-body" id="new_radio" style="height: 75px;">


                </div>
                <input type="hidden" id="companyid" name="projectid" value="">
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>    

        </div>
    </div>
</div>
<?php echo $footer; ?>

<script>
    $('#record_table').on('click', '.mystatus', function(){
        
        var id = $(this).data("id");
        var newstatus = $(this).attr("data-val");
        //$('.prettycheckbox input').prop('checked', false);
        $('#new_radio').html("");
        var html = '<div class="col-md-12">';
        html += '<div class="col-md-3"><input type="radio"  name="status" class="myradio" id="Pending"  value="Pending"> Pending </div>';
        html += '<div class="col-md-3"><input type="radio" name="status" class="myradio" id="Approved" value="Approved"> Approved</div>';
        html += '<div class="col-md-3"><input type="radio" name="status" class="myradio" id="Completed" value="Completed"> Completed</div>';
        html += '<div class="col-md-3"><input type="radio" name="status" class="myradio" id="Canceled" value="Canceled"> Canceled </div>';
        html += '<br>';
        $('#new_radio').html(html);
        $("#companyid").val(id);
        $('#' + newstatus).attr('checked', true);
        $("#modal_status").modal();

    });
   
        $(document).ready(function() {

             var data = '<?php echo $vpdfid; ?>';
            if(data != '') {
              
                $('#pdfdata')[0].click();
            }

				var dataTable = $('#record_table').DataTable( {
					"processing": true,
					"serverSide": true,
                                        "aaSorting": [],
                                        "aoColumnDefs": [
                                            {
                                               bSortable: false,
                                               aTargets: [ -1,-2 ]
                                            }
                                          ],
					"ajax":{
						url :"<?php echo base_url().'vendor/project/getalldata' ?>", // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".record_table-error").html("");
							$("#record_table").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
							$("#record_table_processing").css("display","none");
							
						}
					},
				});
    });
</script>