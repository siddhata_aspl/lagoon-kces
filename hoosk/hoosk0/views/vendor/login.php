<?php echo $header; ?>
<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading" style="background-color: #ecf0f1 !important; ">
                        <img src="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteLogo']; ?>" class="login_logo" />
                    </div>
                    <div class="panel-body">
                    <?php if (isset($error)){
					if ($error == "1"){
						echo "<div class='alert alert-danger'>".$this->lang->line('login_incorrect')."</div>";
					}
				} ?>
                        <?php echo form_open(BASE_URL.'/vendor/login/check'); ?>
                            <fieldset>
                                <div class="form-group">
                                    <label for="email"><?php echo $this->lang->line('login_email'); ?><span style="color:red"><sup>*</sup></span></label>
					<?php 	$data = array(
                                              'name'        => 'email',
                                              'id'          => 'email',
                                              'required'    => 'required',                                      
                                              'class'       => 'form-control',
                                              'value'           => set_value('email'),
                                              'placeholder'     => $this->lang->line('login_email')
                                            );

                                            echo form_input($data); ?>
                                </div>
                                <div class="form-group">
                                    <label for="password"><?php echo $this->lang->line('login_password'); ?><span style="color:red"><sup>*</sup></span></label>
					<?php 	$data = array(
                                          'name'        => 'password',
                                          'id'          => 'password',
                                          'required'    => 'required',                                   
                                          'class'       => 'form-control',
                                          'value'           => set_value('password'),
                                          'placeholder'     => $this->lang->line('login_password')
                                        );

                                        echo form_password($data); ?>
                                </div>

                                <div class="form-group">
                                    <input style="background: #2d2e74;border: #2d2e74;" type="submit" class="btn btn-success btn-block" value="<?php echo $this->lang->line('login_signin'); ?>">
                                </div>
                                <hr>
                                <a style="color:#2d2e74;" href="<?php echo BASE_URL; ?>/vendor/vendor/forgot"><?php echo $this->lang->line('login_forgot'); ?></a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- jQuery -->
    <script src="<?php echo ADMIN_THEME; ?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo ADMIN_THEME; ?>/js/bootstrap.min.js"></script>

</body>

</html>
