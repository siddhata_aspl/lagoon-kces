<div style="margin-right: auto; margin-left: auto;box-sizing: border-box;display: block;">
        <div style="box-sizing: border-box; display: block; border:0.5px solid #2c3e50;">
            <div style="float: left;position: relative; min-height: 1px; box-sizing: border-box;display: block;">
                
                <div style="height:10px;background-color:#ecf0f1;width:100%;"><h3 style="text-align:center;font-size:20px;">Project</h3></div>
                <div style="padding-left: 15px;padding-right: 15px;">
                <div>
                    <h4 style="color: #302a75;margin-top: 10.5px;margin-bottom: 0px;font-weight: bold;font-size:20px;font-family: Lato, "Helvetica Neue", Helvetica, Arial, sans-serif;line-height: 1.1;">PROJECT INFORMATION</h4>
                                        <form style="margin-top: 0px;">
                        <h4 style="display: inline-block;max-width: 100%;font-weight: 700;cursor: default;font-size: 16px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">NAME</h4>
        <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                        <label style="border-width: 2px;box-shadow: none;
width: 97.5%;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: -14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_name%</label>
    </div>
                        <div style="float:left; width:50%;">
                        <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">NUMBER</h4>
                        <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                            <label style="border-width: 2px;box-shadow: none;
width: 475px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_number%</label>
                            
                         </div>
                         </div>
                         <div style="float:left;width:50%;">
                         
                                <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">MANAGER</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                                    <label style="border-width: 2px;box-shadow: none;
width: 465px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%company_name%</label>
                                    
                                
                            </div>
                         </div>
                         <div style="float: left;width:33%;">
                            <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">ADDRESS</h4>
                            <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 310px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%address%</label>
                          </div>
                        </div>
                         <div style="float: left;width:33%;">
                            <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">CITY</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 310px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%city%</label>
                          </div>
                        </div>
                        <div style="float: left;width:33%;">
                             <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">ST</h4>
                               <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 310px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_state%</label>
                          </div>
                        </div>
                    </form>
                    <br>
                        <div style="height:30px;">
                    <hr style="clear: both;border-top: 1px solid black;">
                        </div><br><br>
                </div> 

                <div>
                    <h4 style="color: #302a75;margin-top: 10.5px;margin-bottom: 0px;font-weight: bold;font-size:20px;font-family: Lato, "Helvetica Neue", Helvetica, Arial, sans-serif;line-height: 1.1;">PROJECT SITE CONTACT</h4>
                     <div style="float: left;width:50%;">
                             <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">NAME</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">                    
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 470px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_site_contact_name%</label>
                              </div>
                            </div>
                         <div style="float: left;width:50%;">
                             <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">EMAIL</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 470px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_site_contact_email%</label>
                          </div>
                        </div> <br>
                        <div style="float: left;width:50%;">
                             <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">PHONE OFFICE</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 470px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_site_contact_office_phone%</label>
                          </div>
                        </div>
                         <div style="float: left;width:50%;">
                            <h4 style="display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;cursor: default;font-size: 17px;
    line-height: 1.42857143;color: #2c3e50;font-weight:bold;">PHONE CELL</h4>
                                <div style="box-sizing: border-box;line-height: 1.1;display: block;">
                             <label height="40" style="border-width: 2px;box-shadow: none;
width: 470px;height: 40px;padding: 5px 8px;background-color: #fff;border: 1px solid #dce4ec;border-radius: 14px;transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;font-size: 25px;
    line-height: 1.42857143;color: #2c3e50;display: block;background-image: none;font-family: inherit;font: inherit;margin: 0;-webkit-appearance: textfield;-webkit-rtl-ordering: logical;
    cursor: text;text-rendering: auto;letter-spacing: normal;word-spacing: normal;text-transform: none;text-indent: 0px;text-shadow: none;text-align: start;-webkit-writing-mode: horizontal-tb !important;">%po_site_contact_cell_phone%</label>
                          </div>
                        </div> 
                       <br>
                        <!-- <div style="height:30px;">
                        <hr style="clear: both;border-top: 1px solid black;">
                        </div> --><br><br><br><br>


                </div>
<formfeed>
                    <div>
                                            
                        <h4 style="color: #302a75;margin-top: 10.5px;margin-bottom: 14px;font-size:20px;float:left;font-weight: bold;">PRODUCTS</h4>
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data" style="margin-bottom: 13px;">
                        <div id="product_copy" class="hidden">
                           %products_block%
                     
                        </div>
                        <br>
                       <!--  <div style="height:50px;">
                        <hr style="clear: both;border-top: 3px solid black;position: relative;min-height:1px;">
                        </div> -->
                            <div>
                        <!--<h4 style="color: #302a75;margin-top: 10.5px;margin-bottom: 14px;font-size:20px;float:left;font-weight: bold;">CUSTOM ITEMS</h4>-->
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data" style="margin-bottom: 13px;">
<!--                        <div id="product_copy" class="hidden">
                            %customs%
                       
                        </div>-->

                        <br>
                        <br>
                       <!--  <div style="height:50px;">
                        <hr style="clear: both;border-top: 4px solid black;position: relative;min-height:1px;">
                        </div> -->

<!-- <formfeed> -->

                    <div class="col-md-12 nopadding" style="margin-top: 15px;width:100%;">
                        <div class="table-responsive col-md-offset-7" style="margin-left: 58.33333333%;">
                            <table class="table table-striped" id="invoice" style="text-align: right;width: 100%;max-width: 100%;margin-bottom: 21px;background-color: transparent;border-collapse: collapse;border-spacing: 0;">
                                <tbody style="display: table-row-group;vertical-align: middle;border-color: inherit;width: 50%;"><tr style="background-color: #f9f9f9;display: table-row;
    vertical-align: inherit;border-color: inherit;box-sizing: border-box;">

                                    <th style="float: right;text-align: right; font-weight:bold;color:#2c3e50;">PRODUCTS TOTAL </th>
                                    <th style="font-weight:bold;color:#2c3e50;">$</th>
                                    <td class="total_show hidden"></td>
                                    <td class="total_show1">%product_total%</td>
                                </tr>
                                <tr>
                                    <th style="float: right;text-align: right;font-weight:bold;color:#2c3e50;">PRODUCTS OVERALL ITEM TOTAL </th>
                                    <th style="font-weight:bold;color:#2c3e50;">$</th>
                                    <td class="total_show_remaining hidden"></td>
                                    <td class="total_show_remaining1">%product_total%</td>
                                </tr>
                               <tr style="background-color: #f9f9f9;display: table-row;
    vertical-align: inherit;border-color: inherit;box-sizing: border-box;">
                                    <th style="float: right;text-align: right;font-weight:bold;color:#2c3e50;">PRODUCTS TOTAL REMAINING </th>
                                    <th style="font-weight:bold;color:#2c3e50;">$</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1">%remaining_total%</td>
                                </tr>
                            
                            </tbody></table>
                        </div>
                    </div>
                   
                    </div>
            </div>
        </div>  

    </div>