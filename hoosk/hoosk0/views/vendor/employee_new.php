<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('employee_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-users"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor/employees"><?php echo $this->lang->line('employee_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('employee_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('employee_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
            <?php echo form_open_multipart(BASE_URL.'/vendor/employees/new/add'); ?>
             <div class="col-md-6 ">
                 <div class="form-group">       
                <?php echo form_error('emp_firstname', '<div class="alert alert-danger">', '</div>'); ?>                                    
                    <label class="control-label" for="emp_firstname"><?php echo $this->lang->line('employee_firstname'); ?></label>
                    <div class="controls">
                    <?php   $data = array(
                          'name'        => 'emp_firstname',
                          'id'          => 'emp_firstname',
                          'class'       => 'form-control',
                          'value'       => set_value('emp_firstname', '', FALSE)
                        );
            
                        echo form_input($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->

                 <!-- <div class="form-group">       
                <?php //echo form_error('employeename', '<div class="alert alert-danger">', '</div>'); ?>                                 
                    <label class="control-label" for="employeename"><?php //echo $this->lang->line('employee_name'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                    // <?php   $data //= array(
                    //       'name'        => 'employeename',
                    //       'id'          => 'employeename',
                    //       'class'       => 'form-control',
                    //       'value'       => set_value('employeename', '', FALSE)
                        //);
            
                        //echo form_input($data); ?>

                        <p class="help-block"><?php //echo $this->lang->line('employee_new_message'); ?></p>
                    </div>  /controls -->               
                <!-- </div> --> <!-- /form-group -->
                
<!--                <div class="form-group">        
                <?php //echo form_error('password', '<div class="alert alert-danger">', '</div>'); ?>                                   
                    <label class="control-label" for="password"><?php //echo $this->lang->line('employee_new_pass'); ?></label>
                    <div class="controls">
                        <?php   
//                                                $data = array(
//                        'name'        => 'password',
//                        'id'          => 'password',
//                        'class'       => 'form-control',
//                        'value'       => set_value('password', '', FALSE)
//                      );
//          
//                      echo form_password($data); 
                                                ?>
                    </div>  /controls               
                </div>  /form-group   -->

                <div class="form-group">       
                <?php echo form_error('emp_email', '<div class="alert alert-danger">', '</div>'); ?>                                    
                    <label class="control-label" for="emp_email"><?php echo $this->lang->line('employee_email'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                         <?php  $data = array(
                          'name'        => 'emp_email',
                          'id'          => 'emp_email',
                          'class'       => 'form-control',
                          'value'       => set_value('emp_email', '', FALSE)
                        );
            
                        echo form_input($data); ?>
                                            <!--<p class="help-block">&nbsp;</p>-->
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                              
             </div>
             
             <div class="col-md-6">
                 <div class="form-group">       
                <?php echo form_error('emp_lastname', '<div class="alert alert-danger">', '</div>'); ?>                                 
                    <label class="control-label" for="emp_lastname"><?php echo $this->lang->line('employee_lastname'); ?></label>
                    <div class="controls">
                    <?php   $data = array(
                          'name'        => 'emp_lastname',
                          'id'          => 'emp_lastname',
                          'class'       => 'form-control',
                          'value'       => set_value('emp_lastname', '', FALSE)
                        );
            
                        echo form_input($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->

                <div class="form-group">
                    <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
                    <label class="control-label" for="file_upload"><?php echo $this->lang->line('employee_profilepic'); ?></label>
                                        
                    <div class="controls">
                        <?php
//                          $data = array(
//                              'name'      => 'file_upload',
//                              'id'        => 'file_upload',
//                              'class'     => 'form-control'
//                          );
//                          echo form_upload($data);
                        ?>
                        <!--<input type="hidden" id="postImage" name="file_upload" />-->
                    <input type="file" name="file_upload" value=""/>
                    </div> <!-- /controls -->
                </div> <!-- /form-group -->  
                 
                <!-- <div class="form-group">        
                <?php //echo form_error('phone', '<div class="alert alert-danger">', '</div>'); ?>                                    
                    <label class="control-label" for="phone"><?php //echo $this->lang->line('employee_phone'); ?></label>
                    <div class="controls">
                         <?php  $data //= array(
                          // 'name'        => 'phone',
                          // 'id'          => 'phone',
                          // 'class'       => 'form-control',
                          // 'value'       => set_value('phone', '', FALSE)
                        //);
            
                        //echo form_input($data); ?>
                            <p class="help-block">&nbsp;</p>
                    </div> --> <!-- /controls -->               
                <!-- </div> --> <!-- /form-group -->
<!--                <div class="form-group">    
                <?php //echo form_error('con_password', '<div class="alert alert-danger">', '</div>'); ?>                                   
                    <label class="control-label" for="con_password"><?php //echo $this->lang->line('employee_new_confirm'); ?></label>
                    <div class="controls">
                        <?php   
//                                                $data = array(
//                        'name'        => 'con_password',
//                        'id'          => 'con_password',
//                        'class'       => 'form-control',
//                        'value'       => set_value('con_password', '', FALSE)
//                      );
//          
//                      echo form_password($data); 
                                                ?>
                    </div>  /controls               
                </div>  /form-group -->
             </div>
                
                </div>
            
            
            <div class="panel-body">    
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <b><?php echo $this->lang->line('employee_permission'); ?></b>
                        </h3>
                    </div>
            </div>
            
            <style>
                th {
                    padding: 0 30px !important;
                    text-align: center;
                }
                td {
                    text-align: center;
                }                
            </style>
            
            
            <div class="panel-body"> 
                
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th><?php echo $this->lang->line('permission_add'); ?></th>
                            <th><?php echo $this->lang->line('permission_edit'); ?></th>
                            <th><?php echo $this->lang->line('permission_view'); ?></th>
                            <th><?php echo $this->lang->line('permission_delete'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th><?php echo $this->lang->line('permission_employee'); ?></th>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'employees_new',
                                          'id'          => 'employees_new',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('employee_new', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>  
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'employees_edit',
                                          'id'          => 'employees_edit',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('employee_edit', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'employees_view',
                                          'id'          => 'employees_view',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('employee_view', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'employees_delete',
                                          'id'          => 'employees_delete',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('employee_delete', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo $this->lang->line('permission_project'); ?></th>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'project_new',
                                          'id'          => 'project_new',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('project_new', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'project_edit',
                                          'id'          => 'project_edit',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('project_edit', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'project_view',
                                          'id'          => 'project_view',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('project_view', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'project_delete',
                                          'id'          => 'project_delete',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('project_delete', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <th><?php echo $this->lang->line('permission_order'); ?></th>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'order_new',
                                          'id'          => 'order_new',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('order_new', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'order_edit',
                                          'id'          => 'order_edit',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('order_edit', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'order_view',
                                          'id'          => 'order_view',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('order_view', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                            <td>
                                <div class="form-group">                                   
                                    <div class="controls">
                                        <?php   $data = array(
                                          'name'        => 'order_delete',
                                          'id'          => 'order_delete',
                                          'class'       => 'form-control',
                                          'style'       => 'height:22px; margin: 20px 0;',
                                          'value'       => set_value('order_delete', 'True', FALSE)
                                        );

                                        echo form_checkbox($data); ?>
                                    </div>                
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table><br /><br />
        
            </div>
            
            
                <div class="panel-footer">
                <?php   $data = array(
                          'name'        => 'submit',
                          'id'          => 'submit',
                          'class'       => 'btn btn-primary',
                          'value'       => $this->lang->line('btn_save'),
                        );
                     echo form_submit($data); ?> 
                    <a class="btn" href="<?php echo BASE_URL; ?>/vendor/employees"><?php echo $this->lang->line('btn_cancel'); ?></a>
                </div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>