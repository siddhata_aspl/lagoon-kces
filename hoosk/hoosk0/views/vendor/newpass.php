<?php echo $header; ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading" style="background-color: #ecf0f1 !important; ">
                        <img src="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteLogo']; ?>" class="login_logo" />
                    </div>
                <div class="panel-body">
                    <h3><?php echo $this->lang->line('register_complete'); ?></h3>
                </div>
                <div class="panel-footer">
                    <a class="button btn btn-success" href="<?php echo BASE_URL; ?>/vendor/login"><?php echo $this->lang->line('login_signin'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>