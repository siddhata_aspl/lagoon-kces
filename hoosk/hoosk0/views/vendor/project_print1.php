<link href="<?php echo ADMIN_THEME; ?>/css/bootstrap.min.css" rel="stylesheet"  media="print">
<link href="<?php echo ADMIN_THEME; ?>/css/sb-admin.css" rel="stylesheet" media="print">
<link href="<?php echo ADMIN_THEME; ?>/css/jquery.fancybox-1.3.4.css" rel="stylesheet" media="print">
<link href="<?php echo ADMIN_THEME; ?>/css/jquery.typeahead.css" rel="stylesheet" media="print">
<link href="<?php echo ADMIN_THEME; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="print">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.bootstrap.min.css" rel="stylesheet" media="print">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.fontAwesome.css" rel="stylesheet" media="print">
<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        Project
                    </h1>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-pencil fa-fw"></i>
                                 Project Summery
                            </h3>
                        </div>
                        <div class="panel-body">                    
                            <h4 style="color: blue;">Project Information</h4>
                            <div class="form-group">		
                                <label class="control-label" for="name">Name</label>
                                <div class="controls">
                                           %po_name%
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            <div class="col-md-12 nopadding">
                                <div class="col-md-5 nopadding" style="width: 40%;display: inline-block;">              
                                    <div class="form-group">										
                                        <label class="control-label" for="name">Number</span></label>
                                        <div class="controls">
                                            %po_number%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-7" style="padding-right: 0px;width: 60%;display: inline-block;">              
                                    <div class="form-group">	
                                        <label class="control-label" for="name">Manager</label>
                                        <div class="controls">
                                           %company_name%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>   
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">		
                                        <label class="control-label" for="name">Address</label>
                                        <div class="controls">
                                            %address%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-4" style="padding-right: 0px">              
                                    <div class="form-group">		
                                        <label class="control-label" for="name">City</label>
                                        <div class="controls">
                                            %city%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-2" style="padding-right: 0px">              
                                    <div class="form-group">		
                                        <label class="control-label" for="name">Age</label>
                                        <div class="controls">
                                            %po_age%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>

                            <br>
                            <hr>
                            
                            <h4 style="color: blue;">Project Site Contact</h4>
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">
                                        <label class="control-label" for="po_site_name">Name</label>
                                        <div class="controls">
                                            %po_site_contact_name%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">              
                                    <div class="form-group">		
                                        <label class="control-label" for="po_site_email">Email</label>
                                        <div class="controls">
                                            %po_site_contact_email%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>
                            <div class="col-md-12 nopadding">
                                <div class="col-md-6 nopadding">              
                                    <div class="form-group">		
                                        <label class="control-label" for="po_office_phone">Office Phone</label>
                                        <div class="controls">
                                            %po_site_contact_office_phone%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                                <div class="col-md-6" style="padding-right: 0px">              
                                    <div class="form-group">		
                                        <label class="control-label" for="po_site_email">Cell Phone</label>
                                        <div class="controls">
                                            %po_site_contact_cell_phone%
                                        </div> <!-- /controls -->				
                                    </div> <!-- /form-group -->                
                                </div>
                            </div>
                            <br>
                            <hr>
                            <pagebreak></pagebreak>
                            <h4 style="color: blue;">Projects</h4>
                            <div class="col-md-12 nopadding" id="product_data">
                                <div id="copy_product">
                                    %products_block%
                                </div>
                            </div>
                            <br>
                            <br>
                            <hr>
                            <h4 style="color: blue;">Custom Items</h4>
                            <div class="col-md-12 nopadding" id="custom_data">
                                <div id="custom_copy" class="hidden">

                                </div>
                                <div id="set_copy">
                                    %customs%
                                </div>

                            </div>
                            <div class="col-md-12 nopadding">
                                <div class="table-responsive col-md-offset-6">
                                    <table class="table table-striped" id="invoice"  style="text-align: right;">
                                        <tr>
                                            <th>PRODUCTS TOTAL $</th>
                                            <td class="total_show">%product_total%</td>
                                        </tr>
                                        <tr>
                                            <th>CUSTOM ITEM TOTAL $</th>
                                            <td class="total_custom_show">%custom_total%</td>
                                        </tr>
                                        <tr>
                                            <th>PROJECT OVERALL  ITEM TOTAL $</th>
                                            <td class="total_custom_show">%t_total%</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            </div>



                    </div>
                </div>
            </div>
        </div>
    <style>
    #country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
    #country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
    #country-list li:hover{background:#ece3d2;cursor: pointer;}
    </style>
    </div>
</div>