<?php echo $header; ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading" style="background-color: #ecf0f1 !important; ">
                        <img src="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteLogo']; ?>" class="login_logo" />
                    </div>
                <div class="panel-body">
                <h3><?php echo $this->lang->line('forgot_reset'); ?></h3>
			<?php echo form_open(BASE_URL.'/vendor/vendor/forgot'); ?>
      		<div class="form-group">		
                <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="email"><?php echo $this->lang->line('forgot_email'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'email',
						  'id'          => 'email',
						  'class'       => 'form-control',
                                                  'required' => 'required',
						  'value'		=> set_value('email')
						);
			
						echo form_input($data); ?>

					</div> <!-- /controls -->				
				</div> <!-- /control-group -->        
                </div>
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('forgot_btn'),
						  'style'       => 'background: #2d2e74;border: #2d2e74;'
						);
					 echo form_submit($data); ?> 
                    <a class="button btn btn-success pull-right" href="<?php echo BASE_URL; ?>/vendor/login"><?php echo $this->lang->line('login_signin'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
                                
        	</div>
        </div>
    </div>
</div>