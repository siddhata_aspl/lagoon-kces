<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo SITE_NAME; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?php echo ADMIN_THEME; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/sb-admin.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.fontAwesome.css" rel="stylesheet">
<script src="<?php echo ADMIN_THEME; ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery-ui-1.9.2.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.nestable.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/excanvas.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/bootstrap.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/base.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/autoNumeric.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<link rel="icon" href="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteFavicon']; ?>" />
</head>
<body>
    <style>
        .mystatus
        {
            width:105px;
        }
        #page-wrapper
        {
            margin-bottom:50px;
        }
        .nopadding
        {
            padding: 0 !important;
        }
        .nomargin
        {
            margin: 0 !important;
        }
        .bg-blue
        {
            background-color: #337ab7 !important;
        }
        .side-nav{
                   background: #272973 !important;
               }
               .navbar-inverse
               {
                   background: #272973 !important;
               }
               .side-nav > li > ul{
                       background: #272973 !important;
               }
             .side-nav > li > ul > li.active{
                   background: #39b098b3 !important;
               }
               .btn-primary
               {
                   background: #272973 !important;
               }
               .footer{
                  background: #272973 !important;
                }
                .side-nav li a:hover
                {
                    background: #15a589 !important;
                }
                .logo-icon {
                   height: 36px;
                   width: 170px;
                }
                .side-nav li a:focus
                {
                    background: #272973 !important;
                }
                .top-nav > li > a:hover
                {
                background: #302a75 !important;
                }
    </style>
<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a class="navbar-brand" href="<?php echo BASE_URL;?>/vendor"><img src="<?php echo BASE_URL; ?>/images/mainlogo.jpg" class="logo-icon" alt="KCES"></a>
                <!--<a class="navbar-brand" href="<?php echo BASE_URL;?>/admin"><img src="" class="logo-icon" alt="KCES"></a>-->
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
             	<!--<li>-->
              <!--      <a href="<?php echo BASE_URL; ?>" target="_blank"><i class="fa fa-home"></i></a>-->
              <!--  </li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('first_name'); ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo BASE_URL ; ?>/vendor/change_password"><i class="fa fa-fw fa-edit"></i> <?php echo $this->lang->line('nav_changepass'); ?></a>                        </li>
                        <li>
                            <a href="<?php echo BASE_URL ; ?>/vendor/logout"><i class="fa fa-fw fa-power-off"></i> <?php echo $this->lang->line('nav_logout'); ?></a>                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php if ($current == "") { echo "active"; } ?>">
                    	<a href="<?php echo BASE_URL; ?>/vendor"><i class="fa fa-dashboard"></i> <span><?php echo $this->lang->line('nav_dash'); ?></span> </a>
                    </li>
					

                    <?php if(($this->Default_vendor_model->check_permission('employees_new')) || ($this->Default_vendor_model->check_permission('employees_edit')) || ($this->Default_vendor_model->check_permission('employees_view')) || ($this->Default_vendor_model->check_permission('employees_delete'))) {  ?> 
        <li class="dropdown <?php if ($current == "employees") { echo "active"; } ?>">
        <a href="javascript:;" data-toggle="collapse" data-target="#employeesmenu"> <i class="fa fa-users"></i> <span><?php echo $this->lang->line('nav_employees'); ?></span> <b class="caret"></b></a>
          <ul id="employeesmenu" class="collapse<?php if ($current == "employees") { echo " in"; } ?>">
            <?php if(($this->Default_vendor_model->check_permission('employees_new')) || ($this->Default_vendor_model->check_permission('employees_edit')) || ($this->Default_vendor_model->check_permission('employees_view')) || ($this->Default_vendor_model->check_permission('employees_delete'))) {  ?> 
              <li<?php if (($current == "employees")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/employees"><?php echo $this->lang->line('nav_employees_all'); ?></a></li>
            <?php } ?>
            <?php if($this->Default_vendor_model->check_permission('employees_new')) {  ?> 
              <li<?php if (($current == "employees")&&($this->uri->segment(3)=="new")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/employees/new"><?php echo $this->lang->line('nav_employees_new'); ?></a></li>
            <?php } ?>
          </ul>
        </li>
   <?php } ?>
        
        
   <?php if(($this->Default_vendor_model->check_permission('project_new')) || ($this->Default_vendor_model->check_permission('project_edit')) || ($this->Default_vendor_model->check_permission('project_view')) || ($this->Default_vendor_model->check_permission('project_delete'))) {  ?>     
        <li class="dropdown <?php if ($current == "project") { echo "active"; } ?>">
        <a href="javascript:;" data-toggle="collapse" data-target="#projectmenu"> <i class="fa  fa-file-text-o" style="font-size: 17px;"></i> <span><?php echo $this->lang->line('nav_project'); ?></span> <b class="caret"></b></a>
          <ul id="projectmenu" class="collapse<?php if ($current == "project") { echo " in"; } ?>">
            <?php if(($this->Default_vendor_model->check_permission('project_new')) || ($this->Default_vendor_model->check_permission('project_edit')) || ($this->Default_vendor_model->check_permission('project_view')) || ($this->Default_vendor_model->check_permission('project_delete'))) {  ?>
              <li <?php if (($current == "project")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/project"><?php echo $this->lang->line('nav_project_all'); ?></a></li>
            <?php } ?>
            <?php if($this->Default_vendor_model->check_permission('project_new')) {  ?> 
              <li<?php if (($current == "project")&&($this->uri->segment(3)=="new")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/project/new"><?php echo $this->lang->line('nav_project_new'); ?></a></li>
            <?php } ?>
          </ul>
        </li>
   <?php } ?>
        
    
   <?php //if(($this->Default_vendor_model->check_permission('order_new')) || ($this->Default_vendor_model->check_permission('order_edit')) || ($this->Default_vendor_model->check_permission('order_view')) || ($this->Default_vendor_model->check_permission('order_delete'))) {  ?>
   <?php if(FALSE) {  ?>
        <li class="dropdown <?php if ($current == "order") { echo "active"; } ?>">
        <a href="javascript:;" data-toggle="collapse" data-target="#ordermenu"> <i class="fa fa-shopping-cart" style="font-size: 18px;"></i> <span><?php echo $this->lang->line('nav_order'); ?></span> <b class="caret"></b></a>
          <ul id="ordermenu" class="collapse<?php if ($current == "order") { echo " in"; } ?>">
            <?php if(($this->Default_vendor_model->check_permission('order_new')) || ($this->Default_vendor_model->check_permission('order_edit')) || ($this->Default_vendor_model->check_permission('order_view')) || ($this->Default_vendor_model->check_permission('order_delete'))) {  ?>
              <li <?php if (($current == "order")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/order"><?php echo $this->lang->line('nav_order_all'); ?></a></li>
            <?php } ?>
            <?php if($this->Default_vendor_model->check_permission('order_new')) {  ?>
              <li<?php if (($current == "order")&&($this->uri->segment(3)=="new")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/vendor/order/new"><?php echo $this->lang->line('nav_order_new'); ?></a></li>
            <?php } ?>
          </ul>
        </li>
   <?php } ?>


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
