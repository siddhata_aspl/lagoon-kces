<?php echo $header; ?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
      <div class="span12">
          <center>
          <div class="widget">
            <div class="widget-header"> <i class="icon-user"></i>
              <h3>Sorry !</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<p>The URL you have entered is invalid, please check and try again.</p>
                
                <!-- /widget-content --> 
            </div>
          </div>
          <a class="button btn btn-success" href="<?php echo BASE_URL; ?>/admin/login"><?php echo $this->lang->line('login_signin'); ?></a>
          <!-- /widget -->
          </center> 
 
         
     </div>
      <!-- /span12 -->

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<?php echo $footer; ?>
