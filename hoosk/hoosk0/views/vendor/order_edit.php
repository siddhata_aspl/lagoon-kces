<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('order_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-shopping-cart"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor/order"><?php echo $this->lang->line('nav_order_all'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('order_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('order_edit_header'); ?>
                    </h3>
                </div>
                <?php
                    foreach ($order as $u) {
                ?>
                <form action="<?php echo BASE_URL . '/vendor/order/edited/'.$this->uri->segment(4); ?>" method="post" accept-charset="utf-8">
                <div class="panel-body"> 
                    
                    <div class="col-md-12 nopadding">
                        <div class="form-group">		
                            <?php echo form_error('order_po', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="name"><?php echo $this->lang->line('order_po'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls" id="setpo">
                                
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->
                    </div>
                    
<!--                    <div class="col-md-6" style="padding-left: 0px;">
                        <div class="form-group">		
                            <?php echo form_error('order_company', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="name"><?php echo $this->lang->line('order_company'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                    $data = array(
                                        'id'        => 'order_company',
                                        'class'     => 'form-control',
                                        'requeired' => 'requeired',
                                        'readonly'  => 'readonly',
                                        'value'     => set_value('order_company', $u['po_id']),
//                                        'onChange'  => 'getpos()',
                                    );

                                    foreach ($company as $value)
                                    {
                                        $option[$value['companyId']] = $value['company_name'];
                                    }

                                    echo form_dropdown('po_manager', $option,$u['po_company'],$data);
                                ?>
                            </div>  /controls 				
                        </div>  /form-group 
                    </div>-->
                    
                    <div class="total_div">
                    <h4 style="color: blue;">Project Information</h4>
                    
                    <div class="col-md-12 nopadding">
                        <div class="col-md-5 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_number', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_number'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'      => 'po_number',
                                        'id'        => 'po_number',
                                        'class'     => 'form-control numberic',
                                        'requeired' => 'requeired',
                                        'readonly' => 'readonly',
                                        'value'     => set_value('po_number', time())
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-7" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_adress', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_adress'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_adress',
                                        'id'    => 'po_adress',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->			
                            </div> <!-- /form-group -->                
                        </div>
                    </div>   
                    <div class="col-md-12 nopadding">
                        
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_city', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_city'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_city',
                                        'id'    => 'po_city',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_city', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_age', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_age'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_age',
                                        'id'    => 'po_age',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_age', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                    <input type="hidden" name="product_rows" id="product_rows" value="1">
                                    <input type="hidden" name="custom_rows" id="custom_rows" value="1">
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>

                    <br>
                    <hr>
                    <h4 style="color: blue;">Project Site Contact</h4>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_name', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_site_name"><?php echo $this->lang->line('po_site_name'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_name',
                                        'id'    => 'po_site_name',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_email', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_site_email'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_email',
                                        'id'    => 'po_site_email',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'type' => 'email',
                                        'value' => set_value('po_site_email', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_office_phone', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_office_phone"><?php echo $this->lang->line('po_office_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_office_phone',
                                        'id'    => 'po_office_phone',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_office_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_cell_phone', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_cell_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_cell_phone',
                                        'id'    => 'po_cell_phone',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_cell_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <br>
                    <hr>
                    <h4 style="color: blue;">Projects</h4>
                    <div class="col-md-12 nopadding" id="product_data">
                        
                        <div id="copy_product">
                            
                        </div>
                    </div>
                    <br>
                    <br>
                    <hr>
                    <h4 style="color: blue;">Custom Items</h4>
                    <div class="col-md-12 nopadding" id="custom_data">
                        <div id="set_copy">
                            
                        </div>                    
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="table-responsive col-md-offset-6">
                            <table class="table table-striped" id="invoice" style="text-align: right;">
                                <tr>
                                    <th>PRODUCTS TOTAL ($)</th>
                                    <td class="total_show"></td>
                                </tr>
                                <tr>
                                    <th>CUSTOM ITEM TOTAL ($)</th>
                                    <td class="total_custom_show"></td>
                                </tr>
                                <tr>
                                    <th>PRODUCTS ORDER TOTAL ($)</th>
                                    <td class="total_show_order"></td>
                                </tr>
                                <tr>
                                    <th>CUSTOM ORDER ITEM TOTAL ($)</th>
                                    <td class="total_custom_show_order"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    </div>
                    </div>
                <div class="panel-footer">
                    <div class="total_div" style="display: inline-block;">
                    <?php 	
                            $data = array(
                              'name'        => 'submit',
                              'id'          => 'submit',
                              'class'       => 'btn btn-primary',
                              'value'	=> $this->lang->line('btn_save'),
                            );
                            echo form_submit($data); 
                    ?>
                    </div>
                    <a class="btn" href="<?php echo BASE_URL; ?>/vendor/order"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    
                </div> <!-- /form-actions -->
            </form>
                <?php
                    }
                ?>

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
</style>
<script>
                    
                    $(document).on('change', '.order_custom_quanity', function(){

                            var custom_row_id = $(this).attr('id');
                            if(custom_row_id != '')
                            {
                                var tdid = $('#'+custom_row_id).closest('td').data("id");
                                var unit_price = $('#order_custom_price'+tdid).val();
                                var quantity = $('#order_custom_quanity'+tdid).val();
                                if(unit_price != '')
                                {
                                    var uprice1 = unit_price*quantity;
                                    $('#'+custom_row_id).closest('tr').find('#custom_row_total'+tdid).html(uprice1.toFixed(2));
                                }
                                getcustomtotal();
                            }   


                    });
                    $(document).on('change', '.order_product_quantity', function(){

                            var custom_row_id = $(this).attr('id');
                            if(custom_row_id != '')
                            {
                                var tdid = $('#'+custom_row_id).closest('td').data("id");
                                var unit_price = $('#order_product_price'+tdid).val();
                                var quantity = $('#order_product_quantity'+tdid).val();
                                if(unit_price != '')
                                {
                                    //console.log(unit_price);
                                    var uprice2 = unit_price*quantity;
                                    $('#'+custom_row_id).closest('tr').find('#product_row_total'+tdid).html(uprice2.toFixed(2));
                                }
                                gettotal();
                            }   
                            

                    });
                    $(document).ready(function () {

                        $(".numberic").keydown(function (e) {
                            // Allow: backspace, delete, tab, escape, enter and .
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                                 // Allow: Ctrl+A, Command+A
                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                                 // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                                     // let it happen, don't do anything
                                     return;
                            }
                            // Ensure that it is a number and stop the keypress
                            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                e.preventDefault();
                            }
                        });

                    });
        
        function gettotal()
        {
                var pro_total_all = 0;
                $('.product_row_total').each(function() {
                    if(parseInt($(this).html()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).html());
                    }
                });
                $('.total_show_order').html(pro_total_all.toFixed(2));
        }
        function getcustomtotal()
        {
                var pro_total_all = 0;
                $('.custom_row_total').each(function() {
                    if(parseInt($(this).html()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).html());
                    }
                });
                $('.total_custom_show_order').html(pro_total_all.toFixed(2));
        }
        getpos();
        function getpos()
        {
                var val = $('#order_company').val();
                     $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/order/getpoofmanager'; ?>',
                        data:'companyid='+val,
                        success: function(data){
                                data = JSON.parse(data);
                                var html = '<select name="order_po" readonly="readonly" id="order_po" class="form-control" requeired="requeired" disabled="disabled">';
                                html += '<option value="-1">Select</option>';
                                var selected = '';
                                $.each(data , function (key, val1) {
                                    //console.log(val['po_id']+val['po_name']);
                                    if(val1['po_id'] == <?php echo $order[0]['po_id']; ?>)
                                    {
                                        selected = 'selected';
                                    }
                                    else
                                    {
                                        selected = '';
                                    }
                                    html += '<option  value="'+val1['po_id']+'" '+selected+' >'+val1['po_name']+'</option>'
                                });
                                html += '</select>'
                                $('#setpo').html(html);
                                $('.total_div').addClass('hidden');
                                
                        }
                    });
                    
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/order/getidcompany'; ?>',
                        data:'companyid='+val,
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_adress").val(data[0]['address']);
                                $("#po_city").val(data[0]['city']);
                                
                        }
                    });
        }
        getpo_data(<?php echo $order[0]['po_id']; ?>);
        function getpo_data(val)
        {
                  if(val > 0)
                  {
                        $.ajax({
                           type: "POST",
                           url: '<?php echo base_url().'vendor/order/getpodata'; ?>',
                           data:'projectid='+val,
                           success: function(data){
                                   data = JSON.parse(data);
                                   console.log(data);
                                   console.log(data.project[0]['po_name']);
                                   $('#copy_product').html('');
                                   $('#set_copy').html('');
                                   if(data.project.length > 0)
                                   {
                                       $('#po_number').val(data.project[0]['po_number']);
                                       $('#po_age').val(data.project[0]['po_age']);
                                       $('#po_site_name').val(data.project[0]['po_site_contact_name']);
                                       $('#po_site_email').val(data.project[0]['po_site_contact_name']);
                                       $('#po_office_phone').val(data.project[0]['po_site_contact_office_phone']);
                                       $('#po_cell_phone').val(data.project[0]['po_site_contact_cell_phone']);
                                       $('.total_div').removeClass('hidden');
                                   }
                                   if(data.product.length > 0)
                                   {
                                          var project_block1 = "<input type='hidden' name='product_data' value='"+ data.product.length +"'>";
                                          $('#copy_product').html(project_block1);
                                          var prod_d =  data.product;
                                          var i = 1;
                                          var complete_total = 0;
                                          $.each(prod_d , function (prod_val) {
                                                project_block = "<table class='table row"+i+"' data-id='"+i+"'>";
                                                project_block +="<thead><tr class='info'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders.'></i> PROJECT QTY & PRICE</th><th class='col-md-2' colspan='2'><i class='fa fa-question-sign' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='ORDER QTY reflect the product quantity to be added to the cart.This quantity total will be reduced to 0,after an order is submitted.'></i>ORDER QTY </th><th class='col-md-2'><i class='fa fa-question-sign' data-toggle='tooltip' data-placement='bottom' title='' data-original-title='REMAINING reflects the product quantity that is left to order.This quantity will be reduced to 0, after all products are orderd.'></i> Project Total </th><th class='col-md-1'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart.'></i> TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
                                                project_block += "<td>"+data.product[prod_val]['product_name']+"</td><td>"+data.product[prod_val]['product_desc']+"</td><td>"+data.product[prod_val]['quantity']+"</td><td>"+data.product[prod_val]['product_unit_price']+"</td><td data-id='"+i+"'><input type='hidden' id='order_product_price"+i+"' name='order_product_price"+i+"' value='"+data.product[prod_val]['product_unit_price']+"'><input type='hidden' name='order_product"+i+"' value='"+data.product[prod_val]['po_product_id']+"' ><input type='number' class='form-control order_product_quantity prod' id='order_product_quantity"+i+"' name='order_product_quantity"+i+"' min='0' max='"+data.product[prod_val]['quantity']+"' class='form-control' name='order_quantity"+i+"' ></td><td></td><td>"+data.product[prod_val]['total']+"</td><td class='product_row_total' id='product_row_total"+i+"'></td></tr><tr><td colspan='8'><b>Comment </b>: "+data.product[prod_val]['comment']+"</td></tr></tbody></table>";
                                                project_block += "</table>";
                                                complete_total =parseFloat(complete_total) +parseFloat(data.product[prod_val]['total']);
                                                $('#copy_product').html($('#copy_product').html()+project_block);
                                                i++;
                                            }); 
                                            $('.total_show').html(complete_total);
//                                            project_block += "</table>";
//                                        var project_block = "<table class='table row1' data-id='1'><thead><tr class='info'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders.'></i> PROJECT QTY & PRICE</th><th class='col-md-1'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart.'></i> TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
//                                        project_block += "<td>".$u1['product_name']."</td><td>".$u1['product_desc']."</td><td>".$u1['quantity']."</td><td>".$u1['product_unit_price']."</td><td>".$u1['total']."</td></tr><tr><td colspan='5'><b>Comment </b>: ".$u1['comment']."</td></tr></tbody></table>";
                                          
                                   }
                                   else
                                   {
                                        $('#copy_product').html('<div class="col-md-12 emptypro"><center>No Data Found</center></div>');
                                   }
                                   if(data.custom.length > 0)
                                   {
                                            var project_block2 = "<input type='hidden' name='custom_data' value='"+ data.custom.length +"'>";
                                            $('#set_copy').html(project_block2);
                                            var prod_d =  data.custom;
                                            var j = 1;
                                            var complete_custom_total = 0;
                                            $.each(prod_d , function (prod_val) {
                                                project_block = '<table class="table customrow1">';
                                                project_block +='<thead><tr class="info"><th class="col-md-2">ITEM</th><th class="col-md-2"> DESCRIPTION</th><th class="col-md-1">QTY</th><th class="col-md-1">PRICE</th><th class="col-md-2" colspan="2"><i class="fa fa-question-sign" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="ORDER QTY reflect the product quantity to be added to the cart.This quantity total will be reduced to 0,after an order is submitted."></i> ORDER QTY </th><th class="col-md-2"><i class="fa fa-question-sign" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="REMAINING reflects the product quantity that is left to order.This quantity will be reduced to 0, after all products are orderd."></i> CUSTOM TOTAL </th><th class="col-md-1">TOTAL</th></tr></thead><tbody class="breadcrumb"><tr><td>'+data.custom[prod_val]["po_custom_title"]+'</td><td>'+ data.custom[prod_val]['po_custom_desc']+'</td><td>'+ data.custom[prod_val]['po_custom_quantity']+'</td><td>'+ data.custom[prod_val]['po_custom_price']+'</td><td data-id="'+j+'"><input type="hidden" id="order_custom_price'+j+'" name="order_custom_price'+j+'" value="'+data.custom[prod_val]['po_custom_price']+'" ><input type="hidden" name="order_custom'+j+'" value="'+data.custom[prod_val]['po_custom_id']+'" ><input class="form-control order_custom_quanity prod" type="number" id="order_custom_quanity'+j+'" name="order_custom_quanity'+j+'" min="0" max="'+ data.custom[prod_val]['po_custom_quantity']+'" ></td><td></td><td>'+ data.custom[prod_val]['total']+'</td><td class="custom_row_total" id="custom_row_total'+j+'"></td></tr><tr><td colspan="8"><b>Comment </b>: '+data.custom[prod_val]['comment']+'</td></tr></tbody>';
                                                project_block += "</table>";
                                                complete_custom_total =parseFloat(complete_custom_total) +parseFloat(data.custom[prod_val]['total']);
                                                $('#set_copy').html($('#set_copy').html()+project_block);
                                                j++
                                            });    
                                            $('.total_custom_show').html(complete_custom_total);
//                                        var project_block = "<table class='table row1' data-id='1'><thead><tr class='info'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders.'></i> PROJECT QTY & PRICE</th><th class='col-md-1'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart.'></i> TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
//                                        project_block += "<td>".$u1['product_name']."</td><td>".$u1['product_desc']."</td><td>".$u1['quantity']."</td><td>".$u1['product_unit_price']."</td><td>".$u1['total']."</td></tr><tr><td colspan='5'><b>Comment </b>: ".$u1['comment']."</td></tr></tbody></table>";
                                          
                                   }
                                   else
                                   {
                                        $('#set_copy').html('<div class="col-md-12 emptypro"><center>No Data Found</center></div>');
                                   }
                                    var iterateMe = <?=json_encode($order_product_data)?>;
                                    var order_pro = 0;
                                    $.each(iterateMe,function(index, value) {
                                        
                                        $('.prod').eq( order_pro ).val(value['quantity']);
                                      
                                        var custom_row_id = $('.prod').eq( order_pro ).attr('id');
                                        var tdid = $('#'+custom_row_id).closest('td').data("id");
                                        if(custom_row_id.indexOf('product') != -1)
                                        {
                                                
                                                var unit_price = $('#order_product_price'+tdid).val();
                                                var quantity = $('#order_product_quantity'+tdid).val();
                                                if(unit_price != '')
                                                {
                                                    //console.log(unit_price);
                                                    $('#'+custom_row_id).closest('tr').find('#product_row_total'+tdid).html(unit_price*quantity);
                                                }
                                                gettotal();
                                                
                                        }
                                        else
                                        {
                                                var unit_price = $('#order_custom_price'+tdid).val();
                                                var quantity = $('#order_custom_quanity'+tdid).val();
                                                if(unit_price != '')
                                                {
                                                    //console.log(unit_price);
                                                    $('#'+custom_row_id).closest('tr').find('#custom_row_total'+tdid).html(unit_price*quantity);
                                                }
                                                getcustomtotal();
                                                  
                                        }
                                        order_pro++;  
                                        
                                    });
                           }
                       });
                  }
        }
        //getpodata
    </script>
    