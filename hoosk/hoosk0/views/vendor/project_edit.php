<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('project_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa  fa-file-text-o"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor/project"><?php echo $this->lang->line('project_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('project_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('project_edit_header'); ?>
                    </h3>
                </div>
                <?php foreach ($project as $u) {
			 echo form_open_multipart(BASE_URL.'/vendor/project/edited/'.$this->uri->segment(4)); ?>
                <div class="panel-body">                    
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT INFORMATION</h4>
                    <div class="form-group">		
                        <?php echo form_error('po_name', '<div class="alert alert-danger">', '</div>'); ?>	
                        <input type="hidden" value="1" name="po_status" id="po_status">
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_name'); ?><span style="color:red"><sup>*</sup></span></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'      => 'po_name',
                                'id'        => 'po_name',
                                'class'     => 'form-control',
                                'requeired' => 'requeired',
                                'value'     => set_value('po_name', $u['po_name'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->
                    <div class="col-md-12 nopadding">
                        <div class="col-md-5 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_number', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_number'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'      => 'po_number',
                                        'id'        => 'po_number',
                                        'class'     => 'form-control',
                                        //'readonly' => 'readonly',
                                        'requeired' => 'requeired',
                                        'value'     => set_value('po_number', $u['po_number'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-7" style="padding-right: 0px">              
<!--                            <div class="form-group">		
                                <?php echo form_error('po_manager', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_manager'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        //'name'      => 'po_manager',
                                        'id'        => 'po_manager',
                                        'class'     => 'form-control',
                                        'requeired' => 'requeired',
                                        'onChange' => 'change_company(this.value)',
                                        'value'     => set_value('po_manager', '')
                                    );
                                    //print_r($company);exit();
                                    foreach ($company as $value)
                                    {
                                        $option[$value['companyId']] = $value['company_name'];
                                    }
                                    
//                      
//                                    echo form_input($data);
                                    
                                    echo form_dropdown('po_manager', $option,$u['po_company'],$data);
                                    ?>
                                </div>  /controls 				
                            </div>  /form-group                 -->
                        </div>
                    </div>   
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_adress', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_adress'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_adress',
                                        'id'    => 'po_adress',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value'     => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-4" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_city', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_city'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_city',
                                        'id'    => 'po_city',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_city', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-2" style="padding-right: 0px">              
                            <div class="form-group hidden">		
                                <?php echo form_error('po_age', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_age'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_age',
                                        'id'    => 'po_age',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_age',$u['po_age'])
                                    );

                                    echo form_input($data);
                                    ?>
                                    <input type="hidden" name="product_rows" id="product_rows" value="<?php echo count($products); ?>">
                                    <input type="hidden" name="custom_rows" id="custom_rows" value="<?php echo count($customs); ?>">
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            <div class="form-group">		
                                <?php echo form_error('po_state', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_state'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_state',
                                        'id'    => 'po_state',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_state',$u['po_state'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                        </div>
                    </div>

                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT SITE CONTACT</h4>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_name', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_site_name"><?php echo $this->lang->line('po_site_name'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_name',
                                        'id'    => 'po_site_name',
                                        'class' => 'form-control',
                                        'value' => set_value('po_adress', $u['po_site_contact_name'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_email', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_site_email'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_email',
                                        'id'    => 'po_site_email',
                                        'class' => 'form-control',
                                        'type' => 'email',
                                        'value' => set_value('po_site_email', $u['po_site_contact_email'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_office_phone', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_office_phone"><?php echo $this->lang->line('po_office_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_office_phone',
                                        'id'    => 'po_office_phone',
                                        'class' => 'form-control',
                                        'placeholder' => '123-456-7890',
                                        'value' => set_value('po_office_phone', $u['po_site_contact_office_phone'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_cell_phone', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_cell_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_cell_phone',
                                        'id'    => 'po_cell_phone',
                                        'class' => 'form-control',
                                        'placeholder' => '123-456-7890',
                                        'value' => set_value('po_cell_phone', $u['po_site_contact_cell_phone'])
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <div>
                        <h4 style="color: #302a75;float:left;font-weight: bold;">PRODUCTS</h4>
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data" style="margin-bottom: 13px;">
                        <div id="product_copy" class="hidden">
                            <table class="table row0" data-id="0" style="max-width: 140%;width: 140%;">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">PRODUCT #</th>
                                    <th class="col-md-3"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflects the product quantity needed for a project, in total. PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> <span style="display: inline-flex;vertical-align: middle;"> PROJECT <br>QTY & PRICE</span></th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="ORDER QTY reflects the product quantity to be added to the cart. This quantity will be reduced to 0, after an order is submitted."></i> <span style="display: inline-flex;vertical-align: middle;"> ORDER <br>QTY & PRICE</span></th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="REMAINING reflects the product quantity that is left to order. This quantity will be reduced to 0, after all products are ordered."></i> REMAINING </th>
                                    <th class="col-md-2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="The TOTAL reflects the current quantity pricing of ORDER QTY. This total will be added to the cart."></i> TOTAL</th>
                                    <th class="" style="background: white;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                           
                            <tr>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product',
                                            'id'    => 'po_row0_product',
                                            'autocomplete'=>'off',
                                            'class' => 'form-control productsearch',
                                            'value' => set_value('po_row0_product', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_row0"></div>
                                    <input type="hidden" name="po_row0_product_id" id="po_row0_product_id">
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_desc',
                                            'id'    => 'po_row0_product_desc',
                                            'class' => 'form-control productsearch',
                                            'autocomplete'=>'off',
                                            'value' => set_value('po_row0_product_desc', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_desc_row0"></div>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_quntity',
                                            'id'    => 'po_row0_product_quntity',
                                            'class' => 'form-control projectquantity numberic',
                                            'value' => set_value('po_row0_product_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_price',
                                            'id'    => 'po_row0_product_price',
                                            'class' => 'form-control productprice',
                                            'readonly'=>'readonly',
                                            'type'  => 'hidden',
                                            'value' => set_value('po_row0_product_price', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_price1',
                                            'id'    => 'po_row0_product_price1',
                                            'class' => 'form-control productprice1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_product_price1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                 <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_order_quntity',
                                            'id'    => 'po_row0_order_quntity',
                                            'class' => 'form-control productquantity numberic',
                                            'value' => set_value('po_row0_order_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_order_price',
                                            'id'    => 'po_row0_order_price',
                                            'class' => 'form-control orderprice',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_order_price', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_remaining',
                                            'id'    => 'po_row0_remaining',
                                            'class' => 'form-control row_remaining',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_remaining', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <input type="hidden" value="0" class="sell" > 
                                    <input type="hidden" value="0" class="remianing_q" > 
                                </td>
                                <td>
                                    <input type="hidden" name="project_remaining_total_row0" id="project_remaining_total_row0" class="pro_remaining_total"> 
                                     <?php
                                        $data = array(
                                            'name'  => 'po_row0_total',
                                            'id'    => 'po_row0_total',
                                            'class' => 'form-control row_total',
                                            'type' =>'hidden',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_total', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_total1',
                                            'id'    => 'po_row0_total1',
                                            'class' => 'form-control row_total1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_total1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
                                    <!--<i class="fa fa-trash trashproduct" id="row0_trash"></i>-->
                                    <img id="row0_trash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashproduct"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_comment',
                                            'id'    => 'po_row0_comment',
                                            'class' => 'form-control',
                                            'rows'   => 1,
                                            'cols'  => 50,
                                            'value' => set_value('po_row0_comment', '', FALSE)
                                        );
                                        echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        <div id="copy_product" class="table-responsive">
                            <?php
                            $i = 1;
                            if(count($products)> 0)
                            {
//                                print_r($remaining);
                                foreach ($products as $u1) 
                                {
                                    //print_r($u1['po_product_id']);exit();
                                    $rem = $u1['quantity'] - $remaining[$u1['po_product_id']];
                            ?>
                            <table class="table row1 <?php echo $rem<=0?'mywatermark':''; ?>" data-id="1" style="max-width: 140%;width: 140%;">
                            <thead>
                                 <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">PRODUCT #</th>
                                    <th class="col-md-3"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflects the product quantity needed for a project, in total. PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> <span style="display: inline-flex;vertical-align: middle;"> PROJECT <br>QTY & PRICE</span></th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="ORDER QTY reflects the product quantity to be added to the cart. This quantity will be reduced to 0, after an order is submitted."></i> <span style="display: inline-flex;vertical-align: middle;"> ORDER <br>QTY & PRICE</span></th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="REMAINING reflects the product quantity that is left to order. This quantity will be reduced to 0, after all products are ordered."></i> REMAINING </th>
                                    <th class="col-md-2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="The TOTAL reflects the current quantity pricing of ORDER QTY. This total will be added to the cart."></i> TOTAL</th>
                                    <th class="" style="background: white;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                            
                            <tr>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_product',
                                            'id'    => 'po_row'.$i.'_product',
                                            'class' => 'form-control',
                                            'disabled'=>'disabled',
                                            'autocomplete'=>'off',
                                            'value' => set_value('po_row'.$i.'_product', $u1['product_name'])
                                        );
                                        echo form_input($data);
                                    ?>
<!--                                    <div id="suggesstion-box" class="<?php echo 'sugg_row'.$i ?>"></div>-->
                                    <input type="hidden" value="<?php echo $u1['product_id']; ?>" name="<?php echo 'po_row'.$i.'_product_id' ?>" id="<?php echo 'po_row'.$i.'_product_id' ?>">
                                    <input type="hidden" value="<?php echo $u1['po_product_id']; ?>" name="<?php echo 'po_row'.$i.'_order_po_id' ?>" id="<?php echo 'po_row'.$i.'_order_po_id' ?>">
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_product_desc',
                                            'id'    => 'po_row'.$i.'_product_desc',
                                            'disabled'=>'disabled',
                                            'class' => 'form-control productsearch',
                                            'value' => set_value('po_row'.$i.'_product_desc', $u1['product_desc'])
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_product_quntity',
                                            'id'    => 'po_row'.$i.'_product_quntity',
                                            'class' => 'form-control projectquantity numberic',
                                            'value' => set_value('po_row'.$i.'_product_quntity', $u1['quantity'])
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_product_price',
                                            'id'    => 'po_row'.$i.'_product_price',
                                            'class' => 'form-control productprice hidden',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_product_price', $u1['product_unit_price'])
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_product_price1',
                                            'id'    => 'po_row'.$i.'_product_price1',
                                            'class' => 'form-control productprice1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_product_price1', number_format(( $u1['product_unit_price']*$u1['quantity']),2))
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $order_quantity = 0;
                                        if($u['po_status']=='Pending')
                                        {
                                            $order_quantity = $u1['po_draft_order'];
                                            
                                        }
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_order_quntity',
                                            'id'    => 'po_row'.$i.'_order_quntity',
                                            'class' => 'form-control productquantity numberic',
                                            'value' => set_value('po_row'.$i.'_order_quntity', $order_quantity)
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_order_price',
                                            'id'    => 'po_row'.$i.'_order_price',
                                            'class' => 'form-control orderprice hidden',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_order_price', $u1['product_unit_price'])
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_order_price1',
                                            'id'    => 'po_row'.$i.'_order_price1',
                                            'class' => 'form-control orderprice1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_order_price1', number_format(0,2))
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td>
                                   <?php
                                    $po_remaining = 0;
                                    if($u['po_status']=='Pending')
                                    {
                                        $po_remaining = $u1['quantity']-$remaining[$u1['po_product_id']]-$u1['po_draft_order'];
                                      
                                   ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_remaining',
                                            'id'    => 'po_row'.$i.'_remaining',
                                            'class' => 'form-control row_remaining',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_remaining', $po_remaining)
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                    <input type="hidden" value="<?php echo isset($remaining[$u1['po_product_id']])?$remaining[$u1['po_product_id']]:0;  ?>" class="sell" > 
                                    <input type="hidden" value="<?php echo ($u1['quantity'] - $remaining[$u1['po_product_id']]);  ?>" class="remianing_q" > 
                                    <?php
                                    } else{
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_remaining',
                                            'id'    => 'po_row'.$i.'_remaining',
                                            'class' => 'form-control row_remaining',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_remaining', ($u1['quantity'] - $remaining[$u1['po_product_id']]))
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                    <input type="hidden" value="<?php echo isset($remaining[$u1['po_product_id']])?$remaining[$u1['po_product_id']]:0;  ?>" class="sell" > 
                                    <input type="hidden" value="<?php echo ($u1['quantity'] - $remaining[$u1['po_product_id']]);  ?>" class="remianing_q" > 
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $po_remaining_total = ($u1['quantity']*$u1['product_unit_price']);
                                        $po_total = 0;
                                        if($u['po_status']=='Pending')
                                        {
                                            $po_remaining_total = ($u1['quantity'] - $u1['po_draft_order'])*$u1['product_unit_price'];
                                            $po_total = $u1['po_draft_order']*$u1['product_unit_price'];
                                        }
                                    ?>
                                    <input type="hidden" name="<?php echo 'project_remaining_total_row'.$i; ?>" id="<?php echo 'project_remaining_total_row'.$i; ?>" class="pro_remaining_total" value="<?php echo $po_remaining_total; ?>"> 
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_total',
                                            'id'    => 'po_row'.$i.'_total',
                                            'class' => 'form-control row_total hidden',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_total',$po_total)
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_total1',
                                            'id'    => 'po_row'.$i.'_total1',
                                            'class' => 'form-control row_total1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row'.$i.'_total1',$po_total)
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
                                    <img id="<?php echo 'row'.$i.'_trash'; ?>" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashproduct"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row'.$i.'_comment',
                                            'id'    => 'po_row'.$i.'_comment',
                                            'class' => 'form-control',
                                            'rows'   => 1,
                                            'cols'  => 50,
                                            'value' => set_value('po_row'.$i.'_comment', $u1['comment'])
                                        );
                                        if($rem<=0)
                                        {
                                            $data['readonly']='readonly';
                                        }
                                        echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                    ?>
                                </td>
                            </tr>
                            
                            </tbody>
                        </table>
                            <?php
                                $i++;
                                }
                            }else{
                            ?>
                            <div class="col-md-12 emptypro">
                                <center>No Item Available</center>
                            </div>
                            <?php
                            }
                            ?>
                            
                        </div>
                        <div style="height: 20px;">&nbsp;</div>
                        <a href="javascript:void(0)" id="product_div" style="color: #302a75;font-size: 13px;font-weight: lighter;text-decoration: none;">
                            <i class="fa fa-plus pls-prdct"></i> ADD ANOTHER ITEM
                        </a>
                    </div>
                    <div style="margin: 30px 0px; ">&nbsp;</div>
                    <hr  style="clear: both;border-top: 1px solid black;">
                    <div style="margin-top: 10px;">
                        <h4 style="color: #302a75;float:left;font-weight: bold;">CUSTOM ITEMS</h4>
                        <div class="toggle1"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="custom_data" style="margin-bottom: 13px;">
                        <div id="custom_copy" class="hidden">
                            <table class="table customrow0">
                            <thead>
                               <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                     <th class="col-md-2">ITEM</th>
                                    <th class="col-md-4"> DESCRIPTION</th>
                                    <th class="col-md-2">QTY</th>
                                    <th class="col-md-2">PRICE</th>
                                    <th class="col-md-2">TOTAL</th>
                                    <th class="" style="background: white;"></th>
                                </tr>
                            </thead>

                            <tbody class="breadcrumb">
                                <tr>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_item',
                                                'id'    => 'po_row0_custom_item',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row0_custom_item', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_desc',
                                                'id'    => 'po_row0_custom_desc',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row0_custom_desc', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_quantity',
                                                'id'    => 'po_row0_custom_quantity',
                                                'class' => 'form-control customquantity get_custom_total numberic',
                                                'type' => 'number',
                                                'value' => set_value('po_row0_custom_quantity', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_price',
                                                'id'    => 'po_row0_custom_price',
                                                'class' => 'form-control customprice get_custom_total numberic',
                                                'type' => 'number',
                                                'step' => '0.01',
                                                'value' => set_value('po_row0_custom_price', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_total',
                                                'id'    => 'po_row0_custom_total',
                                                'readonly'=>'readonly',
                                                'class' => 'form-control row_custom_total',
                                                'value' => set_value('po_row0_custom_total', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
                                        <!--<i class="fa fa-trash trashcustom" id="row0_customtrash"></i>-->
                                        <img id="row0_customtrash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashcustom"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_comment',
                                                'id'    => 'po_row0_custom_comment',
                                                'class' => 'form-control',
                                                'rows'  => "1",
                                                'cols'  => "50",
                                                'value' => set_value('po_row0_custom_comment', '', FALSE)
                                            );
                                            echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div id="set_copy">
                            <?php
                            $j = 1;//print_r($customs);exit();
                            if(count($products)> 0)
                            {
                                foreach ($customs as $u2) 
                                {
                            ?>
                            
                            <table class="table customrow1">
                            <thead>
                               <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                     <th class="col-md-2">ITEM</th>
                                    <th class="col-md-4"> DESCRIPTION</th>
                                    <th class="col-md-2">QTY</th>
                                    <th class="col-md-2">PRICE</th>
                                    <th class="col-md-2">TOTAL</th>
                                    <th class="" style="background: white;"></th>
                                </tr>
                            </thead>

                            
                            <tbody class="breadcrumb">
                                
                                <tr>
                                    <input type="hidden" value="<?php echo $u2['po_custom_id']; ?>" name="<?php echo 'po_row'.$j.'_order_id' ?>" id="<?php echo 'po_row'.$j.'_order_id' ?>">
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_item',
                                                'id'    => 'po_row'.$j.'_custom_item',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row'.$j.'_custom_item', $u2['po_custom_title'])
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_desc',
                                                'id'    => 'po_row'.$j.'_custom_desc',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row'.$j.'_custom_desc', $u2['po_custom_desc'])
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_quantity',
                                                'id'    => 'po_row'.$j.'_custom_quantity',
                                                'type' => 'number',
                                                'class' => 'form-control customquantity get_custom_total numberic',
                                                'value' => set_value('po_row'.$j.'_custom_quantity', $u2['po_custom_quantity'])
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_price',
                                                'id'    => 'po_row'.$j.'_custom_price',
                                                'type' => 'number',
                                                'step' => '0.01',
                                                'class' => 'form-control customprice get_custom_total numberic',
                                                'value' => set_value('po_row'.$j.'_custom_price',  $u2['po_custom_price'])
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_total',
                                                'id'    => 'po_row'.$j.'_custom_total',
                                                'readonly'=>'readonly',
                                                'class' => 'form-control row_custom_total',
                                                'value' => set_value('po_row'.$j.'_custom_total', $u2['total'])
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
<!--                                        <i class="fa fa-trash trashcustom" id="<?php echo 'row'.$i.'_customtrash'; ?>"></i>-->
                                        <img id="<?php echo 'row'.$i.'_customtrash'; ?>" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashcustom"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row'.$j.'_custom_comment',
                                                'id'    => 'po_row'.$j.'_custom_comment',
                                                'class' => 'form-control',
                                                'rows'  => "1",
                                                'cols'  => "50",
                                                'value' => set_value('po_row'.$j.'_custom_comment', $u2['comment'])
                                            );
                                            echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                        ?>
                                    </td>
                                </tr>
                                 
                            </tbody>
                            </table>
                            <?php
                                $j++;
                                }
                            }else{
                            ?>
                            <div class="col-md-12 empty">
                                <center>No Item Available</center>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <a href="javascript:void(0)" id="custom_div" style="color: #302a75;font-size: 13px;font-weight: lighter;text-decoration: none;">
                            <i class="fa fa-plus"></i> ADD ANOTHER ITEM
                        </a>
                    </div>
                    <hr  style="clear: both;border-top: 3px solid black;">
                    <div class="col-md-12 nopadding" style="margin-top: 15px;">
                        <div class="table-responsive col-md-offset-6">
                            <table class="table table-striped" id="invoice" style="text-align: right;">
                                <tr>
                                    <th style="float: right;">PRODUCTS TOTAL </th>
                                    <th>$</th>
                                    <td class="total_show hidden"></td>
                                    <td class="total_show1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">PRODUCTS TOTAL REMAINING </th>
                                    <th>$</th>
                                    <td class="total_show_remaining hidden"></td>
                                    <td class="total_show_remaining1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">CUSTOM ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">PROJECT OVERALL  ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="overall_total hidden"></td>
                                    <td class="overall_total1"></td>
                                </tr>
                            </table>
                        </div>
                        <hr  style="clear: both;border-top: 1px solid black;">
                        <span style="color: red;float: right">Pricing is subject to change</span>
                        <div class="col-md-12 nopadding" style="margin-top: 15px;">
                        <div class="table-responsive col-md-offset-6">
                                <div class="col-md-5"><button class="btn btn-info col-md-12" type="button">Export CSV</button></div>
                                <div class="col-md-offset-2 col-md-5 nopadding"><button id="pdfdownload" type="button" class="btn btn-info col-md-12">Download PDF</button>
                                    <br>
                                    <span style="color: #302a75;font-size: 10px;">Download to save a copy of your original pricing for your records.</span>
                                </div>
                            </div>
                            <center>
                                <div class="g-recaptcha" data-sitekey="6Lcyhm4UAAAAALTANNVA1Sk5APoZtjVcnxOPgizx"></div>
                            </center>
                        </div>
                    </div>                    
                    </div>
                <div class="panel-footer">
                    <center>
                    <?php 	
                            $data = array(
                              'name'        => 'submit',
                              'id'          => 'submit',
                              'class'       => 'btn btn-primary',
                              'value'	=> $this->lang->line('btn_save'),
                            );
                            echo form_submit($data); 
                    ?>
                    <?php 	
                            $data = array(
                              'name'        => 'submit_order',
                              'id'          => 'submit_order',
                              'class'       => 'btn btn-primary',
//                              'type'      =>   'button',
                              'value'	=> $this->lang->line('btn_submit_order'),
                            );
                            echo form_submit($data); 
                    ?>
                    <a class="btn btn-danger" href="<?php echo BASE_URL; ?>/vendor/project"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    </center>
                </div> <!-- /form-actions -->
                <?php  
                        echo form_close(); 
                         }
                ?>
           
                

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
.white-tooltip + .tooltip > .tooltip-inner {background-color: #fff;color: black;border: 1px solid #302a75;color: #302a75}
.white-tooltip + .tooltip > .tooltip-inner {background-color: #fff;color: black;}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 999;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
.toggle{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle1{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.toggle1.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.mywatermark {  
  display: block;
  position: relative;
  height:200px;
  width:100%;
  border: 2px solid;
}

.mywatermark::after {
  content: "";
  background:url("<?php echo base_url().'images/fullfilled.png'; ?>") no-repeat;
  background-position: center top;
  opacity: 1;
  top: 35%;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: 999;  
  height:100%;
  width:100%;
}
.mywatermark tbody
{
  border: red 1px solid;
}
.mywatermark input
{
    border: red 1px solid;
}
</style>
<script>
        $("#product_div").click(function () {
          
            var next_num = $('#product_rows').val();
            next_num = parseInt(next_num) + 1;
            if(next_num > 0)
            {
                 $('.emptypro').remove();
            }
            $('#product_rows').val(next_num);
            var a = $('#product_copy').html().replace(/row0/g,"row"+next_num);
            a = a.replace(/data-id="0"/g,"data-id='"+next_num+"'");
            $('#copy_product').append(a);
            $('[data-toggle="tooltip"]').tooltip();
        });

        $("#custom_div").click(function () {
            
            var next_num = $('#custom_rows').val();
            next_num = parseInt(next_num) + 1;
            if(next_num > 0)
            {
                 $('.empty').remove();
            }
            $('#custom_rows').val(next_num);
            //var next_num = $('#custom_data table').length;
            var b = $('#custom_copy').html().replace(/row0/g,"row"+next_num);
            $('#set_copy').append(b);
            $('[data-toggle="tooltip"]').tooltip();
            
        });
        
        $(document).on('click', '.trashproduct', function(){
            
            //console.log("enter");
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            console.log(close_div);
            if($('#product_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                gettotal();
            }
            else
            {
                
            }
            
            
            
        });
        $(document).on('click', '.trashcustom', function(){
            
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            //console.log(close_div);
            if($('#custom_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                getcustomtotal();
            }
            else
            {
                
            }
            
        });
        
        $(document).on('change', '.projectquantity', function(){
            
                var val = parseInt($(this).val());
                if(!isNaN(val) && val > 0)
                {
                    $(this).val(val);
                }
                else
                {
                    val = 0;
                    $(this).val(val);
                }
                var product_row_id = $(this).attr('id');
                //po_row1_remaining_q
                if(product_row_id != '')
                {
                    var project_total = $('#'+product_row_id).closest('tr').find('.productquantity').val();   
                    var unit_price = $('#'+product_row_id).closest('tr').find('.productprice').val();
                    var sell = $('#'+product_row_id).closest('tr').find('.sell').val();
                    var remianing_q = $('#'+product_row_id).closest('tr').find('.remianing_q').val();
                    
                    if(project_total > val)
                    {
                        alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                        $(this).val(sell);
                        $('#'+product_row_id).closest('tr').find('.productquantity').val(0);
                        $('#'+product_row_id).closest('tr').find('.row_remaining').val(0);
                        return;
                    }
                    if(val < parseInt(sell) + parseInt(project_total) )
                    {
                            alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                            $(this).val(parseInt(sell) + parseInt(project_total));
                            val = parseInt(sell) + parseInt(project_total) ;
                            
                    } 
                    if(val < parseInt(sell) + parseInt(project_total) )
                    {
                            alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                            $(this).val(parseInt(sell) + parseInt(project_total));
                            val = parseInt(sell) + parseInt(project_total) ;
                            
                    }
                    if(val < sell )
                    {
                            alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                            $(this).val(sell);
                            val = sell;
                    }
                    var total = parseFloat(unit_price)* parseInt(val);
                    $('#'+product_row_id).closest('tr').find('.pro_remaining_total').val(total.toFixed(2));
                    if(project_total != '')
                    {       
                            //alert("asasdsa");
                            $('#'+product_row_id).closest('tr').find('.row_remaining').val(parseInt(val) - parseInt(project_total) - parseInt(sell));
                            $('#'+product_row_id).closest('tr').find('.remianing_q').val(parseInt(val) - parseInt(sell));
                            $('#'+product_row_id).closest('tr').find('.productprice1').val(total.toFixed(2));
                            $('#'+product_row_id).closest('tr').find('.productquantity').attr('max',val);
                            
                    }
                }
                gettotal();
                
        })
        $(document).on('change', '.productquantity', function(){
                
                var val = parseInt($(this).val());//productquantity
                if(!isNaN(val) && val > 0)
                {
                    $(this).val(val);
                }
                else
                {
                    val = 0;
                    $(this).val(val);
                }
                var product_row_id = $(this).attr('id');
                
                if(product_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+product_row_id).closest('tr').find('.productprice').val();
                    var project_total = $('#'+product_row_id).closest('tr').find('.projectquantity').val();
                    var sell = $('#'+product_row_id).closest('tr').find('.sell').val();
                    var remianing_q = $('#'+product_row_id).closest('tr').find('.remianing_q').val();
                    
                    if(val > remianing_q)
                    {
                        //alert('adsdsa');
                        alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                        $(this).val(0);
                        val = 0;
                    }
                    if(project_total != '')
                    {
                            if(typeof(sell) != "undefined" && sell !== null)
                            {
                                $('#'+product_row_id).closest('tr').find('.row_remaining').val(parseInt(project_total) - parseInt(val) - parseInt(sell));
                            }
                            else
                            {
                                $('#'+product_row_id).closest('tr').find('.row_remaining').val(parseInt(project_total) - parseInt(val));
                            }
                            
                    }
                    if(unit_price != '')
                    {
                        //console.log(unit_price);
                        var total = parseFloat(unit_price)* parseInt(val);
                        console.log(total);
                        if(!isNaN(total))
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(total.toFixed(2));
                            $('#'+product_row_id).closest('tr').find('.orderprice1').val(addCommas(total.toFixed(2)));
                            $('#'+product_row_id).closest('tr').find('.row_total1').val(addCommas(total.toFixed(2)));
                        }
                        else
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(0);
                        }
                        
                    }
                }
                
                gettotal();
                
                
        });
       
        $(document).on('change', '.get_custom_total', function(){
            
                var custom_row_id = $(this).attr('id');
                if(custom_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+custom_row_id).closest('tr').find('.customprice').val();
                    var quantity = $('#'+custom_row_id).closest('tr').find('.customquantity').val();
                    if(unit_price != '')
                    {
                        //console.log(unit_price);
                        var total = parseFloat(unit_price) * parseInt(quantity) ;
                        if(!isNaN(total))
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(total.toFixed(2));
                        }
                        else
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(0);
                        }
                    }
                }
                
                getcustomtotal();
                
                
        });
        $(document).ready(function () {

                $('input').attr('ondrop','return false;');
                $('input').attr('onpaste','return false;');
                
                $('#pdfdownload').click(function(e){
                    //alert("asdads");
                    $('#po_status').val('2');
                    $('#submit')[0].click();

                })
                
                var $content = $("#product_data").show();
                $(".toggle").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content.slideToggle();
                });
                
                var $content1 = $("#custom_data").show();
                $(".toggle1").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content1.slideToggle();
                });
                
            $('[data-toggle="tooltip"]').tooltip();
//            $(".numberic").keydown(function (e) {
                $(document).on('keydown', '.numberic', function(e){
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
//        $(document).ready(function(){
            //$(".productsearch").keyup(function(){
            $(document).on('keyup', '.productsearch', function(){
                    //alert("dsadsads");
//                    var textboxname = $(this).attr('name');
//                    //console.log(textboxname);
//                    var dataid = $('input[name='+textboxname+']').closest('table').data("id")
//                    //console.log($('input[name='+textboxname+']').closest('table').data("id"));
//                    $.ajax({
//                    type: "POST",
//                    url: '<?php //echo base_url().'vendor/project/getproduct'; ?>',
//                    data:'productname='+$(this).val()+'&rowid='+dataid,
//                    beforeSend: function(){
//                            $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
//                    },
//                    success: function(data){
//                            $(".sugg_row"+dataid).show();
//                            $(".sugg_row"+dataid).html(data);
//                            $("#po_row"+dataid+"_product").css("background","#FFF");
//                    }
//                    });
                    //alert("dsadsads");
                    var textboxname = $(this).attr('name');
//                    console.log(textboxname);
                    var dataid = $('input[name='+textboxname+']').closest('table').data("id")
                    //console.log($('input[name='+textboxname+']').closest('table').data("id"));
                    var type= '';
                    if(textboxname.toString().indexOf('product_desc') != -1)
                    {
                        type = 'desc';
                    }
                    else
                    {
                        type = 'title';
                    }
                    $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'vendor/project/getproduct'; ?>',
                    data:'productname='+$(this).val()+'&rowid='+dataid+'&type='+type,
                    beforeSend: function(){
                            $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        if(textboxname.toString().indexOf('product_desc') != -1)
                        {
                            $(".sugg_desc_row"+dataid).show();
                            $(".sugg_desc_row"+dataid).html(data);
                        }
                        else
                        {
                            $(".sugg_row"+dataid).show();
                            $(".sugg_row"+dataid).html(data);
                            $("#po_row"+dataid+"_product").css("background","#FFF");
                            
                            //$("#po_row"+dataid+"_product").css("background","#FFF");
                        }
                    }
                    });

            });
            
//        });
        function selectCountry(val,dataid) {
            if(val != '' && dataid != '')
            {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/project/getidproduct'; ?>',
                        data:'productid='+val,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_row"+dataid+"_product").val(data[0]['product_name']);
                                $("#po_row"+dataid+"_product_desc").val(data[0]['product_desc']);
                                $("#po_row"+dataid+"_order_price").val(data[0]['product_unit_price']);
                                $("#po_row"+dataid+"_order_price1").val(addCommas(0));
                                $("#po_row"+dataid+"_product_price").val(data[0]['product_unit_price']);
                                $("#po_row"+dataid+"_product_price1").val(addCommas(0));
                                $("#po_row"+dataid+"_product_quntity").val(0);
                                $("#po_row"+dataid+"_order_quntity").val(0);
                                $("#po_row"+dataid+"_remaining").val(0);
                                $("#po_row"+dataid+"_total1").val(0);
                                $("#po_row"+dataid+"_product_id").val(data[0]['product_id']);
                                $(".sugg_row"+dataid).hide();
                                $(".sugg_desc_row"+dataid).hide();
                                  
                        }
                    });
            }
            else
            {
                    $("#po_row"+dataid+"_product").val('');
                    $("#po_row"+dataid+"_product_desc").val('');
                    $("#po_row"+dataid+"_order_price").val('');
                    $("#po_row"+dataid+"_order_price1").val('');
                    $("#po_row"+dataid+"_product_price").val('');
                    $("#po_row"+dataid+"_product_price1").val('');
                    $("#po_row"+dataid+"_product_quntity").val('');
                    $("#po_row"+dataid+"_product_id").val('');
                    $(".sugg_row"+dataid).hide();
                    $(".sugg_desc_row"+dataid).hide();
            }
            
        }
        change_company();
        gettotal();
        getcustomtotal();
        function change_company(id)
        {
                    id = $('#po_manager').val();
                   
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/project/getidcompany'; ?>',
                        data:'companyid='+id,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_adress").val(data[0]['address']);
                                $("#po_city").val(data[0]['city']);
                                
                        }
                    });
        }
        function gettotal()
        {
                var pro_remaining_total = 0;
                var pro_total_all = 0;
                 $('.pro_remaining_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_remaining_total = parseFloat(pro_remaining_total) + parseFloat($(this).val());
                    }
                });
                $('.row_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                $('.total_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_show_remaining1').html(addCommas(parseFloat(pro_remaining_total).toFixed(2)));
                $('.total_show_remaining').html(parseFloat(pro_remaining_total).toFixed(2));
                var ct = !isNaN(parseInt($('.total_custom_show').html())) ? parseInt($('.total_custom_show').html()) : 0;
                var t = !isNaN(parseInt($('.total_show').html())) ? parseInt($('.total_show').html()) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t)
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));
                //$('.overall_total').html(parseInt($('.total_custom_show').html())+parseInt($('.total_show').html()));
        }
        function getcustomtotal()
        {
                var pro_total_all = 0;
                $('.row_custom_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_custom_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_custom_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                var ct = !isNaN(parseFloat($('.total_custom_show').html())) ? parseFloat($('.total_custom_show').html()).toFixed(2) : 0;
                var t = !isNaN(parseFloat($('.total_show').html())) ? parseFloat($('.total_show').html()).toFixed(2) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t)
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));
//                $('.overall_total').html(parseInt($('.total_custom_show').html())+parseInt($('.total_show').html()));
        }
        function addCommas(nStr)
        {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
        }
        $("form").submit(function(event) {
            var recaptcha = $("#g-recaptcha-response").val();
            if (recaptcha === "") {
               event.preventDefault();
               alert("Please check the recaptcha");
            }
         });
    </script>

<script>

document.getElementById('po_cell_phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

document.getElementById('po_office_phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

</script>
    
