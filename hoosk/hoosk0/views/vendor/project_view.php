<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('project_view_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa  fa-file-text-o"></i>
                    <a href="<?php echo BASE_URL; ?>/vendor/project"><?php echo $this->lang->line('project_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('project_view_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('project_view_header'); ?>
                    </h3>
                </div>
                <?php foreach ($project as $u) {
			  ?>

        <div class="panel-body">                    
            <h4 style="color: #302a75;font-weight: bold;">PROJECT INFORMATION</h4>
            <div class="form-group">		
                <?php echo form_error('po_name', '<div class="alert alert-danger">', '</div>'); ?>									
                <label class="control-label" for="name"><?php echo $this->lang->line('po_name'); ?><span style="color:red"><sup>*</sup></span></label>
                <div class="controls">
                    <?php
                    $data = array(
                        'name'      => 'po_name',
                        'id'        => 'po_name',
                        'class'     => 'form-control',
                        'requeired' => 'requeired',
                        'readonly' => 'readonly',
                        'value'     => set_value('po_name', $u['po_name'])
                    );

                    echo form_input($data);
                    ?>
                </div> <!-- /controls -->				
            </div> <!-- /form-group -->
            <div class="col-md-12 nopadding">
                <div class="col-md-5 nopadding">              
                    <div class="form-group">		
                        <?php echo form_error('po_number', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_number'); ?><span style="color:red"><sup>*</sup></span></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'      => 'po_number',
                                'id'        => 'po_number',
                                'class'     => 'form-control',
                                'readonly' => 'readonly',
                                'requeired' => 'requeired',
                                'value'     => set_value('po_number', $u['po_number'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-7" style="padding-right: 0px">              
<!--                            <div class="form-group">		
                                <?php echo form_error('po_manager', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_manager'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        //'name'      => 'po_manager',
                                        'id'        => 'po_manager',
                                        'class'     => 'form-control',
                                        'requeired' => 'requeired',
                                        'onChange' => 'change_company(this.value)',
                                        'value'     => set_value('po_manager', '')
                                    );
                                    //print_r($company);exit();
                                    foreach ($company as $value)
                                    {
                                        $option[$value['companyId']] = $value['company_name'];
                                    }
                                    
//                      
//                                    echo form_input($data);
                                    
                                    echo form_dropdown('po_manager', $option,$u['po_company'],$data);
                                    ?>
                                </div>  /controls 				
                            </div>  /form-group                 -->     
                </div>
            </div>   
            <div class="col-md-12 nopadding">
                <div class="col-md-6 nopadding">              
                    <div class="form-group">		
                        <?php echo form_error('po_adress', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_adress'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_adress',
                                'id'    => 'po_adress',
                                'class' => 'form-control',
                                'readonly'=>'readonly',
                                'value'     => set_value('po_adress', '', FALSE)
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-4" style="padding-right: 0px">              
                    <div class="form-group">		
                        <?php echo form_error('po_city', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_city'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_city',
                                'id'    => 'po_city',
                                'class' => 'form-control',
                                'readonly'=>'readonly',
                                'value' => set_value('po_city', '', FALSE)
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-2" style="padding-right: 0px">              
                    <div class="form-group hidden">		
                        <?php echo form_error('po_age', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_age'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_age',
                                'id'    => 'po_age',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('po_age',$u['po_age'])
                            );

                            echo form_input($data);
                            ?>
                            <input type="hidden" name="product_rows" id="product_rows" value="<?php echo count($products); ?>">
                            <input type="hidden" name="custom_rows" id="custom_rows" value="<?php echo count($customs); ?>">
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->   
                    <div class="form-group">		
                        <?php echo form_error('po_state', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_state'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_state',
                                'id'    => 'po_state',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('po_state',$u['po_state'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->   
                </div>
            </div>

            <br>
            <hr style="clear: both;border-top: 1px solid black;">
            <h4 style="color: #302a75;font-weight: bold;">PROJECT SITE CONTACT</h4>
            <div class="col-md-12 nopadding">
                <div class="col-md-6 nopadding">              
                    <div class="form-group">		
                        <?php echo form_error('po_site_name', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="po_site_name"><?php echo $this->lang->line('po_site_name'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_site_name',
                                'id'    => 'po_site_name',
                                'class' => 'form-control',
                                'readonly' => 'readonly',
                                'value' => set_value('po_adress', $u['po_site_contact_name'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-6" style="padding-right: 0px">              
                    <div class="form-group">		
                        <?php echo form_error('po_site_email', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_site_email'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_site_email',
                                'id'    => 'po_site_email',
                                'class' => 'form-control',
                                'type' => 'email',
                                'readonly' => 'readonly',
                                'value' => set_value('po_site_email', $u['po_site_contact_email'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
            </div>
            <div class="col-md-12 nopadding">
                <div class="col-md-6 nopadding">              
                    <div class="form-group">		
                        <?php echo form_error('po_office_phone', '<div class="alert alert-danger">', '</div>'); ?>									
                        <label class="control-label" for="po_office_phone"><?php echo $this->lang->line('po_office_phone'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_office_phone',
                                'id'    => 'po_office_phone',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('po_office_phone', $u['po_site_contact_office_phone'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-6" style="padding-right: 0px">              
                    <div class="form-group">		
                        <?php echo form_error('po_cell_phone', '<div class="alert alert-danger">', '</div>'); ?>
                        <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_cell_phone'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'po_cell_phone',
                                'id'    => 'po_cell_phone',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('po_cell_phone', $u['po_site_contact_cell_phone'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
            </div>
            
                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <div>
                        <h4 style="color: #302a75;float:left;font-weight: bold;">PRODUCTS</h4>
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data">
                        <div id="copy_product">
                            <?php
                            $i = 1;
                            if(count($products) > 0)
                            {
                                foreach ($products as $u1) 
                                {
                            ?>
                            <table class="table row1" data-id="1">
                            <thead>
                               
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">PRODUCT #</th>
                                    <th class="col-md-3"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflects the product quantity needed for a project, in total. PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> <span style="display: inline-flex;vertical-align: middle;"> PROJECT <br>QTY & PRICE</span></th>
                                    <th class="col-md-2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="The TOTAL reflects the current quantity pricing of ORDER QTY. This total will be added to the cart."></i> TOTAL</th>
                                    
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                            
                            <tr>
                                <td>
                                    <?php
                                        echo $u1['product_name'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $u1['product_desc'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $u1['quantity'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo '$'.number_format($u1['product_unit_price'],2);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_total',
                                            'id'    => 'po_row0_total',
                                            'class' => 'form-control row_total',
                                            'readonly'=>'readonly',
                                            'type'=>'hidden',
                                            'value' => set_value('po_row0_total', $u1['quantity']*$u1['product_unit_price'])
                                        );
                                        echo form_input($data);
                                        echo '$'.number_format($u1['quantity']*$u1['product_unit_price'],2);
                                    ?>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="5">
                                    <?php
                                        echo '<b>PROJECT NOTES </b>: '.$u1['comment'];
                                    ?>
                                </td>
                            </tr>
                            
                            </tbody>
                        </table>
                            <?php
                                $i++;
                                }
                            }else{
                            ?>
                            <div class="col-md-12 emptypro">
                                <center>No Data Found</center>
                            </div>
                            <?php
                            }
                            ?>
                        </div>

                    </div>
                    <div style="margin: 30px 0px; ">&nbsp;</div>
                    <hr  style="clear: both;border-top: 1px solid black;">
                    <div style="margin-top: 10px;">
                        <h4 style="color: #302a75;float:left;font-weight: bold;">CUSTOM ITEMS</h4>
                        <div class="toggle1"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="custom_data">
                        <div id="custom_copy" class="hidden">
                            
                        </div>
                        <div id="set_copy">
                            <?php
                                $j = 1;
                            if(count($customs))
                            {
                                foreach ($customs as $u2) 
                                {
                            ?>
                            <table class="table customrow1">
                            <thead>
                               <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                     <th class="col-md-2">ITEM</th>
                                    <th class="col-md-4"> DESCRIPTION</th>
                                    <th class="col-md-2">QTY</th>
                                    <th class="col-md-2">PRICE</th>
                                    <th class="col-md-2">TOTAL</th>
                                    
                                </tr>
                            </thead>

                            
                            <tbody class="breadcrumb">
                                
                                <tr>
                                    <td>
                                        <?php
                                            echo $u2['po_custom_title'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo $u2['po_custom_desc'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo '$'.$u2['po_custom_quantity'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            echo '$'.$u2['po_custom_price'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                             $data = array(
                                                'name'  => 'po_row'.$j.'_custom_total',
                                                'id'    => 'po_row'.$j.'_custom_total',
                                                'class' => 'form-control row_custom_total',
                                                 'type'=>'hidden',
                                                'value' => set_value('po_row'.$j.'_custom_total', $u2['total'])
                                            );
                                            echo form_input($data);
                                            echo '$'.$u2['total'];
                                        ?>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <?php
                                            echo '<b>PROJECT NOTES </b>: '.$u2['comment'];
                                        ?>
                                    </td>
                                </tr>
                                 
                            </tbody>
                            </table>
                            <?php
                                }
                            }else{
                            ?>
                            <div class="col-md-12 emptypro">
                                <center>No Data Found</center>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        
                    </div>
                    <hr  style="clear: both;border-top: 3px solid black;">
                    <div class="col-md-12 nopadding">
                        <div class="table-responsive col-md-offset-6">
                            <table class="table table-striped" id="invoice" style="text-align: right;">
                                <tr>
                                    <th  style="float: right;">PRODUCTS TOTAL </th>
                                    <th>$</th>
                                    <td class="total_show hidden"></td>
                                    <td class="total_show1"></td>
                                </tr>
                                <tr>
                                    <th  style="float: right;">CUSTOM ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1"></td>
                                </tr>
                                <tr>
                                    <th  style="float: right;">PROJECT OVERALL  ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="overall_total hidden"></td>
                                    <td class="overall_total1"></td>
                                </tr>
                            </table>
                        </div>
                        <hr  style="clear: both;border-top: 1px solid black;">
                        <span style="color: red;float: right">Pricing is subject to change</span>
                        <div class="col-md-12 nopadding" style="margin-top: 15px;">
                            <div class="table-responsive col-md-offset-6">
                                <div class="col-md-5"><button class="btn btn-info col-md-12" type="button">Export CSV</button></div>
                                <div class="col-md-offset-2 col-md-5 nopadding"><a href="<?php echo BASE_URL.'/admin/project/pdf/'.$this->uri->segment(4); ?>"><button id="pdfdownload" type="button" class="btn btn-info col-md-12">Download PDF</button></a>
                                    <br>
                                    <span style="color: #302a75;font-size: 10px;">Download to save a copy of your original pricing for your records.</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    </div>
                <div class="panel-footer">
                    <?php 	
//                            $data = array(
//                              'name'        => 'submit',
//                              'id'          => 'submit',
//                              'class'       => 'btn btn-primary',
//                              'value'	=> $this->lang->line('btn_save'),
//                            );
//                            echo form_submit($data); 
                    ?> 
                    <a class="btn" href="<?php echo BASE_URL; ?>/vendor/project"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    
                </div> <!-- /form-actions -->
                <?php  
                        echo form_close(); 
                         }
                ?>
           
                

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
.toggle{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle1{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.toggle1.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.mywatermark {  
  display: block;
  position: relative;
  height:200px;
  width:100%;
  border: 2px solid;
}

.mywatermark::after {
  content: "";
  background:url(https://www.google.co.in/images/srpr/logo11w.png) no-repeat;
  background-position: center top;
  opacity: 0.2;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: 1;  
  height:100%;
  width:100%;
}
</style>
<script>
        $("#product_div").click(function () {
          
            var next_num = $('#product_rows').val();
            next_num = parseInt(next_num) + 1;
            $('#product_rows').val(next_num);
            var a = $('#product_copy').html().replace(/row0/g,"row"+next_num);
            a = a.replace(/data-id="0"/g,"data-id='"+next_num+"'");
            $('#copy_product').append(a);
            $('[data-toggle="tooltip"]').tooltip();
            });

        $("#custom_div").click(function () {
            
            var next_num = $('#custom_rows').val();
            next_num = parseInt(next_num) + 1;
            $('#custom_rows').val(next_num);
            //var next_num = $('#custom_data table').length;
            var b = $('#custom_copy').html().replace(/row0/g,"row"+next_num);
            $('#set_copy').append(b);
            $('[data-toggle="tooltip"]').tooltip();
            
        });
        
        $(document).on('click', '.trashproduct', function(){
            
            //console.log("enter");
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            console.log(close_div);
            if($('#product_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                gettotal();
            }
            else
            {
                
            }
            
            
            
        });
        $(document).on('click', '.trashcustom', function(){
            
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            //console.log(close_div);
            if($('#custom_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                getcustomtotal();
            }
            else
            {
                
            }
            
        });
        $(document).on('change', '.productquantity', function(){
                
                var val = $(this).val();
                var product_row_id = $(this).attr('id');
                if(product_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+product_row_id).closest('tr').find('.productprice').val();
                    if(unit_price != '')
                    {
                        //console.log(unit_price);
                        var total = parseFloat(unit_price)* parseInt(val);
                        if(!isNaN(total))
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(total.toFixed(2));
                        }
                        else
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(0);
                        }
                    }
                }
                
                gettotal();
                
                
        });
        $('#get_pdf').click(function (){
            
                    var html = $("html").html();
                    $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'vendor/project/test'; ?>',
                    data:'fullhtml='+html,
                    //contentType: "application/json",
                    success: function(data){
                            
                    }
                    });
        });
        $(document).on('change', '.get_custom_total', function(){

                
                var custom_row_id = $(this).attr('id');
                if(custom_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+custom_row_id).closest('tr').find('.customprice').val();
                    var quantity = $('#'+custom_row_id).closest('tr').find('.customquantity').val();
                    if(unit_price != '')
                    {
                        //console.log(unit_price);
                        var total = parseFloat(unit_price) * parseInt(quantity) ;
                        if(!isNaN(total))
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(total.toFixed(2));
                        }
                        else
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(0);
                        }
                        //$('#'+custom_row_id).closest('tr').find('.row_custom_total').val(unit_price*quantity);
                    }
                }
                getcustomtotal();
        });
        $(document).ready(function () {

                var $content = $("#product_data").show();
                $(".toggle").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content.slideToggle();
                });
                
                var $content1 = $("#custom_data").show();
                $(".toggle1").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content1.slideToggle();
                });
                
            $('[data-toggle="tooltip"]').tooltip();
            $(".numberic").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
//        $(document).ready(function(){
            //$(".productsearch").keyup(function(){
            $(document).on('keyup', '.productsearch', function(){
                    //alert("dsadsads");
                    var textboxname = $(this).attr('name');
                    //console.log(textboxname);
                    var dataid = $('input[name='+textboxname+']').closest('table').data("id")
                    //console.log($('input[name='+textboxname+']').closest('table').data("id"));
                    $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'vendor/project/getproduct'; ?>',
                    data:'productname='+$(this).val()+'&rowid='+dataid,
                    beforeSend: function(){
                            $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                            $(".sugg_row"+dataid).show();
                            $(".sugg_row"+dataid).html(data);
                            $("#po_row"+dataid+"_product").css("background","#FFF");
                    }
                    });
            });
            
//        });
        function selectCountry(val,dataid) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/project/getidproduct'; ?>',
                        data:'productid='+val,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
//                                console.log(data[0]['product_id']);
//                                console.log(data[0]['product_name']);
//                                console.log(data[0]['product_desc']);
//                                console.log(data[0]['product_desc']);
//                                console.log(data[0]['product_unit_price']);
//                                console.log(data[0]['product_available_quantity']);
//                                console.log(data[0]['product_status']);
                                $("#po_row"+dataid+"_product").val(data[0]['product_name']);
                                $("#po_row"+dataid+"_product_desc").val(data[0]['product_desc']);
                                $("#po_row"+dataid+"_order_price").val(data[0]['product_unit_price']);
                                $("#po_row"+dataid+"_product_price").val(data[0]['product_unit_price']);
                                $("#po_row"+dataid+"_product_quntity").val(0);
                                $("#po_row"+dataid+"_product_id").val(data[0]['product_id']);
//                                $("#po_row"+dataid+"_product_remaining").val(0);
                                $(".sugg_row"+dataid).hide();
                                  
                        }
                    });
            
            
        }
        change_company();
        gettotal();
        getcustomtotal();
        function change_company(id)
        {
                    id = $('#po_manager').val();
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'vendor/project/getidcompany'; ?>',
                        data:'companyid='+id,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_adress").val(data[0]['address']);
                                $("#po_city").val(data[0]['city']);
                                
                        }
                    });
        }
        function gettotal()
        {
                var pro_total_all = 0;
                $('.row_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                var ct = !isNaN(parseInt($('.total_custom_show').html())) ? parseInt($('.total_custom_show').html()) : 0;
                var t = !isNaN(parseInt($('.total_show').html())) ? parseInt($('.total_show').html()) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t)
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));
        }
        function getcustomtotal()
        {
                var pro_total_all = 0;
                $('.row_custom_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_custom_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_custom_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                var ct = !isNaN(parseFloat($('.total_custom_show').html())) ? parseFloat($('.total_custom_show').html()).toFixed(2) : 0;
                var t = !isNaN(parseFloat($('.total_show').html())) ? parseFloat($('.total_show').html()).toFixed(2) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t)
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));
        }
        function addCommas(nStr)
        {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
        }
    </script>
