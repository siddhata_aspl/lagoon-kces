<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('people_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-users"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/our_people"><?php echo $this->lang->line('people_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                	<?php echo $this->lang->line('people_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('people_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
			<?php echo form_open_multipart(BASE_URL.'/admin/our_people/new/add'); ?>
             
                 <div class="form-group">		
                <?php echo form_error('people_name', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="people_name"><?php echo $this->lang->line('people_title'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'people_name',
						  'id'          => 'people_name',
						  'class'       => 'form-control',
						  'value'           => set_value('people_name', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                         
                                
                        <div class="form-group">		
                <?php echo form_error('people_post', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="people_post"><?php echo $this->lang->line('people_post'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'people_post',
						  'id'          => 'people_post',
						  'class'       => 'form-control',
						  'value'           => set_value('people_post', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                
                <div class="form-group">
            		<?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('people_photo'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
						<!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input type="file" name="file_upload" value=""/>
					</div> <!-- /controls -->
				</div> <!-- /form-group --> 
                                

                </div>
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?> 
					<a class="btn" href="<?php echo BASE_URL; ?>/admin/our_people"><?php echo $this->lang->line('btn_cancel'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
		</div>
	</div>
</div>
<?php echo $footer; ?>