<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('nav_changepass'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('nav_changepass'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
      <?php
            if ($this->session->flashdata('message')) {
                ?>
                <!--  start message-red -->
                <div class="box-body">
                    <div class=" alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <!--  end message-red -->
            <?php } ?>
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <!--  start message-green -->
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <!--  end message-green -->
            <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('nav_changepass'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
            <?php echo form_open_multipart(BASE_URL.'/admin/admin/changepass'); ?>
             <div class="col-md-6 ">
                 <div class="form-group">       
                <?php echo form_error('cur_pass', '<div class="alert alert-danger">', '</div>'); ?>                                    
                    <label class="control-label" for="cur_pass"><?php echo $this->lang->line('cur_pass'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                    <?php   $data = array(
                          'name'        => 'cur_pass',
                          'id'          => 'cur_pass',
                          'class'       => 'form-control',
                          'required'    => 'required',
                          'value'       => set_value('cur_pass', '', FALSE)
                        );
            
                        echo form_password($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                <div class="form-group">        
                <?php echo form_error('new_pass', '<div class="alert alert-danger">', '</div>'); ?>                                 
                    <label class="control-label" for="new_pass"><?php echo $this->lang->line('new_pass'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                        <?php   $data = array(
                          'name'        => 'new_pass',
                          'id'          => 'new_pass',
                          'class'       => 'form-control',
                          'required'    => 'required',
                          'value'       => set_value('new_pass', '', FALSE)
                        );
            
                        echo form_password($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->  
                <div class="form-group">    
                <?php echo form_error('conf_new_pass', '<div class="alert alert-danger">', '</div>'); ?>                                 
                    <label class="control-label" for="conf_new_pass"><?php echo $this->lang->line('conf_new_pass'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                        <?php   $data = array(
                          'name'        => 'conf_new_pass',
                          'id'          => 'conf_new_pass',
                          'class'       => 'form-control',
                          'required'    => 'required',
                          'value'       => set_value('conf_new_pass', '', FALSE)
                        );
            
                        echo form_password($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->            
             </div>
             
            <!--  <div class="col-md-6">
                
             </div> -->
                
                </div>
                <div class="panel-footer">
                <?php   $data = array(
                          'name'        => 'submit',
                          'id'          => 'submit',
                          'class'       => 'btn btn-primary',
                          'value'       => $this->lang->line('btn_save'),
                        );
                     echo form_submit($data); ?> 
                    <a class="btn" href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('btn_cancel'); ?></a>
                </div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>