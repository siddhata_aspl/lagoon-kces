<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('user_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-user"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/users"><?php echo $this->lang->line('user_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                	<?php echo $this->lang->line('user_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('user_edit_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
             <?php foreach ($users as $u) {
			 echo form_open_multipart(BASE_URL.'/admin/users/edited/'.$this->uri->segment(4)); ?>
             <div class="col-lg-6">
                 <div class="form-group">
                 <?php echo form_error('firstname', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="firstname"><?php echo $this->lang->line('user_new_firstname'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'firstname',
						  'id'          => 'firstname',
						  'class'       => 'form-control disabled',
						  'value'		=> set_value('username', $u['User_firstname'])
						);
			
						echo form_input($data); ?>

					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                   <div class="form-group">
                <?php echo form_error('username', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="username"><?php echo $this->lang->line('user_new_username'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'username',
						  'id'          => 'username',
						  'class'       => 'form-control disabled',
						  'value'		=> set_value('username', $u['userName']),
						  'readonly'	=> ''
						);
			
						echo form_input($data); ?>

						<p class="help-block"><?php echo $this->lang->line('user_new_message'); ?></p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
				 <div class="form-group">                
                <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('user_new_profilepic'); ?></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
					
                                            <input type="file" name="file_upload" value=""/>
					</div> <!-- /controls -->
				</div> <!-- /form-group -->   
				<div class="form-group">	
                    <img src="<?php echo BASE_URL.'/uploads/users/'.$u['user_img'] ?>" alt="" width="100px" height="100px">
                </div>
<!--                 <div class="form-group">		
                <?php //echo form_error('password', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="password"><?php //echo $this->lang->line('user_new_pass'); ?></label>
					<div class="controls">
						<?php 	
//                                                $data = array(
//						  'name'        => 'password',
//						  'id'          => 'password',
//						  'class'       => 'form-control',
//						  'value'		=> set_value('password', base64_decode($u['password']))
//						);
//			
//						echo form_password($data); 
                                                ?>
					</div>  /controls 				
				</div>  /form-group    -->
                                
             </div>
             <div class="col-lg-6">
                 <div class="form-group">
                 <?php echo form_error('lastname', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="lastname"><?php echo $this->lang->line('user_new_lastname'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'lastname',
						  'id'          => 'lastname',
						  'class'       => 'form-control disabled',
						  'value'		=> set_value('username', $u['User_lastname'])
						);
			
						echo form_input($data); ?>

					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                <div class="form-group">		
                <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="email"><?php echo $this->lang->line('user_new_email'); ?></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'email',
						  'id'          => 'email',
						  'class'       => 'form-control',
						  'value'		=> set_value('email', $u['email']),
                                                  'readonly'	=> '',
						);
			
						echo form_input($data); ?>
                                                <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->  
<!--                <div class="form-group">	
                <?php //echo form_error('con_password', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="con_password"><?php //echo $this->lang->line('user_new_confirm'); ?></label>
					<div class="controls">
						<?php 	
//                                                $data = array(
//						  'name'        => 'con_password',
//						  'id'          => 'con_password',
//						  'class'       => 'form-control',
//						  'value'		=> set_value('con_password', base64_decode($u['password']))
//						);
//			
//						echo form_password($data); 
                                                ?>
					</div>  /controls 				
				</div>  /form-group  -->
                
                <div class="form-group">		
                <?php echo form_error('phone', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="phone"><?php echo $this->lang->line('user_phone'); ?></span></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'phone',
						  'id'          => 'phone',
						  'class'       => 'form-control',
						  'placeholder' => '123-456-7890',
						  'value'		=> set_value('phone', $u['phone']),
						  //'readonly'	=> ''
						);
			
						echo form_input($data); ?>
                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
             </div>
                		
                

				
                
               

				
                </div>
                
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?> 
					<a class="btn" href="<?php echo BASE_URL; ?>/admin/users"><?php echo $this->lang->line('btn_cancel'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); 
			 }
			 ?>
			</div>
		</div>
	</div>
</div>

<script>

document.getElementById('phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

</script>

<?php echo $footer; ?>
