<?php echo $header; ?>

<style type="text/css">
    
.info th {
    background-color: #a9ceec !important;
    color: #302a75;
    font-weight: 400;
    font-size: 11px;
}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('site_order_view_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-shopping-cart"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/site_orders"><?php echo $this->lang->line('site_order_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('site_order_view_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('site_order_view_header'); ?>
                    </h3>
                </div>
                <?php //foreach ($userdata as $u) {
			  ?>
                  
        <div class="panel-body">                    
       <?php //echo $u['userName'];?>
            <br>
            <h4 style="color: blue;">User Information</h4>
            <div class="col-md-12 nopadding">
                <div class="col-md-6 nopadding">              
                    <div class="form-group">									
                        <label class="control-label" for="userName"><?php echo $this->lang->line('site_order_username'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'userName',
                                'id'    => 'userName',
                                'class' => 'form-control',
                                'readonly' => 'readonly',
                                'value' => set_value('userName', $userdata[0]['userName'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-6" style="padding-right: 0px">              
                    <div class="form-group">		
                        <label class="control-label" for="email"><?php echo $this->lang->line('site_order_email'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'email',
                                'id'    => 'email',
                                'class' => 'form-control',
                                'type' => 'email',
                                'readonly' => 'readonly',
                                'value' => set_value('email', $userdata[0]['email'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
            </div>
            <div class="col-md-12 nopadding">
                <div class="col-md-6 nopadding">              
                    <div class="form-group">		                       								
                        <label class="control-label" for="orderID"><?php echo $this->lang->line('order_id'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'orderID',
                                'id'    => 'orderID',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('orderID', $orderanddate[0]['orderID'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
                <div class="col-md-6" style="padding-right: 0px">              
                    <div class="form-group">		                     
                        <label class="control-label" for="created_datetime"><?php echo $this->lang->line('order_date'); ?></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'  => 'created_datetime',
                                'id'    => 'created_datetime',
                                'class' => 'form-control numberic',
                                'readonly' => 'readonly',
                                'value' => set_value('created_datetime', $orderanddate[0]['created_datetime'])
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->                
                </div>
            </div>
                    <br />
                    <br />          
                    <hr />
                    
                    <div class="col-md-12 nopadding" id="product_data">
                        <div id="copy_product">
                        	<br />
                            <h4 style="color: blue;">Order</h4>
                            <table class="table row1" data-id="1">
                            <thead>
                               <tr class="info">
                                    <th class="col-md-3"> PRODUCT NAME</th>
                                    <th class="col-md-2"> PRICE</th>
                                    <th class="col-md-2"> QUANTITY</th>
                                    <th class="col-md-2"> TOTAL</th>
<!--                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> PROJECT QTY & PRICE</th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart."></i> TOTAL</th>-->
                                    
                                </tr>
                            </thead>
                            <?php
                            //$i = 1;
                            if(count($orderdata) > 0)
                            {
                                $total = 0;                               
                                $count = 0;
                                foreach ($orderdata as $o) 
                                {
                                    $count++;
                            ?>
                            
                            <tbody class="breadcrumb">
                            
                            <tr>
                                <td>
                                    <?php
                                        echo $o['product_name'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo '$'.number_format($o['product_price'],2);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo $o['product_quantity'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        echo '$'.number_format($o['product_total'],2);
                                    ?>
                                </td>
<!--                                <td>
                                    <?php
//                                        $data = array(
//                                            'name'  => 'po_row0_total',
//                                            'id'    => 'po_row0_total',
//                                            'class' => 'form-control row_total',
//                                            'readonly'=>'readonly',
//                                            'type'=>'hidden',
//                                            'value' => set_value('po_row0_total', $u1['total'])
//                                        );
//                                        echo form_input($data);
//                                        echo '$'.number_format($u1['total'],2);
                                    ?>
                                </td>-->

                            </tr>
<!--                            <tr>
                                <td colspan="5">
                                    <?php
                                        //echo '<b>PROJECT NOTES </b>: '.$u1['comment'];
                                    ?>
                                </td>
                            </tr>-->
                            
                            </tbody>
                       
                            <?php
//                                $i++;
//                                }
//                            }else{
                            ?>
<!--                            <div class="col-md-12 emptypro">
                                <center>No Data Found</center>
                            </div>-->
                            <?php
                                 $total = $total+$o['product_total'];
                                }
                            }
                            ?>
                             </table>
                        </div>

                    </div>
                    <br>
                    <br>
                    <hr>
                    
                    <div class="col-md-12 nopadding">
                        <div class="table-responsive col-md-offset-8">
                            <table class="table table-striped" id="invoice" style="text-align: right;">
                                <tr>
                                    <th  style="float: right;">PRODUCTS TOTAL </th>
                                    <th>$</th>
<!--                                    <td class="total_show hidden"></td>-->
                                    <td class="total_show1"><?php echo number_format($total,2);?></td>
                                </tr>
<!--                                <tr>
                                    <th  style="float: right;">CUSTOM ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1"></td>
                                </tr>
                                <tr>
                                    <th  style="float: right;">PROJECT OVERALL  ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="overall_total hidden"></td>
                                    <td class="overall_total1"></td>
                                </tr>-->
                            </table>
                        </div>
                        <br />
                    </div>
                    
                    </div>
                <div class="panel-footer">
                    <!-- <?php 	
                            $data = array(
                              'name'        => 'submit',
                              'id'          => 'submit',
                              'class'       => 'btn btn-primary',
                              'value'	=> $this->lang->line('btn_save'),
                            );
                            echo form_submit($data); 
                    ?>  -->
                    <a class="btn" href="<?php echo BASE_URL; ?>/admin/site_orders"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    
                </div> <!-- /form-actions -->
                <?php  
                        echo form_close(); 
                        // }
                ?>
           
                

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
</style>
