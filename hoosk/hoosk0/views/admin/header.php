<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo SITE_NAME; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="<?php echo ADMIN_THEME; ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/sb-admin.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="<?php echo ADMIN_THEME; ?>/css/dataTables.fontAwesome.css" rel="stylesheet">
<script src="<?php echo ADMIN_THEME; ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery-ui-1.9.2.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.nestable.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/excanvas.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/bootstrap.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/base.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_THEME; ?>/js/dataTables.bootstrap.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<link rel="icon" href="<?php echo BASE_URL; ?>/images/<?php echo $settings['siteFavicon']; ?>" />
</head>
<body>
    <style>
        .mystatus
        {
            width:105px;
        }
        #page-wrapper
        {
            margin-bottom:50px;
        }
        .nopadding
        {
            padding: 0 !important;
        }
        .nomargin
        {
            margin: 0 !important;
        }
        .bg-blue
        {
            background-color: #337ab7 !important;
        }
    </style>
    <style>
               .side-nav{
                   background: #272973 !important;
               }
               .navbar-inverse
               {
                   background: #272973 !important;
               }
               .side-nav > li > ul{
                       background: #272973 !important;
               }
             .side-nav > li > ul > li.active{
                   background: #39b098b3 !important;
               }
               .btn-primary
               {
                   background: #272973 !important;
               }
               .footer{
                background: #272973 !important;
                }
                .side-nav li a:hover
                {
                    background: #15a589 !important;
                }
                .logo-icon {
                   height: 36px;
                   width: 170px;
                }
                .side-nav li a:focus
                {
                    background: #272973 !important;
                }
                .top-nav > li > a:hover
                {
                background: #302a75 !important;
                }
           </style>
<div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo BASE_URL;?>/admin"><img src="<?php echo BASE_URL; ?>/images/mainlogo.jpg" class="logo-icon" alt="KCES"></a>
                <!--<a class="navbar-brand" href="<?php echo BASE_URL;?>/admin"><img src="" class="logo-icon" alt="KCES"></a>-->
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
             	<!--<li>-->
              <!--      <a href="<?php echo BASE_URL; ?>" target="_blank"><i class="fa fa-home">&nbsp;</i></a>-->
              <!--  </li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user">&nbsp;</i> <?php echo $this->session->userdata('userName'); ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo BASE_URL ; ?>/admin/change_password"><i class="fa fa-fw fa-edit">&nbsp;</i> <?php echo $this->lang->line('nav_changepass'); ?></a>                        </li>
                        <li>
                            <a href="<?php echo BASE_URL ; ?>/admin/logout"><i class="fa fa-fw fa-power-off">&nbsp;</i> <?php echo $this->lang->line('nav_logout'); ?></a>                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php if ($current == "") { echo "active"; } ?>">
                    	<a href="<?php echo BASE_URL; ?>/admin"><i class="fa fa-dashboard">&nbsp;</i> <span><?php echo $this->lang->line('nav_dash'); ?></span> </a>
                    </li>
					   
             <li class='dropdown <?php if ($current == "pages" || $current == "menu" || $current == "news_events" || $current == "users" || $current == "banners" || $current == "our_people" || $current == "our_service" || $current == "our_contact" || $current == "our_career" || $current == "product_category" || $current == "product_spotlight" ) { echo "active"; } ?>'>

                      <a href="javascript:;" data-toggle="collapse" data-target="#cmsmenu"> <i class="fa fa-folder"> </i> <span>CMS</span> <i class="fa fa-fw fa-caret-down">&nbsp;</i></a>

                      <ul id="cmsmenu" class='collapse <?php if ($current == "pages" || $current == "menu" || $current == "news_events" || $current == "users" || $current == "banners" || $current == "our_people" || $current == "our_service" || $current == "our_contact" || $current == "our_career" || $current == "product_category" || $current == "product_spotlight" ) { echo "in"; } ?>'>

                        <li<?php if (($current == "pages")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/pages"><i class="fa fa-file">&nbsp;</i><?php echo $this->lang->line('nav_pages_all'); ?></a></li>

                        <li class="dropdown <?php if ($current == "menu") { echo "active"; } ?>"><a href="<?php echo BASE_URL; ?>/admin/menu"><i class="fa fa-list-alt">&nbsp;</i>  <?php echo $this->lang->line('nav_navigation_all'); ?></a></li>
                        
                        <li<?php if (($current == "news_events")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/news_events"><i class="fa fa-newspaper-o">&nbsp;</i><?php echo $this->lang->line('nav_news_events_all'); ?></a></li>

                         <li<?php if (($current == "users")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/users"><i class="fa fa-user">&nbsp;</i><?php echo $this->lang->line('nav_users_all'); ?></a></li>


                         <li<?php if (($current == "banners")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/banners"><i class="fa fa-image">&nbsp;</i><?php echo $this->lang->line('nav_banners_all'); ?></a></li>


                         <li<?php if (($current == "our_people")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/our_people"><i class="fa fa-users">&nbsp;</i><?php echo $this->lang->line('nav_peoples_all'); ?></a></li>


                         <li<?php if (($current == "our_service")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/our_service"><i class="fa fa-cog">&nbsp;</i><?php echo $this->lang->line('nav_services_all'); ?></a></li>


                         <li<?php if (($current == "our_contact")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/our_contact"><i class="fa fa-phone">&nbsp;</i><?php echo $this->lang->line('nav_contacts_all'); ?></a></li>


                          <li<?php if (($current == "our_career")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/our_career"><i class="fa fa-briefcase">&nbsp;</i><?php echo $this->lang->line('nav_careers_all'); ?></a></li>

                          

                          <li<?php if (($current == "product_category")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/product_category"><i class="fa fa-list">&nbsp;</i><?php echo $this->lang->line('nav_product_category_all'); ?></a></li>


                          <li<?php if (($current == "product_spotlight")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/product_spotlight"><i class="fa fa-lightbulb-o">&nbsp;</i><?php echo $this->lang->line('nav_product_spotlight_all'); ?></a></li>

                        


                      </ul>
              </li>
              
              <li class='dropdown <?php if ($current == "project" || $current == "company" || $current == "site_orders"  ) { echo " active"; } ?>'>

                      <a href="javascript:;" data-toggle="collapse" data-target="#pmsmenu"> <i class="fa fa-sitemap"> </i> <span>Project Management</span> <i class="fa fa-fw fa-caret-down">&nbsp;</i></a>

                      <ul id="pmsmenu" class='collapse <?php if ($current == "project" || $current == "company" || $current == "site_orders"  ) { echo " in"; } ?>'>


                          <li <?php if (($current == "company")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/company"><i class="fa fa-university">&nbsp;</i><?php echo $this->lang->line('company_header'); ?></a></li>

                          <li <?php if (($current == "project")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/project"><i class="fa fa-envelope">&nbsp;</i><?php echo $this->lang->line('nav_project_all'); ?></a></li>


                       

                          <li class="<?php if ($current == "site_orders") { echo "active"; } ?>"><a href="<?php echo BASE_URL; ?>/admin/site_orders"><i class="fa fa-shopping-cart" style="font-size: 18px;">&nbsp;</i>&nbsp;&nbsp; <span><?php echo $this->lang->line('nav_site_order'); ?></span> </a> </li>



                      </ul>
              </li>
              <li class='dropdown <?php if ($current == "email_template" || $current == "social" || $current == "settings"  ) { echo " active"; } ?>'
              >
                      <a href="javascript:;" data-toggle="collapse" data-target="#settingmenu"> <i class="fa fa-cog"> </i> <span>Settings</span> <i class="fa fa-fw fa-caret-down">&nbsp;</i></a>

                      <ul id="settingmenu" class='collapse <?php if ($current == "email_template" || $current == "social" || $current == "settings"  ) { echo " in"; } ?>'>


                          <li<?php if (($current == "email_template")&&($this->uri->segment(3)=="")) { echo ' class="active"'; } ?>><a href="<?php echo BASE_URL; ?>/admin/email_template"><i class="fa fa-envelope">&nbsp;</i> <span><?php echo $this->lang->line('nav_email_template_all'); ?></span></a></li>

                          

                          <li class="<?php if ($current == "social") { echo "active"; } ?>"><a href="<?php echo BASE_URL; ?>/admin/social"><i class="fa fa-share-alt">&nbsp;</i> <span><?php echo $this->lang->line('nav_social'); ?></span> </a> </li>

                          <li class="<?php if ($current == "settings") { echo "active"; } ?>"><a href="<?php echo BASE_URL; ?>/admin/settings"><i class="fa fa-cogs">&nbsp;</i> <span><?php echo $this->lang->line('nav_settings'); ?></span> </a> </li>



                      </ul>
              </li>

         

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
