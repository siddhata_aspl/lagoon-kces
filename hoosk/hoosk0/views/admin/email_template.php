<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!--<div class="col-lg-8">-->
            <h1 class="page-header">
                <?php echo $this->lang->line('email_template_header'); ?>
            </h1>
            <!--</div>-->
<!--            <div class="col-lg-4">
        	<div class="input-group searchContainer">
			  <input type="text" class="form-control" id="searchString">
			  <span class="input-group-btn">
				<button class="btn btn-default" id="searchBtn" type="button" onClick="doEmailTemplateSearch();"><span class="fa fa-search"></span></button>
			  </span>
			</div> /input-group 
            </div>-->
        </div>
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-envelope"></i>
                	<!--<a href="<?php echo BASE_URL; ?>/admin/banners"><?php echo $this->lang->line('email_template_header'); ?></a>-->
                	<?php echo $this->lang->line('email_template_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="container-fluid">
  	<div class="row">
      	<div class="col-md-12">
        <?php
            if ($this->session->flashdata('message')) {
                ?>
                <!--  start message-red -->
                <div class="box-body">
                    <div class=" alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                </div>
                <!--  end message-red -->
            <?php } ?>
            <?php
            if ($this->session->flashdata('success')) {
                ?>
                <!--  start message-green -->
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                    <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <!--  end message-green -->
            <?php } ?>
                <table id="record_table" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th> <?php echo $this->lang->line('email_template_title'); ?> </th>
                    <th> <?php echo $this->lang->line('email_template_subject'); ?> </th>
                    <th> <?php echo $this->lang->line('email_template_desc'); ?> </th>
                    <th class="td-actions"> <?php echo $this->lang->line('email_template_action'); ?> </th>
                  </tr>
                </thead>
                <tbody id="email_templateContainer">
                    <?php 
					foreach ($email_template as $u) {
                                                $extra = strlen($u["email_template_desc"])>=35?'...':' ';
						echo '<tr>';
						echo '<td>'.$u['email_template_title'].'</td>';
						echo '<td>'.$u['email_template_subject'].'</td>';
                                                echo '<td>'.substr(strip_tags($u["email_template_desc"]), 0, 35).' '.$extra.'</td>';
						echo '<td class="td-actions"><a href="'.BASE_URL.'/admin/email_template/edit/'.base64_encode($u['email_template_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a></td>';
						echo '</tr>';
					} ?>
                </tbody>
              </table>
              <div class="text-center" id="loadingSpinner">
              	<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
              </div>
              <?php echo $this->pagination->create_links(); ?>
        	</div>
      </div>
 </div>
<?php echo $footer; ?>
<script>
    $(document).ready(function() {
        $('#record_table').DataTable({
            
            "paging":   false,
            "aaSorting": [],
            //"searching": true,
            "columnDefs": [
                { orderable: false, targets: -1 },
             ],
                
    
        });
    });
</script>
