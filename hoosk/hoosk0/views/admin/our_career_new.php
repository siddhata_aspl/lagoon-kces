<?php echo $header; ?>
<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('career_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                  <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-briefcase"></i>
                  <a href="<?php echo BASE_URL; ?>/admin/our_career"><?php echo $this->lang->line('career_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                  <?php echo $this->lang->line('career_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('career_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
      <?php echo form_open_multipart(BASE_URL.'/admin/our_career/new/add'); ?>
             
                 <div class="form-group">   
                <?php echo form_error('career_title', '<div class="alert alert-danger">', '</div>'); ?>                  
          <label class="control-label" for="career_title"><?php echo $this->lang->line('career_title'); ?><span style="color:red"><sup>*</sup></span></label>
          <div class="controls">
                    <?php   $data = array(
              'name'        => 'career_title',
              'id'          => 'career_title',
              'class'       => 'form-control',
              'value'           => set_value('career_title', '', FALSE)
            );
      
            echo form_input($data); ?>
          </div> <!-- /controls -->       
        </div> <!-- /form-group -->
        
             
                 <div class="form-group">   
                <?php echo form_error('career_link', '<div class="alert alert-danger">', '</div>'); ?>                  
          <label class="control-label" for="career_link"><?php echo $this->lang->line('career_link'); ?><span style="color:red"><sup>*</sup></span></label>
          <div class="controls">
                    <?php   $data = array(
              'name'        => 'career_link',
              'id'          => 'career_link',
              'class'       => 'form-control',
              'value'           => set_value('career_link', '', FALSE)
            );
      
            echo form_input($data); ?>
          </div> <!-- /controls -->       
        </div> <!-- /form-group -->
        
        <div class="form-group">    
                                <?php echo form_error('career_desc', '<div class="alert alert-danger">', '</div>'); ?>                  
                            <label class="control-label" for="career_desc"><?php echo $this->lang->line('career_desc'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                $data = array(
                                    'name'  => 'career_desc',
                                    'id'    => 'career_desc',
                                    'class' => 'form-control',
                                    'value' => set_value('career_desc', $u['career_desc'])
                                );

                                echo form_textarea($data);
                                ?>
                            </div> <!-- /controls -->       
                        </div> <!-- /form-group -->
       
        
        <div class="form-group">    
                                <?php echo form_error('career_req', '<div class="alert alert-danger">', '</div>'); ?>                  
                            <label class="control-label" for="career_req"><?php echo $this->lang->line('career_req'); ?></label>
                            <div class="controls">
                                <?php
                                $data = array(
                                    'name'  => 'career_req',
                                    'id'    => 'career_req',
                                    'class' => 'form-control',
                                    'value' => set_value('career_req', $u['career_req'])
                                );

                                echo form_textarea($data);
                                ?>
                            </div> <!-- /controls -->       
                        </div> <!-- /form-group -->
                                     

                </div>
                <div class="panel-footer">
                <?php   $data = array(
              'name'        => 'submit',
              'id'          => 'submit',
              'class'       => 'btn btn-primary',
              'value'   => $this->lang->line('btn_save'),
            );
           echo form_submit($data); ?> 
          <a class="btn" href="<?php echo BASE_URL; ?>/admin/our_career"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('career_req', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>