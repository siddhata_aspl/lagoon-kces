<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('project_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-file-text-o"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/project"><?php echo $this->lang->line('nav_project_all'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('project_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('project_new_header'); ?>
                    </h3>
                </div>
                <form action="<?php echo BASE_URL . '/admin/project/new/add'; ?>" method="post" accept-charset="utf-8">
                <div class="panel-body">                    
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT INFORMATION</h4>
                    <div class="form-group">		
                        <?php echo form_error('po_name', '<div class="alert alert-danger">', '</div>'); ?>									
                        <input type="hidden" value="1" name="po_status" id="po_status">
                        <label class="control-label" for="name"><?php echo $this->lang->line('po_name'); ?><span style="color:red"><sup>*</sup></span></label>
                        <div class="controls">
                            <?php
                            $data = array(
                                'name'      => 'po_name',
                                'id'        => 'po_name',
                                'class'     => 'form-control',
                                'required' => 'required',
                                'value'     => set_value('po_name', '', FALSE)
                            );

                            echo form_input($data);
                            ?>
                        </div> <!-- /controls -->				
                    </div> <!-- /form-group -->
                    <div class="col-md-12 nopadding">
                        <div class="col-md-5 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_number', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_number'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'      => 'po_number',
                                        'id'        => 'po_number',
                                        'class'     => 'form-control',
                                        'required' => 'required',
                                        //'readonly' => 'readonly',
                                        'value'     => set_value('po_number', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-7" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_manager', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_manager'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        //'name'      => 'po_manager',
                                        'id'        => 'po_manager',
                                        'class'     => 'form-control',
                                        'required' => 'required',
                                        'onChange' => 'change_company(this.value)',
                                        'value'     => set_value('po_manager', '', FALSE)
                                    );
                                    //print_r($company);exit();
                                    foreach ($company as $value)
                                    {
                                        $option[$value['companyId']] = $value['company_name'];
                                    }
                                    
//                      
//                                    echo form_input($data);
                                    
                                    echo form_dropdown('po_manager', $option,'',$data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>   
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_adress', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_adress'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_adress',
                                        'id'    => 'po_adress',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-4" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_city', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_city'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_city',
                                        'id'    => 'po_city',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_city', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-2" style="padding-right: 0px">              
                            <div class="form-group" style="display: none;">		
                                <?php echo form_error('po_age', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_age'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_age',
                                        'id'    => 'po_age',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_age', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                    <input type="hidden" name="product_rows" id="product_rows" value="1">
                                    <input type="hidden" name="custom_rows" id="custom_rows" value="1">
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->   
                            <div class="form-group">		
                                <?php echo form_error('po_state', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_state'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_state',
                                        'id'    => 'po_state',
                                        'class' => 'form-control',
                                        'value' => set_value('po_state', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->   
                        </div>
                        
                    </div>

                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT SITE CONTACT</h4>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_name', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_site_name"><?php echo $this->lang->line('po_site_name'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_name',
                                        'id'    => 'po_site_name',
                                        'class' => 'form-control',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_email', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_site_email'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_email',
                                        'id'    => 'po_site_email',
                                        'class' => 'form-control',
                                        'type' => 'email',
                                        'value' => set_value('po_site_email', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_office_phone', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_office_phone"><?php echo $this->lang->line('po_office_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_office_phone',
                                        'id'    => 'po_office_phone',
                                        'class' => 'form-control',
                                        'placeholder' => '123-456-7890',
                                        'value' => set_value('po_office_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_cell_phone', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_cell_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_cell_phone',
                                        'id'    => 'po_cell_phone',
                                        'class' => 'form-control',
                                        'placeholder' => '123-456-7890',
                                        'value' => set_value('po_cell_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <div>
                        <h4 style="color: #302a75;float:left;font-weight: bold;">PRODUCTS</h4>
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data" style="margin-bottom: 13px;">
                        <div id="product_copy" class="hidden">
                            <table class="table row0" data-id="0" style="max-width: 140%;width: 140%;">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">PRODUCT #</th>
                                    <th class="col-md-3"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflects the product quantity needed for a project, in total. PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> <span style="display: inline-flex;vertical-align: middle;"> PROJECT <br>QTY & PRICE</span></th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="ORDER QTY reflects the product quantity to be added to the cart. This quantity will be reduced to 0, after an order is submitted."></i> <span style="display: inline-flex;vertical-align: middle;"> ORDER <br>QTY & PRICE</span></th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="REMAINING reflects the product quantity that is left to order. This quantity will be reduced to 0, after all products are ordered."></i> REMAINING </th>
                                    <th class="col-md-2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="The TOTAL reflects the current quantity pricing of ORDER QTY. This total will be added to the cart."></i> TOTAL</th>
                                    <th class="" style="background: white;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                           
                            <tr>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product',
                                            'id'    => 'po_row0_product',
                                            'autocomplete'=>'off',
                                            'class' => 'form-control productsearch',
                                            'value' => set_value('po_row0_product', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_row0"></div>
                                    <input type="hidden" name="po_row0_product_id" id="po_row0_product_id">
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_desc',
                                            'id'    => 'po_row0_product_desc',
                                            'autocomplete'=>'off',
                                            'class' => 'form-control productsearch',
                                            'value' => set_value('po_row0_product_desc', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_desc_row0"></div>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_quntity',
                                            'id'    => 'po_row0_product_quntity',
                                            'class' => 'form-control projectquantity numberic',
                                            'value' => set_value('po_row0_product_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_price1',
                                            'id'    => 'po_row0_product_price1',
                                            'class' => 'form-control productprice1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_product_price1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_product_price',
                                            'id'    => 'po_row0_product_price',
                                            'class' => 'form-control productprice',
                                            'readonly'=>'readonly',
                                            'type'  => 'hidden',
                                            'value' => set_value('po_row0_product_price', '', FALSE)
                                        );
                                        echo form_input($data)
                                    ?>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_order_quntity',
                                            'id'    => 'po_row0_order_quntity',
                                            'type'  => 'number',
                                            'min'   => 0,
                                            'class' => 'form-control productquantity numberic',
                                            'value' => set_value('po_row0_order_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td >
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_order_price',
                                            'id'    => 'po_row0_order_price',
                                            'class' => 'form-control orderprice',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_order_price', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_remaining',
                                            'id'    => 'po_row0_remaining',
                                            'class' => 'form-control row_remaining',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_remaining', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <input type="hidden" name="project_remaining_total_row0" id="project_remaining_total_row0" class="pro_remaining_total"> 
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_total',
                                            'id'    => 'po_row0_total',
                                            'class' => 'form-control row_total',
                                            'readonly'=>'readonly',
                                            'type' => 'hidden',
                                            'value' => set_value('po_row0_total', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_total1',
                                            'id'    => 'po_row0_total1',
                                            'class' => 'form-control row_total1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row0_total1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
<!--                                    <i id="row0_trash" class="fa fa-close trashproduct"></i>-->
                                    <img id="row0_trash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashproduct"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row0_comment',
                                            'id'    => 'po_row0_comment',
                                            'class' => 'form-control',
                                            'rows'   => 1,
                                            'cols'  => 50,
                                            'value' => set_value('po_row0_comment', '', FALSE)
                                        );
                                        echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        <div id="copy_product" class="table-responsive">
                            <table class="table row1" data-id="1" style="max-width: 140%;width: 140%;">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">PRODUCT #</th>
                                    <th class="col-md-3"> DESCRIPTION</th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;" data-toggle="tooltip" data-placement="bottom" title="PROJECT QTY reflects the product quantity needed for a project, in total. PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders."></i> <span style="display: inline-flex;vertical-align: middle;"> PROJECT <br>QTY & PRICE</span></th>
                                    <th class="col-md-2" colspan="2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="ORDER QTY reflects the product quantity to be added to the cart. This quantity will be reduced to 0, after an order is submitted."></i> <span style="display: inline-flex;vertical-align: middle;"> ORDER <br>QTY & PRICE</span></th>
                                    <th class="col-md-1">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="REMAINING reflects the product quantity that is left to order. This quantity will be reduced to 0, after all products are ordered."></i> REMAINING </th>
                                    <th class="col-md-2">
                                        <i class="fa fa-question-circle white-tooltip" style="font-size: 17px;"  data-toggle="tooltip" data-placement="bottom" title="The TOTAL reflects the current quantity pricing of ORDER QTY. This total will be added to the cart."></i> TOTAL</th>
                                    <th class="" style="background: white;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody class="breadcrumb">
                            
                            <tr>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_product',
                                            'id'    => 'po_row1_product',
                                            'class' => 'form-control productsearch',
                                            'autocomplete'=>'off',
                                            'value' => set_value('po_row1_product', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_row1"></div>
                                    <input type="hidden" name="po_row1_product_id" id="po_row1_product_id">
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_product_desc',
                                            'id'    => 'po_row1_product_desc',
                                            'class' => 'form-control productsearch',
                                            'autocomplete'=>'off',
                                            'value' => set_value('po_row1_product_desc', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <div id="suggesstion-box" class="sugg_desc_row1"></div>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_product_quntity',
                                            'id'    => 'po_row1_product_quntity',
                                            'class' => 'form-control projectquantity numberic',
                                            'value' => set_value('po_row1_product_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_product_price1',
                                            'id'    => 'po_row1_product_price1',
                                            'class' => 'form-control productprice1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row1_product_price1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_product_price',
                                            'id'    => 'po_row1_product_price',
                                            'class' => 'form-control productprice',
                                            'readonly'=>'readonly',
                                            'type'  => 'hidden',
                                            'value' => set_value('po_row1_product_price', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td style="width: 170px;">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_order_quntity',
                                            'id'    => 'po_row1_order_quntity',
                                            'type'  => 'number',
                                            'min'   => 0,
                                            'class' => 'form-control productquantity numberic',
                                            'value' => set_value('po_row1_order_quntity', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_order_price',
                                            'id'    => 'po_row1_order_price',
                                            'class' => 'form-control orderprice',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row1_order_price', '', FALSE)
                                        ); 
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_remaining',
                                            'id'    => 'po_row1_remaining',
                                            'class' => 'form-control row_remaining',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row1_remaining', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                </td>
                                <td>
                                    <input type="hidden" name="project_remaining_total_row1" id="project_remaining_total_row1" class="pro_remaining_total"> 
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_total',
                                            'id'    => 'po_row1_total',
                                            'type' => 'hidden',
                                            'class' => 'form-control row_total',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row1_total', '', FALSE)
                                        );
                                        echo form_input($data);
                                    ?>
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_total1',
                                            'id'    => 'po_row1_total1',
                                            'class' => 'form-control row_total1',
                                            'readonly'=>'readonly',
                                            'value' => set_value('po_row1_total1', '', FALSE)
                                        );
                                        echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                    ?>
                                </td>
                                <td rowspan="2"  class="" style="background: white;padding: 0;vertical-align: inherit;">
                                    <!--<i id="row1_trash" class="fa fa-close trashproduct"></i>-->
                                    <img id="row1_trash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashproduct"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <?php
                                        $data = array(
                                            'name'  => 'po_row1_comment',
                                            'id'    => 'po_row1_comment',
                                            'class' => 'form-control',
                                            'rows'   => 1,
                                            'cols'  => 50,
                                            'value' => set_value('po_row1_comment', '', FALSE)
                                        );
                                        echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                        <div style="height: 20px;">&nbsp;</div>
                        <a href="javascript:void(0)" id="product_div" style="color: #302a75;font-size: 13px;font-weight: lighter;text-decoration: none;">
                            <i class="fa fa-plus pls-prdct"></i> ADD ANOTHER ITEM
                        </a>
                    </div>
                    <div style="margin: 30px 0px; ">&nbsp;</div>
                    <hr  style="clear: both;border-top: 1px solid black;">
                    <div style="margin-top: 10px;">
                        <h4 style="color: #302a75;float:left;font-weight: bold;">CUSTOM ITEMS</h4>
                        <div class="toggle1"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="custom_data" style="margin-bottom: 13px;">
                        <div id="custom_copy" class="hidden">
                            <table class="table customrow0">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                    <th class="col-md-2">ITEM</th>
                                    <th class="col-md-4"> DESCRIPTION</th>
                                    <th class="col-md-2">QTY</th>
                                    <th class="col-md-2">PRICE</th>
                                    <th class="col-md-2">TOTAL</th>
                                    <th class="" style="background: white;"></th>
                                </tr>
                            </thead>

                            <tbody class="breadcrumb">
                                <tr>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_item',
                                                'id'    => 'po_row0_custom_item',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row0_custom_item', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_desc',
                                                'id'    => 'po_row0_custom_desc',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row0_custom_desc', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_quantity',
                                                'id'    => 'po_row0_custom_quantity',
                                                'type' => 'number',
                                                'min'  => 0,
                                                'class' => 'form-control customquantity get_custom_total numberic',
                                                'value' => set_value('po_row0_custom_quantity', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_price',
                                                'id'    => 'po_row0_custom_price',
                                                'step' => '0.01',
                                                'type' => 'number',
                                                'min'  => 0,
                                                'class' => 'form-control customprice get_custom_total numberic',
                                                'value' => set_value('po_row0_custom_price', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td>
                                        
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_total',
                                                'id'    => 'po_row0_custom_total',
                                                'class' => 'form-control row_custom_total',
                                                'readonly'=>'readonly',
                                                'value' => set_value('po_row0_custom_total', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
<!--                                        <i class="fa fa-close trashcustom" id="row0_customtrash"></i>-->
                                        <img id="row0_customtrash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashcustom"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row0_custom_comment',
                                                'id'    => 'po_row0_custom_comment',
                                                'class' => 'form-control',
                                                'rows'  => "1",
                                                'cols'  => "50",
                                                'value' => set_value('po_row0_custom_comment', '', FALSE)
                                            );
                                            echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <div id="set_copy">
                            <table class="table customrow1">
                            <thead>
                                <tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;">
                                     <th class="col-md-2">ITEM</th>
                                    <th class="col-md-4"> DESCRIPTION</th>
                                    <th class="col-md-2">QTY</th>
                                    <th class="col-md-2">PRICE</th>
                                    <th class="col-md-2">TOTAL</th>
                                    <th class="" style="background: white;"></th>
                                </tr>
                            </thead>

                            <tbody class="breadcrumb">
                                <tr>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_item',
                                                'id'    => 'po_row1_custom_item',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row1_custom_item', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_desc',
                                                'id'    => 'po_row1_custom_desc',
                                                'class' => 'form-control',
                                                'value' => set_value('po_row1_custom_desc', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_quantity',
                                                'id'    => 'po_row1_custom_quantity',
                                                'type' => 'number',
                                                'min'  => 0,
                                                'class' => 'form-control customquantity get_custom_total numberic',
                                                'value' => set_value('po_row1_custom_quantity', '', FALSE)
                                            );
                                            echo form_input($data);
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_price',
                                                'id'    => 'po_row1_custom_price',
                                                'step' => '0.01',
                                                'type' => 'number',
                                                'min'  => 0,
                                                'class' => 'form-control customprice get_custom_total numberic',
                                                'value' => set_value('po_row1_custom_price', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_total',
                                                'id'    => 'po_row1_custom_total',
                                                'class' => 'form-control row_custom_total',
                                                'readonly'=>'readonly',
                                                'value' => set_value('po_row1_custom_total', '', FALSE)
                                            );
                                            echo '<div class="input-group dollarinput"><span class="input-group-addon" style="padding: 9px 15px;">$</span>'.form_input($data).'</div>';
                                        ?>
                                    </td>
                                    <td rowspan="2" style="background: white;padding: 0;vertical-align: inherit;">
                                        <!--<i class="fa fa-close trashcustom" id="row1_customtrash"></i>-->
                                        <img id="row1_customtrash" src="<?php echo base_url().'images/delbtn.png'; ?>" alt="" style="height: 30px;width: 30px;" class="trashcustom"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <?php
                                            $data = array(
                                                'name'  => 'po_row1_custom_comment',
                                                'id'    => 'po_row1_custom_comment',
                                                'class' => 'form-control',
                                                'rows'  => "1",
                                                'cols'  => "50",
                                                'value' => set_value('po_row1_custom_comment', '', FALSE)
                                            );
                                            echo '<div class="input-group"><span class="input-group-addon" style="padding: 9px 15px;">PROJECT NOTES:</span>'.form_textarea($data).'</div>';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <a href="javascript:void(0)" id="custom_div" style="color: #302a75;font-size: 13px;font-weight: lighter;text-decoration: none;">
                            <i class="fa fa-plus"></i> ADD ANOTHER ITEM
                        </a>
                    </div>
                    <hr  style="clear: both;border-top: 3px solid black;">
                    <div class="col-md-12 nopadding" style="margin-top: 15px;">
                        <div class="table-responsive col-md-offset-7">
                            <table class="table table-striped" id="invoice" style="text-align: right;">
                                <tr>
                                    <th style="float: right;">PRODUCTS TOTAL </th>
                                    <th>$</th>
                                    <td class="total_show hidden"></td>
                                    <td class="total_show1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">PRODUCTS TOTAL REMAINING </th>
                                    <th>$</th>
                                    <td class="total_show_remaining hidden"></td>
                                    <td class="total_show_remaining1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">CUSTOM ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1"></td>
                                </tr>
                                <tr>
                                    <th style="float: right;">PROJECT OVERALL  ITEM TOTAL </th>
                                    <th>$</th>
                                    <td class="overall_total hidden"></td>
                                    <td class="overall_total1"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <hr  style="clear: both;border-top: 1px solid black;">
                    <span style="color: red;float: right">Pricing is subject to change</span>
                    <div class="col-md-12 nopadding" style="margin-top: 15px;">
                        <div class="table-responsive col-md-offset-6">
                            <div class="col-md-5"><button class="btn btn-info col-md-12" type="button">Export CSV</button></div>
                            <div class="col-md-offset-2 col-md-5 nopadding"><button id="pdfdownload" type="button" class="btn btn-info col-md-12">Download PDF</button>
                                <br>
                                <span style="color: #302a75;font-size: 10px;">Download to save a copy of your original pricing for your records.</span>
                            </div>
                        </div>
                        <center>
                            <div class="g-recaptcha" data-sitekey="6Lcyhm4UAAAAALTANNVA1Sk5APoZtjVcnxOPgizx"></div>
                        </center>
                    </div>
                    
                    </div>
                <div class="panel-footer">
                    <center>
                    <?php 	
                            $data = array(
                              'name'        => 'submit',
                              'id'          => 'submit',
                              'class'       => 'btn btn-primary',
                              'value'	=> $this->lang->line('btn_save'),
                            );
                            echo form_submit($data); 
                    ?>
                    <?php 	
                            $data = array(
                              'name'        => 'submit_order',
                              'id'          => 'submit_order',
                              'class'       => 'btn btn-primary',
//                              'type'      =>   'button',
                              'value'	=> $this->lang->line('btn_submit_order'),
                            );
                            echo form_submit($data); 
                    ?>
                    <a class="btn btn-danger" href="<?php echo BASE_URL; ?>/admin/project"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    </center>   
                </div> <!-- /form-actions -->
            </form>
                

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
.white-tooltip + .tooltip > .tooltip-inner {background-color: #fff;color: black;border: 1px solid #302a75;color: #302a75}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 999;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
.toggle{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle1{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.toggle1.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
</style>
<script>
        $("#product_div").click(function () {
          
            var next_num = $('#product_rows').val();
            next_num = parseInt(next_num) + 1;
            $('#product_rows').val(next_num);
            var a = $('#product_copy').html().replace(/row0/g,"row"+next_num);
            a = a.replace(/data-id="0"/g,"data-id='"+next_num+"'");
            $('#copy_product').append(a);
            $('[data-toggle="tooltip"]').tooltip();
        });

       $("#custom_div").click(function () {
            
            var next_num = $('#custom_rows').val();
            next_num = parseInt(next_num) + 1;
            $('#custom_rows').val(next_num);
            //var next_num = $('#custom_data table').length;
            var b = $('#custom_copy').html().replace(/row0/g,"row"+next_num);
            $('#set_copy').append(b);
            $('[data-toggle="tooltip"]').tooltip();
            
        });
        
        $(document).on('click', '.trashproduct', function(){
            
            console.log("enter");
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            console.log(close_div);
            if($('#product_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                gettotal();
            }
            else
            {
                
            }
            
            
            
        });
            
            
        $(document).on('click', '.trashcustom', function(){
            
            //console.log($(this).attr('id'));
            var close_div = $(this).attr('id');
            //console.log(close_div);
            if($('#custom_data table').length > 2)
            {
                $('#'+close_div).closest('table').remove();
                getcustomtotal();
            }
            else
            {
                
            }
            
        });
        
        $(document).on('change', '.projectquantity', function(){
            
                var val = parseInt($(this).val());
                if(!isNaN(val) && val > 0)
                {
                    $(this).val(val);
                }
                else
                {
                    val = 0;
                    $(this).val(val);
                }
                var product_row_id = $(this).attr('id');
                if(product_row_id != '')
                {
                    var project_total = $('#'+product_row_id).closest('tr').find('.productquantity').val();  
                    var unit_price = $('#'+product_row_id).closest('tr').find('.productprice').val();
                    if(project_total != '')
                    {
                            if(project_total > val)
                            {
                                alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                                $(this).val(0);
                                $('#'+product_row_id).closest('tr').find('.productquantity').val(0);
                                $('#'+product_row_id).closest('tr').find('.row_remaining').val(0);
                            }
                            else
                            {
                                var tal = parseInt(val) - parseInt(project_total);
                                if(isNaN(tal)) {
                                    var tal = 0;
                                }
                                
                                var total1 = parseFloat(unit_price)* parseInt(val);
                                var total2 = parseFloat(unit_price)* (parseInt(val) - parseInt(project_total));
                                $('#'+product_row_id).closest('tr').find('.row_remaining').val(tal);
                                $('#'+product_row_id).closest('tr').find('.productprice1').val(addCommas(total1.toFixed(2)));
                                var total = parseFloat(unit_price)* parseInt(tal);
                                $('#'+product_row_id).closest('tr').find('.pro_remaining_total').val(total2.toFixed(2));
                                $('#'+product_row_id).closest('tr').find('.productquantity').attr('max',val);
                            }
                            
                    }
                }
                
        });
        
        $(document).on('change', '.productquantity', function(){
                
                var val = parseInt($(this).val());
                if(!isNaN(val) && val > 0)
                {
                    $(this).val(val);
                }
                else
                {
                    val = 0;
                    $(this).val(val);
                }
                var product_row_id = $(this).attr('id');
                //console.log(product_row_id);
                if(product_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+product_row_id).closest('tr').find('.productprice').val();
                    var project_total = $('#'+product_row_id).closest('tr').find('.projectquantity').val();   
                    if(project_total < val )
                    {
                        alert("Sorry, the Order Quantity cannot exceed Project Quantity. Please change quantity.");
                        $(this).val(0);
                        val = 0;
                    }
                    if(project_total != '')
                    {
                            //alert(project_total);
                            //alert(val);
                            var tal1 = parseInt(project_total) - parseInt(val);
                            if(isNaN(tal1)) {
                                    var tal1 = 0;
                                }
                            $('#'+product_row_id).closest('tr').find('.row_remaining').val(tal1);
                            
                    }
                    if(unit_price != '')
                    {
                        //console.log(unit_pri  ce);
                        
                        var total = parseFloat(unit_price)* parseInt(val);
                        var total1 = parseFloat(unit_price)* (parseInt(project_total) - parseInt(val));
                        if(!isNaN(total))
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(total.toFixed(2));
                            $('#'+product_row_id).closest('tr').find('.orderprice').val(addCommas(total.toFixed(2)));
                            $('#'+product_row_id).closest('tr').find('.pro_remaining_total').val(total1.toFixed(2));
                            $('#'+product_row_id).closest('tr').find('.row_total1').val(addCommas(total.toFixed(2)));
                        }
                        else
                        {
                            $('#'+product_row_id).closest('tr').find('.row_total').val(0);
                        }
//                        $('#'+product_row_id).closest('tr').find('.row_total').val(unit_price*val);
                    }
                }
                
                gettotal();
                
                
        });
        
    
        $('#get_pdf').click(function (){

            var html = $("html").html();
                $.ajax({
                type: "POST",
                url: '<?php echo base_url().'admin/project/test'; ?>',
                data:'fullhtml='+html,
                //contentType: "application/json",
                success: function(data){

                }
            });
        });
//        $('#submit_order').click(function (){
//
//            var html = $("html").html();
//                $.ajax({
//                type: "POST",
//                url: '<?php echo base_url().'admin/project/test'; ?>',
//                data:'fullhtml='+html,
//                //contentType: "application/json",
//                success: function(data){
//
//                }
//            });
//        });
        
        $(document).on('change', '.get_custom_total', function(){
            
                var custom_row_id = $(this).attr('id');
                if(custom_row_id != '')
                {
                    //console.log(product_row_id);
                    var unit_price = $('#'+custom_row_id).closest('tr').find('.customprice').val();
                    var quantity = $('#'+custom_row_id).closest('tr').find('.customquantity').val();
                    if(unit_price != '')
                    {
                        //console.log(unit_price);
                        var total = parseFloat(unit_price) * parseInt(quantity) ;
                        if(!isNaN(total))
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(total.toFixed(2));
                        }
                        else
                        {
                            $('#'+custom_row_id).closest('tr').find('.row_custom_total').val(0);
                        }
                        //$('#'+custom_row_id).closest('tr').find('.row_custom_total').val(unit_price*quantity);
                    }
                }
                
                getcustomtotal();
                
                
        });
        $(document).ready(function () {

            $('input').attr('ondrop','return false;');
            $('input').attr('onpaste','return false;');
            
            $('#pdfdownload').click(function(e){
                $('#po_status').val('2');
                $('#submit')[0].click();
                
            })
//            $('#submit_order').click(function(e){
//                $('#po_status').val('2');
//                $('#submit_order')[0].click();
//                
//            })
            
                var $content = $("#product_data").show();
                $(".toggle").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content.slideToggle();
                });
                
                var $content1 = $("#custom_data").show();
                $(".toggle1").on("click", function(e){
                  $(this).toggleClass("expanded");
                  $content1.slideToggle();
                });
            
            $('[data-toggle="tooltip"]').tooltip();
            //$(".numberic").keydown(function (e) {
                $(document).on('keydown', '.numberic', function(e){
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
//        $(document).ready(function(){
            //$(".productsearch").keyup(function(){
            $(document).on('keyup', '.productsearch', function(){
                    //alert("dsadsads");
                    var textboxname = $(this).attr('name');
//                    console.log(textboxname);
                    var dataid = $('input[name='+textboxname+']').closest('table').data("id");
                    //console.log($('input[name='+textboxname+']').closest('table').data("id"));
                    var type= '';
                    //if(textboxname == 'po_row1_product_desc')
                    //console.log(dataid);
                    if(textboxname.toString().indexOf('product_desc') != -1)
                    {
                        type = 'desc';
                    }
                    else
                    {
                        type = 'title';
                    }
                    $.ajax({
                    type: "POST",
                    url: '<?php echo base_url().'admin/project/getproduct'; ?>',
                    data:'productname='+$(this).val()+'&rowid='+dataid+'&type='+type,
                    beforeSend: function(){
                            $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                    },
                    success: function(data){
                        if(textboxname.toString().indexOf('product_desc') != -1)
                        {
                            $(".sugg_desc_row"+dataid).show();
                            $(".sugg_desc_row"+dataid).html(data);
                        }
                        else
                        {
                            $(".sugg_row"+dataid).show();
                            $(".sugg_row"+dataid).html(data);
                            $("#po_row"+dataid+"_product").css("background","#FFF");
                            
                            //$("#po_row"+dataid+"_product").css("background","#FFF");
                        }
                    }
                    });
            });
            
//        });
        function selectCountry(val,dataid) {
             if(val != '' && dataid != '')
               {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'admin/project/getidproduct'; ?>',
                        data:'productid='+val,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_row"+dataid+"_product").val(data[0]['product_name']);
                                $("#po_row"+dataid+"_product_desc").val(data[0]['product_desc']);
                                $("#po_row"+dataid+"_order_price").val(addCommas(0));
                                $("#po_row"+dataid+"_product_price").val(data[0]['product_unit_price']);
                                $("#po_row"+dataid+"_product_price1").val(addCommas(0));
                                $("#po_row"+dataid+"_product_quntity").val(0);
                                $("#po_row"+dataid+"_order_quntity").val(0);
                                $("#po_row"+dataid+"_remaining").val(0);
                                $("#po_row"+dataid+"_total1").val(0);
                                $("#po_row"+dataid+"_product_id").val(data[0]['product_id']);
                                $(".sugg_row"+dataid).hide();
                                $(".sugg_desc_row"+dataid).hide();
                                  
                        }
                    });
               }
               else
               {
                    $("#po_row"+dataid+"_product").val('');
                    $("#po_row"+dataid+"_product").val('');
                    $("#po_row"+dataid+"_product_desc").val('');
                    $("#po_row"+dataid+"_order_price").val('');
                    $("#po_row"+dataid+"_product_price").val('');
                    $("#po_row"+dataid+"_product_price1").val('');
                    $("#po_row"+dataid+"_product_quntity").val('');
                    $("#po_row"+dataid+"_order_quntity").val('');
                    $("#po_row"+dataid+"_remaining").val('');
                    $("#po_row"+dataid+"_product_id").val('');
                    $(".sugg_row"+dataid).hide();
                    $(".sugg_desc_row"+dataid).hide();
               }
            
        }
        change_company();
        function change_company(id)
        {
                    id = $('#po_manager').val();
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'admin/project/getidcompany'; ?>',
                        data:'companyid='+id,
                        beforeSend: function(){
                                $("#po_row1_product").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_adress").val(data[0]['address']);
                                $("#po_city").val(data[0]['city']);
                                
                        }
                    });
        }
        function gettotal()
        {
                var pro_remaining_total = 0;
                var pro_total_all = 0;
                
                $('.pro_remaining_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_remaining_total = parseFloat(pro_remaining_total) + parseFloat($(this).val());
                    }
                });
                $('.row_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                $('.total_show_remaining').html(parseFloat(pro_remaining_total).toFixed(2));
                $('.total_show_remaining1').html(addCommas(parseFloat(pro_remaining_total).toFixed(2)));
                var ct = !isNaN(parseInt($('.total_custom_show').html())) ? parseInt($('.total_custom_show').html()) : 0;
                var t = !isNaN(parseInt($('.total_show').html())) ? parseInt($('.total_show').html()) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t)
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));
                
                //$('.overall_total').html(parseInt($('.total_custom_show').html())+parseInt($('.total_show').html()));
        }
        function getcustomtotal()
        {
                var pro_total_all = 0;
                $('.row_custom_total').each(function() {
                    if(parseInt($(this).val()) > 0 )
                    {
                        pro_total_all = parseFloat(pro_total_all) + parseFloat($(this).val());
                    }
                });
                $('.total_custom_show').html(parseFloat(pro_total_all).toFixed(2));
                $('.total_custom_show1').html(addCommas(parseFloat(pro_total_all).toFixed(2)));
                var ct = !isNaN(parseFloat($('.total_custom_show').html())) ? parseFloat($('.total_custom_show').html()).toFixed(2) : 0;
                var t = !isNaN(parseFloat($('.total_show').html())) ? parseFloat($('.total_show').html()).toFixed(2) : 0 ;
                var t_t = parseFloat(ct) + parseFloat(t);
                $('.overall_total').html(t_t.toFixed(2));
                $('.overall_total1').html(addCommas(t_t.toFixed(2)));   
//                $('.overall_total').html(parseInt($('.total_custom_show').html())+parseInt($('.total_show').html()));
        }
        function addCommas(nStr)
        {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
        }
        
        $("form").submit(function(event) {
            var recaptcha = $("#g-recaptcha-response").val();
            if (recaptcha === "") {
               event.preventDefault();
               alert("Please check the recaptcha");
            }
         });
    </script>

<script>

document.getElementById('po_cell_phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

document.getElementById('po_office_phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

</script>
    