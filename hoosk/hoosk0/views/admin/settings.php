<?php echo $header; ?> 
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('settings_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-cogs "></i>
                	<a href="<?php echo BASE_URL; ?>/admin/settings"><?php echo $this->lang->line('settings_header'); ?></a>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  	<div class="row">
      	<div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-cogs fa-fw"></i>
                    <?php echo $this->lang->line('settings_header'); ?>
                </h3>
            </div>
         	<div class="panel-body">
            <?php echo $this->lang->line('settings_message'); ?>
            </div> 
          </div>


	<div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fa fa-cogs fa-fw"></i>
                <?php echo $this->lang->line('settings_info'); ?>
            </h3>
        </div>
        <div class="panel-body">
      		<div class="form-group">	
            <?php foreach ($settings as $s) {
			echo form_open_multipart(BASE_URL.'/admin/settings/update'); ?>
            		<?php echo form_error('siteTitle', '<div class="alert alert-danger">', '</div>'); ?>						
					<label class="control-label" for="siteTitle"><?php echo $this->lang->line('settings_name'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'siteTitle',
						  'id'          => 'siteTitle',
						  'class'       => 'form-control',
						  'value'		=> set_value('siteTitle', $s['siteTitle'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                
            <div class="form-group">	
					<label class="control-label" for="siteFooter"><?php echo $this->lang->line('settings_footer'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'siteFooter',
						  'id'          => 'siteFooter',
						  'class'       => 'form-control',
						  'value'		=> set_value('siteFooter', $s['siteFooter'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
            <div class="form-group">	
					<label class="control-label" for="siteFooter2"><?php echo $this->lang->line('settings_footer2'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'siteFooter2',
						  'id'          => 'siteFooter2',
						  'class'       => 'form-control',
						  'value'		=> set_value('siteFooter2', $s['siteFooter2'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
            
<!--            <div class="form-group">
					<label class="control-label" for="themes"><?php echo $this->lang->line('settings_theme'); ?></label>
					<div class="controls">
					<?php
//						$att = 'id="siteTheme" class="form-control"';
//						$data = array();
//						foreach ($themesdir as $t){
//							if (!is_dir($t)){
//								if (($t != "index.html") && ($t != "admin/") && ($t != "admin")){
//									$t = str_replace("/", "", $t);
//									$data[$t] = $t;	
//								}
//							}
//						}
//	
//						echo form_dropdown('siteTheme', $data, $s['siteTheme'], $att); ?>
					</div>  /controls 				
				</div>  /form-group -->
<!--                <div class="form-group">
					<label class="control-label" for="siteLang"><?php echo $this->lang->line('settings_lang'); ?></label>
					<div class="controls">
					<?php
//						$att = 'id="siteLang" class="form-control"';
//						$data = array();
//						foreach ($langdir as $l){
//							if (!is_dir($l)){
//								if ($l != "index.html"){
//									$l = str_replace("/", "", $l);
//									$data[$l] = $l;		
//								}
//							}
//						}
//						
//						echo form_dropdown('siteLang', $data, $s['siteLang'], $att); ?>
					</div>  /controls 				
				</div>  /form-group -->
            <hr />
            
				<div class="form-group">		
            		<?php echo form_error('file_upload', '<div class="alert">', '</div>'); ?>									
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('settings_logo'); ?></label>
					<div class="controls">
						<div><img src="<?php if ($s['siteLogo'] != "") { echo BASE_URL.'/images/'.$s['siteLogo']; } ?>" id="logo_preloaded" style="width:150px" <?php if ($s['siteLogo'] == "") { echo "style='display:none;width:150px'"; } ?>></div>
						<img src="<?php echo BASE_URL; ?>/theme/admin/images/ajax-loader.gif" style="margin:-7px 5px 0 5px;display:none;width:150px" id="loading_pic" />
						<?php
							$data = array(
								'name'		=> 'file_upload',
								'id'		=> 'file_upload',
								'class'		=> 'form-control'
							);
							echo form_upload($data); 
						?>
						<input type="hidden" id="siteLogo" name="siteLogo" />
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
               <hr />
               <div class="form-group">	
					<label class="control-label" for="LogoBottomTitle"><?php echo $this->lang->line('settings_logobottomtitle'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'LogoBottomTitle',
						  'id'          => 'LogoBottomTitle',
						  'class'       => 'form-control',
						  'value'		=> set_value('LogoBottomTitle', $s['LogoBottomTitle'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
	           </div> <!-- /form-group -->
               <div class="form-group">		
            		<?php echo form_error('file_upload', '<div class="alert">', '</div>'); ?>									
					<label class="control-label" for="favicon_upload"><?php echo $this->lang->line('settings_favicon'); ?></label>
					<div class="controls">
						<div><img src="<?php if ($s['siteFavicon'] != "") { echo BASE_URL.'/images/'.$s['siteFavicon']; } ?>" id="favicon_preloaded" <?php if ($s['siteFavicon'] == "") { echo "style='display:none;'"; } ?>></div>
						<img src="<?php echo BASE_URL; ?>/theme/admin/images/ajax-loader.gif" style="margin:-7px 5px 0 5px;display:none;" id="loading_pic_favicon" />
						<?php
							$data = array(
								'name'		=> 'favicon_upload',
								'id'		=> 'favicon_upload',
								'class'		=> 'form-control'
							);
							echo form_upload($data); 
						?>
						<input type="hidden" id="siteFavicon" name="siteFavicon" />
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                <div class="form-group">		
            		<?php echo form_error('file_upload', '<div class="alert">', '</div>'); ?>									
					<label class="control-label" for="footer_logo_upload"><?php echo $this->lang->line('settings_footer_logo'); ?></label>
					<div class="controls">
						<div><img src="<?php if ($s['siteFooterLogo'] != "") { echo BASE_URL.'/uploads/'.$s['siteFooterLogo']; } ?>" id="footer_logo_preloaded" style="width:150px" <?php if ($s['siteFooterLogo'] == "") { echo "style='display:none;width:150px'"; } ?>></div>
						<img src="<?php echo BASE_URL; ?>/theme/admin/images/ajax-loader.gif" style="margin:-7px 5px 0 5px;display:none;width:150px" id="loading_pic_footer_logo" />
						<?php
							$data = array(
								'name'		=> 'footer_logo_upload',
								'id'		=> 'footer_logo_upload',
								'class'		=> 'form-control'
							);
							echo form_upload($data); 
						?>
						<input type="hidden" id="siteFooterLogo" name="siteFooterLogo" />
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                <hr />
                <div class="form-group">	
					<label class="control-label" for="add1_title"><?php echo $this->lang->line('settings_address1_title'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1_title',
						  'id'          => 'add1_title',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1_title', $s['add1_title'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add1"><?php echo $this->lang->line('settings_address1'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1',
						  'id'          => 'add1',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1', $s['add1'], FALSE)
						);
			
						echo form_textarea($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add1_phone"><?php echo $this->lang->line('settings_address1_phone'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1_phone',
						  'id'          => 'add1_phone',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1_phone', $s['add1_phone'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add1_fax"><?php echo $this->lang->line('settings_address1_fax'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1_fax',
						  'id'          => 'add1_fax',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1_fax', $s['add1_fax'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add1_hours"><?php echo $this->lang->line('settings_address1_hours'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1_hours',
						  'id'          => 'add1_hours',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1_hours', $s['add1_hours'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add1_map"><?php echo $this->lang->line('settings_address1_map'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add1_map',
						  'id'          => 'add1_map',
						  'class'       => 'form-control',
						  'value'		=> set_value('add1_hours', $s['add1_map'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2_title"><?php echo $this->lang->line('settings_address2_title'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2_title',
						  'id'          => 'add2_title',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2_title', $s['add2_title'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2"><?php echo $this->lang->line('settings_address2'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2',
						  'id'          => 'add2',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2', $s['add2'], FALSE)
						);
			
						echo form_textarea($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2_phone"><?php echo $this->lang->line('settings_address2_phone'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2_phone',
						  'id'          => 'add2_phone',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2_phone', $s['add2_phone'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2_fax"><?php echo $this->lang->line('settings_address2_fax'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2_fax',
						  'id'          => 'add2_fax',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2_fax', $s['add2_fax'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2_hours"><?php echo $this->lang->line('settings_address2_hours'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2_hours',
						  'id'          => 'add2_hours',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2_hours', $s['add2_hours'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                <div class="form-group">	
					<label class="control-label" for="add2_map"><?php echo $this->lang->line('settings_address2_map'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'add2_map',
						  'id'          => 'add2_map',
						  'class'       => 'form-control',
						  'value'		=> set_value('add2_hours', $s['add2_map'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->

		<div class="form-group">	
					<label class="control-label" for="domain"><?php echo $this->lang->line('settings_domain'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'domain',
						  'id'          => 'domain',
						  'class'       => 'form-control',
						  'value'		=> set_value('domain', $s['domain'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                
                <div class="form-group">	
					<label class="control-label" for="api_key"><?php echo $this->lang->line('settings_api_key'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'api_key',
						  'id'          => 'api_key',
						  'class'       => 'form-control',
						  'value'		=> set_value('api_key', $s['api_key'], FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
		</div> <!-- /form-group -->
                
<!--                <div class="form-group">
					<label class="control-label" for="siteMaintenance"><?php echo $this->lang->line('settings_maintenance'); ?></label>
					<div class="controls">
					<?php
//						$att = 'id="siteMaintenance" class="form-control"';
//						$data = array(
//						"0" => "Disabled",
//						"1" => "Enabled"
//						);
//						
//						
//						echo form_dropdown('siteMaintenance', $data, $s['siteMaintenance'], $att); ?>
					</div>  /controls 				
				</div>  /form-group -->
<!--               <div class="form-group">	
					<label class="control-label" for="siteMaintenanceHeading"><?php echo $this->lang->line('settings_maintenance_heading'); ?></label>
					<div class="controls">
                    <?php 	
//                    $data = array(
//						  'name'        => 'siteMaintenanceHeading',
//						  'id'          => 'siteMaintenanceHeading',
//						  'class'       => 'form-control',
//						  'value'		=> set_value('siteMaintenanceHeading', $s['siteMaintenanceHeading'], FALSE)
//						);
//			
//						echo form_input($data); ?>
					</div>  /controls 				
				</div>  /form-group -->
<!--               <div class="form-group">	
					<label class="control-label" for="siteMaintenanceMeta"><?php echo $this->lang->line('settings_maintenance_meta'); ?></label>
					<div class="controls">
                    <?php 	
//                    $data = array(
//						  'name'        => 'siteMaintenanceMeta',
//						  'id'          => 'siteMaintenanceMeta',
//						  'class'       => 'form-control',
//						  'value'		=> set_value('siteMaintenanceMeta', $s['siteMaintenanceMeta'], FALSE)
//						);
//			
//						echo form_input($data); ?>
					</div>  /controls 				
				</div>  /form-group -->
<!--                <div class="form-group">	
					<label class="control-label" for="siteMaintenanceContent"><?php echo $this->lang->line('settings_maintenance_content'); ?></label>
					<div class="controls">
                    <?php 	
//                    $data = array(
//						  'name'        => 'siteMaintenanceContent',
//						  'id'          => 'siteMaintenanceContent',
//						  'class'       => 'form-control',
//						  'value'		=> set_value('siteMaintenanceContent', $s['siteMaintenanceContent'], FALSE)
//						);
//			
//						echo form_textarea($data); ?>
					</div>  /controls 				
				</div>  /form-group -->
			  <hr/>
			  <div class="form-group">	
					<label class="control-label" for="siteAdditionalJS"><?php echo $this->lang->line('settings_additional_js'); ?></label>
					<div class="controls">
					<?php 	$data = array(
						  'name'        => 'siteAdditionalJS',
						  'id'          => 'siteAdditionalJS',
						  'class'       => 'form-control',
						  'value'		=> set_value('siteAdditionalJS', $s['siteAdditionalJS'], FALSE)
						);

						echo form_textarea($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
           </div>
           <div class="panel-footer">

             
 				 <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?>  
					 <a class="btn" href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('btn_cancel'); ?></a>
					 <?php echo form_close();
			} ?>
			</div> 
		</div>
	</div>
</div>
<script type="text/javascript">
$(function () {
	
	if(document.getElementById('file_upload'))
		{
			function prepareUpload(event)
			{
				files = event.target.files;
				uploadFiles(event);
			}
	
			function uploadFiles(event)
			{
				event.stopPropagation();
				event.preventDefault();
	
				$('#loading_pic').show();
	
				var data = new FormData();
				$.each(files, function(key, value){ data.append(key, value); });
				
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit/?files',
					type: 'POST',
					data: data,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data, textStatus, jqXHR){
						if(data!='0')
						{
							$('#logo_preloaded').show();
							document.getElementById('logo_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
							document.getElementById('siteLogo').value = data;
							$('#loading_pic').hide();
						}
						else
							alert('<?php echo $this->lang->line('settings_image_error'); ?>');
					}
				});
			}
	
			function submitForm(event, data)
			{
				$form = $(event.target);
				var formData = $form.serialize();
				$.each(data.files, function(key, value){ formData = formData + '&filenames[]=' + value; });
	
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit',
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
					success: function(data, textStatus, jqXHR){
						if(typeof data.error === 'undefined')
							console.log('SUCCESS: ' + data.success);
						else
							console.log('ERRORS: ' + data.error);
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log('ERRORS: ' + textStatus);
					},
					complete: function()
					{
						$('#loading_pic').hide();
					}
				});
			}
			
			var files;
			$('input[name=file_upload]').on('change', prepareUpload);
		}
	if(document.getElementById('favicon_upload'))
		{
			function prepareUploadFavi(event)
			{
				files = event.target.files;
				uploadFilesFavi(event);
			}
	
			function uploadFilesFavi(event)
			{
				event.stopPropagation();
				event.preventDefault();
	
				$('#loading_pic_favicon').show();
	
				var data = new FormData();
				$.each(files, function(key, value){ data.append(key, value); });
				
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit/?files',
					type: 'POST',
					data: data,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data, textStatus, jqXHR){
						if(data!='0')
						{
							$('#favicon_preloaded').show();
							document.getElementById('favicon_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
							document.getElementById('siteFavicon').value = data;
							$('#loading_pic_favicon').hide();
						}
						else
							alert('<?php echo $this->lang->line('settings_image_error'); ?>');
					}
				});
			}
	
			function submitForm(event, data)
			{
				$form = $(event.target);
				var formData = $form.serialize();
				$.each(data.files, function(key, value){ formData = formData + '&filenames[]=' + value; });
	
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit',
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
					success: function(data, textStatus, jqXHR){
						if(typeof data.error === 'undefined')
							console.log('SUCCESS: ' + data.success);
						else
							console.log('ERRORS: ' + data.error);
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log('ERRORS: ' + textStatus);
					},
					complete: function()
					{
						$('#loading_pic_favicon').hide();
					}
				});
			}
			
			var files;
			$('input[name=favicon_upload]').on('change', prepareUploadFavi);
		}
        if(document.getElementById('footer_logo_upload'))
		{
			function prepareUploadFavi(event)
			{
				files = event.target.files;
				uploadFilesFavi(event);
			}
	
			function uploadFilesFavi(event)
			{
				event.stopPropagation();
				event.preventDefault();
	
				$('#loading_pic_footer_logo').show();
	
				var data = new FormData();
				$.each(files, function(key, value){ data.append(key, value); });
				
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit/?files',
					type: 'POST',
					data: data,
					cache: false,
					dataType: 'json',
					processData: false,
					contentType: false,
					success: function(data, textStatus, jqXHR){
						if(data!='0')
						{
							$('#footer_logo_preloaded').show();
							document.getElementById('footer_logo_preloaded').src = '<?php echo BASE_URL; ?>/uploads/' + data;
							document.getElementById('siteFooterLogo').value = data;
							$('#loading_pic_footer_logo').hide();
						}
						else
							alert('<?php echo $this->lang->line('settings_image_error'); ?>');
					}
				});
			}
	
			function submitForm(event, data)
			{
				$form = $(event.target);
				var formData = $form.serialize();
				$.each(data.files, function(key, value){ formData = formData + '&filenames[]=' + value; });
	
				$.ajax({
					url: '<?php echo BASE_URL; ?>/admin/settings/submit',
					type: 'POST',
					data: formData,
					cache: false,
					dataType: 'json',
					success: function(data, textStatus, jqXHR){
						if(typeof data.error === 'undefined')
							console.log('SUCCESS: ' + data.success);
						else
							console.log('ERRORS: ' + data.error);
					},
					error: function(jqXHR, textStatus, errorThrown){
						console.log('ERRORS: ' + textStatus);
					},
					complete: function()
					{
						$('#loading_pic_footer_logo').hide();
					}
				});
			}
			
			var files;
			$('input[name=footer_logo_upload]').on('change', prepareUploadFavi);
		}
	});	
</script>
<?php echo $footer; ?>
