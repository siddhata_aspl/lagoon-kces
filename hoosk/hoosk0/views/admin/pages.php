<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <h1 class="page-header">
                <?php echo $this->lang->line('pages_header'); ?>
            </h1>
		</div>
           <div class="col-lg-4">
                <div class="page-header pull-right">
                <a href="<?php echo BASE_URL; ?>/admin/pages/new" class="btn btn-small btn-primary"><i class="fa fa-plus"></i> Add</a> 
                </div>
        </div>
           <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-file"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/pages"><?php echo $this->lang->line('nav_pages_all'); ?></a>
                </li>
            </ol>
            
        </div>
    </div>
</div>

<div class="container-fluid">
  	<div class="row">
      	<div class="col-md-12">
			<table class="table table-striped table-bordered"  id="record_table">
                <thead>
                  <tr>
                    <th> <?php echo $this->lang->line('pages_table_page'); ?> </th>
                      <th> <?php echo $this->lang->line('pages_table_published'); ?> </th>
                    <th class="td-actions"> <?php echo $this->lang->line('pages_action'); ?></th>
                  </tr>
                </thead>
                
              </table>
              <div class="text-center" id="loadingSpinner">
              	<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
              </div>
             <div id="paginationContainer">
				  
			  </div>
     </div>
      <!-- /colmd12 -->
  </div>
  <!-- /row --> 
</div>
<!-- /container --> 
<?php echo $footer; ?>

<script>
     
    $(document).ready(function() {
                
                var dataTable = $('#record_table').DataTable({
                    
                    "processing": true,
                    "serverSide": true,
                    "aaSorting": [],
                    "aoColumnDefs": [
                                {
                                   bSortable: false,
                                   aTargets: [ -1,-2 ]
                                }
                              ],
                    "ajax":{
                        url :"<?php echo base_url().'admin/pages/getalldata'; ?>", // json datasource
                        type: "post",  // method  , by default get
                        error: function(){  // error handling
                            $(".record_table-error").html("");
                            $("#record_table").append('<tbody class="record_table-error"><tr><th colspan="4">No data found in the server</th></tr></tbody>');
                            $("#record_table_processing").css("display","none");
                            
                        }
                    },
                });
                
//              $('#record_table').on('click', '.mystatus', function(){
//         
//                    var id = $(this).data("id");
//                    var newstatus = $(this).attr("data-val");
//                    //$('.prettycheckbox input').prop('checked', false);
//                    $('#new_radio').html("");
//                    var html = '<div class="col-md-12">';
//                    html += '<div class="col-md-6"><input type="radio"  name="status" class="myradio" id="Disabled"  value="Disabled"> Disabled</div>';
//                    html += '<div class="col-md-6"><input type="radio" name="status" class="myradio" id="Enabled" value="Enabled"> Enabled</div>';
//                    html += '<div class="col-md-4"><input type="radio" name="status" class="myradio" id="Deleted" value="Deleted"> Delete</div>';
//                    html += '<br>';
//                    $('#new_radio').html(html);
//                    $("#pid").val(id);
//                    $('#' + newstatus).attr('checked', true);
//                    $("#modal_status").modal();
//                    
//                 });
    });
</script>