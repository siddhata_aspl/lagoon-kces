<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('company_view_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-university"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/company"><?php echo $this->lang->line('company_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('company_view_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('company_view_header'); ?>
                    </h3>
                </div>

                <div class="panel-body">
                    <?php
                    foreach ($users as $u)
                    {
                        
                        ?>



                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label class="control-label" for="firstname"><?php echo $this->lang->line('company_name'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['company_name'];
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            <div class="form-group">		
                                <label class="control-label" for="lastname"><?php echo $this->lang->line('user_new_lastname'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['last_name'];
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            <div class="form-group">		
                                <label class="control-label" for="phone"><?php echo $this->lang->line('company_phone'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['phone'];
                                    ?>
                                    <p class="help-block">&nbsp;</p>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->



                            <div class="form-group">
                                
                                <label class="control-label" for="state"><?php echo $this->lang->line('company_state'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['state'];
                                    ?>
                                    <p class="help-block">&nbsp;</p>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            
                            <div class="form-group">
                                <label class="control-label" for="phone"><?php echo $this->lang->line('company_new_profilepic'); ?></label>
                                <div class="controls">
                                    <img src="<?php echo BASE_URL . '/uploads/company/' . $u['company_img'] ?>" alt="" width="100px" height="100px">
                                </div>
                            </div>



                        </div>

                        <div class="col-md-6">
                            <div class="form-group">		
                                <label class="control-label" for="firstname"><?php echo $this->lang->line('company_first_name'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['first_name'];
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->


                            <div class="form-group">		
                                <label class="control-label" for="email"><?php echo $this->lang->line('company_email'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['email'];
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->


                            <div class="form-group">		
                                <label class="control-label" for="city"><?php echo $this->lang->line('company_city'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['city'];
                                    ?>
                                    <p class="help-block">&nbsp;</p>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->


                            <div class="form-group">
                                <label class="control-label" for="address"><?php echo $this->lang->line('company_address'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['address'];
                                    ?>
                                    <p class="help-block">&nbsp;</p>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->
                            
                            <div class="form-group">
                                <label class="control-label" for="zipcode"><?php echo $this->lang->line('company_zipcode'); ?></label>
                                <div class="controls">
                                    <?php  
                                        echo $u['zipcode'];
                                    ?>
                                    <p class="help-block">&nbsp;</p>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->


                        </div>







                    </div>

                    <div class="panel-footer">
                        <a class="btn" href="<?php echo BASE_URL; ?>/admin/company"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    </div> <!-- /form-actions -->
                    <?php
                    }
                    ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
