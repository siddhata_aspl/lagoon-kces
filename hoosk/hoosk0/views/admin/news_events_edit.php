<?php echo $header; ?>
<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('news_events_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                  <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-newspaper-o"></i>
                  <a href="<?php echo BASE_URL; ?>/admin/news_events"><?php echo $this->lang->line('news_events_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                  <?php echo $this->lang->line('news_events_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('news_events_edit_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
             <?php foreach ($news_events as $u) {
       echo form_open_multipart(BASE_URL.'/admin/news_events/edited/'.$this->uri->segment(4)); ?>
             
              <div class="form-group">    
                <?php echo form_error('news_title', '<div class="alert alert-danger">', '</div>'); ?>                  
          <label class="control-label" for="news_title"><?php echo $this->lang->line('news_events_title'); ?><span style="color:red"><sup>*</sup></span></label>
          <div class="controls">
                    <?php   $data = array(
              'name'        => 'news_title',
              'id'          => 'news_title',
              'class'       => 'form-control',
              'value'   => set_value('news_title', $u['news_title'])
            );
      
            echo form_input($data); ?>
          </div> <!-- /controls -->       
        </div> <!-- /form-group -->


        <div class="form-group">    
                <?php echo form_error('short_content', '<div class="alert alert-danger">', '</div>'); ?>                  
                            <label class="control-label" for="short_content"><?php echo $this->lang->line('news_events_content'); ?> <span id="charactersRemaining" style="color:red">(170 characters)</span></label>
                            <div class="controls">
                                <?php
                                $data = array(
                                    'name'  => 'short_content',
                                    'id'    => 'short_content',
                                    'maxlength' => '170',
                                    'class' => 'form-control',
                                    'value' => set_value('short_content', $u['short_content'])
                                );

                                echo form_textarea($data);
                                ?>
                            </div> <!-- /controls -->       
                </div> <!-- /form-group -->
         
        
        <div class="form-group">    
                <?php echo form_error('news_desc', '<div class="alert alert-danger">', '</div>'); ?>                  
                            <label class="control-label" for="news_desc"><?php echo $this->lang->line('news_events_content'); ?></label>
                            <div class="controls">
                                <?php
                                // $data = array(
                                //     'name'  => 'news_desc',
                                //     'id'    => 'news_desc',
                                //     'class' => 'form-control',
                                //     'value' => set_value('news_desc', $u['news_desc'])
                                // );

                                // echo form_textarea($data);
                                ?>
                                <textarea class="form-control" name="news_desc" id="news_desc"><?php echo $u['news_desc'];?></textarea>
                            </div> <!-- /controls -->       
                </div> <!-- /form-group -->
                
                
        <div class="form-group">
                <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
          <label class="control-label" for="file_upload"><?php echo $this->lang->line('news_events_image'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
          <div class="controls">
            <?php
//              $data = array(
//                'name'    => 'file_upload',
//                'id'    => 'file_upload',
//                'class'   => 'form-control'
//              );
//              echo form_upload($data);
            ?>
            <!--<input type="hidden" id="contentImage" name="file_upload" />-->
                <input class="col-md-6" type="file" name="file_upload" value=""/>
                <img  src="<?php echo BASE_URL.'/uploads/news_events/'.$u['news_image']; ?>" alt="Image" height="100px" width="100px">       
          </div> <!-- /controls -->
                                        
        </div> <!-- /form-group --> 
                                
             
                </div>
                
                <div class="panel-footer">
                <?php   $data = array(
              'name'        => 'submit',
              'id'          => 'submit',
              'class'       => 'btn btn-primary',
              'value'   => $this->lang->line('btn_save'),
            );
           echo form_submit($data); ?> 
          <a class="btn" href="<?php echo BASE_URL; ?>/admin/news_events"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
               <?php  echo form_close(); 
       }
       ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('news_desc', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>

<script>
    var el;                                                    

    function countCharacters(e) {                                    
      var textEntered, countRemaining, counter;          
      textEntered = document.getElementById('short_content').value;  
      counter = (170 - (textEntered.length));
      countRemaining = document.getElementById('charactersRemaining'); 
      countRemaining.textContent = "(" + counter + " characters)";       
    }
    el = document.getElementById('short_content');                   
    el.addEventListener('keyup', countCharacters, false);
</script>