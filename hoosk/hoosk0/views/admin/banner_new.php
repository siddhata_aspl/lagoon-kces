<?php echo $header; ?>

<style>
    #bcheckbox {
        width: 2%;
        height: 24px;
    }    
</style>

<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('banner_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-image"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/banners"><?php echo $this->lang->line('banner_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('banner_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('banner_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
            <?php echo form_open_multipart(BASE_URL.'/admin/banners/new/add'); ?>
             
                        <div class="form-group">        
                        <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>                                    
                    <label class="control-label" for="title"><?php echo $this->lang->line('banner_title'); ?></label>
                    <div class="controls">
                        <?php   $data = array(
                          'name'        => 'title',
                          'id'          => 'title',
                          'class'       => 'form-control',
                          'value'       => set_value('title', '', FALSE)
                        );
            
                        echo form_input($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                                
                        <div class="form-group">        
                        <?php echo form_error('bcheckbox', '<div class="alert alert-danger">', '</div>'); ?>                                    
                            <label class="control-label" for="bcheckbox"><?php echo $this->lang->line('bcheckbox'); ?></label>
                    <div class="controls">
                        <?php   $data = array(
                          'name'        => 'bcheckbox',
                          'id'          => 'bcheckbox',
                          'class'       => '',
                          'style'       => 'display:inline',
                          'value'       => set_value('bcheckbox', 'True', FALSE)
                        );
            
                        echo form_checkbox($data).'<span style="display:inline; font-size:15px; vertical-align:super;">With Text</span>'; ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                                
                        <div class="form-group" id="bcontent">      
                        <?php echo form_error('banner_content', '<div class="alert alert-danger">', '</div>'); ?>                                   
                    <label class="control-label" for="banner_content"><?php echo $this->lang->line('banner_content'); ?></label>
                    <div class="controls">
                        <?php   $data = array(
                          'name'        => 'banner_content',
                          'id'          => 'banner_content',
                          'class'       => 'form-control',
                          'value'       => set_value('banner_content', '', FALSE)
                        );
            
                        echo form_textarea($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
             
                        <div class="form-group">
                    <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
                    <label class="control-label" for="file_upload"><?php echo $this->lang->line('banner_image'); ?><span style="color:red"><sup>*</sup></span></label>                                 
                    <div class="controls">
                        <?php
//                          $data = array(
//                              'name'      => 'file_upload',
//                              'id'        => 'file_upload',
//                              'class'     => 'form-control'
//                          );
//                          echo form_upload($data);
                        ?>
                        <!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input type="file" name="file_upload" value=""/>
                    </div> <!-- /controls -->
                </div> <!-- /form-group -->             
                
                </div>
                <div class="panel-footer">
                <?php   $data = array(
                          'name'        => 'submit',
                          'id'          => 'submit',
                          'class'       => 'btn btn-primary',
                          'value'       => $this->lang->line('btn_save'),
                        );
                     echo form_submit($data); ?> 
                    <a class="btn" href="<?php echo BASE_URL; ?>/admin/banners"><?php echo $this->lang->line('btn_cancel'); ?></a>
                </div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('banner_content', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>
<script>
$(function () {
         $("#bcontent").hide();
        $("#bcheckbox").click(function () {
            if ($(this).is(":checked")) {
                $("#bcontent").show();
                //$("#AddPassport").hide();
            } else {
                $("#bcontent").hide();
                //$("#AddPassport").show();
            }
        });
    });
</script>
