<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('company_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-university"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/company"><?php echo $this->lang->line('company_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                	<?php echo $this->lang->line('company_new_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('company_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
			<?php echo form_open_multipart(BASE_URL.'/admin/company/new/add'); ?>
             <div class="col-md-6 ">
                 <div class="form-group">		
                <?php echo form_error('companyname', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="firstname"><?php echo $this->lang->line('company_name'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'companyname',
						  'id'          => 'companyname',
						  'class'       => 'form-control',
						  'value'		=> set_value('companyname', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                  <div class="form-group">		
                <?php echo form_error('firstname', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="firstname"><?php echo $this->lang->line('company_first_name'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'firstname',
						  'id'          => 'firstname',
						  'class'       => 'form-control',
						  'value'		=> set_value('firstname', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                 <div class="form-group">		
                <?php echo form_error('lastname', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="lastname"><?php echo $this->lang->line('user_new_lastname'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'lastname',
						  'id'          => 'lastname',
						  'class'       => 'form-control',
						  'value'		=> set_value('lastname', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                 <div class="form-group">		
                <?php echo form_error('phone', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="phone"><?php echo $this->lang->line('company_phone'); ?></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'phone',
						  'id'          => 'phone',
						  'class'       => 'form-control',
						  'placeholder' => '123-456-7890',
						  'value'		=> set_value('phone', '', FALSE)
						);
			
						echo form_input($data); ?>
                                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                        
                                
                                
                                 <div class="form-group">		
                <?php echo form_error('state', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="state"><?php echo $this->lang->line('company_state'); ?></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'state',
						  'id'          => 'state',
						  'class'       => 'form-control',
						  'value'		=> set_value('state', '', FALSE)
						);
			
						echo form_input($data); ?>
                                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                <div class="form-group">		
                
            		<?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('company_new_profilepic'); ?></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
						<!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input type="file" name="file_upload" value=""/>
					</div> <!-- /controls -->
				</div> <!-- /form-group -->         
                                
                                
                                
                                
             </div>
             
             <div class="col-md-6">
                  <div class="form-group">		
                <?php echo form_error('client_id', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="client_id"><?php echo $this->lang->line('client_id'); ?></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'client_id',
						  'id'          => 'client_id',
						  'class'       => 'form-control',
						  'value'		=> set_value('client_id', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                 
                 <div class="form-group">		
                <?php echo form_error('email', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="email"><?php echo $this->lang->line('user_new_email'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'email',
						  'id'          => 'email',
						  'class'       => 'form-control',
						  'value'		=> set_value('email', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                 
                 <div class="form-group">		
                <?php echo form_error('city', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="city"><?php echo $this->lang->line('company_city'); ?></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'city',
						  'id'          => 'city',
						  'class'       => 'form-control',
						  'value'		=> set_value('city', '', FALSE)
						);
			
						echo form_input($data); ?>
                                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
            
			<div class="form-group">		
                <?php echo form_error('address', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="address"><?php echo $this->lang->line('company_address'); ?></label>
					<div class="controls">
                                            <?php 	$data = array(
						  'name'        => 'address',
						  'id'          => 'address',
						  'class'       => 'form-control',
						  'value'		=> set_value('address', '', FALSE)
						);
			
						echo form_textarea($data); ?>
			
                                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                 <div class="form-group">		
                <?php echo form_error('zipcode', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="zipcode"><?php echo $this->lang->line('company_zipcode'); ?></label>
					<div class="controls">
						 <?php 	$data = array(
						  'name'        => 'zipcode',
						  'id'          => 'zipcode',
						  'class'       => 'form-control',
						  'value'		=> set_value('zipcode', '', FALSE)
						);
			
						echo form_input($data); ?>
                                            <p class="help-block">&nbsp;</p>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                
                
 </div>
				
                
                </div>
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?> 
					<a class="btn" href="<?php echo BASE_URL; ?>/admin/company"><?php echo $this->lang->line('btn_cancel'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
		</div>
	</div>
</div>

<script>

document.getElementById('phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '' + x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});

</script>

<?php echo $footer; ?>