<?php echo $header; ?>
<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('service_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-cog"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/our_service"><?php echo $this->lang->line('service_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                	<?php echo $this->lang->line('service_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('service_edit_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
             <?php foreach ($service as $u) {
			 echo form_open_multipart(BASE_URL.'/admin/our_service/edited/'.$this->uri->segment(4)); ?>
             
              <div class="form-group">		
                <?php echo form_error('service_name', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="service_name"><?php echo $this->lang->line('service_title'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'service_name',
						  'id'          => 'service_name',
						  'class'       => 'form-control',
						  'value'           => set_value('service_name', $u['service_name'])
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
             
                        
                       
                     <!--    <div class="form-group">		
                <?php //echo form_error('service_content', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="service_content"><?php //echo $this->lang->line('service_content'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	
      //               $data = array(
						//   'name'        => 'service_content',
						//   'id'          => 'service_content',
						//   'class'       => 'form-control',
						//   'value'		=> set_value('service_content', $u['service_content'])
						// );
			
						// echo form_textarea($data); ?>
                        <textarea class="form-control" name="service_content" id="service_content"><?php //echo $u['service_content'];?></textarea>
					</div> <!-- /controls -->				
				<!-- </div> --> <!-- /form-group -->


                <div class="form-group">        
                <?php echo form_error('service_content', '<div class="alert alert-danger">', '</div>'); ?>                                  
                            <label class="control-label" for="service_content"><?php echo $this->lang->line('service_content'); ?></label>
                            <div class="controls">
                                <?php
                                // $data = array(
                                //     'name'  => 'career_req',
                                //     'id'    => 'career_req',
                                //     'class' => 'form-control',
                                //     'value' => set_value('career_req', $u['career_req'])
                                // );

                                // echo form_textarea($data);
                                ?>
                                <textarea class="form-control" name="service_content" id="service_content"><?php echo $u['service_content'];?></textarea>
                            </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                                
                                
                <div class="form-group">
            		<?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('service_image'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
						<!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input class="col-md-6" type="file" name="file_upload" value=""/>
                                            <img  src="<?php echo BASE_URL.'/uploads/services/'.$u['service_image']; ?>" alt="Image" height="100px" width="100px">       
					</div> <!-- /controls -->
                                        
				</div> <!-- /form-group --> 
                                
                                
             
                </div>
                
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?> 
					<a class="btn" href="<?php echo BASE_URL; ?>/admin/our_service"><?php echo $this->lang->line('btn_cancel'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); 
			 }
			 ?>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('service_content', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>