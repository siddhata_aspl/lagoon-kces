<?php echo $header; ?>

<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('service_new_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                	<a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-cog"></i>
                	<a href="<?php echo BASE_URL; ?>/admin/our_service"><?php echo $this->lang->line('service_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                	<?php echo $this->lang->line('service_add_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('service_new_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
			<?php echo form_open_multipart(BASE_URL.'/admin/our_service/new/add'); ?>
             
                 <div class="form-group">		
                <?php echo form_error('service_name', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="service_name"><?php echo $this->lang->line('service_title'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'service_name',
						  'id'          => 'service_name',
						  'class'       => 'form-control',
						  'value'           => set_value('service_name', '', FALSE)
						);
			
						echo form_input($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                         
                                
                        <div class="form-group">		
                <?php echo form_error('service_content', '<div class="alert alert-danger">', '</div>'); ?>									
					<label class="control-label" for="service_content"><?php echo $this->lang->line('service_content'); ?><span style="color:red"><sup>*</sup></span></label>
					<div class="controls">
                    <?php 	$data = array(
						  'name'        => 'service_content',
						  'id'          => 'service_content',
						  'class'       => 'form-control',
						  'value'           => set_value('service_content', '', FALSE)
						);
			
						echo form_textarea($data); ?>
					</div> <!-- /controls -->				
				</div> <!-- /form-group -->
                                
                                
                <div class="form-group">
            		<?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
					<label class="control-label" for="file_upload"><?php echo $this->lang->line('service_image'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
					<div class="controls">
						<?php
//							$data = array(
//								'name'		=> 'file_upload',
//								'id'		=> 'file_upload',
//								'class'		=> 'form-control'
//							);
//							echo form_upload($data);
						?>
						<!--<input type="hidden" id="postImage" name="file_upload" />-->
                                            <input type="file" name="file_upload" value=""/>
					</div> <!-- /controls -->
				</div> <!-- /form-group --> 
                                

                </div>
                <div class="panel-footer">
                <?php 	$data = array(
						  'name'        => 'submit',
						  'id'          => 'submit',
						  'class'       => 'btn btn-primary',
						  'value'		=> $this->lang->line('btn_save'),
						);
					 echo form_submit($data); ?> 
					<a class="btn" href="<?php echo BASE_URL; ?>/admin/our_service"><?php echo $this->lang->line('btn_cancel'); ?></a>
				</div> <!-- /form-actions -->
               <?php  echo form_close(); ?>
            </div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('service_content', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>