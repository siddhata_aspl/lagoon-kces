<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title"><?php echo $this->lang->line('product_category_delete').": ".$form[0]['category_name'] ?>?</h4>
</div>          <!-- /modal-header -->
<div class="modal-body">
        <div class="alert alert-danger" role="alert"><strong>WARNING: </strong><?php  echo '<p>'.$this->lang->line('delete_confirm_message').'</p>';
             ?></div>
</div>          <!-- /modal-body -->
<div class="modal-footer">
    <form ACTION="<?php echo BASE_URL.'/admin/product_category/delete/'.$form[0]['category_id']; ?>" method="POST" name="pageform" id="pageform">
        <input name="deleteid" type="hidden" id="deleteid" value="<?php echo $form[0]['category_id'] ?>" />
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel'); ?></button>
        <input class="btn btn-danger" type="submit" value="<?php echo $this->lang->line('btn_delete'); ?>"/>
    </form> 
</div>  <!-- /modal-footer -->