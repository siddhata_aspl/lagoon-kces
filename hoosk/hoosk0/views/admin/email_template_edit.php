<?php echo $header; ?>
<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<style>
    .variable_style
    {
        border: 1px black solid;
        padding: 6px;
        background: #eae9e9;
        color: red;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('email_template_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-envelope"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/email_template"><?php echo $this->lang->line('email_template_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('email_template_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('email_template_edit_header'); ?>
                    </h3>
                </div>

                <div class="panel-body">
                    <?php foreach ($email_tempalate as $u)
                    {
                        echo form_open_multipart(BASE_URL . '/admin/email_template/edited/' . $this->uri->segment(4));
                        ?>

                        <div class="form-group">		
                                <?php echo form_error('title', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="title"><?php echo $this->lang->line('email_template_title'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                $data = array(
                                    'name'  => 'title',
                                    'id'    => 'title',
                                    'class' => 'form-control',
                                    'value' => set_value('title', $u['email_template_title'])
                                );

                                echo form_input($data);
                                ?>
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->
                        <div class="form-group">		
                                <?php echo form_error('subject', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="subject"><?php echo $this->lang->line('email_template_subject'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                $data = array(
                                    'name'  => 'subject',
                                    'id'    => 'subject',
                                    'class' => 'form-control',
                                    'value' => set_value('subject', $u['email_template_subject'])
                                );

                                echo form_input($data);
                                ?>
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->    
                        <div class="form-group" disabled>	
                            <label class="control-label" for="variable"><?php echo $this->lang->line('email_template_variable'); ?></label>
                            <div class="controls variable_style">
                                <?php echo $u['email_template_variables']; ?>
                            </div> 
                        </div>
                        <div class="form-group">		
                                <?php echo form_error('desc', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="desc"><?php echo $this->lang->line('email_template_desc'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                // $data = array(
                                //     'name'  => 'editor1',
                                //     'id'    => 'editor1',
                                //     'class' => 'form-control',
                                //     'value' => set_value('desc', $u['email_template_desc'])
                                // );

                                // echo form_textarea($data);
                                ?>
                                <textarea class="form-control" name="editor1" id="editor1"><?php echo $u['email_template_desc'];?></textarea>
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->





                    </div>

                    <div class="panel-footer">
                    <?php
                    $data = array(
                        'name'  => 'submit',
                        'id'    => 'submit',
                        'class' => 'btn btn-primary',
                        'value' => $this->lang->line('btn_update'),
                    );
                    echo form_submit($data);
                    ?> 
                        <a class="btn" href="<?php echo BASE_URL; ?>/admin/email_template"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    </div> <!-- /form-actions -->
    <?php
    echo form_close();
}
?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('editor1', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>

