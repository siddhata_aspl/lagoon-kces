<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('product_category_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-list"></i>
                    <a href="<?php echo BASE_URL; ?>/admin/product_category"><?php echo $this->lang->line('product_category_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('product_category_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('product_category_edit_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
             <?php foreach ($product_category as $u) {
             echo form_open_multipart(BASE_URL.'/admin/product_category/edited/'.$this->uri->segment(4)); ?>
             
              <div class="form-group">      
                <?php echo form_error('category_name', '<div class="alert alert-danger">', '</div>'); ?>                                  
                    <label class="control-label" for="category_name"><?php echo $this->lang->line('product_category_title'); ?><span style="color:red"><sup>*</sup></span></label>
                    <div class="controls">
                    <?php   $data = array(
                          'name'        => 'category_name',
                          'id'          => 'category_name',
                          'class'       => 'form-control',
                          'value'       => set_value('category_name', $u['category_name'])
                        );
            
                        echo form_input($data); ?>
                    </div> <!-- /controls -->               
                </div> <!-- /form-group -->
                                
                                
                <div class="form-group">
                    <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
                    <label class="control-label" for="file_upload"><?php echo $this->lang->line('product_category_image'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
                    <div class="controls">
                        <?php
//                          $data = array(
//                              'name'      => 'file_upload',
//                              'id'        => 'file_upload',
//                              'class'     => 'form-control'
//                          );
//                          echo form_upload($data);
                        ?>
                        <!--<input type="hidden" id="postImage" name="file_upload" />-->
                        <input class="col-md-6" type="file" name="file_upload" value=""/>
                        <img  src="<?php echo BASE_URL.'/uploads/products/'.$u['category_image']; ?>" alt="Image" height="100px" width="100px">       
                    </div> <!-- /controls -->
                                        
                </div> <!-- /form-group --> 
                                
             
                </div>
                
                <div class="panel-footer">
                <?php   $data = array(
                          'name'        => 'submit',
                          'id'          => 'submit',
                          'class'       => 'btn btn-primary',
                          'value'       => $this->lang->line('btn_save'),
                        );
                     echo form_submit($data); ?> 
                    <a class="btn" href="<?php echo BASE_URL; ?>/admin/product_category"><?php echo $this->lang->line('btn_cancel'); ?></a>
                </div> <!-- /form-actions -->
               <?php  echo form_close(); 
             }
             ?>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
