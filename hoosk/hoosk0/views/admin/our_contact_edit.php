<?php echo $header; ?>
<script type="text/javascript" src='<?php echo base_url() . "ckeditor/ckeditor.js"; ?>'></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('contact_edit_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                <i class="fa fa-dashboard"></i>
                  <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                <i class="fa fa-fw fa-phone"></i>
                  <a href="<?php echo BASE_URL; ?>/admin/our_contact"><?php echo $this->lang->line('contact_header'); ?></a>
                </li>
                <li class="active">
                <i class="fa fa-fw fa-pencil"></i>
                  <?php echo $this->lang->line('contact_edit_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fa fa-pencil fa-fw"></i>
                    <?php echo $this->lang->line('contact_edit_header'); ?>
                </h3>
            </div>
            
         <div class="panel-body">
             <?php foreach ($contact as $u) {
       echo form_open_multipart(BASE_URL.'/admin/our_contact/edited/'.$this->uri->segment(4)); ?>
             
              <div class="form-group">    
                <?php echo form_error('contact_title', '<div class="alert alert-danger">', '</div>'); ?>                  
          <label class="control-label" for="contact_title"><?php echo $this->lang->line('contact_title'); ?><span style="color:red"><sup>*</sup></span></label>
          <div class="controls">
                    <?php   $data = array(
              'name'        => 'contact_title',
              'id'          => 'contact_title',
              'class'       => 'form-control',
              'value'   => set_value('contact_title', $u['contact_title'])
            );
      
            echo form_input($data); ?>
          </div> <!-- /controls -->       
        </div> <!-- /form-group -->
        
        
        <div class="form-group">    
                <?php echo form_error('contact_link', '<div class="alert alert-danger">', '</div>'); ?>                  
          <label class="control-label" for="contact_link"><?php echo $this->lang->line('contact_link'); ?><span style="color:red"><sup>*</sup></span></label>
          <div class="controls">
                    <?php   $data = array(
              'name'        => 'contact_link',
              'id'          => 'contact_link',
              'class'       => 'form-control',
              'value'   => set_value('contact_link', $u['contact_link'])
            );
      
            echo form_input($data); ?>
          </div> <!-- /controls -->       
        </div> <!-- /form-group -->
                    
        
        <div class="form-group">		
                <?php echo form_error('contact_content', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="contact_content"><?php echo $this->lang->line('contact_content'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                // $data = array(
                                //     'name'  => 'contact_content',
                                //     'id'    => 'contact_content',
                                //     'class' => 'form-control',
                                //     'value' => set_value('contact_content', $u['contact_content'])
                                // );

                                // echo form_textarea($data);
                                ?>
                                <textarea class="form-control" name="contact_content" id="contact_content"><?php echo $u['contact_content'];?></textarea>
                            </div> <!-- /controls -->				
                </div> <!-- /form-group -->
                
                
        <div class="form-group">
                <?php echo form_error('file_upload', '<div class="alert alert-danger">', '</div>'); ?>
          <label class="control-label" for="file_upload"><?php echo $this->lang->line('contact_image'); ?><span style="color:red"><sup>*</sup></span></label>
                                        
          <div class="controls">
            <?php
//              $data = array(
//                'name'    => 'file_upload',
//                'id'    => 'file_upload',
//                'class'   => 'form-control'
//              );
//              echo form_upload($data);
            ?>
            <!--<input type="hidden" id="contentImage" name="file_upload" />-->
                                            <input class="col-md-6" type="file" name="file_upload" value=""/>
                                            <img  src="<?php echo BASE_URL.'/uploads/contacts/'.$u['contact_image']; ?>" alt="Image" height="100px" width="100px">       
          </div> <!-- /controls -->
                                        
        </div> <!-- /form-group --> 
                                
             
                </div>
                
                <div class="panel-footer">
                <?php   $data = array(
              'name'        => 'submit',
              'id'          => 'submit',
              'class'       => 'btn btn-primary',
              'value'   => $this->lang->line('btn_save'),
            );
           echo form_submit($data); ?> 
          <a class="btn" href="<?php echo BASE_URL; ?>/admin/our_contact"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
               <?php  echo form_close(); 
       }
       ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script>
    CKEDITOR.replace('contact_content', {
        toolbar: 'Full',
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_P
    });
</script>