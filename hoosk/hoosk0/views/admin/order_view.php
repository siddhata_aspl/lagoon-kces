<?php echo $header; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?php echo $this->lang->line('order_view_header'); ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i>
                    <a href="<?php echo BASE_URL; ?>/admin"><?php echo $this->lang->line('nav_dash'); ?></a>
                </li>
                <li>
                    <i class="fa fa-fw fa-file-text-o"></i>
                    <a href="<?php echo BASE_URL.'/admin/project/'; ?>"><?php echo $this->lang->line('project_header'); ?></a>
                </li>
                <li>
                    <!--<i class="fa fa-fw fa-shopping-cart"></i>-->
                    <img src="<?php echo BASE_URL.'/images/cart2.png'; ?>" height="17px" width="17px" />
                    <a href="<?php echo BASE_URL.'/admin/project/order/'.base64_encode($order[0]['po_id']); ?>"><?php echo $this->lang->line('order_header'); ?></a>
                </li>
                <li class="active">
                    <i class="fa fa-fw fa-pencil"></i>
                    <?php echo $this->lang->line('order_view_header'); ?>
                </li>
            </ol>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-pencil fa-fw"></i>
                        <?php echo $this->lang->line('order_view_header'); ?>
                    </h3>
                </div>
                <?php
//                    print_r($order);exit();
                    foreach ($order as $u) {
                ?>
                <div class="panel-body"> 
                    
                    <div class="col-md-6" style="padding-left: 0px;">
                        <div class="form-group">		
                            <?php echo form_error('order_company', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="name"><?php echo $this->lang->line('order_company'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls">
                                <?php
                                    $data = array(
                                        'id'        => 'order_company',
                                        'class'     => 'form-control',
                                        'requeired' => 'requeired',
                                        'readonly'  => 'readonly',
                                        'disabled'  => 'disabled',
                                        'value'     => set_value('order_company', $u['po_id']),
//                                        'onChange'  => 'getpos()',
                                    );

                                    foreach ($company as $value)
                                    {
                                        $option[$value['companyId']] = $value['company_name'];
                                    }

                                    echo form_dropdown('po_manager', $option,$u['po_company'],$data);
                                ?>
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->
                    </div>
                    <div class="col-md-6 nopadding">
                        <div class="form-group">		
                            <?php echo form_error('order_po', '<div class="alert alert-danger">', '</div>'); ?>									
                            <label class="control-label" for="name"><?php echo $this->lang->line('order_po'); ?><span style="color:red"><sup>*</sup></span></label>
                            <div class="controls" id="setpo">
                                
                            </div> <!-- /controls -->				
                        </div> <!-- /form-group -->
                    </div>
                    <div class="total_div">
                    <hr style="clear: both;border-top: 1px solid black;">    
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT INFORMATION</h4>
                    
                    <div class="col-md-12 nopadding">
                        <div class="col-md-5 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_number', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_number'); ?><span style="color:red"><sup>*</sup></span></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'      => 'po_number',
                                        'id'        => 'po_number',
                                        'class'     => 'form-control numberic',
                                        'requeired' => 'requeired',
                                        'readonly' => 'readonly',
                                        'value'     => set_value('po_number', time())
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-7" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_adress', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_adress'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_adress',
                                        'id'    => 'po_adress',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->			
                            </div> <!-- /form-group -->                
                        </div>
                    </div>   
                    <div class="col-md-12 nopadding">
                        
                        <div class="col-md-6" style="padding-left: 0px;padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_city', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_city'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_city',
                                        'id'    => 'po_city',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_city', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group hidden">		
                                <?php echo form_error('po_age', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_age'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_age',
                                        'id'    => 'po_age',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_age', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                    <input type="hidden" name="product_rows" id="product_rows" value="1">
                                    <input type="hidden" name="custom_rows" id="custom_rows" value="1">
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                            <div class="form-group">		
                                <?php echo form_error('po_state', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="name"><?php echo $this->lang->line('po_state'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_state',
                                        'id'    => 'po_state',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_state', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->  
                        </div>
                    </div>

                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <h4 style="color: #302a75;font-weight: bold;">PROJECT SITE CONTACT</h4>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_name', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_site_name"><?php echo $this->lang->line('po_site_name'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_name',
                                        'id'    => 'po_site_name',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'value' => set_value('po_adress', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_site_email', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_site_email'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_site_email',
                                        'id'    => 'po_site_email',
                                        'class' => 'form-control',
                                        'readonly'=>'readonly',
                                        'type' => 'email',
                                        'value' => set_value('po_site_email', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6 nopadding">              
                            <div class="form-group">		
                                <?php echo form_error('po_office_phone', '<div class="alert alert-danger">', '</div>'); ?>									
                                <label class="control-label" for="po_office_phone"><?php echo $this->lang->line('po_office_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_office_phone',
                                        'id'    => 'po_office_phone',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_office_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                        <div class="col-md-6" style="padding-right: 0px">              
                            <div class="form-group">		
                                <?php echo form_error('po_cell_phone', '<div class="alert alert-danger">', '</div>'); ?>
                                <label class="control-label" for="po_site_email"><?php echo $this->lang->line('po_cell_phone'); ?></label>
                                <div class="controls">
                                    <?php
                                    $data = array(
                                        'name'  => 'po_cell_phone',
                                        'id'    => 'po_cell_phone',
                                        'readonly'=>'readonly',
                                        'class' => 'form-control numberic',
                                        'value' => set_value('po_cell_phone', '', FALSE)
                                    );

                                    echo form_input($data);
                                    ?>
                                </div> <!-- /controls -->				
                            </div> <!-- /form-group -->                
                        </div>
                    </div>
                    <br>
                    <hr style="clear: both;border-top: 1px solid black;">
                    <div>
                        <h4 style="color: #302a75;float:left;font-weight: bold;">PRODUCTS</h4>
                        <div class="toggle"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="product_data">
                        
                        <div id="copy_product">
                            
                        </div>
                    </div>
                    <div style="margin: 30px 0px; ">&nbsp;</div>
                    <hr  style="clear: both;border-top: 1px solid black;">
                    <div style="margin-top: 10px;">
                        <h4 style="color: #302a75;float:left;font-weight: bold;">CUSTOM ITEMS</h4>
                        <div class="toggle1"></div>
                    </div>
                    <div class="col-md-12 nopadding" id="custom_data">
                        <div id="set_copy">
                            
                        </div>                    
                    </div>
                    <hr  style="clear: both;border-top: 3px solid black;">
                    <div class="col-md-12 nopadding">
                        <div class="table-responsive col-md-offset-6">
                            <table class="table table-striped" id="invoice" style="text-align: right">
                                <tr>
                                    <th style="text-align: right">PRODUCTS TOTAL</th>
                                    <th>($)</th>
                                    <td class="total_show hidden"></td>
                                    <td class="total_show1"></td>
                                </tr>
                                <tr>
                                    <th style="text-align: right">CUSTOM ITEM TOTAL</th>
                                    <th>($)</th>
                                    <td class="total_custom_show hidden"></td>
                                    <td class="total_custom_show1"></td>
                                </tr>
                                <tr>
                                    <th style="text-align: right">PRODUCTS ORDER TOTAL</th>
                                    <th>($)</th>
                                    <td class="total_show_order hidden"></td>
                                    <td class="total_show_order1"></td>
                                </tr>
                                <tr class="hidden">
                                    <th style="text-align: right">CUSTOM ORDER ITEM TOTAL</th>
                                    <th>($)</th>
                                    <td class="total_custom_show_order hidden"></td>
                                    <td class="total_custom_show_order1"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    </div>
                    </div>
                <div class="panel-footer">
                    <div class="total_div" style="display: inline-block;">
                    </div>
                    <a class="btn" href="<?php echo BASE_URL.'/admin/project/order/'.base64_encode($order[0]['po_id']); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
                    
                </div> <!-- /form-actions -->
            
                <?php
                    }
                ?>

            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
.white-tooltip + .tooltip > .tooltip-inner {background-color: #fff;color: black;border: 1px solid #302a75;color: #302a75}
.white-tooltip + .tooltip > .tooltip-inner {background-color: #fff;color: black;}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;z-index: 999;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
.toggle{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle1{
    display:inline-block;
    height:39px;
    width:39px;
    float:right;
    background:url("<?php echo base_url().'images/mnsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
.toggle.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.toggle1.expanded{
    background:url("<?php echo base_url().'images/plsbtn.png'; ?>");
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}
.mywatermark {  
  display: block;
  position: relative;
  height:200px;
  width:100%;
  border: 2px solid;
}

.mywatermark::after {
  content: "";
  background:url("<?php echo base_url().'images/fullfilled.png'; ?>") no-repeat;
  background-position: center top;
  opacity: 1;
  top: 35%;
  left: 0;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: 999;  
  height:100%;
  width:100%;
}
.mywatermark tbody
{
  border: red 1px solid;
}
.mywatermark input
{
    border: red 1px solid;
}
</style>
<script>
                    
                    $(document).on('change', '.order_custom_quanity', function(){

                            var custom_row_id = $(this).attr('id');
                            if(custom_row_id != '')
                            {
                                var tdid = $('#'+custom_row_id).closest('td').data("id");
                                var unit_price = $('#order_custom_price'+tdid).val();
                                var quantity = $('#order_custom_quanity'+tdid).val();
                                if(unit_price != '')
                                {
                                    //console.log(unit_price);
                                    var total = unit_price*quantity;
                                    $('#'+custom_row_id).closest('tr').find('#custom_row_total'+tdid).html(addCommas(total.toFixed(2)));
                                }
                                getcustomtotal();
                            }   


                    });
                    $(document).on('change', '.order_product_quantity', function(){

                            var custom_row_id = $(this).attr('id');
                            if(custom_row_id != '')
                            {
                                var tdid = $('#'+custom_row_id).closest('td').data("id");
                                var unit_price = $('#order_product_price'+tdid).val();
                                var quantity = $('#order_product_quantity'+tdid).val();
                                if(unit_price != '')
                                {
                                    //console.log(unit_price);
//                                    alert("asd");
                                    var total = unit_price*quantity;
                                    $('#'+custom_row_id).closest('tr').find('#product_row_total'+tdid).html(addCommas(total.toFixed(2)));
                                }
                                gettotal();
                            }   
                            

                    });
                    $(document).ready(function () {
                        
                            var $content = $("#product_data").show();
                            $(".toggle").on("click", function(e){
                              $(this).toggleClass("expanded");
                              $content.slideToggle();
                            });

                            var $content1 = $("#custom_data").show();
                            $(".toggle1").on("click", function(e){
                              $(this).toggleClass("expanded");
                              $content1.slideToggle();
                            });
                            
                        $(".numberic").keydown(function (e) {
                            // Allow: backspace, delete, tab, escape, enter and .
                            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                                 // Allow: Ctrl+A, Command+A
                                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                                 // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                                     // let it happen, don't do anything
                                     return;
                            }
                            // Ensure that it is a number and stop the keypress
                            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                e.preventDefault();
                            }
                        });

                    });
        
        function gettotal()
        {
                var pro_total_all = 0;
                $('.product_row_total').each(function() {
                    if(parseInt($(this).html()) > 0 )
                    {
                        pro_total_all = parseInt(pro_total_all) + parseInt($(this).html());
                    }
                });
                $('.total_show_order').html(pro_total_all);
                $('.total_show_order1').html(addCommas(pro_total_all));
        }        
        function getcustomtotal()
        {
                var pro_total_all = 0;
                $('.custom_row_total').each(function() {
                    if(parseInt($(this).html()) > 0 )
                    {
                        pro_total_all = parseInt(pro_total_all) + parseInt($(this).html());
                    }
                });
                //alert(pro_total_all);
                $('.total_custom_show_order').html(pro_total_all);
                $('.total_custom_show_order1').html(addCommas(pro_total_all));
        }
        getpos();
        function getpos()
        {
                var val = $('#order_company').val();
                     $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'admin/order/getpoofmanager'; ?>',
                        data:'companyid='+val,
                        success: function(data){
                                data = JSON.parse(data);
                                var html = '<select name="order_po" disabled="disabled" id="order_po" class="form-control" requeired="requeired">';
                                html += '<option value="-1">Select</option>';
                                var selected = '';
                                $.each(data , function (key, val1) {
                                    //console.log(val['po_id']+val['po_name']);
                                    if(val1['po_id'] == <?php echo $order[0]['po_id']; ?>)
                                    {
                                        selected = 'selected';
                                    }
                                    else
                                    {
                                        selected = '';
                                    }
                                    html += '<option  value="'+val1['po_id']+'" '+selected+' >'+val1['po_name']+'</option>'
                                });
                                html += '</select>'
                                $('#setpo').html(html);
                                $('.total_div').addClass('hidden');
                                
                        }
                    });
                    
                    $.ajax({
                        type: "POST",
                        url: '<?php echo base_url().'admin/project/getidcompany'; ?>',
                        data:'companyid='+val,
                        success: function(data){
                                data = JSON.parse(data);
                                $("#po_adress").val(data[0]['address']);
                                $("#po_city").val(data[0]['city']);
                                
                        }
                    });
        }
        getpo_data(<?php echo $order[0]['po_id']; ?>);
        function getpo_data(val)
        {
                  if(val > 0)
                  {
                        $.ajax({
                           type: "POST",
                           url: '<?php echo base_url().'admin/order/getpodata'; ?>',
                           data:'projectid='+val,
                           success: function(data){
                                   data = JSON.parse(data);
                                   console.log(data);
                                   console.log(data.project[0]['po_name']);
                                   $('#copy_product').html('');
                                   $('#set_copy').html('');
                                   if(data.project.length > 0)
                                   {
                                       $('#po_number').val(data.project[0]['po_number']);
                                       $('#po_age').val(data.project[0]['po_age']);
                                       $('#po_state').val(data.project[0]['po_state']);
                                       $('#po_site_name').val(data.project[0]['po_site_contact_name']);
                                       $('#po_site_email').val(data.project[0]['po_site_contact_name']);
                                       $('#po_office_phone').val(data.project[0]['po_site_contact_office_phone']);
                                       $('#po_cell_phone').val(data.project[0]['po_site_contact_cell_phone']);
                                       $('.total_div').removeClass('hidden');
                                   }
                                   if(data.product.length > 0)
                                   {
                                          var project_block1 = "<input type='hidden' name='product_data' value='"+ data.product.length +"'>";
                                          $('#copy_product').html(project_block1);
                                          var prod_d =  data.product;
                                          var i = 1;
                                          var complete_total = 0;
                                          $.each(prod_d , function (prod_val) {
                                              //  if(data.product[prod_val]['quantity']>0) {
                                                console.log(data.product[prod_val]['quantity']);
                                                project_block = "<table class='table row"+i+"' data-id='"+i+"'>";
                                                project_block +="<thead><tr style='background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'> PROJECT QTY & PRICE</th><th class='col-md-2' colspan='2'> ORDER QTY </th><th class='col-md-1'>TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
                                                project_block += "<td>"+data.product[prod_val]['product_name']+"</td><td>"+data.product[prod_val]['product_desc']+"</td><td>"+data.product[prod_val]['quantity']+"</td><td> $ "+data.product[prod_val]['product_unit_price']+"</td><td data-id='"+i+"'><input type='hidden' id='order_product_price"+i+"' name='order_product_price"+i+"' value='"+data.product[prod_val]['product_unit_price']+"'><input type='hidden' name='order_product"+i+"' value='"+data.product[prod_val]['po_product_id']+"' ><label type='number' class='order_product_quantity prod' id='order_product_quantity"+i+"' name='order_product_quantity"+i+"' class='form-control' name='order_quantity"+i+"' ></label></td><td></td><td class='product_row_total1' id='product_row_total1_"+i+"'></td><td class='product_row_total hidden' id='product_row_total"+i+"'></td></tr><tr><td colspan='8'><b>Comment </b>: "+data.product[prod_val]['comment']+"</td></tr></tbody></table>";
                                                project_block += "</table>";
                                                complete_total =parseInt(complete_total) +parseInt(data.product[prod_val]['total']);
                                                $('#copy_product').html($('#copy_product').html()+project_block);
                                                i++; 
                                                // }
                                            }); 
                                            $('.total_show').html(complete_total);
                                            $('.total_show1').html(addCommas(complete_total));
//                                            project_block += "</table>";
//                                        var project_block = "<table class='table row1' data-id='1'><thead><tr class='info'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders.'></i> PROJECT QTY & PRICE</th><th class='col-md-1'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart.'></i> TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
//                                        project_block += "<td>".$u1['product_name']."</td><td>".$u1['product_desc']."</td><td>".$u1['quantity']."</td><td>".$u1['product_unit_price']."</td><td>".$u1['total']."</td></tr><tr><td colspan='5'><b>Comment </b>: ".$u1['comment']."</td></tr></tbody></table>";
                                          
                                   }
                                   else
                                   {
                                        $('#copy_product').html('<div class="col-md-12 emptypro"><center>No Data Found</center></div>');
                                   }
                                   if(data.custom.length > 0)
                                   {
                                            var project_block2 = "<input type='hidden' name='custom_data' value='"+ data.custom.length +"'>";
                                            $('#set_copy').html(project_block2);
                                            var prod_d =  data.custom;
                                            var j = 1;
                                            var complete_custom_total = 0;
                                            $.each(prod_d , function (prod_val) {
                                                project_block = '<table class="table customrow1">';
                                                project_block +='<thead><tr style="background: #a9ceec;color: #302a75;font-weight: 400;font-size: 11px;"><th class="col-md-2">ITEM</th><th class="col-md-2"> DESCRIPTION</th><th class="col-md-1">QTY</th><th class="col-md-1">PRICE</th><th class="col-md-2"><i class="fa fa-question-sign" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="REMAINING reflects the product quantity that is left to order. This quantity will be reduced to 0, after all products are orderd."></i> CUSTOM TOTAL </th></tr></thead><tbody class="breadcrumb"><tr><td>'+data.custom[prod_val]["po_custom_title"]+'</td><td>'+ data.custom[prod_val]['po_custom_desc']+'</td><td>'+ data.custom[prod_val]['po_custom_quantity']+'</td><td> $'+ addCommas(data.custom[prod_val]['po_custom_price'])+'</td><td data-id="'+j+'" class="hidden"><input type="hidden" id="order_custom_price'+j+'" name="order_custom_price'+j+'" value="'+data.custom[prod_val]['po_custom_price']+'" ><input type="hidden" name="order_custom'+j+'" value="'+data.custom[prod_val]['po_custom_id']+'" ><label class="order_custom_quanity prod" id="order_custom_quanity'+j+'" name="order_custom_quanity'+j+'" ></label></td><td class="custom_row_total1" id="custom_row_total1_'+j+'">$'+addCommas(data.custom[prod_val]['total'])+'</td><td class="custom_row_total hidden" id="custom_row_total'+j+'">'+ data.custom[prod_val]['total']+'</td><tr><tr><td colspan="8"><b>Comment </b>: '+data.custom[prod_val]['comment']+'</td></tr></tbody>';
                                                project_block += "</table>";
                                                complete_custom_total =parseInt(complete_custom_total) +parseInt(data.custom[prod_val]['total']);
                                                $('#set_copy').html($('#set_copy').html()+project_block);
                                                j++
                                            });    
                                            $('.total_custom_show').html(complete_custom_total);
                                            $('.total_custom_show1').html(addCommas(complete_custom_total));
//                                        var project_block = "<table class='table row1' data-id='1'><thead><tr class='info'><th class='col-md-1'>PRODUCT #</th><th class='col-md-2'> DESCRIPTION</th><th class='col-md-2' colspan='2'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='PROJECT QTY reflect the product quantity needed for a project, in total.PROJECT QTY is reduced based on the fulfilled/completed ORDER QTY orders.'></i> PROJECT QTY & PRICE</th><th class='col-md-1'><i class='fa fa-question-circle' data-toggle='tooltip' data-placement='bottom' title='The tottal reflect the current quantity pricing of ORDER QTY.This total will be added to the cart.'></i> TOTAL</th></tr></thead><tbody class='breadcrumb'><tr>";
//                                        project_block += "<td>".$u1['product_name']."</td><td>".$u1['product_desc']."</td><td>".$u1['quantity']."</td><td>".$u1['product_unit_price']."</td><td>".$u1['total']."</td></tr><tr><td colspan='5'><b>Comment </b>: ".$u1['comment']."</td></tr></tbody></table>";
                                          
                                   }
                                   else
                                   {
                                        $('#set_copy').html('<div class="col-md-12 emptypro"><center>No Data Found</center></div>');
                                   }
                                    var iterateMe = <?=json_encode($order_product_data)?>;
                                    var order_pro = 0;
                                    $.each(iterateMe,function(index, value) {

                                        var custom_row_id = $('.prod').eq( order_pro ).attr('id');

                                        if(value['quantity']>0) 
                                        {
                                            $('.prod').eq( order_pro ).text(value['quantity']);
                                             //console.log(custom_row_id);
                                            var tdid = $('#'+custom_row_id).closest('td').data("id");
                                            if(custom_row_id.indexOf('product') != -1)
                                            {
                                                    
                                                    var unit_price = $('#order_product_price'+tdid).val();
                                                    var quantity = $('#order_product_quantity'+tdid).text();
                                                    if(unit_price != '')
                                                    {
                                                        //console.log(unit_price);
                                                        $('#'+custom_row_id).closest('tr').find('#product_row_total'+tdid).html(unit_price*quantity);
                                                        $('#'+custom_row_id).closest('tr').find('#product_row_total1_'+tdid).html(addCommas((unit_price*quantity).toFixed(2)));
                                                    }
                                                    gettotal();
                                                    
                                            }
                                            else
                                            {
                                                //alert("asddsa");
                                                    var unit_price = $('#order_custom_price'+tdid).val();
                                                    var quantity = $('#order_custom_quanity'+tdid).text();
                                                    if(unit_price != '')
                                                    {
                                                        //console.log(unit_price);
                                                        var total = unit_price*quantity;
                                                        $('#'+custom_row_id).closest('tr').find('#custom_row_total'+tdid).html(addCommas(total.toFixed(2)));
                                                    }
                                                    getcustomtotal();
                                                      
                                            }
                                            order_pro++;  
                                            getcustomtotal();
                                        }
                                        else
                                        {

                                            $('#'+custom_row_id).closest('td').closest('tr').closest('table').remove();

                                        }
               
                                        
                                    });

                                    var ids = 0;
                                    $('.prod').each(function() {

                                        if($(this).html() == '' )
                                        {

                                            var custom_row_id = $('.prod').eq( ids ).attr('id');
                                            $('#'+custom_row_id).closest('td').closest('tr').closest('table').remove();                                            
                                        }
                                        ids++;

                                    });
                           }
                       });
                  }
        }
        function addCommas(nStr)
        {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
        }
        //getpodata
    </script>
    