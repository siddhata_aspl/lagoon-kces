<?php
//Navigation Bar
$lang['nav_dash']			= "Dashboard";
$lang['nav_pages']			= "Pages";
$lang['nav_pages_new']		= "Add Page";
$lang['nav_pages_all']		= "Pages";
$lang['nav_posts']			= "Posts";
$lang['nav_posts_new']		= "Add Post";
$lang['nav_posts_all']		= "Posts";
$lang['nav_categories_new']	= "Add Category";
$lang['nav_categories_all']	= "Categories";
$lang['nav_users']			= "Users";
$lang['nav_users_new']		= "Add User";
$lang['nav_users_all']		= "Users";
$lang['nav_navigation']		= "Navigation";
$lang['nav_navigation_new']	= "New Navigation Menu";
$lang['nav_navigation_all']	= "Menus";
$lang['nav_settings']		= "Settings";
$lang['nav_social']			= "Social";
$lang['nav_view_site']		= "View Site";
$lang['nav_logout']			= "Logout";
$lang['nav_profile']		= "Profile";
$lang['nav_banners']		= "Banners";
$lang['nav_banners_all']	= "Banners";
$lang['nav_banners_new']	= "Add Banner";
$lang['nav_peoples']		= "Our People";
$lang['nav_peoples_all']	= "People";
$lang['nav_peoples_new']	= "Add People";
$lang['nav_services']		= "Our Services";
$lang['nav_services_all']	= "Services";
$lang['nav_services_new']	= "Add Service";
$lang['nav_contacts']       = "Our Contacts";
$lang['nav_contacts_all']   = "Contacts";
$lang['nav_contacts_new']   = "Add Contact";
$lang['nav_careers']        = "Our Careers";
$lang['nav_careers_all']    = "Careers";
$lang['nav_careers_new']    = "Add Career";
$lang['nav_news_events']	= "News & Events";
$lang['nav_news_events_all']	= "News & Events";
$lang['nav_news_events_new']	= "Add News & Event";
$lang['nav_product_category']        = "Product Category";
$lang['nav_product_category_all']    = "Product Category";
$lang['nav_product_category_new']    = "Add Product Category";
$lang['nav_product_spotlight']        = "Product Spotlight";
$lang['nav_product_spotlight_all']    = "Product Spotlight";
$lang['nav_product_spotlight_new']    = "Add Product Spotlight";
$lang['nav_company']		= "Company";
$lang['nav_company_all']	= "Company";
$lang['nav_company_new']	= "Add Company";
$lang['nav_email_template']	= "Email Template";
$lang['nav_order']	= "Order";
$lang['nav_site_order']         = "Site Orders";

//Login Page
$lang['login_message']		= "Please provide your details";
$lang['login_username']		= "Username";
$lang['login_password']		= "Password";
$lang['login_signin']		= "Sign In";
$lang['login_reset']		= "Reset Password";
$lang['login_forgot']		= "Forgot Password";
$lang['login_incorrect']	= "The Username or Password entered is incorrect.";
$lang['login_expired']		= "Your session has expired, please use the form below to sign back in!";
$lang['register_complete']	= "You are ready for login";
$lang['captcha_incorrect']  = "Please check the recaptcha";

//Forgot Password
$lang['forgot_reset']		= "Password Reset";
$lang['forgot_email']		= "Email Address";
$lang['forgot_password']	= "New Password";
$lang['forgot_confirm']		= "Confirm Password";
$lang['forgot_complete']	= "Your password has been reset";
$lang['forgot_btn']			= "Reset";
$lang['forgot_check_email']	= "Please check your email for further instructions";

//Email Template
$lang['nav_email_template_all']					= "Email Templates";
$lang['email_template_header']					= "Email Templates";
$lang['email_template_title']					= "Email Template Title";
$lang['email_template_desc']					= "Email Template Description";
$lang['email_template_action']					= "Action";
$lang['email_template_edit_header']				= "Edit Email Template";
$lang['email_template_title']                                   = "Title";
$lang['email_template_desc']                                   = "Description";
$lang['email_template_subject']                                   = "Subject";
$lang['email_template_variable']                                   = "Variables";

//Dashboard
$lang['dash_welcome']					= "Welcome <small>to KCES</small>";
$lang['dash_unreachable']				= "The news feed cannot be reached.";
$lang['dash_recent']					= "Recently Updated Pages";
$lang['dash_maintenance_message']		= "Your site is currently in maintenance mode, visit the settings page to disable this and allow visitors to view your site.";
$lang['dash_message']					= "This is your KCES dashboard, Using the navigation bar you can add or edit the users, pages or navigation menus on your website. KCES is built around the bootstrap framework, this should make your website fully responsive with ease.";
$lang['feed_heading']					= "KCES News";


//Settings Page
$lang['settings_header']				= "Settings";
$lang['settings_message']				= "Some basic settings for your site ";
$lang['settings_info']					= "Information";
$lang['settings_name']					= "Site Name";
$lang['settings_footer']				= "Site Footer Message";
$lang['settings_footer2']				= "Site Footer Message2";
$lang['settings_theme']					= "Current Theme:";
$lang['settings_lang']					= "Current Dash Language:";
$lang['settings_logo']					= "Site Logo";
$lang['settings_logobottomtitle']       = "Site Logo Bottom Title";
$lang['settings_favicon']				= "Site Favicon";
$lang['settings_footer_logo']			= "Site Footer Logo";
$lang['settings_address1_title']		= "Address1 Title";
$lang['settings_address1']			    = "Address1";
$lang['settings_address1_phone']		= "Address1 Phone";
$lang['settings_address1_fax']          = "Address1 fax";
$lang['settings_address1_hours']		= "Address1 Hours";
$lang['settings_address1_map']          = "Address1 Map";
$lang['settings_address2_title']		= "Address2 Title";
$lang['settings_address2']              = "Address2";
$lang['settings_address2_phone']		= "Address2 Phone";
$lang['settings_address2_fax']          = "Address2 fax";
$lang['settings_address2_hours']		= "Address2 Hours";
$lang['settings_address2_map']          = "Address2 Map";
$lang['settings_domain']          = "Domain";
$lang['settings_api_key']          = "API Key";
$lang['settings_maintenance']			= "Enable Maintenance Mode?";
$lang['settings_maintenance_heading']	= "Maintenance Page Heading:";
$lang['settings_maintenance_meta']		= "Maintenance Page Meta Description";
$lang['settings_maintenance_content']	= "Maintenance Page Content:";
$lang['settings_additional_js']			= "Additional Javascript (such as Google Analytics) this will be loaded into all pages, remember to include script tags where necessary:";
$lang['settings_image_error']			= "Error! The file is not an image.";


// Project 
$lang['nav_project_all']                                   = "Project";
$lang['project_add_header']                                   = "Add Project";
$lang['nav_project_new']                                   = "Add Project";
$lang['project_header']                                   = "Project";
$lang['project_header']                                   = "Project";
$lang['po_action']                                   = "Action";
$lang['po_status']                                   = "Status";
$lang['po_number']                                   = "P.O. Number";
$lang['po_name']                                   = "ProjectName";
$lang['project_new_header']                                   = "Add Project";
$lang['po_name']                                   = "NAME";
$lang['po_number']                                   = "NUMBER";
$lang['po_manager']                                   = "MANAGER";
$lang['po_adress']                                   = "ADDRESS";
$lang['po_city']                                   = "CITY";
$lang['po_age']                                   = "AGE";
$lang['po_state']                                   = "ST";
$lang['po_site_email']                                   = "EMAIL";
$lang['po_site_name']                                   = "NAME";
$lang['po_cell_phone']                                   = "PHONE CELL";
$lang['po_office_phone']                                   = "PHONE OFFICE";
$lang['project_edit_header']                                   = "Edit Project";
$lang['project_delete']                                   = "Delete Project";
$lang['project_view_header']                                   = "View Project";
$lang['nav_project']	= "Project";

// Site Orders
$lang['order_id']                                            = "OrderID";
$lang['site_order_header']                                   = "Site Orders";
$lang['site_order_product_name']                             = "Product Name";
$lang['site_order_product_quantity']                         = "Quantity";
$lang['site_order_product_price']                            = "Product Price";
$lang['site_order_product_total']                            = "Total";
$lang['site_order_header']                                   = "Site Orders";
$lang['site_order_view_header']                              = "View Site Order";
$lang['site_order_username']                                 = "Username";
$lang['site_order_email']                                    = "Email";
$lang['site_order_phone']                                    = "Phone";
$lang['site_order_fullname']                                 = "User";
$lang['site_order_first_name']                               = "First name";
$lang['site_order_last_name']                                = "Last name";
$lang['order_date']                                          = "Order Date";


//Page - All Pages
$lang['pages_header']			= "Pages";
$lang['pages_table_page']		= "Page";
$lang['pages_table_updated']	= "Updated";
$lang['pages_table_created']	= "Created";
$lang['pages_table_published']	= "Published";
$lang['pages_delete']			= "Delete page ";
$lang['pages_delete_message']	= "This is permanent and cannot be undone…";
$lang['pages_delete_home']		= "You cannot delete the home page…";


//Page - New Page/Edit Page
$lang['pages_new_header']			= "Add Page";
$lang['pages_edit_header']			= "Edit Page";
$lang['pages_new_attributes']		= "Page Attributes";
$lang['pages_new_required']			= "All fields marked with * are required!";
$lang['pages_new_title']			= "Page/Meta Title*";
$lang['pages_new_nav']				= "Navigation Title* (this is displayed on navigation menus)";
$lang['pages_new_keywords']			= "Meta Keywords";
$lang['pages_new_description']		= "Meta Description";
$lang['pages_new_url']				= "Page URL* (no spaces or special characters allowed)";
$lang['pages_new_publish']			= "Publish Page?";
$lang['pages_new_template']			= "Page Template";




//Page - Jumbotron
$lang['pages_jumbo_intro']		= "a \"Jumbotron\" is a large heading area at the top of the page used for a hero message, image slider or both!";
$lang['pages_slider_header']	= "Image Slider";
$lang['pages_slider_btn']		= "Image Slider Options";
$lang['pages_slider_enable']	= "Enable Slider?";
$lang['pages_slider_add']		= "Add Slide";
$lang['pages_slider_title']		= "Slide";
$lang['pages_slider_alt']		= "Image Alt Tag";
$lang['pages_slider_link']		= "Image Link";
$lang['pages_jumbo_header']		= "Jumbotron Content";
$lang['pages_jumbo_header_att']	= "Jumbotron";
$lang['pages_jumbo_enable']		= "Enable Jumbotron?";
$lang['pages_jumbo_edit']		= "Edit Jumbotron";
$lang['pages_action']					= "Action";


//Posts - All Posts
$lang['posts_header']			= "Posts";
$lang['posts_table_post']		= "Post Title";
$lang['posts_table_category']	= "Category";
$lang['posts_table_posted']		= "Date Posted";
$lang['posts_table_published']	= "Published";
$lang['posts_delete']			= "Delete post ";
$lang['posts_delete_message']	= "This is permanent and cannot be undone…";


//Posts - New Post/Edit Post
$lang['posts_new_header']			= "Add Post";
$lang['posts_edit_header']			= "Edit Post";
$lang['posts_new_attributes']		= "Post Attributes";
$lang['posts_new_required']			= "All fields marked with * are required!";
$lang['posts_new_title']			= "Post/Meta Title*";
$lang['posts_new_feature']			= "Feature Image";
$lang['posts_new_excerpt']			= "Excerpt*";
$lang['posts_new_category']			= "Category";
$lang['posts_new_url']				= "Post URL* (no spaces or special characters allowed)";
$lang['posts_new_date']				= "Date";
$lang['posts_new_published']		= "Published?";

//Posts - All Categories
$lang['cat_header']			= "Categories";
$lang['cat_table_category']	= "Category Title";
$lang['cat_delete']			= "Delete category ";
$lang['cat_delete_message']	= "This is permanent and cannot be undone…";



//Posts - New Category/Edit Category
$lang['cat_new_header']			= "Add Category";
$lang['cat_edit_header']		= "Edit Category";
$lang['cat_new_required']		= "All fields marked with * are required!";
$lang['cat_new_title']			= "Category Title*";
$lang['cat_new_url']			= "Category URL Slug* (this should contain no special characters or spaces)";
$lang['cat_new_description']	= "Category Description";


//Company - All Company
$lang['company_header'] = "Company";
$lang['company_name'] = "Company name";
$lang['client_id'] = "Client ID";
$lang['company_phone'] = "Phone";
$lang['company_address'] = "Address";
$lang['company_state'] = "State";
$lang['company_city'] = "City";
$lang['company_zipcode'] = "Zipcode";
$lang['company_new_profilepic'] = "Company Logo";
$lang['company_first_name'] = "First name";
$lang['company_last_name'] = "Last name";
$lang['company_email'] = "Email";
$lang['company_delete'] = "Delete company";
$lang['company_delete_message'] = "Are you sure you want to delete?";
$lang['company_status'] = "Status";
$lang['company_action'] = "Action";
$lang['company_view_header'] = "View Company";


$lang['company_new_header'] = "Add Company";
$lang['company_add_header'] = "Add Company";

$lang['company_edit_header'] = "Edit Company";


//Users - All Users
$lang['user_header']			= "Users";
$lang['user_username']			= "Username";
$lang['user_email']				= "Email Address";
$lang['user_delete']			= "Delete user ";
$lang['user_delete_message']	= "Make sure this is not your only user account";


//Users - New User/Edit User
$lang['user_new_header']		= "Add User";
$lang['user_add_header']	    = "Add User";
$lang['user_edit_header']		= "Edit User";
$lang['user_new_firstname']		= "First name";
$lang['user_new_lastname']		= "Last name";
$lang['user_phone']		        = "Phone";
$lang['user_new_username']		= "Username";
$lang['user_new_message']		= "Your username is for logging in and cannot be changed.";
$lang['user_new_email']			= "Email";
$lang['user_new_profilepic']	= "Profile picture";
$lang['user_fullname']			= "Name";
$lang['user_new_pass']			= "Password";
$lang['user_status']			= "Status";
$lang['user_action']			= "Action";
$lang['user_new_confirm']		= "Confirm Password";

//Banners
$lang['banner_name']		= "Title";
$lang['image']		        = "Image";
$lang['banner_status']		= "Status";
$lang['banner_action']		= "Action";
$lang['banner_header']		= "Banners";
$lang['banner_new_header']	= "Add Banner";
$lang['banner_add_header']	= "Add Banner";
$lang['banner_title']	= "Banner title";
$lang['banner_content']	= "Banner content";
$lang['banner_image']	= "Banner image";
$lang['banner_edit_header']   = "Edit Banner";
$lang['banner_delete']			= "Delete banner ";


//Peoples
$lang['people_name']		= "Title";
$lang['photo']		        = "Photo";
//$lang['people_status']		= "Status";
$lang['people_action']		= "Action";
$lang['people_header']		= "Our People";
$lang['people_new_header']	= "Add People";
$lang['people_add_header']	= "Add People";
$lang['people_title']	        = "People name";
$lang['people_photo']	        = "People photo";
$lang['people_post']	        = "People designation";
$lang['people_edit_header']     = "Edit People";
$lang['people_delete']		= "Delete people";


//Services
$lang['service_name']		= "Title";
$lang['photo']		        = "Photo";
//$lang['service_status']		= "Status";
$lang['service_action']		= "Action";
$lang['service_header']		= "Our Services";
$lang['service_new_header']	= "Add Service";
$lang['service_add_header']	= "Add Service";
$lang['service_title']	        = "Service name";
$lang['service_image']	        = "Service image";
$lang['service_content']	    = "Service content";
$lang['service_edit_header']    = "Edit service";
$lang['service_delete']		= "Delete service";


//contacts
$lang['contact_name']       = "Title";
$lang['photo']              = "Photo";
//$lang['contact_status']       = "Status";
$lang['contact_action']     = "Action";
$lang['contact_header']     = "Our Contacts";
$lang['contact_new_header'] = "Add Contact";
$lang['contact_add_header'] = "Add Contact";
$lang['contact_title']          = "Contact title";
$lang['contact_link']          = "Contact link";
$lang['contact_image']          = "Contact image";
$lang['contact_content']        = "Contact content";
$lang['contact_edit_header']    = "Edit Contact";
$lang['contact_delete']     = "Delete contact";


//careers
$lang['career_name']        = "Title";
$lang['desc']              = "Short Description";
//$lang['career_status']       = "Status";
$lang['career_action']     = "Action";
$lang['career_header']     = "Our Careers";
$lang['career_new_header'] = "Add Career";
$lang['career_add_header'] = "Add Career";
$lang['career_title']          = "Career title";
$lang['career_link']          = "Career link";
//$lang['career_image']          = "Career image";
$lang['career_desc']        = "Career description";
$lang['career_req']        = "Career requirement";
$lang['career_edit_header']    = "Edit Career";
$lang['career_delete']     = "Delete career";


//news_eventss
$lang['news_events_name']        = "Title";
//$lang['desc']              = "Short Description";
$lang['news_events_status']       = "Status";
$lang['news_events_action']     = "Action";
$lang['news_events_header']     = "All News & Events";
$lang['news_events_new_header'] = "Add News & Event";
$lang['news_events_add_header'] = "Add News & Event";
$lang['news_events_title']          = "News & events title";
//$lang['news_events_link']         = "News events link";
$lang['news_events_image']          = "News & events image";
$lang['news_events_content']        = "News & events short description";
$lang['news_events_desc']        = "News &  events full description";
$lang['news_events_edit_header']    = "Edit news & event";
$lang['news_events_delete']     = "Delete news & event";


//product_categorys
$lang['product_category_name']        = "Title";
$lang['photo']              = "Photo";
//$lang['product_category_status']        = "Status";
$lang['product_category_action']      = "Action";
$lang['product_category_header']      = "Our Product Category";
$lang['product_category_new_header']  = "Add Product Category";
$lang['product_category_add_header']  = "Add Product Category";
$lang['product_category_title']           = "Product category name";
$lang['product_category_image']           = "Product category image";
$lang['product_category_edit_header']     = "Edit Product Category";
$lang['product_category_delete']      = "Delete Product Category";


//product_spotlights
$lang['product_spotlight_name']        = "Title";
$lang['photo']                         = "Photo";
//$lang['product_spotlight_status']        = "Status";
$lang['product_spotlight_action']      = "Action";
$lang['product_spotlight_header']      = "Our Product Spotlight";
$lang['product_spotlight_new_header']  = "Add Product Spotlight";
$lang['product_spotlight_add_header']  = "Add Product Spotlight";
$lang['product_spotlight_title']           = "Product spotlight name";
$lang['product_spotlight_description']     = "Product spotlight description";
$lang['product_spotlight_link']     = "Product spotlight link";
$lang['product_spotlight_image']           = "Product spotlight image";
$lang['product_spotlight_edit_header']     = "Edit Product Spotlight";
$lang['product_spotlight_delete']      = "Delete Product Spotlight";


//Siddhata - Change Password
$lang['nav_changepass']		= "Change Password";
$lang['cur_pass']           = "Current Password";
$lang['new_pass']           = "New Password";
$lang['conf_new_pass']      = "Confirm New Password";
//<<<<<<< HEAD
$lang['new']      = "New";
//=======
$lang['test']      = "test";

//>>>>>>> origin/master


$lang['delete_confirm_message']	= "Are you sure you want to delete?";


//Navigation - All Menus
$lang['menu_header']				= "Menus";
$lang['menu_table_title']			= "Menu Title";
$lang['menu_delete']				= "Delete nav ";
$lang['menu_delete_message']		= "This is permanent and cannot be undone…";
$lang['menu_delete_message_header']	= "You cannot delete the header menu…";

//order
$lang['nav_order_new']                                   = "Order New";
$lang['nav_order_all']                                   = "Order";
$lang['nav_order_new']                                   = "Add Order";
$lang['nav_order_add']                                   = "Add Order";
$lang['order_header']                                   = "Order";
$lang['order_header']                                   = "Order";
$lang['order_po_name']                                   = "Project Name";
$lang['order_po_number']                                   = "P.O. Number";
$lang['created_date']                                   = "Created Date";
$lang['order_action']                                   = "Action";
$lang['order_status']                                   = "Status";
$lang['order_new_header']                                   = "Add Order";
$lang['nav_order_all']                                   = "All Order";
$lang['order_company']                                   = "Manager";
$lang['order_po']                                   = "Project";
$lang['order_delete']                                   = " Order Delete with Project ";
$lang['order_edit_header']                                   = "Edit Order";
$lang['order_view_header']                                   = "View Order";

//Navigation - New Menu/Edit Menu
$lang['menu_new_nav']				= "Menus";
$lang['menu_new_pages']				= "Edit Menu";
$lang['menu_new_nav_slug']			= "Nav Slug* (max 10 characters, no spaces allowed)";
$lang['menu_new_nav_title']			= "Nav Title*";
$lang['menu_new_add_page']			= "Add Pages";
$lang['menu_new_select_page']		= "Select a page";
$lang['menu_new_select_product_category']        = "Select product category";
$lang['menu_new_custom_title']		= "Custom link title";
$lang['menu_new_custom_link']		= "Custom link URL(include http://)";
$lang['menu_new_drop_down']			= "Add Drop Down Section";
$lang['menu_new_drop_title']		= "Title";
$lang['menu_new_drop_link']			= "URL Slug (this is added in to the URL, no spaces allowed)";
$lang['menu_new_drop_error']		= "Your drop down slug contains spaces or special characters";


//Social Page
$lang['social_header']			= "Social Settings";
$lang['social_message']			= "Here you can set up the links for your various social accounts.<br/>These links will appear in the specified area of your theme.";
$lang['social_enable']			= "To enable an icon tick the box beside it.";
$lang['social_twitter']			= "Twitter";
$lang['social_facebook']		= "Facebook";
$lang['social_google-plus']			= "Google+";
$lang['social_pinterest']		= "Pinterest";
$lang['social_foursquare']		= "Foursquare";
$lang['social_linkedin']		= "LinkedIn";
$lang['social_myspace']			= "Myspace";
$lang['social_soundcloud']		= "Soundcloud";
$lang['social_spotify']			= "Spotify";
$lang['social_lastfm']			= "LastFM";
$lang['social_youtube']			= "YouTube";
$lang['social_vimeo']			= "Vimeo";
$lang['social_dailymotion']		= "DailyMotion";
$lang['social_vine']			= "Vine";
$lang['social_flickr']			= "Flickr";
$lang['social_instagram']		= "Instagram";
$lang['social_tumblr']			= "Tumblr";
$lang['social_reddit']			= "Reddit";
$lang['social_envato']			= "Envato";
$lang['social_github']			= "Github";
$lang['social_tripadvisor']		= "TripAdvisor";
$lang['social_stackoverflow']	= "Stack Overflow";
$lang['social_persona']			= "Mozilla Persona";
$lang['social_table_title']		= "Social Site";
$lang['social_table_link']		= "Profile Link";
$lang['social_table_enabled']	= "Enabled?";


// COMMON BUTTONS
$lang['btn_save']		= "Save";
$lang['btn_submit_order']		= "Submit Order";
$lang['btn_update']		= "Update";
$lang['btn_jumbotron']	= "Jumbotron";
$lang['btn_cancel']		= "Cancel";
$lang['btn_delete']		= "Delete";
$lang['btn_next']		= "Next";
$lang['btn_back']		= "Back";
$lang['btn_add']		= "Add";


$lang['option_yes']		= "Yes";
$lang['option_no']		= "No";





// EMAILS
$lang['email_reset_subject']= "Reset your password";
$lang['email_reset_message']= "Please click the link below to reset your password.";


// ERROR MESSAGES
$lang['no_results']			= "No results found.";
$lang['email_check']		= "This email address does not exist.";
$lang['required']			= "The %s field is required.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "The %s field must contain a valid email address.";
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_unique'] 			= "This %s already exists.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";

