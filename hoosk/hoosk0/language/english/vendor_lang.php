<?php

//Navigation Bar
$lang['nav_dash']		= "Dashboard";
$lang['nav_logout']		= "Logout";
$lang['nav_employees']			= "Employees";
$lang['nav_employees_new']		= "Add Employee";
$lang['nav_employees_all']		= "Employees";


//Dashboard
$lang['dash_welcome']				= "Welcome <small>to KCES</small>";
$lang['dash_unreachable']			= "The news feed cannot be reached.";
$lang['dash_recent']				= "Recently Updated Pages";
$lang['dash_maintenance_message']		= "Your site is currently in maintenance mode, visit the settings page to disable this and allow visitors to view your site.";
$lang['dash_message']				= "This is your KCES dashboard, Using the navigation bar you can add or edit the users, pages or navigation menus on your website. KCES is built around the bootstrap framework, this should make your website fully responsive with ease.";
$lang['feed_heading']				= "KCES News";


//Login Page
$lang['login_message']		= "Please provide your details";
$lang['login_email']		= "Email";
$lang['login_password']		= "Password";
$lang['login_signin']		= "Sign In";
$lang['login_reset']		= "Reset Password";
$lang['login_forgot']		= "Forgot Password";
$lang['login_incorrect']	= "The Email Address or Password entered is incorrect.";
$lang['login_expired']		= "Your session has expired, please use the form below to sign back in!";
$lang['register_complete']	= "You are ready for login";


//Forgot Password
$lang['forgot_reset']		= "Password Reset";
$lang['forgot_email']		= "Email Address";
$lang['forgot_password']	= "New Password";
$lang['forgot_confirm']		= "Confirm Password";
$lang['forgot_complete']	= "Your password has been reset";
$lang['forgot_btn']		= "Reset";
$lang['forgot_check_email']	= "Please check your email for further instructions";


//Change Password
$lang['nav_changepass']		    = "Change Password";
$lang['cur_pass']               = "Current Password";
$lang['new_pass']               = "New Password";
$lang['conf_new_pass']          = "Confirm New Password";


// COMMON BUTTONS
$lang['btn_save']		= "Save";
$lang['btn_submit_order']		= "Submit Order";
$lang['btn_update']		= "Update";
$lang['btn_jumbotron']	        = "Jumbotron";
$lang['btn_cancel']		= "Cancel";
$lang['btn_delete']		= "Delete";
$lang['btn_next']		= "Next";
$lang['btn_back']		= "Back";
$lang['btn_add']		= "Add";


//employees - All employees
$lang['employee_header']			= "Employees";
$lang['employee_name']		        = "Employee name";
$lang['employee_email']				= "Email address";
$lang['employee_delete']			= "Delete employee ";
$lang['employee_delete_message']	= "Make sure this is not your only employee account";


//employees - New employee/Edit employee
$lang['employee_new_header']		= "Add Employee";
$lang['employee_add_header']	    = "Add Employee";
$lang['employee_edit_header']		= "Edit Employee";
$lang['employee_firstname']		    = "First name";
$lang['employee_lastname']		    = "Last name";
$lang['employee_phone']		        = "Phone";
$lang['employee_name']	= "Employee name";
$lang['employee_message']		= "Your employee name is for logging in and cannot be changed.";
$lang['employee_email']			    = "Email";
$lang['employee_profilepic']	= "Profile picture";
$lang['employee_fullname']			= "Name";
$lang['employee_new_pass']			= "Password";
$lang['employee_status']			= "Status";
$lang['employee_action']			= "Action";
$lang['employee_new_confirm']		= "Confirm Password";

$lang['employee_permission']       = "Permission";
$lang['permission_employee']       = "Employee";
$lang['permission_project']        = "Project";
$lang['permission_order']          = "Order";
$lang['permission_add']            = "Add";
$lang['permission_edit']           = "Edit";
$lang['permission_view']           = "View";
$lang['permission_delete']         = "Delete";


$lang['delete_confirm_message']	= "Are you sure you want to delete?";


//projects
// $lang['nav_project_all']           = "Project";
// $lang['project_add_header']        = "Add Project";
// $lang['nav_project_new']           = "Add Project";
// $lang['project_header']            = "Project";
// $lang['project_header']            = "Project";
// $lang['po_action']                 = "Action";
// $lang['po_status']                 = "Status";
// $lang['po_number']                 = "P.O. Number";
// $lang['po_name']                   = "ProjectName";
// $lang['project_new_header']        = "Add Project";
// $lang['po_name']                   = "Name";
// $lang['po_number']                 = "Number";
// $lang['po_manager']                = "Manager";
// $lang['po_adress']                 = "Address";
// $lang['po_city']                   = "City";
// $lang['po_age']                    = "Age";
// $lang['po_site_email']             = "Email";
// $lang['po_site_name']              = "Name";
// $lang['po_cell_phone']             = "Cell Phone";
// $lang['po_office_phone']           = "Office Phone";
// $lang['project_edit_header']       = "Edit Project";
// $lang['project_delete']            = "Delete Project";
// $lang['project_view_header']       = "View Project";
$lang['nav_project']	           = "Project";
$lang['company_city']              = "City";

// Project
$lang['nav_project_all']                                   = "Project";
$lang['project_add_header']                                   = "Add Project";
$lang['nav_project_new']                                   = "Add Project";
$lang['project_header']                                   = "Project";
$lang['project_header']                                   = "Project";
$lang['po_action']                                   = "Action";
$lang['po_status']                                   = "Status";
$lang['po_number']                                   = "P.O. Number";
$lang['po_name']                                   = "ProjectName";
$lang['project_new_header']                                   = "Add Project";
$lang['po_name']                                   = "NAME";
$lang['po_number']                                   = "NUMBER";
$lang['po_manager']                                   = "MANAGER";
$lang['po_adress']                                   = "ADDRESS";
$lang['po_city']                                   = "CITY";
$lang['po_age']                                   = "AGE";
$lang['po_state']                                   = "ST";
$lang['po_site_email']                                   = "EMAIL";
$lang['po_site_name']                                   = "NAME";
$lang['po_cell_phone']                                   = "PHONE CELL";
$lang['po_office_phone']                                   = "PHONE OFFICE";
$lang['project_edit_header']                                   = "Edit Project";
$lang['project_delete']                                   = "Delete Project";
$lang['project_view_header']                                   = "View Project";

//order
$lang['nav_order_new']             = "Order New";
$lang['nav_order_all']             = "Order";
$lang['nav_order_new']             = "Add Order";
$lang['nav_order_add']             = "Add Order";
$lang['order_header']              = "Order";
$lang['order_header']              = "Order";
$lang['order_po_name']             = "Project Name";
$lang['order_po_number']           = "P.O. Number";
$lang['created_date']              = "Created Date";
$lang['order_action']              = "Action";
$lang['order_status']              = "Status";
$lang['order_new_header']          = "Add Order";
$lang['nav_order_all']             = "All Order";
$lang['order_company']             = "Manager";
$lang['order_po']                  = "Project";
$lang['order_delete']              = " Order Delete with Project ";
$lang['order_edit_header']         = "Edit Order";
$lang['order_view_header']         = "View Order";
$lang['nav_order']	= "Order";


// ERROR MESSAGES
$lang['no_results']			= "No results found.";
$lang['email_check']		= "This email address does not exist.";
$lang['required']			= "The %s field is required.";
$lang['isset']				= "The %s field must have a value.";
$lang['valid_email']		= "The %s field must contain a valid email address.";
$lang['valid_emails']		= "The %s field must contain all valid email addresses.";
$lang['valid_url']			= "The %s field must contain a valid URL.";
$lang['valid_ip']			= "The %s field must contain a valid IP.";
$lang['min_length']			= "The %s field must be at least %s characters in length.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']		= "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']		= "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "The %s field must contain only numbers.";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['regex_match']		= "The %s field is not in the correct format.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_unique'] 			= "This %s already exists.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']	= "The %s field must contain a number greater than zero.";
$lang['decimal']			= "The %s field must contain a decimal number.";
$lang['less_than']			= "The %s field must contain a number less than %s.";
$lang['greater_than']		= "The %s field must contain a number greater than %s.";


?>
