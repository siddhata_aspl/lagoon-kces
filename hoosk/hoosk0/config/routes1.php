<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['install/complete'] = "admin/admin/complete";
$route['attachments'] = "admin/admin/upload";
$route['admin'] = "admin/admin";
$route['admin/login'] = "admin/admin/login";
$route['admin/login/check'] = "admin/admin/loginCheck";
$route['admin/logout'] = "admin/admin/logout";
$route['admin/check/session'] = "admin/admin/checkSession";
$route['admin/ajax/login'] = "admin/admin/ajaxLogin";
$route['admin/users'] = "admin/users";
$route['admin/users/new'] = "admin/users/addUser";
$route['admin/users/getalldata'] = "admin/users/get_all_data";
$route['admin/users/change_status'] = "admin/users/change_status";
$route['admin/users/new/add'] = "admin/users/confirm";
$route['admin/users/delete/(:any)'] = "admin/users/delete";
$route['admin/users/edit/(:any)'] = "admin/users/editUser";
$route['admin/users/edited/(:any)'] = "admin/users/edited";


$route['vendor'] = "vendor/vendor";
$route['vendor/login'] = "vendor/vendor/login";
$route['vendor/login/check'] = "vendor/vendor/loginCheck";
$route['vendor/logout'] = "vendor/vendor/logout";
$route['vendor/check/session'] = "vendor/vendor/checkSession";
$route['vendor/ajax/login'] = "vendor/vendor/ajaxLogin";
$route['vendor/vendor/forgot'] = 'vendor/vendor/forgot'; //
//$route['admin/users/(:any)'] = "admin/users";
$route['vendor/reset/(:any)'] = 'vendor/vendor/getPassword'; //


//Vendor Employee
$route['vendor/employees'] = "vendor/employees";
$route['vendor/employees/new'] = "vendor/employees/addemployee";
$route['vendor/employees/getalldata'] = "vendor/employees/get_all_data";
$route['vendor/employees/change_status'] = "vendor/employees/change_status";
$route['vendor/employees/new/add'] = "vendor/employees/confirm";
$route['vendor/employees/delete/(:any)'] = "vendor/employees/delete";
$route['vendor/employees/edit/(:any)'] = "vendor/employees/editemployee";
$route['vendor/employees/edited/(:any)'] = "vendor/employees/edited";

$route['vendor/employee/forgot'] = 'vendor/employees/forgot'; //
$route['vendor/employees/(:any)'] = "vendor/employees";
$route['vendor/employees/reset/(:any)'] = 'vendor/employees/getPassword'; //
$route['vendor/newemployee/(:any)'] = 'vendor/employees/newemployee'; //


$route['admin/company'] = "admin/company";
$route['admin/company/new'] = "admin/company/addCompany";
$route['admin/company/view/(:any)'] = "admin/company/viewCompany";
$route['vendor/newcompany/(:any)'] = 'admin/company/newcompany'; //
$route['admin/company/getalldata'] = "admin/company/get_all_data";
$route['admin/company/change_status'] = "admin/company/change_status";
$route['admin/company/new/add'] = "admin/company/confirm";
$route['admin/company/delete/(:any)'] = "admin/company/delete";
$route['admin/company/edit/(:any)'] = "admin/company/editCompany";
$route['admin/company/edited/(:any)'] = "admin/company/edited";


$route['admin/company'] = "admin/company";
$route['admin/company/new'] = "admin/company/addCompany";
$route['admin/company/view/(:any)'] = "admin/company/viewCompany";
$route['admin/newcompany/(:any)'] = 'admin/company/newcompany'; //
$route['admin/company/getalldata'] = "admin/company/get_all_data";
$route['admin/company/change_status'] = "admin/company/change_status";
$route['admin/company/new/add'] = "admin/company/confirm";
$route['admin/company/delete/(:any)'] = "admin/company/delete";
$route['admin/company/edit/(:any)'] = "admin/company/editCompany";
$route['admin/company/edited/(:any)'] = "admin/company/edited";


//Siddhata - Change Password
$route['admin/change_password'] = "admin/admin/change_password";
$route['admin/admin/changepass'] = "admin/admin/confirm_changepass";

//Change Password
$route['vendor/change_password'] = "vendor/vendor/change_password";
$route['vendor/vendor/changepass'] = "vendor/vendor/confirm_changepass";


//Our People
$route['admin/our_people'] = "admin/our_people";
$route['admin/our_people/new'] = "admin/our_people/addPeople";
$route['admin/our_people/getalldata'] = "admin/our_people/get_all_data";
//$route['admin/our_people/change_status'] = "admin/our_people/change_status";
$route['admin/our_people/new/add'] = "admin/our_people/confirm";
$route['admin/our_people/delete/(:any)'] = "admin/our_people/delete";
$route['admin/our_people/edit/(:any)'] = "admin/our_people/editPeople";
$route['admin/our_people/edited/(:any)'] = "admin/our_people/edited";
$route['admin/our_people/(:any)'] = "admin/our_people";


//Our service
$route['admin/our_service'] = "admin/our_service";
$route['admin/our_service/new'] = "admin/our_service/addservice";
$route['admin/our_service/getalldata'] = "admin/our_service/get_all_data";
//$route['admin/our_service/change_status'] = "admin/our_service/change_status";
$route['admin/our_service/new/add'] = "admin/our_service/confirm";
$route['admin/our_service/delete/(:any)'] = "admin/our_service/delete";
$route['admin/our_service/edit/(:any)'] = "admin/our_service/editservice";
$route['admin/our_service/edited/(:any)'] = "admin/our_service/edited";
$route['admin/our_service/(:any)'] = "admin/our_service";


//Our contact
$route['admin/our_contact'] = "admin/our_contact";
$route['admin/our_contact/new'] = "admin/our_contact/addcontact";
$route['admin/our_contact/getalldata'] = "admin/our_contact/get_all_data";
//$route['admin/our_contact/change_status'] = "admin/our_contact/change_status";
$route['admin/our_contact/new/add'] = "admin/our_contact/confirm";
$route['admin/our_contact/delete/(:any)'] = "admin/our_contact/delete";
$route['admin/our_contact/edit/(:any)'] = "admin/our_contact/editcontact";
$route['admin/our_contact/edited/(:any)'] = "admin/our_contact/edited";
$route['admin/our_contact/(:any)'] = "admin/our_contact";


//Our career
$route['admin/our_career'] = "admin/our_career";
$route['admin/our_career/new'] = "admin/our_career/addcareer";
$route['admin/our_career/getalldata'] = "admin/our_career/get_all_data";
//$route['admin/our_career/change_status'] = "admin/our_career/change_status";
$route['admin/our_career/new/add'] = "admin/our_career/confirm";
$route['admin/our_career/delete/(:any)'] = "admin/our_career/delete";
$route['admin/our_career/edit/(:any)'] = "admin/our_career/editcareer";
$route['admin/our_career/edited/(:any)'] = "admin/our_career/edited";
$route['admin/our_career/(:any)'] = "admin/our_career";

//Our News Events
$route['admin/news_events'] = "admin/news_events";
$route['admin/news_events/new'] = "admin/news_events/addnews_events";
$route['admin/news_events/getalldata'] = "admin/news_events/get_all_data";
$route['admin/news_events/change_status'] = "admin/news_events/change_status";
$route['admin/news_events/new/add'] = "admin/news_events/confirm";
$route['admin/news_events/delete/(:any)'] = "admin/news_events/delete";
$route['admin/news_events/edit/(:any)'] = "admin/news_events/editnews_events";
$route['admin/news_events/edited/(:any)'] = "admin/news_events/edited";
$route['admin/news_events/(:any)'] = "admin/news_events";
$route['news_events/(:any)'] = "hoosk_default/viewnews_events";


//Product Category
$route['admin/product_category'] = "admin/product_category";
$route['admin/product_category/new'] = "admin/product_category/addproduct_category";
$route['admin/product_category/getalldata'] = "admin/product_category/get_all_data";
//$route['admin/product_category/change_status'] = "admin/product_category/change_status";
$route['admin/product_category/new/add'] = "admin/product_category/confirm";
$route['admin/product_category/delete/(:any)'] = "admin/product_category/delete";
$route['admin/product_category/edit/(:any)'] = "admin/product_category/editproduct_category";
$route['admin/product_category/edited/(:any)'] = "admin/product_category/edited";
$route['admin/product_category/(:any)'] = "admin/product_category";


//Product Spotlight
$route['admin/product_spotlight'] = "admin/product_spotlight";
$route['admin/product_spotlight/new'] = "admin/product_spotlight/addproduct_spotlight";
$route['admin/product_spotlight/getalldata'] = "admin/product_spotlight/get_all_data";
//$route['admin/product_spotlight/change_status'] = "admin/product_spotlight/change_status";
$route['admin/product_spotlight/new/add'] = "admin/product_spotlight/confirm";
$route['admin/product_spotlight/delete/(:any)'] = "admin/product_spotlight/delete";
$route['admin/product_spotlight/edit/(:any)'] = "admin/product_spotlight/editproduct_spotlight";
$route['admin/product_spotlight/edited/(:any)'] = "admin/product_spotlight/edited";
$route['admin/product_spotlight/(:any)'] = "admin/product_spotlight";


//project
$route['admin/project'] = "admin/project";
$route['admin/project/new'] = "admin/project/addProject";
$route['admin/project/test'] = "admin/project/test";
$route['admin/project/view/(:any)'] = "admin/project/view";
$route['admin/project/pdf/(:any)'] = "admin/project/pdfgen";
$route['admin/project/getproduct'] = "admin/project/get_product_ajax";
$route['admin/project/getidproduct'] = "admin/project/get_product_byid_ajax";
$route['admin/project/getidcompany'] = "admin/project/get_company_byid_ajax";
$route['admin/project/getalldata'] = "admin/project/get_all_data";
$route['admin/project/change_status'] = "admin/project/change_status";
$route['admin/project/new/add'] = "admin/project/confirm";
$route['admin/project/delete/(:any)'] = "admin/project/delete";
$route['admin/project/edit/(:any)'] = "admin/project/editProject";
$route['admin/project/edited/(:any)'] = "admin/project/edited";
$route['admin/project/(:any)'] = "admin/project";


//project
$route['vendor/project'] = "vendor/project";
$route['vendor/project/new'] = "vendor/project/addProject";
$route['vendor/project/test'] = "vendor/project/test";
$route['vendor/project/view/(:any)'] = "vendor/project/view";
$route['vendor/project/pdf/(:any)'] = "vendor/project/pdfgen";
$route['vendor/project/getproduct'] = "vendor/project/get_product_ajax";
$route['vendor/project/getidproduct'] = "vendor/project/get_product_byid_ajax";
$route['vendor/project/getidcompany'] = "vendor/project/get_company_byid_ajax";
$route['vendor/project/getalldata'] = "vendor/project/get_all_data";
$route['vendor/project/change_status'] = "vendor/project/change_status";
$route['vendor/project/new/add'] = "vendor/project/confirm";
$route['vendor/project/delete/(:any)'] = "vendor/project/delete";
$route['vendor/project/edit/(:any)'] = "vendor/project/editProject";
$route['vendor/project/edited/(:any)'] = "vendor/project/edited";
$route['vendor/project/(:any)'] = "vendor/project";


//order
$route['admin/order'] = "admin/order";
$route['admin/order/new'] = "admin/order/addOrder";
$route['admin/order/view/(:any)'] = "admin/order/view";
$route['admin/order/getproduct'] = "admin/order/get_order_ajax";
$route['admin/order/getidorder'] = "admin/order/get_order_byid_ajax";
$route['admin/order/getalldata'] = "admin/order/get_all_data";
$route['admin/order/getpoofmanager'] = "admin/order/get_po_of_manager";
$route['admin/order/getpodata'] = "admin/order/get_po_data";
$route['admin/order/change_status'] = "admin/order/change_status";
$route['admin/order/new/add'] = "admin/order/confirm";
$route['admin/order/delete/(:any)'] = "admin/order/delete";
$route['admin/order/edit/(:any)'] = "admin/order/editOrder";
$route['admin/order/edited/(:any)'] = "admin/order/edited";
$route['admin/order/(:any)'] = "admin/order";


//order
$route['vendor/order'] = "vendor/order";
$route['vendor/order/new'] = "vendor/order/addOrder";
$route['vendor/order/view/(:any)'] = "vendor/order/view";
$route['vendor/order/getproduct'] = "vendor/order/get_order_ajax";
$route['vendor/order/getidorder'] = "vendor/order/get_order_byid_ajax";
$route['vendor/order/getidcompany'] = "vendor/order/get_company_byid_ajax";
$route['vendor/order/getalldata'] = "vendor/order/get_all_data";
$route['vendor/order/getpoofmanager'] = "vendor/order/get_po_of_manager";
$route['vendor/order/getpodata'] = "vendor/order/get_po_data";
$route['vendor/order/change_status'] = "vendor/order/change_status";
$route['vendor/order/new/add'] = "vendor/order/confirm";
$route['vendor/order/delete/(:any)'] = "vendor/order/delete";
$route['vendor/order/edit/(:any)'] = "vendor/order/editOrder";
$route['vendor/order/edited/(:any)'] = "vendor/order/edited";
$route['vendor/order/(:any)'] = "vendor/order";


$route['admin/banners'] = "admin/banners";
$route['admin/banners/new'] = "admin/banners/addBanners";
$route['admin/banners/getalldata'] = "admin/banners/get_all_data";
$route['admin/banners/change_status'] = "admin/banners/change_status";
$route['admin/banners/new/add'] = "admin/banners/confirm";
$route['admin/banners/delete/(:any)'] = "admin/banners/delete";
$route['admin/banners/edit/(:any)'] = "admin/banners/editBanner";
$route['admin/banners/edited/(:any)'] = "admin/banners/edited";
$route['admin/banners/(:any)'] = "admin/banners";

$route['admin/email_template'] = "admin/email_template";
$route['admin/email_template/new'] = "admin/email_template/addEmail_template";
$route['admin/email_template/edit/(:any)'] = "admin/email_template/editEmail_template";
$route['admin/email_template/edited/(:any)'] = "admin/email_template/edited";

$route['admin/user/forgot'] = 'admin/users/forgot'; //
$route['admin/users/(:any)'] = "admin/users";
$route['admin/reset/(:any)'] = 'admin/users/getPassword'; //
$route['admin/newuser/(:any)'] = 'admin/users/newuser'; //
$route['admin/pages'] = "admin/pages";
$route['admin/pages/new'] = "admin/pages/addPage";
$route['admin/pages/new/add'] = "admin/pages/confirm";
$route['admin/pages/getalldata'] = "admin/pages/get_all_data";
$route['admin/pages/delete/(:any)'] = "admin/pages/delete";
$route['admin/pages/edit/(:any)'] = "admin/pages/editPage";
$route['admin/pages/edited/(:any)'] = "admin/pages/edited";
$route['admin/pages/jumbo/(:any)'] = "admin/pages/jumbo";
$route['admin/pages/jumbotron/(:any)'] = "admin/pages/jumboAdd";
$route['admin/pages/(:any)'] = "admin/pages";
$route['admin/menu'] = "admin/navigation";
$route['admin/menu/new'] = "admin/navigation/newNav";
$route['admin/menu/edit/(:any)'] = "admin/navigation/editNav";
$route['admin/menu/delete/(:any)'] = "admin/navigation/deleteNav";
$route['admin/navadd/(:any)'] = "admin/navigation/navAdd";
$route['admin/navigation/insert'] = "admin/navigation/insert";
$route['admin/navigation/update/(:any)'] = "admin/navigation/update";
$route['admin/navigation/(:any)'] = "admin/navigation";
$route['admin/settings'] = "admin/admin/settings";
$route['admin/settings/submit'] = "admin/admin/uploadLogo";
$route['admin/settings/update'] = "admin/admin/updateSettings";
$route['admin/social'] = "admin/admin/social";
$route['admin/social/update'] = "admin/admin/updateSocial";
$route['admin/posts'] = "admin/posts";
$route['admin/posts/new'] = "admin/posts/addPost";
$route['admin/posts/new/add'] = "admin/posts/confirm";
$route['admin/posts/delete/(:any)'] = "admin/posts/delete";
$route['admin/posts/edit/(:any)'] = "admin/posts/editPost";
$route['admin/posts/edited/(:any)'] = "admin/posts/edited";
$route['admin/posts/categories'] = "admin/categories";
$route['admin/posts/categories/new'] = "admin/categories/addCategory";
$route['admin/posts/categories/new/add'] = "admin/categories/confirm";
$route['admin/posts/categories/delete/(:any)'] = "admin/categories/delete";
$route['admin/posts/categories/edit/(:any)'] = "admin/categories/editCategory";
$route['admin/posts/categories/edited/(:any)'] = "admin/categories/edited";
$route['admin/posts/categories/(:any)'] = "admin/categories";
$route['admin/posts/(:any)'] = "admin/posts";
$route['admin/ajax/post-search'] = "admin/posts/postSearch";
$route['admin/ajax/page-search'] = "admin/pages/pageSearch";
$route['admin/ajax/email_template-search'] = "admin/email_template/email_templateSearch";

$route['category/(:any)'] = "hoosk_default/category";
$route['category/(:any)/(:any)'] = "hoosk_default/category";
$route['article/(:any)'] = "hoosk_default/article";
$route['feed/(:any)'] = "hoosk_default/feed";
$route['(.+)'] = "hoosk_default";
$route['default_controller'] = "hoosk_default";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;




