<?php

class Site_orders_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    /*     * *************************** */
    /*     * ** Site Orders Querys ************ */
    /*     * *************************** */
    
    
    function countSiteOrders(){
        return $this->db->count_all('site_orders');
    }
	
    function getSiteOrders($limit, $offset=0) {
        // Get a list of all user accounts
        //$this->db->select("User_firstname,User_lastname,userName, email, userID,user_status");
        $this->db->select("*");
        $this->db->join("hoosk_user", "site_orders.login_id = hoosk_user.userID");
        $this->db->order_by("userName", "desc");
	$this->db->limit($limit, $offset);
        $query = $this->db->get('site_orders');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function resultperpage()
    {
        $this->db->select("resultperpage");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_site_orders_datatable_data($module,$requestData)
    {
        $columns = array( 
                0 => 'userName', 
                1 => 'orderID',
                2 => 'total',
                3 => 'created_date',
//                4 => 'product_total',
//                5 => 'created_date',
        );
       
        // getting total number records without any search
        $sql = "SELECT *, SUM(product_total) as total";
        $sql.=" FROM $module join hoosk_user on $module.login_id = hoosk_user.userID group by orderID";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT *, SUM(product_total) as total";
        $sql.=" FROM $module join hoosk_user on $module.login_id = hoosk_user.userID WHERE 1=1 group by orderID";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( userName LIKE '".$requestData['search']['value']."%' ";    
        	
//        	$sql.=" OR User_firstname LIKE '".$requestData['search']['value']."%' ";
//        
//        	$sql.=" OR User_lastname LIKE '".$requestData['search']['value']."%' ";
//        	
//        	$sql.=" OR email LIKE '".$requestData['search']['value']."%' ";
        	
        	$sql.=" OR site_orders.orderID LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result. 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY site_order_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
        $query=$this->db->query($sql);
        
        //echo $this->db->last_query();die;
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["userName"];
                    $nestedData[] = $row["orderID"];
//                    $nestedData[] = '$'.number_format($row["product_price"],2);
//                    $nestedData[] = $row["product_quantity"];
                    $nestedData[] = '$'.number_format($row["total"],2);
                    $nestedData[] = $row["created_datetime"];

//                    $statusbutton = '<button style="color:white" data-id="'.$row["userID"].'" data-val="'.$row["user_status"].'"';
//                    if ($row['user_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['user_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/site_orders/view/'.base64_encode($row['userID']).'/'.base64_encode($row['orderID']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
                        return json_encode($json_data);
        
    }
    
    function getUserByOrderUserId($uid)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("userID", $uid);
       
        $query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    function getOrderIdDate($oid)
    {
        // Get the order details
        $this->db->select("site_orders.orderID, site_orders.created_datetime");
        $this->db->where("orderID", $oid);
       
        $query = $this->db->get('site_orders');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    function getSiteOrderById($oid)
    {
        // Get the order details
        $this->db->select("*");
        $this->db->where("orderID", $oid);
       
        $query = $this->db->get('site_orders');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }   
    
}
?>