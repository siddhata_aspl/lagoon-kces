<?php

class Our_contact_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    /*     * *************************** */
    /*     * ** Our contact Querys ************ */
    /*     * *************************** */
    
    function get_contact_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'contact_title'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( contact_title LIKE '".$requestData['search']['value']."%' ) ";    
            
//          $sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY contact_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["contact_title"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/contacts/'.$row['contact_image'].'" width="100px" height="100px" >';
//                    $statusbutton = '<button style="color:white" data-id="'.$row["contact_id"].'" data-val="'.$row["contact_status"].'"';
//                    if ($row['contact_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['banner_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/our_contact/edit/'.base64_encode($row['contact_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/our_contact/delete/'.base64_encode($row['contact_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
         return json_encode($json_data);
    }
    
    function countcontacts()
    {
        return $this->db->count_all('contacts');
    }
    
    function getcontacts($limit, $offset)
    {
         // Get a list of all contacts
        $this->db->select("*");
        $this->db->order_by("contact_id", "desc");
    $this->db->limit($limit, $offset);
        $query = $this->db->get('contacts');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getcontact($id)
    {
        // Get the contact details
        $this->db->select("*");
        $this->db->where("contact_id", $id);
        $query = $this->db->get('contacts');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removecontact($id)
    {
        // Delete a contacts
        $this->db->delete('contacts', array('contact_id' => $id));
        
    }
    
    function changecontact_status($data, $id) {
        
        $this->db->where('contact_id', $id);
        if($this->db->update('contacts', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createcontacts($filename = '')
    {
        // Create the contacts
        $data = array(
            'contact_title' => strip_tags($this->input->post('contact_title')),
            'contact_link' => strip_tags($this->input->post('contact_link')),
            //'contact_status'=>'Enabled',
            'contact_image'=>$filename,
            'contact_content' => $this->input->post('contact_content'),
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('contacts', $data);
    }
    
    function updatecontacts($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'contact_title' => strip_tags($this->input->post('contact_title')),
                'contact_link' => strip_tags($this->input->post('contact_link')),
                'contact_image'=> $filename,
                'contact_content' => $this->input->post('contact_content'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'contact_title' => strip_tags($this->input->post('contact_title')),
                'contact_link' => strip_tags($this->input->post('contact_link')),
                'contact_content' => $this->input->post('contact_content'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('contact_id', $id);
        $this->db->update('contacts', $data);
    }
    
}
?>