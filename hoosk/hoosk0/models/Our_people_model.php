<?php

class Our_people_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    /*     * *************************** */
    /*     * ** Our People Querys ************ */
    /*     * *************************** */
    
    function get_people_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'people_name'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( people_name LIKE '".$requestData['search']['value']."%' ) ";    
        	
//        	$sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY people_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["people_name"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/peoples/'.$row['people_photo'].'" width="100px" height="100px" >';
//                    $statusbutton = '<button style="color:white" data-id="'.$row["people_id"].'" data-val="'.$row["people_status"].'"';
//                    if ($row['people_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['banner_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/our_people/edit/'.base64_encode($row['people_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/our_people/delete/'.base64_encode($row['people_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
    
    function countPeoples()
    {
        return $this->db->count_all('peoples');
    }
    
    function getPeoples($limit, $offset)
    {
         // Get a list of all peoples
        $this->db->select("*");
        $this->db->order_by("people_id", "desc");
	$this->db->limit($limit, $offset);
        $query = $this->db->get('peoples');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getPeople($id)
    {
        // Get the people details
        $this->db->select("*");
        $this->db->where("people_id", $id);
        $query = $this->db->get('peoples');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removePeople($id)
    {
        // Delete a peoples
        $this->db->delete('peoples', array('people_id' => $id));
        
    }
    
    function changepeople_status($data, $id) {
        
        $this->db->where('people_id', $id);
        if($this->db->update('peoples', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createPeoples($filename = '')
    {
        // Create the peoples
        $data = array(
            'people_name' => strip_tags($this->input->post('people_name')),
            //'people_status'=>'Enabled',
            'people_photo'=>$filename,
            'people_post' => strip_tags($this->input->post('people_post')),
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('peoples', $data);
    }
    
    function updatePeoples($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'people_name' => strip_tags($this->input->post('people_name')),
                'people_photo'=> $filename,
                'people_post' => strip_tags($this->input->post('people_post')),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'people_name' => strip_tags($this->input->post('people_name')),
                'people_post' => strip_tags($this->input->post('people_post')),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('people_id', $id);
        $this->db->update('peoples', $data);
    }
    
}
?>