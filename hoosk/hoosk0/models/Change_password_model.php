<?php

class Change_password_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    /*     * *************************** */
    /*     * ** Login Change Password Querys ************ */
    /*     * *************************** */

    public function checkCurPass()
    {
        $cpass = $this->input->post('cur_pass');
        $this->db->where('userName', $this->session->userdata('userName'));
        $this->db->where('password', base64_encode($cpass));
        $query = $this->db->get('admin');
        if($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function saveNewPass()
    {
        $npass = $this->input->post('new_pass');
        $data = array('password' => base64_encode($npass));
        $this->db->where('userName', $this->session->userdata('userName'));
        $this->db->update('admin', $data);
        return true;
    }
}
?>