<?php

class Vendor_project_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    /*     * *************************** */
    /*     * ** Project Querys ************ */
    /*     * *************************** */
    
    function is_product_exist_po($pro_id,$po_id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $po_id);
        $this->db->where("product_id", $pro_id);
        $query = $this->db->get('po_products');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    function confirm_update_product($data,$id)
    {
        $this->db->where('po_product_id', $id);
        if($this->db->update('po_products', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function resultperpage()
    {
        $this->db->select("resultperpage");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_project_datatable_data($requestData,$company_id)
    {
         $columns = array( 
             
                0 =>'hoosk_company.company_name',
                1 =>'purchase_order.po_name',
                2 =>'purchase_order.po_number',
                3 =>'hoosk_company.city',
                
            );
        $sql = "SELECT hoosk_company.*,purchase_order.* ";
        $sql.=" FROM  purchase_order join hoosk_company on purchase_order.po_company = hoosk_company.companyId WHERE purchase_order.po_company=$company_id";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT hoosk_company.*,purchase_order.* ";
        $sql.=" FROM  purchase_order join hoosk_company on purchase_order.po_company = hoosk_company.companyId $module WHERE 1=1 AND purchase_order.po_company=$company_id";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( hoosk_company.company_name LIKE '".$requestData['search']['value']."%' ";    
        	
                $sql.=" OR purchase_order.po_number LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR purchase_order.po_status LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR hoosk_company.city LIKE '".$requestData['search']['value']."%' ";
                
        	$sql.=" OR purchase_order.po_name LIKE '".$requestData['search']['value']."%' )";
        }
        
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY purchase_order.po_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        //echo $this->db->last_query();die();
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
//                    $nestedData[] = $row["company_name"];
                    $nestedData[] = $row["po_name"];
                    $nestedData[] = $row["po_number"];
                    $nestedData[] = $row["city"];
                    $statusbutton = '<button style="color:white" data-id="'.$row["po_id"].'" data-val="'.$row["po_status"].'"';
                    if ($row['po_status'] == "Pending") {
                        $statusbutton .= ' class="bg-orange mystatus btn" disabled>Pending</button>';
                    } elseif ($row['po_status'] == "Approved") {
                        $statusbutton .= ' class="bg-green mystatus btn" disabled>Approved</button>';
                    } elseif ($row['po_status'] == "Completed") {
                        $statusbutton .= ' class="mystatus btn bg-blue" disabled>Completed</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn " disabled>Canceled</button>';
                    }
                    //$nestedData[] = $statusbutton;
                    //if($row['po_status'] != "Pending") {
                    if(FALSE) {
                        if($this->Default_vendor_model->check_permission('project_view')) {
                            $nestedData[] = '<td class="td-actions"> <a href="'.BASE_URL.'/vendor/project/view/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> <a href="'.BASE_URL.'/vendor/project/pdf/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-file-pdf-o"> </i></a> </td>';
                        } 
                        else {
                            $nestedData[] = '<td class="td-actions"></td>';
                        }
                    }
                    else {
                        if(($this->Default_vendor_model->check_permission('project_edit')) || ($this->Default_vendor_model->check_permission('project_view')) || ($this->Default_vendor_model->check_permission('project_delete'))) {
                            $listper = '<td class="td-actions">';
                            if($this->Default_vendor_model->check_permission('project_edit')) {
                                $listper .= '<a href="'.BASE_URL.'/vendor/project/edit/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> ';
                            }
                            if($this->Default_vendor_model->check_permission('project_delete')) {
                                $listper .= '<a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/vendor/project/delete/'.base64_encode($row['po_id']).'"><i class="fa fa-remove"> </i></a> ';
                            }
                            if($this->Default_vendor_model->check_permission('project_view')) {
                                $listper .= '<a href="'.BASE_URL.'/vendor/project/view/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> <a href="'.BASE_URL.'/vendor/project/pdf/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-file-pdf-o"> </i></a> ';
                            }
                            if($this->Default_vendor_model->check_permission('order_view')) {
                                $listper .= '<a href="'.BASE_URL.'/vendor/project/order/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><img src="'.BASE_URL.'/images/cart1.png" height="20px" width="20px" /></a> ';
                            }
                            $listper .= '</td>';
                            $nestedData[] = $listper;
                        }
                        //$nestedData[] = '<td class="td-actions"> <a href="'.BASE_URL.'/vendor/project/edit/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/vendor/project/delete/'.base64_encode($row['po_id']).'"><i class="fa fa-remove"> </i></a> <a href="'.BASE_URL.'/vendor/project/view/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> <a href="'.BASE_URL.'/vendor/project/pdf/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa  fa-file-pdf-o"> </i></a> </td>';
                    }
                    
                    $data[] = $nestedData;
                    
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
    
    
    function get_enabled_company()
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("company_status", 'Enabled');
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function get_product($productname,$type)
    {
        
                // Get the user details
                $this->db->select("*");
                if($type != 'desc')
                {
                    //$this->db->where("product_name", $productname);
                    $this->db->like("product_desc", $productname);
                }
                else
                {
                    $this->db->like("product_desc", $productname);
                }
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function  get_company_by_id($companyid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("companyId", $companyid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    function get_product_by_id($productid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("product_id", $productid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    function  confirm_project($data)
    {
        if($this->db->insert('purchase_order', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
        
    }
    
    
    function confirm_product($data)
    {
        if($this->db->insert('po_products', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
    
//    function confirm_update_product($data,$id)
//    {
//        $this->db->where('po_product_id', $id);
//        if($this->db->update('po_products', $data))
//        {
//            return true;
//        }
//        else
//        {
//            return false;
//        }
//    }
    function confirm_update_order($data,$id)
    {
        $this->db->where('po_custom_id', $id);
        if($this->db->update('po_custom', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    function getProjectById($id,$mode = '')
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        
        if($mode != '')
        {
            $this->db->join('hoosk_company', 'hoosk_company.companyId = purchase_order.po_company');
        }
        $query = $this->db->get('purchase_order');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function update_project($data,$id)
    {
        $this->db->where('po_id', $id);
        if($this->db->update('purchase_order', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    
    function  delete_product_related($id)
    {
        $this->db->delete('po_products', array('po_id' => $id));
    }
    
    
    function  delete_custom_related($id)
    {
        $this->db->delete('po_custom', array('po_id' => $id));
    }
    
    
    function confirm_custom_product($data)
    {
        if($this->db->insert('po_custom', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
    
    
    function get_products_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $this->db->order_by("po_products.po_product_id", "asc");
        $this->db->join('products', 'products.product_id = po_products.product_id');
        $query = $this->db->get('po_products');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_custom_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $query = $this->db->get('po_custom');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function removeProject($id)
    {
        $this->db->delete('purchase_order', array('po_id' => $id));
        $this->db->delete('po_products', array('po_id' => $id));
        $this->db->delete('po_custom', array('po_id' => $id));
    }
    
    
    function change_project_status($data, $id) {
        
        $this->db->where('po_id', $id);
        if($this->db->update('purchase_order', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}
    
    
?>