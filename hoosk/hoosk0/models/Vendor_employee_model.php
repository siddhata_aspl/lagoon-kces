<?php

class Vendor_employee_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }   


    /*     * *************************** */
    /*     * ** Employees Querys ************ */
    /*     * *************************** */

    function get_employee_datatable_data($module,$requestData,$company_id)
    {
        $columns = array( 
                0 => 'emp_firstname',
                1 => 'emp_lastname',
                2 => 'emp_email',
        );
        
        // getting total number records without any search
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE com_id=$company_id";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1 AND com_id=$company_id";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( emp_firstname LIKE '".$requestData['search']['value']."%' ";    
        
            $sql.=" OR emp_lastname LIKE '".$requestData['search']['value']."%' ";
            
            $sql.=" OR emp_email LIKE '".$requestData['search']['value']."%' ";
            
            $sql.=" OR emp_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result. 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY emp_id desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */    
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["emp_firstname"];
                    $nestedData[] = $row["emp_lastname"];
                    $nestedData[] = $row["emp_email"];
                    $statusbutton = '<button style="color:white" data-id="'.$row["emp_id"].'" data-val="'.$row["emp_status"].'"';
                    $listper = '';
                    if($this->Default_vendor_model->check_permission('employees_edit')) { 
                        if ($row['emp_status'] == "Disabled") {
                            $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
                        } elseif ($row['emp_status'] == "Enabled") {
                            $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
                        } else {
                            $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
                        }
                    }
                    else {
                        if ($row['emp_status'] == "Disabled") {
                            $statusbutton .= ' class="bg-orange mystatus btn" disabled>Disabled</button>';
                        } elseif ($row['emp_status'] == "Enabled") {
                            $statusbutton .= ' class="bg-green mystatus btn" disabled>Enabled</button>';
                        } else {
                            $statusbutton .= 'class="bg-red mystatus btn" disabled>Deleted</button>';
                        }
                    }
                    
                    $nestedData[] = $statusbutton;
                    //echo $this->Default_vendor_model->check_permission('employees_edit');die();
                    
                    if($this->Default_vendor_model->check_permission('employees_edit')) {
                        $listper = '<td class="td-actions"><a href="'.BASE_URL.'/vendor/employees/edit/'.base64_encode($row['emp_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> </td>';
                    }
                    if($this->Default_vendor_model->check_permission('employees_delete')) {
                        $listper .= '<td class="td-actions"><a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/vendor/employees/delete/'.base64_encode($row['emp_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    }
                    
                    $nestedData[] = $listper;
//                  if($this->Default_vendor_model->check_permission('employees_edit')) {
//                else
 //                {
 //                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/vendor/employees/edit/'.base64_encode($row['emp_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/vendor/employees/delete/'.base64_encode($row['emp_id']).'"><i class="fa fa-remove"> </i></a></td>';
 //                }
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
                        return json_encode($json_data);
        
    }


    function countEmployees(){
        return $this->db->count_all('employees');
    }
    
    function getEmployees($limit, $offset=0) {
        // Get a list of all employee accounts
        $this->db->select("emp_firstname,emp_lastname,emp_email,emp_id,emp_status");
        $this->db->order_by("emp_firstname", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('employees');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function resultperpage()
    {
        $this->db->select("resultperpage");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getEmployee($id) {
        // Get the employee details
        $this->db->select("*");
        $this->db->where("emp_id", $id);
        $query = $this->db->get('employees');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function getEmployeeEmail($id) {
        // Get the employee email address
        $this->db->select("email");
        $this->db->where("emp_id", $id);
        $query = $this->db->get('employees');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $email = $rows->email;
                return $email;
            }
        }
    }
    
    function change_employeestatus($data, $id) {
        
        $this->db->where('emp_id', $id);
        if($this->db->update('employees', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    function change_project_status($data, $id) {
        
        $this->db->where('po_id', $id);
        if($this->db->update('purchase_order', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function createEmployee($filename = '',$cid) {
        
        // Create the employee account
        
        //user checkbox data
        $uadd = $this->input->post('employees_new'); 
        if($uadd == '')
        {
            $employees_new = 'False';
        }
        else
        {
            $employees_new = 'True';
        }
        
        $uedit = $this->input->post('employees_edit');
        if($uedit == '')
        {
            $employees_edit = 'False';
        }
        else
        {
            $employees_edit = 'True';
        }
        
        $uview = $this->input->post('employees_view');
        if($uview == '')
        {
            $employees_view = 'False';
        }
        else
        {
            $employees_view = 'True';
        }
        
        $udelete = $this->input->post('employees_delete');
        if($udelete == '')
        {
            $employees_delete = 'False';
        }
        else
        {
            $employees_delete = 'True';
        }
        
        
        //project checkbox data
        $padd = $this->input->post('project_new'); 
        if($padd == '')
        {
            $project_new = 'False';
        }
        else
        {
            $project_new = 'True';
        }
        
        $pedit = $this->input->post('project_edit');
        if($pedit == '')
        {
            $project_edit = 'False';
        }
        else
        {
            $project_edit = 'True';
        }
        
        $pview = $this->input->post('project_view');
        if($pview == '')
        {
            $project_view = 'False';
            $order_view1 = 'False';
        }
        else
        {
            $project_view = 'True';
            $order_view1 = 'True';
        }
        
        $pdelete = $this->input->post('project_delete');
        if($pdelete == '')
        {
            $project_delete = 'False';
        }
        else
        {
            $project_delete = 'True';
        }
        
        
        //order checkbox data
        $oadd = $this->input->post('order_new'); 
        if($oadd == '')
        {
            $order_new = 'False';
        }
        else
        {
            $order_new = 'True';
        }
        
        $oedit = $this->input->post('order_edit');
        if($oedit == '')
        {
            $order_edit = 'False';
        }
        else
        {
            $order_edit = 'True';
        }
        
        $oview = $this->input->post('order_view');
        if($oview == '')
        {
            $order_view = 'False';
        }
        else
        {
            $order_view = 'True';
        }
        
        $odelete = $this->input->post('order_delete');
        if($odelete == '')
        {
            $order_delete = 'False';
        }
        else
        {
            $order_delete = 'True';
        }
               
        
        $data = array(     
            'com_id' => $cid,
            'emp_firstname' => strip_tags($this->input->post('emp_firstname')),
            'emp_lastname' => strip_tags($this->input->post('emp_lastname')),
            'emp_email' => strip_tags($this->input->post('emp_email')),
            //'password' => md5($this->input->post('password').SALT),
            //'password' => base64_encode($this->input->post('password')),
            'emp_status' => 'Disabled',
            'emp_image' => $filename,
            'employees_new' => $employees_new,
            'employees_edit' => $employees_edit,
            'employees_view' => $employees_view,
            'employees_delete' => $employees_delete,
            'project_new' => $project_new,
            'project_edit' => $project_edit,
            'project_view' => $project_view,
            'project_delete' => $project_delete,
            'order_new' => $order_new,
            'order_edit' => $order_edit,
            'order_view' => $order_view1,
            'order_delete' => $order_delete,
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('employees', $data);
    }

    function updateEmployee($id,$filename = '') {
        
        // Update the employee account
        
        //user checkbox data
        $uadd = $this->input->post('employees_new'); 
        if($uadd == '')
        {
            $employees_new = 'False';
        }
        else
        {
            $employees_new = 'True';
        }
        
        $uedit = $this->input->post('employees_edit');
        if($uedit == '')
        {
            $employees_edit = 'False';
        }
        else
        {
            $employees_edit = 'True';
        }
        
        $uview = $this->input->post('employees_view');
        if($uview == '')
        {
            $employees_view = 'False';
        }
        else
        {
            $employees_view = 'True';
        }
        
        $udelete = $this->input->post('employees_delete');
        if($udelete == '')
        {
            $employees_delete = 'False';
        }
        else
        {
            $employees_delete = 'True';
        }
        
        
        //project checkbox data
        $padd = $this->input->post('project_new'); 
        if($padd == '')
        {
            $project_new = 'False';
        }
        else
        {
            $project_new = 'True';
        }
        
        $pedit = $this->input->post('project_edit');
        if($pedit == '')
        {
            $project_edit = 'False';
        }
        else
        {
            $project_edit = 'True';
        }
        
        $pview = $this->input->post('project_view');
        if($pview == '')
        {
            $project_view = 'False';
            $order_view1 = 'False';
        }
        else
        {
            $project_view = 'True';
            $order_view1 = 'True';
        }
        
        $pdelete = $this->input->post('project_delete');
        if($pdelete == '')
        {
            $project_delete = 'False';
        }
        else
        {
            $project_delete = 'True';
        }
        
        
        //order checkbox data
        $oadd = $this->input->post('order_new'); 
        if($oadd == '')
        {
            $order_new = 'False';
        }
        else
        {
            $order_new = 'True';
        }
        
        $oedit = $this->input->post('order_edit');
        if($oedit == '')
        {
            $order_edit = 'False';
        }
        else
        {
            $order_edit = 'True';
        }
        
        $oview = $this->input->post('order_view');
        if($oview == '')
        {
            $order_view = 'False';
        }
        else
        {
            $order_view = 'True';
        }
        
        $odelete = $this->input->post('order_delete');
        if($odelete == '')
        {
            $order_delete = 'False';
        }
        else
        {
            $order_delete = 'True';
        }
        
        if($filename)
        {
            // update the employee account
            $data = array(
                'emp_firstname' => strip_tags($this->input->post('emp_firstname')),
                'emp_lastname' => strip_tags($this->input->post('emp_lastname')),
                'emp_email' => strip_tags($this->input->post('emp_email')),
                //'password' => base64_encode($this->input->post('password')),
                'emp_image'=> $filename,
                'employees_new' => $employees_new,
                'employees_edit' => $employees_edit,
                'employees_view' => $employees_view,
                'employees_delete' => $employees_delete,
                'project_new' => $project_new,
                'project_edit' => $project_edit,
                'project_view' => $project_view,
                'project_delete' => $project_delete,
                'order_new' => $order_new,
                'order_edit' => $order_edit,
                'order_view' => $order_view1,
                'order_delete' => $order_delete,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the employee account
            $data = array(
                'emp_firstname' => strip_tags($this->input->post('emp_firstname')),
                'emp_lastname' => strip_tags($this->input->post('emp_lastname')),
                'emp_email' => strip_tags($this->input->post('emp_email')),
                //'password' => base64_encode($this->input->post('password')),
                'employees_new' => $employees_new,
                'employees_edit' => $employees_edit,
                'employees_view' => $employees_view,
                'employees_delete' => $employees_delete,
                'project_new' => $project_new,
                'project_edit' => $project_edit,
                'project_view' => $project_view,
                'project_delete' => $project_delete,
                'order_new' => $order_new,
                'order_edit' => $order_edit,
                'order_view' => $order_view1,
                'order_delete' => $order_delete,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('emp_id', $id);
        $this->db->update('employees', $data);
    }

    function removeEmployee($id) {
        // Delete a employee account
        $this->db->delete('employees', array('emp_id' => $id));
    }
    
    
    function update_employee_for_getmail($data, $email)
    {
        $this->db->where('emp_email', $email);
    $this->db->update('employees', $data);
        $this->db->select("*");
        $this->db->where('emp_email', $email);
        $query = $this->db->get('employees');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    
    function getmail_template_for_emp_login($id)
    {
        $this->db->select("*");
        $this->db->where("email_template_id", $id);
        $query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

}