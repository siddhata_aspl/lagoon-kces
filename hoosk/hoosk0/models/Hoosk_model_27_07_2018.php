<?php

class Hoosk_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
	
	/*     * *************************** */
    /*     * ** Dash Querys ************ */
    /*     * *************************** */
	function getSiteName() {
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteTitle'];			
			endforeach; 
		}
        return array();
    }
    function getSiteFooterLine() {
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteFooter'];			
			endforeach; 
		}
        return array();
    }
    function get_banner_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'banner_title'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( banner_title LIKE '".$requestData['search']['value']."%' ";    
        	
        	$sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY banner_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["banner_title"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/banners/'.$row['banner_image'].'" width="100px" height="100px" >';
                    $statusbutton = '<button style="color:white" data-id="'.$row["banner_id"].'" data-val="'.$row["banner_status"].'"';
                    if ($row['banner_status'] == "Disabled") {
                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
                    } elseif ($row['banner_status'] == "Enabled") {
                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
                    }
                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/banners/edit/'.base64_encode($row['banner_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/banners/delete/'.base64_encode($row['banner_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
    
    function get_product($productname)
    {
                // Get the user details
                $this->db->select("*");
                //$this->db->where("product_name", $productname);
                $this->db->like("product_name", $productname);
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    function get_enabled_company()
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("company_status", 'Enabled');
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    function  get_company_by_id($companyid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("companyId", $companyid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    function get_product_by_id($productid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("product_id", $productid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
            
    function get_project_datatable_data($requestData)
    {
        
         $columns = array( 
             
                0 =>'hoosk_company.company_name',
                1 =>'purchase_order.po_name',
                2 =>'purchase_order.po_number',
                3 =>'hoosk_company.city',
                
            );
        $sql = "SELECT hoosk_company.*,purchase_order.* ";
        $sql.=" FROM  purchase_order join hoosk_company on purchase_order.po_company = hoosk_company.companyId";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT hoosk_company.*,purchase_order.* ";
        $sql.=" FROM  purchase_order join hoosk_company on purchase_order.po_company = hoosk_company.companyId $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( hoosk_company.company_name LIKE '".$requestData['search']['value']."%' ";    
        	
                $sql.=" OR purchase_order.po_number LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR purchase_order.po_status LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR hoosk_company.city LIKE '".$requestData['search']['value']."%' ";
                
        	$sql.=" OR purchase_order.po_name LIKE '".$requestData['search']['value']."%' )";
        }
        
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY purchase_order.po_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        $query=$this->db->query($sql);
        // echo $this->db->last_query();
        // exit();
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                    
                    $nestedData=array();
                    $nestedData[] = $row["company_name"];
                    $nestedData[] = $row["po_name"];
                    $nestedData[] = $row["po_number"];
                    $nestedData[] = $row["city"];
                    $statusbutton = '<button style="color:white" data-id="'.$row["po_id"].'" data-val="'.$row["po_status"].'"';
                    if ($row['po_status'] == "Pending") {
                        $statusbutton .= ' class="bg-orange mystatus btn">Pending</button>';
                    } elseif ($row['po_status'] == "Approved") {
                        $statusbutton .= ' class="bg-green mystatus btn">Approved</button>';
                    } elseif ($row['po_status'] == "Completed") {
                        $statusbutton .= ' class="mystatus btn bg-blue">Completed</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn ">Canceled</button>';
                    }
                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/project/edit/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/project/delete/'.base64_encode($row['po_id']).'"><i class="fa fa-remove"> </i></a> <a href="'.BASE_URL.'/admin/project/view/'.base64_encode($row['po_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> </td>';
                    $data[] = $nestedData;
                    
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
    function get_company_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'company_name',
                1=>'first_name',
                2=>'email',
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( company_name LIKE '".$requestData['search']['value']."%' ";    
        	
        	$sql.=" OR first_name LIKE '".$requestData['search']['value']."%' ";
        	
        	$sql.=" OR company_status LIKE '".$requestData['search']['value']."%' ";
        	
        	$sql.=" OR email LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY companyId  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["company_name"];
                    $nestedData[] = $row["first_name"];
                    $nestedData[] = $row["email"];
                    $statusbutton = '<button style="color:white" data-id="'.$row["companyId"].'" data-val="'.$row["company_status"].'"';
                    if ($row['company_status'] == "Disabled") {
                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
                    } elseif ($row['company_status'] == "Enabled") {
                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
                    }
                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/company/edit/'.base64_encode($row['companyId']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/company/delete/'.base64_encode($row['companyId']).'"><i class="fa fa-remove"> </i></a> <a href="'.BASE_URL.'/admin/company/view/'.base64_encode($row['companyId']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
	function checkMaintenance(){
		$this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteMaintenance'];			
			endforeach; 
		}
        return array();
	}
	function get_datatable_data($module,$requestData)
    {
        $columns = array( 
                0 =>'userName', 
                1 => 'User_firstname',
                2=> 'User_lastname',
                3=>'email',
        );
        // $data = array();
        // $totalData = $this->db->count_all($module);
        // $totalFiltered = $this->db->count_all($module);
        // $this->db->select('*');
        // if( !empty($requestData['search']['value']) ) {
            
        //     $this->db->like("userName", $requestData['search']['value']);
        //     $this->db->or_like("User_firstname", $requestData['search']['value']);
        //     $this->db->or_like("User_lastname", $requestData['search']['value']);
        //     $this->db->or_like("email", $requestData['search']['value']);
        //     $this->db->or_like("user_status", $requestData['search']['value']);
        // }
        // $this->db->order_by($columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir']);
        // $this->db->limit($requestData['length'], $requestData['start']);
        // $query = $this->db->get($module);
        // $totalFiltered = $query->num_rows();
        // $rows = $query->result_array();
        // if ($query->num_rows() > 0) {
        //     foreach($rows as $row) {  
                
        //             $nestedData=array();
        //             $nestedData[] = $row["userName"];
        //             $nestedData[] = $row["User_firstname"];
        //             $nestedData[] = $row["User_lastname"];
        //             $nestedData[] = $row["email"];
        //             $statusbutton = '<button style="color:white" data-id="'.$row["userID"].'" data-val="'.$row["user_status"].'"';
        //             if ($row['user_status'] == "Disabled") {
        //                 $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
        //             } elseif ($row['user_status'] == "Enabled") {
        //                 $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
        //             } else {
        //                 $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
        //             }
        //             $nestedData[] = $statusbutton;
        //             $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/users/edit/'.base64_encode($row['userID']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/users/delete/'.base64_encode($row['userID']).'"><i class="fa fa-remove"> </i></a></td>';
        //             $data[] = $nestedData;
        //     }
            
        // }
        
        // getting total number records without any search
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( userName LIKE '".$requestData['search']['value']."%' ";    
        	
        	$sql.=" OR User_firstname LIKE '".$requestData['search']['value']."%' ";
        
        	$sql.=" OR User_lastname LIKE '".$requestData['search']['value']."%' ";
        	
        	$sql.=" OR email LIKE '".$requestData['search']['value']."%' ";
        	
        	$sql.=" OR user_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result. 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY userID  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.="  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["userName"];
                    $nestedData[] = $row["User_firstname"];
                    $nestedData[] = $row["User_lastname"];
                    $nestedData[] = $row["email"];
                    $statusbutton = '<button style="color:white" data-id="'.$row["userID"].'" data-val="'.$row["user_status"].'"';
                    if ($row['user_status'] == "Disabled") {
                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
                    } elseif ($row['user_status'] == "Enabled") {
                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
                    }
                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/users/edit/'.base64_encode($row['userID']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/users/delete/'.base64_encode($row['userID']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
                        return json_encode($json_data);
        
    }
	function getTheme() {
        // Get Theme
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteTheme'];			
			endforeach; 
		}
        return array();
    }
	function getLang() {
        // Get Theme
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteLang'];			
			endforeach; 
		}
        return array();
    }
    function getUpdatedPages() {
        // Get most recently updated pages
        $this->db->select("pageTitle, hoosk_page_attributes.pageID, pageUpdated, pageContentHTML");
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
		$this->db->order_by("pageUpdated", "desc");
		$this->db->limit(5);
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    /*     * *************************** */
    /*     * ** Company Querys ************ */
    /*     * *************************** */

    function countCompany()
    {
        return $this->db->count_all('hoosk_company');
    }

    function getCompany($limit, $offset = 0)
    {
        // Get a list of all company accounts
        $this->db->select("*");
        $this->db->order_by("companyId", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_company');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }

    function change_status_company($data, $id)
    {

        $this->db->where('companyID', $id);
        if ($this->db->update('hoosk_company', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function createCompany($filename = '')
    {
        // Create the company account
        $data = array(
            'company_name' => $this->input->post('companyname'),
            'first_name' => $this->input->post('firstname'),
            'last_name' => $this->input->post('lastname'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'address' => $this->input->post('address'),
            'state' => $this->input->post('state'),
            'city' => $this->input->post('city'),
            'zipcode' => $this->input->post('zipcode'),
            'company_status' => 'Disabled',
            'company_img' => $filename,
            'created_date' => date("Y-m-d H:i:s"),
            'modified_date' => date("Y-m-d H:i:s"),
        );
//        echo "<pre>";print_r($data);die;
        $this->db->insert('hoosk_company', $data);
    }
    
    function  confirm_project($data)
    {
        if($this->db->insert('purchase_order', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
        
    }
    
    function confirm_product($data)
    {
        if($this->db->insert('po_products', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
    function confirm_custom_product($data)
    {
        if($this->db->insert('po_custom', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
            
    function update_company_for_forgotmail($data, $email)
    {
        $this->db->where('email', $email);
        $this->db->update('hoosk_company', $data);
        $this->db->select("*");
        $this->db->where('email', $email);
        $query = $this->db->get('hoosk_company');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }

    function removeCompany($id)
    {
        // Delete a company account
        $this->db->delete('hoosk_company', array('companyID' => $id));
    }

    function getCompanyById($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("companyId", $id);
        $query = $this->db->get('hoosk_company');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    function getProjectById($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $query = $this->db->get('purchase_order');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    function get_products_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $this->db->join('products', 'products.product_id = po_products.product_id');
        $query = $this->db->get('po_products');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    function get_custom_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $query = $this->db->get('po_custom');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    function update_project($data,$id)
    {
        $this->db->where('po_id', $id);
        if($this->db->update('purchase_order', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    function  delete_product_related($id)
    {
        $this->db->delete('po_products', array('po_id' => $id));
    }
    function  delete_custom_related($id)
    {
        $this->db->delete('po_custom', array('po_id' => $id));
    }
    function updateCompany($id, $filename = '')
    {
        if ($filename)
        {
            $data = array(
                'company_name' => $this->input->post('companyname'),
                'first_name' => $this->input->post('firstname'),
                'last_name' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'zipcode' => $this->input->post('zipcode'),
                'company_img' => $filename,
                'modified_date' => date("Y-m-d H:i:s"),
            );
        }
        else
        {
            $data = array(
                'company_name' => $this->input->post('companyname'),
                'first_name' => $this->input->post('firstname'),
                'last_name' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'state' => $this->input->post('state'),
                'city' => $this->input->post('city'),
                'zipcode' => $this->input->post('zipcode'),
                'modified_date' => date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('companyId', $id);
        $this->db->update('hoosk_company', $data);
    }
    
    /*     * *************************** */
    /*     * ** Banners Querys ************ */
    /*     * *************************** */
    function countBanners()
    {
        return $this->db->count_all('banners');
    }
    function getBanners($limit, $offset)
    {
         // Get a list of all banners
        $this->db->select("*");
        $this->db->order_by("banner_id", "desc");
	$this->db->limit($limit, $offset);
        $query = $this->db->get('banners');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function getBanner($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("banner_id", $id);
        $query = $this->db->get('banners');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function removeBanners($id)
    {
        // Delete a banners
        $this->db->delete('banners', array('banner_id' => $id));
        
    }
    function changebanner_status($data, $id) {
        
        $this->db->where('banner_id', $id);
        if($this->db->update('banners', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    function  createBanners($filename = '')
    {
        // Create the banners
        $data = array(
            'banner_title' => $this->input->post('title'),
            'banner_status'=>'Enabled',
            'banner_image'=>$filename,
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('banners', $data);
    }
    function updateBanner($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'banner_title' => $this->input->post('title'),
                'banner_image'=> $filename,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'banner_title' => $this->input->post('title'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('banner_id', $id);
        $this->db->update('banners', $data);
    }

    /*     * *************************** */
    /*     * ** User Querys ************ */
    /*     * *************************** */
	function countUsers(){
        return $this->db->count_all('hoosk_user');
   	}
	
    function getUsers($limit, $offset=0) {
        // Get a list of all user accounts
        $this->db->select("User_firstname,User_lastname,userName, email, userID,user_status");
        $this->db->order_by("userName", "desc");
	$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function resultperpage()
    {
        $this->db->select("resultperpage");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function getUser($id) {
        // Get the user details
        $this->db->select("*");
        $this->db->where("userID", $id);
        $query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function getUserEmail($id) {
        // Get the user email address
        $this->db->select("email");
        $this->db->where("userID", $id);
        $query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $email = $rows->email;
                return $email;
            }
        }
    }
    function change_status($data, $id) {
        
        $this->db->where('userID', $id);
        if($this->db->update('hoosk_user', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    function change_project_status($data, $id) {
        
        $this->db->where('po_id', $id);
        if($this->db->update('purchase_order', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    function createUser($filename = '') {
        // Create the user account
        $data = array(
            'User_firstname' => $this->input->post('firstname'),
            'User_lastname' => $this->input->post('lastname'),
            'userName' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            //'password' => md5($this->input->post('password').SALT),
            //'password' => base64_encode($this->input->post('password')),
            'user_status'=>'Disabled',
            'user_img'=>$filename,
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('hoosk_user', $data);
    }

    function updateUser($id,$filename = '') {
        if($filename)
        {
            // update the user account
            $data = array(
                'User_firstname' => $this->input->post('firstname'),
                'User_lastname' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                //'password' => base64_encode($this->input->post('password')),
                'user_img'=> $filename,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'User_firstname' => $this->input->post('firstname'),
                'User_lastname' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                //'password' => base64_encode($this->input->post('password')),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('userID', $id);
        $this->db->update('hoosk_user', $data);
    }

    function removeUser($id) {
        // Delete a user account
        $this->db->delete('hoosk_user', array('userID' => $id));
    }
        
    function getmail_template($id)
    {
        $this->db->select("*");
        $this->db->where("email_template_id", $id);
       	$query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
            
    function update_user_for_forgotmail($data, $email)
    {
        $this->db->where('email', $email);
	$this->db->update('hoosk_user', $data);
        $this->db->select("*");
        $this->db->where('email', $email);
       	$query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function login($username, $password) {
        $this->db->select("*");
        $this->db->where("userName", $username);
        $this->db->where("password", $password);
        $query = $this->db->get("hoosk_user");
        //print_r($this->db->last_query());        exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $data = array(
                    'userID' => $rows->userID,
                    'userName' => $rows->userName,
                    'logged_in' => TRUE,
                );
                $this->session->sess_expiration = '14400';
                $this->session->set_userdata('userID', $rows->userID);
                $this->session->set_userdata('userName', $rows->userName);
                $this->session->set_userdata('logged_in', TRUE);
                $this->session->set_userdata($data);
                return true;
            }
        } else {
            return false;
        }
    }

    // /*     * *************************** */
    // /*     * ** Login Change Password Querys ************ */
    // /*     * *************************** */

    // public function checkCurPass()
    // {
    //     $cpass = $this->input->post('cur_pass');
    //     $this->db->where('userName', $this->session->userdata('userName'));
    //     $this->db->where('password', base64_encode($cpass));
    //     $query = $this->db->get('hoosk_user');
    //     if($query->num_rows() > 0)
    //         return 1;
    //     else
    //         return 0;
    // }

    // public function saveNewPass()
    // {
    //     $npass = $this->input->post('new_pass');
    //     $data = array('password' => base64_encode($npass));
    //     $this->db->where('userName', $this->session->userdata('userName'));
    //     $this->db->update('hoosk_user', $data);
    //     return true;
    // }

    /*     * *************************** */
    /*     * ** Page Querys ************ */
    /*     * *************************** */
	function pageSearch($term){
		$this->db->select("*");
		$this->db->like("pageTitle", $term);
		$this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
		$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_page_attributes');
		if($term==""){
			$this->db->limit(15);
		}
		if ($query->num_rows() > 0) {
			$results = $query->result_array();
			foreach ($results as $p): 
				echo '<tr>';
					echo '<td>'.$p['navTitle'].'</td>';
					echo '<td>'.$p['pageUpdated'].'</td>';
					echo '<td>'.$p['pageCreated'].'</td>';
					echo '<td>'.($p['pagePublished'] ? '<span class="fa fa-2x fa-check-circle"></span>' : '<span class="fa fa-2x fa-times-circle"></span>').'</td>';
					echo '<td class="td-actions"><a href="'.BASE_URL.'/admin/pages/jumbo/'.$p['pageID'].'" class="btn btn-small btn-primary">'.$this->lang->line('btn_jumbotron').'</a> <a href="'.BASE_URL.'/admin/pages/edit/'.$p['pageID'].'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/pages/delete/'.$p['pageID'].'"><i class="fa fa-remove"> </i></a></td>';
				echo '</tr>';		
			endforeach; 
		} else {
			echo "<tr><td colspan='5'><p>".$this->lang->line('no_results')."</p></td></tr>";
		}
	}
	function countPages(){
        return $this->db->count_all('hoosk_page_attributes');
    }
    function getPages($limit, $offset=0) {  
        // Get a list of all pages
        $this->db->select("*");
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
		$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function getPagesAll() {  
        // Get a list of all pages
        $this->db->select("*");
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function createPage() {
        // Create the page
        $data = array(
            'pagePublished' => $this->input->post('pagePublished'),
			'pageTemplate' => $this->input->post('pageTemplate'),
            'pageURL' => $this->input->post('pageURL'),
        );
        $this->db->insert('hoosk_page_attributes', $data);
		if ($this->input->post('content') != ""){
        $sirTrevorInput = $this->input->post('content');
        $converter = new Converter();
        $HTMLContent = $converter->toHtml($sirTrevorInput);} else {
		$HTMLContent = "";	
		}

        $this->db->select("*");
        $this->db->where("pageURL", $this->input->post('pageURL'));
        $query = $this->db->get("hoosk_page_attributes");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $contentdata = array(
                    'pageID' => $rows->pageID,
                    'pageTitle' => $this->input->post('pageTitle'),
            		'navTitle' => $this->input->post('navTitle'),
                    'pageContent' => $this->input->post('content'),
                    'pageContentHTML' => $HTMLContent,
                );
                $this->db->insert('hoosk_page_content', $contentdata);
                $metadata = array(
                    'pageID' => $rows->pageID,
                    'pageKeywords' => $this->input->post('pageKeywords'),
                    'pageDescription' => $this->input->post('pageDescription'),
                );
                $this->db->insert('hoosk_page_meta', $metadata);
            }
        }
    }

    function getPage($id) {
        // Get the page details
        $this->db->select("*");
        $this->db->where("hoosk_page_attributes.pageID", $id);
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
		$query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

	function getPageBanners($id) {
        // Get the page banners
        $this->db->select("*");
        $this->db->where("pageID", $id);
        $this->db->order_by("slideOrder ASC");
       	$query = $this->db->get('hoosk_banner');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	
    function removePage($id) {
        // Delete a page
        $this->db->delete('hoosk_page_content', array('pageID' => $id));
        $this->db->delete('hoosk_page_meta', array('pageID' => $id));
        $this->db->delete('hoosk_page_attributes', array('pageID' => $id));
    }

    function getPageURL($id) {
        // Get the page URL
        $this->db->select("pageURL");
        $this->db->where("pageID", $id);
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $pageURL = $rows->pageURL;
                return $pageURL;
            }
        }
    }

    function updatePage($id) {
        // Update the page

       	if ($this->input->post('content') != ""){
        $sirTrevorInput = $this->input->post('content');
        $converter = new Converter();
        $HTMLContent = $converter->toHtml($sirTrevorInput);} else {
		$HTMLContent = "";	
		}
		
		if ($id != 1){
        	$data = array(
            'pagePublished' => $this->input->post('pagePublished'),
            'pageURL' => $this->input->post('pageURL'),
			'pageTemplate' => $this->input->post('pageTemplate'),
        );
		} else {
			$data = array(
            'pagePublished' => $this->input->post('pagePublished'),
			'pageTemplate' => $this->input->post('pageTemplate'),
       		);			
		}
        $this->db->where("pageID", $id);
        $this->db->update('hoosk_page_attributes', $data);
        $contentdata = array(
            'pageTitle' => $this->input->post('pageTitle'),
            'navTitle' => $this->input->post('navTitle'),
            'pageContent' => $this->input->post('content'),
            'pageContentHTML' => $HTMLContent,
        );
        $this->db->where("pageID", $id);
        $this->db->update('hoosk_page_content', $contentdata);
        $metadata = array(
            'pageKeywords' => $this->input->post('pageKeywords'),
            'pageDescription' => $this->input->post('pageDescription'),
        );
        $this->db->where("pageID", $id);
        $this->db->update('hoosk_page_meta', $metadata);
    }

	 function updateJumbotron($id) {
        // Update the jumbotron
		if ($this->input->post('jumbotron') != ""){
        $sirTrevorInput = $this->input->post('jumbotron');
        $converter = new Converter();
        $HTMLContent = $converter->toHtml($sirTrevorInput);} else {
		$HTMLContent = "";	
		}
		$data = array(
		'enableJumbotron' => $this->input->post('enableJumbotron'),
		'enableSlider' => $this->input->post('enableSlider'),
       	);			
		
        $this->db->where("pageID", $id);
        $this->db->update('hoosk_page_attributes', $data);
        $contentdata = array(
			'jumbotron' => $this->input->post('jumbotron'),
			'jumbotronHTML' => $HTMLContent,
        );
        $this->db->where("pageID", $id);
        $this->db->update('hoosk_page_content', $contentdata);
       
	  	// Clear the sliders
		$this->db->delete('hoosk_banner', array('pageID' => $id));

		/*for($i=0;$i<=$_POST['total_upload_pics'];$i++)
		{
			if(isset($_POST['slide' . $i]))
			{
				$slidedata = array(
				'pageID' => $id,
				'slideImage' => $this->input->post('slide'.$i),
				'slideLink' => $this->input->post('link'.$i),
				'slideOrder' => $i,
				);
                $this->db->insert('hoosk_banner', $slidedata));
			}
		}*/
		
		$sliders = explode('{', $this->input->post('pics'));
		
		for($i=1;$i<count($sliders);$i++)
		{
			$div = explode('|', $sliders[$i]);
			
			$slidedata = array(
				'pageID' => $id,
				'slideImage' => $div[0],
				'slideLink' => $div[1],
				'slideAlt' => substr($div[2],0,-1),
				'slideOrder' => $i-1,
			);
			
			$this->db->insert('hoosk_banner', $slidedata);
		}
    }
	
    /*     * *************************** */
    /*     * ** Navigation Querys ****** */
    /*     * *************************** */
	function countNavigation(){
        return $this->db->count_all('hoosk_navigation');
    }	

    function getAllNav($limit, $offset=0) {
        // Get a list of all pages
        $this->db->select("*");
		$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_navigation');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	
    function getNav($id) {
        // Get a list of all pages
        $this->db->select("*");
		$this->db->where("navSlug", $id);
        $query = $this->db->get('hoosk_navigation');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }	
    //Get page details for building nav
    function getPageNav($url) {
        // Get the page details
        $this->db->select("*");
        $this->db->where("hoosk_page_attributes.pageURL", $url);
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	
	function insertNav() {
		$navigationHTML = $this->input->post('convertedNav');
		$navigationHTML = str_replace("<ul></ul>", "", $navigationHTML);
		$navigationEdit = $this->input->post('seriaNav');
		$navigationEdit = str_replace('<button data-action="collapse" type="button">Collapse</button><button style="display: none;" data-action="expand" type="button">Expand</button>', "", $navigationEdit);
		
        $data = array(
            'navSlug' => $this->input->post('navSlug'),
            'navTitle' => $this->input->post('navTitle'),
            'navEdit' => $navigationEdit,
            'navHTML' => $navigationHTML,
        );
        $this->db->insert('hoosk_navigation', $data);
	}
	
	function updateNav($id) {
		$navigationHTML = $this->input->post('convertedNav');
		$navigationHTML = str_replace("<ul></ul>", "", $navigationHTML);
		$navigationEdit = $this->input->post('seriaNav');
		$navigationEdit = str_replace('<button data-action="collapse" type="button">Collapse</button><button style="display: none;" data-action="expand" type="button">Expand</button>', "", $navigationEdit);
		
        $data = array(
            'navTitle' => $this->input->post('navTitle'),
            'navEdit' => $navigationEdit,
            'navHTML' => $navigationHTML,
        );
		$this->db->where("navSlug", $id);
        $this->db->update('hoosk_navigation', $data);
	}
	
	function removeNav($id) {
        // Delete a nav
        $this->db->delete('hoosk_navigation', array('navSlug' => $id));
    }
	
	
	
	function getSettings() {
        // Get the settings
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	
	
	function updateSettings() {
		$data = array(
			'siteTheme' => 'dark',
			'siteLang' => $this->input->post('siteLang'),
			'LogoBottomTitle' => $this->input->post('LogoBottomTitle'),
			'siteFooter' => $this->input->post('siteFooter'),
			'siteFooter2' => $this->input->post('siteFooter2'),
            'add1_title' => $this->input->post('add1_title'),
            'add1' => $this->input->post('add1'),
            'add1_phone' => $this->input->post('add1_phone'),
            'add1_fax' => $this->input->post('add1_fax'),
            'add1_hours' => $this->input->post('add1_hours'),
            'add1_map' => $this->input->post('add1_map'),
            'add2_title' => $this->input->post('add2_title'),
            'add2' => $this->input->post('add2'),
            'add2_phone' => $this->input->post('add2_phone'),
            'add2_fax' => $this->input->post('add2_fax'),
            'add2_hours' => $this->input->post('add2_hours'),
            'add2_map' => $this->input->post('add2_map'),
			'siteMaintenance' => $this->input->post('siteMaintenance'),
			'siteMaintenanceHeading' => $this->input->post('siteMaintenanceHeading'),
			'siteMaintenanceMeta' => $this->input->post('siteMaintenanceMeta'),
			'siteMaintenanceContent' => $this->input->post('siteMaintenanceContent'),
			'siteAdditionalJS' => $this->input->post('siteAdditionalJS')
		);
		
		if ($this->input->post('siteTitle') != "") {
			 $data['siteTitle'] = $this->input->post('siteTitle');
		}
		if ($this->input->post('siteLogo') != ""){
			$data['siteLogo'] = $this->input->post('siteLogo');
		}
		if ($this->input->post('siteFavicon') != ""){
			$data['siteFavicon'] = $this->input->post('siteFavicon');
		}
		if ($this->input->post('siteFooterLogo') != ""){
			$data['siteFooterLogo'] = $this->input->post('siteFooterLogo');
		}
		$this->db->where("siteID", 0);
		$this->db->update('hoosk_settings', $data);
	}
	
	
	/*     * *************************** */
    /*     * ** Post Querys ************ */
    /*     * *************************** */
		function postSearch($term){
			$this->db->select("*");
			$this->db->like("postTitle", $term);
			$this->db->join('hoosk_post_category', 'hoosk_post_category.categoryID = hoosk_post.categoryID');
			$this->db->order_by("unixStamp", "desc");
			if($term==""){
				$this->db->limit(15);
			}
			$query = $this->db->get('hoosk_post');
			if ($query->num_rows() > 0) {
				$results = $query->result_array();
				foreach ($results as $p): 
					echo '<tr>';
						echo '<td>'.$p['postTitle'].'</td>';
						echo '<td>'.$p['categoryTitle'].'</td>';
						echo '<td>'.$p['datePosted'].'</td>';
						echo '<td>'.($p['published'] ? '<span class="fa fa-2x fa-check-circle"></span>' : '<span class="fa fa-2x fa-times-circle"></span>').'</td>';
						echo '<td class="td-actions"><a href="'.BASE_URL.'/admin/posts/edit/'.$p['postID'].'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/posts/delete/'.$p['postID'].'"><i class="fa fa-remove"> </i></a></td>';
					echo '</tr>';		
				endforeach; 
			} else {
				echo "<tr><td colspan='5'><p>".$this->lang->line('no_results')."</p></td></tr>";
			}
		}
		function countPosts(){
			return $this->db->count_all('hoosk_post');
		}    
		function getPosts($limit, $offset=0) { 	
        // Get a list of all posts
        $this->db->select("*");
        $this->db->join('hoosk_post_category', 'hoosk_post_category.categoryID = hoosk_post.categoryID');
		$this->db->order_by("unixStamp", "desc");
		$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_post');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function createPost() {
        // Create the post
		if ($this->input->post('content') != ""){
        $sirTrevorInput = $this->input->post('content');
        $converter = new Converter();
        $HTMLContent = $converter->toHtml($sirTrevorInput);} else {
		$HTMLContent = "";	
		}
        $data = array(
		    'postTitle' => $this->input->post('postTitle'),
			'categoryID' => $this->input->post('categoryID'),
            'postURL' => $this->input->post('postURL'),
			'postContent' => $this->input->post('content'),
            'postContentHTML' => $HTMLContent,
			'postExcerpt' => $this->input->post('postExcerpt'),
            'published' => $this->input->post('published'),
			'datePosted' => $this->input->post('datePosted'),
			'unixStamp' => $this->input->post('unixStamp'),
        );
		if ($this->input->post('postImage') != ""){
				$data['postImage'] = $this->input->post('postImage');
		}	
        $this->db->insert('hoosk_post', $data);
    }

    function getPost($id) {
        // Get the post details
        $this->db->select("*");
        $this->db->where("postID", $id);
        $this->db->join('hoosk_post_category', 'hoosk_post_category.categoryID = hoosk_post.categoryID');
        $query = $this->db->get('hoosk_post');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function removePost($id) {
        // Delete a post
        $this->db->delete('hoosk_post', array('postID' => $id));
    }

    function updatePost($id) {
        // Update the post

       	if ($this->input->post('content') != ""){
        $sirTrevorInput = $this->input->post('content');
        $converter = new Converter();
        $HTMLContent = $converter->toHtml($sirTrevorInput);} else {
		$HTMLContent = "";	
		}
	 	$data = array(
		    'postTitle' => $this->input->post('postTitle'),
			'categoryID' => $this->input->post('categoryID'),
            'postURL' => $this->input->post('postURL'),
			'postContent' => $this->input->post('content'),
            'postContentHTML' => $HTMLContent,
			'postExcerpt' => $this->input->post('postExcerpt'),
            'published' => $this->input->post('published'),
			'datePosted' => $this->input->post('datePosted'),
			'unixStamp' => $this->input->post('unixStamp'),
        );
		if ($this->input->post('postImage') != ""){
				$data['postImage'] = $this->input->post('postImage');
		}	
		$this->db->where("postID", $id);
        $this->db->update('hoosk_post', $data);
    }
	
	
	/*     * *************************** */
    /*     * ** Category Querys ******** */
    /*     * *************************** */
		function countCategories(){
        return $this->db->count_all('hoosk_post_category');
   		 }
	    function getCategories() {
        // Get a list of all categories
        $this->db->select("*");
        $query = $this->db->get('hoosk_post_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	
		
		 function getCategoriesAll($limit, $offset=0) {
        // Get a list of all categories
        $this->db->select("*");
		$this->db->limit($limit, $offset);
        $query = $this->db->get('hoosk_post_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
	function createCategory() {
        // Create the category

        $data = array(
		    'categoryTitle' => $this->input->post('categoryTitle'),
			'categorySlug' => $this->input->post('categorySlug'),
            'categoryDescription' => $this->input->post('categoryDescription')
        );

        $this->db->insert('hoosk_post_category', $data);
    }

    function getCategory($id) {
        // Get the category details
        $this->db->select("*");
        $this->db->where("categoryID", $id);
        $query = $this->db->get('hoosk_post_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function removeCategory($id) {
        // Delete a category
        $this->db->delete('hoosk_post_category', array('categoryID' => $id));
    }

    function updateCategory($id) {
        // Update the category
	 	$data = array(
		    'categoryTitle' => $this->input->post('categoryTitle'),
			'categorySlug' => $this->input->post('categorySlug'),
            'categoryDescription' => $this->input->post('categoryDescription')
        );

		$this->db->where("categoryID", $id);
        $this->db->update('hoosk_post_category', $data);
    }
	
	/*     * *************************** */
    /*     * ** Social Querys ********** */
    /*     * *************************** */
	
	function getSocial(){
		$this->db->select("*");
        $query = $this->db->get('hoosk_social');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
	}
	
	
	function updateSocial() {
		$this->db->select("*");
        $query = $this->db->get("hoosk_social");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
				$data = array();
				$data['socialLink'] = $this->input->post($rows->socialName);
				if (isset($_POST['checkbox'.$rows->socialName])){
					$data['socialEnabled'] = $this->input->post('checkbox'.$rows->socialName);
				} else {
					$data['socialEnabled'] = 0;
				}
				$this->db->where("socialName", $rows->socialName);
        		$this->db->update('hoosk_social', $data);
			}
		}
	}
}
?>