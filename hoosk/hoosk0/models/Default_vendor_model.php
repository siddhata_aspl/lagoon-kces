<?php

class Default_vendor_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper(array('url'));
    }


    function send_mail($to, $from, $subject, $from_name, $message, $filename = '', $file_to_attach = '') {
        
//        $filename = __dir__.'../upload/images/'.$file_name;
        $dstr = 'DOMAIN';
        $domain = constant($dstr);
        $dapi = 'API_KEY';
        $api_key = constant($dapi);
        $url = "https://api.mailgun.net/v3/$domain/messages";

        $frommail = 'FROM_EMAIL';
        $from = constant($frommail);
        $to = $to;
//        $to = 'siddhata@ashapurasoftech.com';
        $message = strip_tags($message);
        $message = html_entity_decode($message);
        if ($filename != '') {
            $parameters = array('from' => $from,
                'to' => $to,
                'subject' => $subject,
                'text' => $message,
                'attachment[0]' => curl_file_create($filename, 'application/pdf', $filename)
            );
        } else {
            $parameters = array('from' => $from,
                'to' => $to,
                'subject' => $subject,
                'text' => $message,
            );
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, "api:$api_key");

        curl_setopt($ch, CURLOPT_POST, true);
        if ($filename != '') {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $y = curl_exec($ch);
        curl_close($ch);
        return true;
    }
	
    /*     * *************************** */
    /*     * ** Permission Querys ************ */
    /*     * *************************** */
    
    
    function check_permission($permission_menu) {
      
        if($this->session->userdata('is_master') == FALSE)
        {
            $emp_id = $this->session->userdata('emp_id');   
            $this->db->select($permission_menu);
            $this->db->where("emp_id", $emp_id);
            $query = $this->db->get("employees");
            if ($query->num_rows() > 0) {
                
                $results = $query->result_array();

                foreach ($results as $u) {
                    if($u[$permission_menu] == 'True')
                    {			             
                        return true;   
                    } 
                    else 
                    { //echo "hiii";die();
                        return false;
                    }
                }
            }
        }
        else
        {
            return true;   
        }
    }
    
    
    function check_url_permission($permission_menu) {
        
        if($this->session->userdata('is_master') == FALSE)
        {
            $emp_id = $this->session->userdata('emp_id');   
            $this->db->select($permission_menu);
            $this->db->where("emp_id", $emp_id);
            $query = $this->db->get("employees");
            if ($query->num_rows() > 0) {
                
                $results = $query->result_array();

                foreach ($results as $u) {
                    if($u[$permission_menu] == 'True')
                    {			             
                        return true;   
                    } 
                    else 
                    { //echo "hiii";die();
                        return false;
                    }
                }
            }
        }
        else
        {
            return true;   
        }
        
    }
    
}
