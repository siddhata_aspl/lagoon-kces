<?php

class Our_career_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    /*     * *************************** */
    /*     * ** Our career Querys ************ */
    /*     * *************************** */
    
    function get_career_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'career_title',
                1=>'career_desc',
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( career_title LIKE '".$requestData['search']['value']."%' ) ";    
            
//          $sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY career_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $extra = strlen($row["career_desc"])>=35?'...':' ';
                    $nestedData[] = $row["career_title"];
                   // $nestedData[] = $row["career_desc"];
                    $nestedData[] = substr(strip_tags($row["career_desc"]), 0, 35).' '.$extra;
//                    $statusbutton = '<button style="color:white" data-id="'.$row["career_id"].'" data-val="'.$row["career_status"].'"';
//                    if ($row['career_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['banner_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/our_career/edit/'.base64_encode($row['career_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/our_career/delete/'.base64_encode($row['career_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
         return json_encode($json_data);
    }
    
    function countcareers()
    {
        return $this->db->count_all('careers');
    }
    
    function getcareers($limit, $offset)
    {
         // Get a list of all careers
        $this->db->select("*");
        $this->db->order_by("career_id", "desc");
    $this->db->limit($limit, $offset);
        $query = $this->db->get('careers');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getcareer($id)
    {
        // Get the career details
        $this->db->select("*");
        $this->db->where("career_id", $id);
        $query = $this->db->get('careers');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removecareer($id)
    {
        // Delete a careers
        $this->db->delete('careers', array('career_id' => $id));
        
    }
    
    function changecareer_status($data, $id) {
        
        $this->db->where('career_id', $id);
        if($this->db->update('careers', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createcareers()
    {
        // Create the careers
        $data = array(
            'career_title' => strip_tags($this->input->post('career_title')),
            'career_link' => strip_tags($this->input->post('career_link')),
            //'career_status'=>'Enabled',
            //'career_image'=>$filename,
            'career_desc' => $this->input->post('career_desc'),
            'career_req' => $this->input->post('career_req'),
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('careers', $data);
    }
    
    function updatecareers($id,$filename)
    {
//        if($filename)
//        {
            // update the user account
            $data = array(
                'career_title' => strip_tags($this->input->post('career_title')),
                'career_link' => strip_tags($this->input->post('career_link')),
                //'career_image'=> $filename,
                'career_desc' => $this->input->post('career_desc'),
                'career_req' => $this->input->post('career_req'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
//        }
//        else
//        {
//            // update the user account
//            $data = array(
//                'career_title' => $this->input->post('career_title'),
//                'career_link' => $this->input->post('career_link'),
//                
//                'career_req' => $this->input->post('career_req'),
//                'modified_date'=>date("Y-m-d H:i:s"),
//            );
//        }
        $this->db->where('career_id', $id);
        $this->db->update('careers', $data);
    }
    
}
?>