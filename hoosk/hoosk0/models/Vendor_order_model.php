<?php

class Vendor_order_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    /*     * *************************** */
    /*     * ** Order Querys ************ */
    /*     * *************************** */
    
    
    function resultperpage()
    {
        $this->db->select("resultperpage");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_order_datatable_data($module,$requestData,$company_id,$po_id)
    {
        $columns = array( 
             
                
                0 =>'purchase_order.po_name',
                1 =>'purchase_order.po_number',
                2 =>'order1.created_date',
                
            );
        $sql = "SELECT order1.*,purchase_order.* ";
        $sql.=" FROM  order1 join purchase_order on purchase_order.po_id = order1.po_id WHERE purchase_order.po_company=$company_id AND purchase_order.po_id = $po_id";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT purchase_order.*,order1.* ";
        $sql.=" FROM  order1 join purchase_order on purchase_order.po_id = order1.po_id  WHERE 1=1 AND purchase_order.po_company=$company_id AND purchase_order.po_id = $po_id";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
        	$sql.=" AND ( purchase_order.po_name LIKE '".$requestData['search']['value']."%' ";    
        	
                $sql.=" OR purchase_order.po_number LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR order1.created_date LIKE '".$requestData['search']['value']."%' ";
                
                $sql.=" OR order1.order_status LIKE '".$requestData['search']['value']."%' )";
        }
        
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY order1.order_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["po_name"];
                    $nestedData[] = $row["po_number"];
                    $nestedData[] = date("m-d-Y", strtotime($row["created_date"]));
                    $statusbutton = '<button style="color:white" data-id="'.$row["order_id"].'" data-val="'.$row["order_status"].'"';
                    if ($row['order_status'] == "Pending") {
                        $statusbutton .= ' class="bg-orange mystatus btn" disabled>Pending</button>';
                    } elseif ($row['order_status'] == "Approved") {
                        $statusbutton .= ' class="bg-green mystatus btn" disabled>Approved</button>';
                    } elseif ($row['order_status'] == "Completed") {
                        $statusbutton .= ' class="mystatus btn bg-blue" disabled>Completed</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn" disabled>Canceled</button>';
                    }
//                    $nestedData[] = $statusbutton;

                    if($row['order_status'] != "Pending") {
                        if($this->Default_vendor_model->check_permission('order_view')) {
                            $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/vendor/order/view/'.base64_encode($row['order_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> </td>';
                        }
                        else {
                            $nestedData[] = '<td class="td-actions"></td>';
                        }
                    }
                    else {
                        $listper = '<td class="td-actions">';
                        if($this->Default_vendor_model->check_permission('order_edit')) {
                            //$listper .= '<a href="'.BASE_URL.'/vendor/order/edit/'.base64_encode($row['order_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> ';
                        }
                        if($this->Default_vendor_model->check_permission('order_delete')) {
                            //$listper .= '<a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/vendor/order/delete/'.base64_encode($row['order_id']).'"><i class="fa fa-remove"> </i></a> ';
                        }
                        if($this->Default_vendor_model->check_permission('order_view')) {
                            $listper .= '<a href="'.BASE_URL.'/vendor/order/view/'.base64_encode($row['order_id']).'" class="btn btn-small btn-success"><i class="fa fa-eye"> </i></a> ';
                        }
                        $listper .= '</td>';
                        $nestedData[] = $listper;
                    }

                    $data[] = $nestedData;
                    
            }
            
        }
        $json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
        
         return json_encode($json_data);
    }
    
    
    function get_enabled_company()
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("company_status", 'Enabled');
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function get_pos_from_company($id)
    {
        $this->db->select("*");
       	$this->db->where("po_company", $id);
       	//  $this->db->where("po_status", 'Approved');
        //$this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
	$query = $this->db->get('purchase_order');
        if ($query->num_rows() > 0) {
            
            $results = $query->result_array();
        
            return $results;
        }
        
        return array();
    }
    
    
    function get_product($productname)
    {
                // Get the user details
                $this->db->select("*");
                //$this->db->where("product_name", $productname);
                $this->db->like("product_name", $productname);
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function  get_company_by_id($companyid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("companyId", $companyid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('hoosk_company');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function get_product_by_id($productid)
    {
                // Get the user details
                $this->db->select("*");
                $this->db->where("product_id", $productid);
                //$this->db->like("product_name", $productname);
                $query = $this->db->get('products');
                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                return array();
    }
    
    
    function confirm_order($data)
    {
        if($this->db->insert('order1', $data))
        {
            return $this->db->insert_id();
        }
        else
        {
            return false;
        }
    }
    
    
    function confirm_order_products($data)
    {
        if($this->db->insert('order_products', $data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    function getOrderById($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("order_id", $id);
        $this->db->join('purchase_order', 'purchase_order.po_id = order1.po_id');
        $query = $this->db->get('order1');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function getProjectById($id,$mode = '')
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        
        if($mode != '')
        {
            $this->db->join('hoosk_company', 'hoosk_company.companyId = purchase_order.po_company');
        }
        $query = $this->db->get('purchase_order');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_products_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $this->db->join('products', 'products.product_id = po_products.product_id');
        $this->db->order_by("po_products.po_product_id", "asc");
        $query = $this->db->get('po_products');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_custom_for_project($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("po_id", $id);
        $query = $this->db->get('po_custom');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_remaining_for_project($id)
    {
        $this->db->select("*");
       	$this->db->where("order1.po_id", $id);
        // $this->db->where("order_products.quantity !=", 0);
        $this->db->join('order_products', 'order_products.order_id = order1.order_id');
	$query = $this->db->get('order1');
        if ($query->num_rows() > 0) {
            
            $results = $query->result_array();
        
            return $results;
        }
        
        return array();
    }
    
    
    function order_products_delete($id)
    {
        $this->db->delete('order_products', array('order_id' => $id));
    }
    
    
    function removeOrder($id)
    {
        // Delete a company account
        $this->db->delete('order1', array('order_id' => $id));
        $this->db->delete('order_products', array('order_id' => $id));
    }
    
    
    function get_order_product_data($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("order_id", $id);
        $query = $this->db->get('order_products');
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        return array();
    }
    
    
    function change_order_status($data,$id)
    {
        $this->db->where('order_id', $id);
        if ($this->db->update('order1', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    
}
    
    
?>