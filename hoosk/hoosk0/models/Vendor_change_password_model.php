<?php

class Vendor_Change_password_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    /*     * *************************** */
    /*     * ** Login Change Password Querys ************ */
    /*     * *************************** */

        public function checkCurPass()
    {
        $cpass = $this->input->post('cur_pass');
        $this->db->where('email', $this->session->userdata('email'));
        $this->db->where('password', base64_encode($cpass));
        $cquery = $this->db->get('hoosk_company');
        if($cquery->num_rows() == 1) {
            return 1;
        } else {
            $epass = $this->input->post('cur_pass');
            $this->db->where('emp_email', $this->session->userdata('email'));
            $this->db->where('password', base64_encode($epass));
            $equery = $this->db->get('employees');
            if($equery->num_rows() == 1) {
                return 1;
            }
            else {
                return 0;
            }
        }           
    }

    public function saveNewPass()
    {
        $str = $this->session->userdata('email');
        
        $cquery = $this->db->get_where('hoosk_company', array('email' => $str), 1);
        if ($cquery->num_rows() == 1) {           
            
            $cpass = $this->input->post('new_pass');
            $data = array('password' => base64_encode($cpass));
            $this->db->where('email', $this->session->userdata('email'));
            $this->db->update('hoosk_company', $data);
            return true;
        } 
        else {
            $equery = $this->db->get_where('employees', array('emp_email' => $str), 1);
            if ($equery->num_rows() == 1)
            {
                $epass = $this->input->post('new_pass');
                $data = array('password' => base64_encode($epass));
                $this->db->where('emp_email', $this->session->userdata('email'));
                $this->db->update('employees', $data);
                return true;
            }
        }
        
    }
}
?>