<?php

class Product_spotlight_model extends CI_Model {

    function __construct() {
        // Call the Model constructor 
        parent::__construct();
        $this->load->database();
       
    }


    /*     * *************************** */
    /*     * ** Product Spotlight Querys ************ */
    /*     * *************************** */
    
    function get_product_spotlight_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'pspot_name'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( pspot_name LIKE '".$requestData['search']['value']."%' ) ";    
            
//          $sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY pspot_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        //echo $this->db->last_query();die();
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["pspot_name"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/products/'.$row['pspot_image'].'" width="100px" height="100px" >';
//                    $statusbutton = '<button style="color:white" data-id="'.$row["product_spotlight_id"].'" data-val="'.$row["product_spotlight_status"].'"';
//                    if ($row['product_spotlight_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['banner_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/product_spotlight/edit/'.base64_encode($row['pspot_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/product_spotlight/delete/'.base64_encode($row['pspot_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
         return json_encode($json_data);
    }
    
    function countproduct_spotlight()
    {
        return $this->db->count_all('product_spotlights');
    }
    
    function getproduct_spotlight($limit, $offset)
    {
         // Get a list of all product_spotlights
        $this->db->select("*");
        $this->db->order_by("pspot_id", "desc");
    $this->db->limit($limit, $offset);
        $query = $this->db->get('product_spotlights');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getproduct_spotlights($id)
    {
        // Get the product_spotlight details
        $this->db->select("*");
        $this->db->where("pspot_id", $id);
        $query = $this->db->get('product_spotlights');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removeproduct_spotlight($id)
    {
        // Delete a product_spotlights
        $this->db->delete('product_spotlights', array('pspot_id' => $id));
        
    }
    
    function changeproduct_spotlight_status($data, $id) {
        
        $this->db->where('pspot_id', $id);
        if($this->db->update('product_spotlights', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createproduct_spotlight($filename = '')
    {
        // Create the product_spotlights
        $data = array(
            'pspot_name' => strip_tags($this->input->post('pspot_name')),
            'pspot_desc' => strip_tags($this->input->post('pspot_desc')),
            'pspot_link' => strip_tags($this->input->post('pspot_link')),
            //'product_spotlight_status'=>'Enabled',
            'pspot_image'=>$filename, 
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('product_spotlights', $data);
    }
    
    function updateproduct_spotlight($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'pspot_name' => strip_tags($this->input->post('pspot_name')),
                'pspot_desc' => strip_tags($this->input->post('pspot_desc')),
                'pspot_link' => strip_tags($this->input->post('pspot_link')),
                'pspot_image'=> $filename,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'pspot_name' => strip_tags($this->input->post('pspot_name')),
                'pspot_desc' => strip_tags($this->input->post('pspot_desc')),
                'pspot_link' => strip_tags($this->input->post('pspot_link')),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('pspot_id', $id);
        $this->db->update('product_spotlights', $data);
    }
    
}
?>