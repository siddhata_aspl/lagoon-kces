<?php

class Front_clients_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    /**     * ************************** */
    /**     * * Front Client Login Querys ************ */

    /**     * ************************** */
    
    function getLang() {
        // Get Theme
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u):
                return $u['siteLang'];
            endforeach;
        }
        return array();
    }

    function add_newsletter_mail() {

       $nmail = $this->input->post('newsmail');

       $data = array(
           'news_mail' => strip_tags($nmail),
           'created_datetime'=>date("Y-m-d H:i:s"),
       );
       $this->db->insert('newsletter_mails', $data);

   }
    
    function registerUser($filename = '') {
        // Create the user account
        $data = array(
            'User_firstname' => strip_tags($this->input->post('firstname')),
            'User_lastname' => strip_tags($this->input->post('lastname')),
            'userName' => strip_tags($this->input->post('username')),
            'email' => strip_tags($this->input->post('email')),
            'phone' => strip_tags($this->input->post('phone')),
            //'password' => md5($this->input->post('password').SALT),
            //'password' => base64_encode($this->input->post('password')),
            'user_status'=>'Disabled',
            'user_img'=>$filename,
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('hoosk_user', $data);
    }

    function login($username, $password) {
        $this->db->select("*");
        $this->db->where("userName", $username);
        $this->db->where("password", $password);
        $this->db->where("user_status", "Enabled");
        $query = $this->db->get("hoosk_user");
        //print_r($this->db->last_query());        exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $data = array(
                    'userID' => $rows->userID,
                    'userName' => $rows->userName,
                    'email' => $rows->email,
                    ///'logged_in' => TRUE,
                );
                $this->session->sess_expiration = '14400';
                $this->session->set_userdata('user_id', $rows->userID);
                $this->session->set_userdata('user_name', $rows->userName);
                $this->session->set_userdata('user_email', $rows->email);
                //$this->session->set_userdata('logged_in', TRUE);
                //$this->session->set_userdata($data);
                return true;
            }
        } else {
            return false;
        }
    }

    function getmail_template($id) {
        $this->db->select("*");
        $this->db->where("email_template_id", $id);
        $query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function update_user_for_forgotmail($data, $email) {
        $this->db->where('email', $email);
        $this->db->update('hoosk_user', $data);
        $this->db->select("*");
        $this->db->where('email', $email);
        $query = $this->db->get('hoosk_user');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    /**     * ************************** */
    /**     * * Front Product Listing Query ************ */

    /**     * ************************** */
    
    function get_product_category_list() {
        // Get product details
        $this->db->select('*');
        //$this->db->where("products.product_id", $pro_id);
        $query = $this->db->get('product_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_product_list() {
        // Get all product details
        $this->db->select('products.product_id pid, products.*,product_images.*');
        $this->db->from('products');
        $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
        $this->db->where("product_status", 'Enabled');
        $this->db->group_by("products.product_id"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_catname_by_catid($cat_id) {
        // Get category details of particular category
        $this->db->select('*');
        $this->db->where("category_id", $cat_id);
        $query = $this->db->get('product_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_subcatname_by_subcatid($subcat_id) {
        // Get category details of particular category
        $this->db->select('*');
        $this->db->where("subcat_id", $subcat_id);
        $query = $this->db->get('product_subcategory');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_subcategory_list_by_catid($cat_id) {
        // Get sub category details of particular category
        $this->db->select('*');
        $this->db->where("category_id", $cat_id);
        $query = $this->db->get('product_subcategory');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function get_product_list_by_sort($sort_id) {
        // Get product list by sorting
        if($sort_id == 'A-Z') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->order_by('product_name', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'Z-A') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->order_by('product_name', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'High-To-Low') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->order_by('product_unit_price', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->order_by('product_unit_price', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
            
            if ($query->num_rows() > 0) {
                //echo "hii";die;
                $results = $query->result_array();  

                $page = '';
                
                    foreach($results as $p) 
                    {                          
                        $url = BASE_URL;
                        $pid = base64_encode($p['pid']);
                        $pname = $p['product_name'];
                        $price = $p['product_unit_price'];
                        $image = $p['product_image'];                   
                                 
                        $page .= "<div class='col-md-3 col-sm-3 product-page'>
                          <div class='product-image'>";
                              
                        if($this->session->userdata('user_name') != '')
                        {                              
                            $page .= "<a href='$url/shop/product/$pid' rel='bookmark' title='$pname'>";                       
                        }
                        else
                        {
                            $page .= "<a href='$url/shop/login'>";                           
                        }
                             
                        if($image != '')
                        {
                    
                         $page .= "<img src='$url/uploads/products/$image'/>";

                        }
                        else 
                        {
      
                         $page .=  "<img src='$url/uploads/products/blank-product-img.png'/>";
                                  
                        }
                                 
                        $page .=  "</a>
                          </div>
                          <div class='product-detail'>";
                             
                        if($this->session->userdata('user_name') != '')
                        {                        
                            $page .=  "<span class='pname'><a href='$url/shop/product/$pid'>$pname</a></span>";
                        }
                        else
                        {
                            $page .=  "<span class='pname'><a href='$url/shop/login'>$pname</a></span>";
                        }
                         
                        if($this->session->userdata('user_name') != '')
                        {      
                            if($price != '0') 
                            {

                              $page .= "<span class='price'>$$price</span>";
                                 
                            }
                            else 
                            {

                              $page .=  "<span class='price'>CALL FOR PRICING</span>";
                                 
                            }
                        }
                            
                          $page .= "</div>
                 </div>";
                    
                }                        
               return $page; 
            }                 
    }

    function get_cat_product_list_by_sort($sort_id,$cat_id) {
        // Get product list by sorting
        if($sort_id == 'A-Z') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('category_id', $cat_id);
            $this->db->order_by('product_name', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'Z-A') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('category_id', $cat_id);
            $this->db->order_by('product_name', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'High-To-Low') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('category_id', $cat_id);
            $this->db->order_by('product_unit_price', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('category_id', $cat_id);
            $this->db->order_by('product_unit_price', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
            
            if ($query->num_rows() > 0) {
                //echo "hii";die;
                $results = $query->result_array();  

                $page = '';
                
                    foreach($results as $p) 
                    {                          
                        $url = BASE_URL;
                        $pid = base64_encode($p['pid']);
                        $pname = $p['product_name'];
                        $price = $p['product_unit_price'];
                        $image = $p['product_image'];                   
                                 
                        $page .= "<div class='col-md-3 col-sm-3 product-page'>
                          <div class='product-image'>";
                        
                        if($this->session->userdata('user_name') != '')
                        {                              
                            $page .= "<a href='$url/shop/product/$pid' rel='bookmark' title='$pname'>";                       
                        }
                        else
                        {
                            $page .= "<a href='$url/shop/login'>";                           
                        }
                                  
                        if($image != '')
                        {
                    
                         $page .= "<img src='$url/uploads/products/$image'/>";

                        }
                        else 
                        {
      
                         $page .=  "<img src='$url/uploads/products/blank-product-img.png'/>";
                                  
                        }
                                 
                        $page .=  "</a>
                          </div>
                          <div class='product-detail'>";
                        
                        if($this->session->userdata('user_name') != '')
                        {                        
                            $page .=  "<span class='pname'><a href='$url/shop/product/$pid'>$pname</a></span>";
                        }
                        else
                        {
                            $page .=  "<span class='pname'><a href='$url/shop/login'>$pname</a></span>";
                        }
                         
                        if($this->session->userdata('user_name') != '')
                        {
                            if($price != '0') 
                            {

                              $page .= "<span class='price'>$$price</span>";

                            }
                            else 
                            {

                              $page .=  "<span class='price'>CALL FOR PRICING</span>";

                            }
                        }
                            
                          $page .= "</div>
                 </div>";
                    
                }                        
               return $page; 
            }         
        
    }
    
    function get_subcat_product_list_by_sort($sort_id,$subcat_id) {
        // Get product list by sorting
        if($sort_id == 'A-Z') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('subcat_id', $subcat_id);
            $this->db->order_by('product_name', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'Z-A') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('subcat_id', $subcat_id);
            $this->db->order_by('product_name', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else if($sort_id == 'High-To-Low') {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('subcat_id', $subcat_id);
            $this->db->order_by('product_unit_price', 'DESC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
        else {
            
            $this->db->select('products.product_id pid, products.*, product_images.*');
            $this->db->from('products');
            $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
            $this->db->where('product_status', 'Enabled');
            $this->db->where('subcat_id', $subcat_id);
            $this->db->order_by('product_unit_price', 'ASC');
            $this->db->group_by('products.product_id'); 
            $query = $this->db->get();
            
        }
            
            if ($query->num_rows() > 0) {
                //echo "hii";die;
                $results = $query->result_array();  

                $page = '';
                
                    foreach($results as $p) 
                    {                          
                        $url = BASE_URL;
                        $pid = base64_encode($p['pid']);
                        $pname = $p['product_name'];
                        $price = $p['product_unit_price'];
                        $image = $p['product_image'];                   
                                 
                        $page .= "<div class='col-md-3 col-sm-3 product-page'>
                          <div class='product-image'>";
                        
                        if($this->session->userdata('user_name') != '')
                        {                              
                            $page .= "<a href='$url/shop/product/$pid' rel='bookmark' title='$pname'>";                       
                        }
                        else
                        {
                            $page .= "<a href='$url/shop/login'>";                           
                        }
                                  
                        if($image != '')
                        {
                    
                         $page .= "<img src='$url/uploads/products/$image'/>";

                        }
                        else 
                        {
      
                         $page .=  "<img src='$url/uploads/products/blank-product-img.png'/>";
                                  
                        }
                                 
                        $page .=  "</a>
                          </div>
                          <div class='product-detail'>";
                        
                        if($this->session->userdata('user_name') != '')
                        {                        
                            $page .=  "<span class='pname'><a href='$url/shop/product/$pid'>$pname</a></span>";
                        }
                        else
                        {
                            $page .=  "<span class='pname'><a href='$url/shop/login'>$pname</a></span>";
                        }
                         
                        if($this->session->userdata('user_name') != '')
                        {
                            if($price != '0') 
                            {

                              $page .= "<span class='price'>$$price</span>";

                            }
                            else 
                            {

                              $page .=  "<span class='price'>CALL FOR PRICING</span>";

                            }
                        }
                            
                          $page .= "</div>
                 </div>";
                    
                }                        
               return $page; 
            }         
        
    }
    
    function get_category_product_list($cat_id) {
        // Get product list of category
        $this->db->select('products.*, products.product_id pid, product_images.*');
        $this->db->from('products');
        $this->db->join('product_category', 'products.category_id = product_category.category_id');
        $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
        $this->db->where("product_status", 'Enabled');
        $this->db->where("products.category_id", $cat_id);
        $this->db->group_by("products.product_id"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    
    function get_subcategory_product_list($subcat_id) {
        // Get product list of category
        $this->db->select('products.*, products.product_id pid, product_images.*, product_category.*, product_subcategory.*');
        $this->db->from('products');
        $this->db->join('product_subcategory', 'products.subcat_id = product_subcategory.subcat_id');
        $this->db->join('product_category', 'products.category_id = product_category.category_id');
        $this->db->join('product_images', 'products.product_id = product_images.product_id', 'left');
        $this->db->where("product_status", 'Enabled');
        $this->db->where("products.subcat_id", $subcat_id);
        $this->db->group_by("products.product_id"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    

    function get_product_details($pro_id) {
        // Get product details
        $this->db->select('*');
        $this->db->where("products.product_id", $pro_id);
        $query = $this->db->get('products');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function getImagesByProductId($pro_id) {
        // Get image detail   
        $this->db->select('product_image');
        $this->db->where("product_id", $pro_id);
        $query = $this->db->get('product_images');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function getLocationsByProductId($pro_id) {
        // Get location detail   
        $this->db->select('location_name,location_quantity');
        $this->db->where("product_id", $pro_id);
        $query = $this->db->get('product_locations');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function add_product_in_cart($uid) {
        
        $price = $this->input->post('product_price');
        $qty = $this->input->post('quantity');
        $pid = $this->input->post('product_id');
        
        $this->db->select("*");
        $this->db->where("login_id", $uid);
        $this->db->where("product_id", $pid);
        $query = $this->db->get('cart_products');
        
        if ($query->num_rows() > 0) {
            
            $getpro = $query->result_array(); 
            
            $p_qty = $getpro[0]['product_quantity'];
            
            $fqty = $qty + $p_qty; 
            
            $total = $price * $fqty;
         
            $data = array(
                'login_id' => $uid,
                'product_id' => $this->input->post('product_id'),
                'product_name' => $this->input->post('product_name'),
                'product_price' => $this->input->post('product_price'),
                'product_quantity' => $fqty,
                'product_total' => $total,
            );
            $this->db->where("login_id", $uid);
            $this->db->where("product_id", $pid);
            $this->db->update('cart_products', $data);
        }
        else {
            
            $fqty = $qty; 
            
            $total = $price * $fqty;
        
            $data = array(
                'login_id' => $uid,
                'product_id' => $this->input->post('product_id'),
                'product_name' => $this->input->post('product_name'),
                'product_price' => $this->input->post('product_price'),
                'product_quantity' => $fqty,
                'product_total' => $total,
            );

            $this->db->insert('cart_products', $data);
        }      
        
    }
    
    function get_total_cart_products($uid) {
        
        $this->db->select("COUNT(product_id) as pcount");
        $this->db->where("login_id", $uid);
        $query = $this->db->get('cart_products');
        
        if ($query->num_rows() > 0) {
            
//            $result = $query->row_array();
//            $count = $result['COUNT(*)'];
            
            $row = $query->row();
            return $pro_count = $row->pcount;
            //echo $pro_count;die;
            //return $pro_count;
            //echo $count;die;
        }
        //return array();
    }
    
    function get_product_cart_details($uid) {
        
        $this->db->select("*");      
        $this->db->join('products', 'cart_products.product_id = products.product_id');
        $this->db->where("login_id", $uid);
        $query = $this->db->get('cart_products');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }

    function update_cart_data($uid) {

       $cid = $this->input->post('cart_id');
       $qty = $this->input->post('new_quantity');
       $tprice = $this->input->post('new_price');

       $data = array(
           'product_quantity' => $qty,
           'product_total' => $tprice,
       );

       $this->db->where("login_id", $uid);
       $this->db->where("cart_id", $cid);
       $this->db->update('cart_products', $data);

   }

   function add_order($uid) {

       $this->db->select("*");
       $this->db->where("login_id", $uid);
       $query = $this->db->get('cart_products');

       $this->load->helper('string');
       $rs = random_string('numeric', 5);
       $oid = 'order'.$rs;

       if($query->num_rows() > 0) {

           $results = $query->result_array();

           foreach ($results as $orders)
           {
               $data = array(
               	   'orderID' => $oid,
                   'login_id' => $uid,
                   'product_id' => $orders['product_id'],
                   'product_name' => $orders['product_name'],
                   'product_price' => $orders['product_price'],
                   'product_quantity' => $orders['product_quantity'],
                   'product_total' => $orders['product_total'],
                   'created_datetime' => date("Y-m-d H:i:s"),
               );

               $this->db->insert('site_orders', $data);
           }
       }
   }
    
    function remove_cart_data($cid,$uid) {
       
        $this->db->where("login_id", $uid);
        $this->db->where("cart_id", $cid);
        $query = $this->db->delete('cart_products');

    }

}
