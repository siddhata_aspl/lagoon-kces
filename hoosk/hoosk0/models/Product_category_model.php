<?php

class Product_category_model extends CI_Model {

    function __construct() {
        // Call the Model constructor 
        parent::__construct();
        $this->load->database();
       
    }


    /*     * *************************** */
    /*     * ** Product Category Querys ************ */
    /*     * *************************** */
    
    function get_product_category_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'category_name'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( category_name LIKE '".$requestData['search']['value']."%' ) ";    
            
//          $sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY category_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        //echo $this->db->last_query();die();
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["category_name"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/products/'.$row['category_image'].'" width="100px" height="100px" >';
//                    $statusbutton = '<button style="color:white" data-id="'.$row["product_category_id"].'" data-val="'.$row["product_category_status"].'"';
//                    if ($row['product_category_status'] == "Disabled") {
//                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
//                    } elseif ($row['banner_status'] == "Enabled") {
//                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
//                    } else {
//                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
//                    }
//                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/product_category/edit/'.base64_encode($row['category_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/product_category/delete/'.base64_encode($row['category_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
         return json_encode($json_data);
    }
    
    function countproduct_category()
    {
        return $this->db->count_all('product_category');
    }
    
    function getproduct_category($limit, $offset)
    {
         // Get a list of all product_categorys
        $this->db->select("*");
        $this->db->order_by("category_id", "desc");
    $this->db->limit($limit, $offset);
        $query = $this->db->get('product_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getproduct_categorys($id)
    {
        // Get the product_category details
        $this->db->select("*");
        $this->db->where("category_id", $id);
        $query = $this->db->get('product_category');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removeproduct_category($id)
    {
        // Delete a product_categorys
        $this->db->delete('product_category', array('category_id' => $id));
        
    }
    
    function changeproduct_category_status($data, $id) {
        
        $this->db->where('category_id', $id);
        if($this->db->update('product_category', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createproduct_category($filename = '')
    {
        // Create the product_categorys
        $data = array(
            'category_name' => strip_tags($this->input->post('category_name')),
            //'product_category_status'=>'Enabled',
            'category_image'=>$filename, 
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('product_category', $data);
    }
    
    function updateproduct_category($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'category_name' => strip_tags($this->input->post('category_name')),
                'category_image'=> $filename,
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'category_name' => strip_tags($this->input->post('category_name')),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('category_id', $id);
        $this->db->update('product_category', $data);
    }
    
}
?>