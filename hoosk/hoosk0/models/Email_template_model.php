<?php

class Email_template_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    function countemail_template(){
        return $this->db->count_all('email_template');
    }
    
    function get_email_template($limit, $offset=0)
    {
        $this->db->select("*");
        $this->db->limit($limit, $offset);
        $this->db->order_by("email_template_id", "asc");
        $query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getemail_template($id)
    {
        // Get the user details
        $this->db->select("*");
        $this->db->where("email_template_id", $id);
        $query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    function updateemail_template($id,$data)
    {
        $this->db->where('email_template_id', $id);
        $this->db->update('email_template', $data);
    }
    
    function email_templateSearch($term,$limit,$offset = 0){
        
		$this->db->select("*");
		$this->db->like("email_template_title", $term);
                $this->db->or_like("email_template_subject", $term);
		$this->db->limit($limit, $offset);
                $query = $this->db->get('email_template');
		if ($query->num_rows() > 0) {
			$results = $query->result_array();
			foreach ($results as $u): 
				$extra = strlen($u["email_template_desc"])>=35?'...':' ';
                                    echo '<tr>';
                                    echo '<td>'.$u['email_template_title'].'</td>';
                                    echo '<td>'.$u['email_template_subject'].'</td>';
                                    echo '<td>'.substr(strip_tags($u["email_template_desc"]), 0, 35).' '.$extra.'</td>';
                                    echo '<td class="td-actions"><a href="'.BASE_URL.'/admin/email_template/edit/'.base64_encode($u['email_template_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a></td>';
                                    echo '</tr>';		
			endforeach; 
		} else {
			echo "<tr><td colspan='4'><p>".$this->lang->line('no_results')."</p></td></tr>";
		}
                
	}
}
?>