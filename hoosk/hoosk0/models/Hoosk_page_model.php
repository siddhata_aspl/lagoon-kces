<?php

class Hoosk_page_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


	/*     * *************************** */
    /*     * ** Page Querys ************ */
    /*     * *************************** */
	/*function getSiteName() {
        // Get Theme
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteTitle'];			
			endforeach; 
		}
        return array();
    }
	
	function getTheme() {
        // Get Theme
        $this->db->select("*");
       	$this->db->where("siteID", 0);
		$query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				return $u['siteTheme'];			
			endforeach; 
		}
        return array();
    }*/
	
    function getPage($pageURL) {
        // Get page
        $this->db->select("*");
        $this->db->join('hoosk_page_content', 'hoosk_page_content.pageID = hoosk_page_attributes.pageID');
        $this->db->join('hoosk_page_meta', 'hoosk_page_meta.pageID = hoosk_page_attributes.pageID');
		$this->db->where("pagePublished", 1);
		$this->db->where("pageURL", $pageURL);
        $query = $this->db->get('hoosk_page_attributes');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				$page = array(
	   	   				'pageID'    			=> $u['pageID'],
	   	   				'pageTitle' 			=> $u['pageTitle'],
						'pageKeywords' 			=> $u['pageKeywords'],
						'pageDescription' 		=> $u['pageDescription'],
	   	   				'pageContentHTML'   	=> $u['pageContentHTML'],
						'pageTemplate'    		=> $u['pageTemplate'],
						'enableJumbotron'   	=> $u['enableJumbotron'],
						'enableSlider'   		=> $u['enableSlider'],
						'jumbotronHTML'    		=> $u['jumbotronHTML'],
                     );      	
			endforeach; 
			return $page;
		
		}
        return array('pageID' => "",'pageTemplate' => "");
    }
	function getCategory($catSlug) {
        // Get category
        $this->db->select("*");
		$this->db->where("categorySlug", $catSlug);
        $query = $this->db->get('hoosk_post_category');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				$category = array(
	   	   				'pageID'    			=> $u['categoryID'],
						'categoryID'    		=> $u['categoryID'],
	   	   				'pageTitle' 			=> $u['categoryTitle'],
						'pageKeywords' 			=> '',
						'pageDescription' 		=> $u['categoryDescription'],
                     );      	
			endforeach; 
			return $category;
		
		}
        return array('categoryID' => "");
    }
	
	function getArticle($postURL) {
        // Get article
        $this->db->select("*");
		$this->db->where("postURL", $postURL);
        $this->db->where("published", 1);
        $this->db->join('hoosk_post_category', 'hoosk_post_category.categoryID = hoosk_post.categoryID');
        $query = $this->db->get('hoosk_post');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				$category = array(
	   	   				'pageID'    			=> $u['postID'],
						'postID'    			=> $u['postID'],
	   	   				'pageTitle' 			=> $u['postTitle'],
						'pageKeywords' 			=> '',
						'pageDescription' 		=> $u['postExcerpt'],
						'postContent' 			=> $u['postContentHTML'],
						'datePosted' 			=> $u['datePosted'],
						'categoryTitle' 		=> $u['categoryTitle'],
						'categorySlug' 			=> $u['categorySlug'],
                     );      	
			endforeach; 
			return $category;
		
		}
        return array('postID' => "");
    }
	
	
   function getSettings() {
        // Get settings
        $this->db->select("*");
		$this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        	foreach ($results as $u): 
				$page = array(
						'siteLogo'    				=> $u['siteLogo'],
						'siteFavicon'    			=> $u['siteFavicon'],
						'siteFooterLogo'            => $u['siteFooterLogo'],
						'siteTitle'    				=> $u['siteTitle'],
						'LogoBottomTitle'    		=> $u['LogoBottomTitle'],
						'siteTheme'    				=> $u['siteTheme'],
						'siteFooter'    			=> $u['siteFooter'],
						
						'siteFooter2' => $u['siteFooter2'],
                        'add1_title' => $u['add1_title'],
                        'add1' => $u['add1'],
                        'add1_phone' => $u['add1_phone'],
                        'add1_fax' => $u['add1_fax'],
                        'add1_hours' => $u['add1_hours'],
                        'add1_map' => $u['add1_map'],
                        'add2_title' => $u['add2_title'],
                        'add2' => $u['add2'],
                        'add2_phone' => $u['add2_phone'],
                        'add2_fax' => $u['add2_fax'],
                        'add2_hours' => $u['add2_hours'],
                        'add2_map' => $u['add2_map'],
						
						'siteMaintenanceHeading'    => $u['siteMaintenanceHeading'],
						'siteMaintenanceMeta'	    => $u['siteMaintenanceMeta'],
						'siteMaintenanceContent'    => $u['siteMaintenanceContent'],
						'siteMaintenance'    		=> $u['siteMaintenance'],
						'siteAdditionalJS'    		=> $u['siteAdditionalJS'],
                     );      	
			endforeach; 
			return $page;
		
		}
        return array();
    }


    function getMethodName($id, $sorting, $limit) {
//        echo $id;die;
        $this->db->select("shortcode_content");
        $this->db->where("shortcode_id", $id);
        $query = $this->db->get('shortcodes');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $shortcode_content = $rows->shortcode_content;

                $response_html = $this->{$shortcode_content}($sorting, $limit);
                return $response_html;
            }
        } else {
            return false;
        }
    }

    function getOurPeople($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("people_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('peoples');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            $page = "<div id='staffwrapper' class='rowtight init-isotope' data-fade-in='1' data-iso-selector='.s_item'>";
            foreach ($results as $u) {
             $photo = $u['people_photo'];
             $people_photo = base_url().'uploads/peoples/'.$u['people_photo'];
             $people_name = $u['people_name'];
             $people_post = $u['people_post'];
                $page .= "<div class='tcol-md-4 tcol-sm-4 tcol-xs-6 tcol-ss-12  s_item'><div class='grid_item staff_item kt_item_fade_in kad_staff_fade_in postclass'><div class='imghoverclass'><img src='$people_photo' alt='$photo' class='' style='display: block;'></div><div class='staff_item_info'><h3>$people_name</h3><h3><hr />$people_post<h3></div></div></div>";
            }
            $page .= "</div>";
            return $page;
        } else {
            return 'test';
        }
    }
    
    
    function getOurService($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("service_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('services');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            $page = "<div class='our_services-all-page'><div class='row'>";
            foreach ($results as $u) {
                $photo = $u['service_image'];
                $service_image = base_url().'uploads/services/'.$u['service_image'];
                $service_name = $u['service_name'];
                $service_content = $u['service_content'];
                $page .= "<div class='service 1 col-md-4 col-sm-4 services-box'>
<h3>$service_name</h3>
<div class='services-image'><img class='alignnone size-full wp-image-703' src='$service_image' alt='$photo' width='359' height='148' data-id='703' /></div>
<p>$service_content</p>
</div>";
            }
            $page .= "</div></div>";
            return $page;
        } else {
            return 'test';
        }
    }
    
    function getOurCareer($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("career_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('careers');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            $page = "<div class='table-formate career_page' style='border: none'>
<div class='contact_form_second'><h3>Current opportunities</h3></div>
 <div class='table-first-row job_row'>   

       <table class='' style=''>
            
                <tbody><tr><th class='job-title border-b' style='color: darkblue; width: 33.33%; border: 1px solid #282973;'>JOB TITLE</th>
                <th class='job-title border-b' style='color: darkblue; width: 33.33%; border: 1px solid #282973;'>JOB DESCRIPTION</th>
                <th class='job-title border-b' style='color: darkblue; width: 33.33%; border: 1px solid #282973;'>JOB REQUIREMENTS</th>
        
            </tr>";
            foreach ($results as $u) {
                //$photo = $u['service_image'];
                //$service_image = base_url().'uploads/services/'.$u['service_image'];
                $career_title = $u['career_title'];
                //$career_link = $u['career_link'];
                $career_desc = $u['career_desc'];
                $career_req = $u['career_req'];
                $page .= "<tr>
                <td class='job-text' style='border: 1px solid #282973;'>$career_title</td>
                <td class='job-text' style='border: 1px solid #282973;'>$career_desc</td>
                <td class='job-text' style='border: 1px solid #282973;'>$career_req</td>
            </tr>";
            }
            $page .= "</tbody></table>
    </div>
  </div>
  <div class='home-margin home-padding kad-animation' data-animation='fade-in' data-delay='0'>
                   <div class='home-widget-box'>
                               <div class='textwidget'>
                               <div class='btn btn-default'><a class='product_item_link' href= 'https://www.ejobapp-validityscreening.com/applicant/companies/kansas-city-electrical-supply-9270a/accounts/kansas-city-electrical-supply-9270a/positions' target='_blank' style='padding: 7px 28px;'>Apply </a></div>
                               </div>
                   </div> <!--home widget box -->
                   </div>
</div>";
            return $page;
        } else {
            return 'test';
        }
    }
    
    
    function getOurContact($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("contact_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('contacts');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            $page = "<div class='row contact-proprt-text mag-top15'>";
            foreach ($results as $u) {
                $photo = $u['contact_image'];
                $contact_image = base_url().'uploads/contacts/'.$u['contact_image'];
                $contact_title = $u['contact_title'];
                $contact_link = $u['contact_link'];
                $contact_content = $u['contact_content'];
                //$career_req = $u['career_req'];
                $page .= "<div class='col-sm-4 col-md-4 col-xs-12'>
<address>
<h4>$contact_title</h4>
<p><a href='$contact_link'><img class='mar-btm12' src='$contact_image' rel='lightbox'  alt='$photo' border='0' /></a><br />
$contact_content
</address>
</div>";
            }
            $page .= "</div>";
            return $page;
        } else {
            return 'test';
        }
    }
    
    
    function getProductCategory($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("category_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('product_category');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {
            $blink = 'window.location.href="'.base_url().'shop/login"';
            $results = $query->result_array();
            // $page = "<div class='col-md-12 col-sm-12' style='text-align:center;'><button type='button' onclick='$blink' class='kad_add_to_cart shop-button' style='float:none'>LOG IN TO SHOP NOW</button></div><div class='product-page-all'>";

            $page = "<div class='col-md-12 col-sm-12' style='text-align:center;'>";
            
            if($this->session->userdata('user_name') == '')
            {
                
            $page .= "<button type='button' onclick='$blink' class='kad_add_to_cart shop-button' style='float:none'>LOG IN TO SHOP NOW</button>";
            
            }
            $page .= "</div><div class='product-page-all'>";

            $i=1;
            foreach ($results as $u) {
                $photo = $u['contact_image'];
                $catlink = base_url().'shop/'.base64_encode($u['category_id']);
                $category_image = base_url().'uploads/products/'.$u['category_image'];
                $category_name = $u['category_name'];
                //$contact_link = $u['contact_link'];
                //$contact_content = $u['contact_content'];
                //$career_req = $u['career_req'];
                $p = '';
                if(($i-1)%3==0)
                {
                    $p .= 'padding-left:0;';
                }
                else 
                {
                    $p .= '';
                }
                if($i%3==0)
                {
                    $p .= 'padding-right:0;';
                }
                else 
                {
                    $p .= '';
                }
                $page .= "<div class='child-thumb col-md-4 col-sm-4 product-page' style='margin-bottom:45px !important;".$p."'>
<h3><a href='$catlink' rel='bookmark' title='$category_name'>$category_name</a></h3>
  <div class='product-image'><a href='$catlink' rel='bookmark' title='$photo'><img src='$category_image' style='height:150px;'/></a></div>
</div>";
            $i++;
            }
            $page .= "</div>";
            return $page;
        } else {
            return 'test';
        }
    }


    function getProductSpotlight($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("pspot_id", $sorting);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('product_spotlights');
//        echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            $page = "<div class='home-product home-desktop home-margin carousel_outerrim home-padding kad-animation' data-animation='fade-in' data-delay='0'>
                <div class='clearfix'><h3 class='hometitle'>Featured Products</h3></div>
        <div class=' fredcarousel'>
        <div id='hp_carouselcontainer' class='rowtight fadein-carousel'>
        <div id='home-product-carousel' class='products caroufedselclass clearfix'>";
            foreach ($results as $u) {
                $photo = $u['pspot_image'];
                $pspot_image = base_url().'uploads/products/'.$u['pspot_image'];
                $pspot_name = $u['pspot_name'];
                $pspot_link = $u['pspot_link'];
                $pspot_desc = $u['pspot_desc'];
                //$career_req = $u['career_req'];
                $page .= "<div class='tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12  kad_product'>
    <div class='hidetheaction grid_item product_item clearfix kad_product_fade_in post-1527 product type-product status-publish has-post-thumbnail first instock featured shipping-taxable product-type-simple'>

    <a href='the-reel-payoff-encore-wire.php' class='woocommerce-LoopProduct-link'>     <a href='the-reel-payoff-encore-wire.php' class='product_item_link product_img_link'>
            <div class='kad-product-noflipper'> 
                     <img width='300' height='300' src='$pspot_image' class='attachment-shop_catalog wp-post-image' alt='$photo'>
                </div>             </a>
        <div class='product_details'>
            <a href='the-reel-payoff-encore-wire.php' class='product_item_link'>
            <h5>$pspot_name</h5>
            </a>
            <div class='product_excerpt'>

$pspot_desc</div>
        </div>
    </a><a href='$pspot_link' rel='nofollow' data-product_id='1527' data-product_sku='' class='button kad-btn headerfont kad_add_to_cart  product_type_simple'>Read more</a>
</div>
</div>";
            }
            $page .= "</div>
<script type='text/javascript'>
     jQuery( window ).load(function () {
        var $wcontainer = jQuery('#hp_carouselcontainer');
        var $container = jQuery('#home-product-carousel');
                    function getUnitWidth() {
                            var width;
                            if(jQuery(window).width() <= 480) {
                            width = $wcontainer.width() / 1;
                                } else if(jQuery(window).width() <= 768) {
                            width = $wcontainer.width() / 2;
                                } else if(jQuery(window).width() <= 990) {
                            width = $wcontainer.width() / 3;
                                } else {
                            width = $wcontainer.width() / 4;
                                }
                            return width;
                        }
                        function setWidths() {
                            var unitWidth = getUnitWidth() -1;
                            $container.children().css({ width: unitWidth });
                        }
                        function initCarousel_product() {
                            $container.carouFredSel({
                                scroll: { items:1, easing: 'swing', duration: 700, pauseOnHover : true},
                                auto: {play: true, timeoutDuration: 9000},
                                prev: '#prevport_product',
                                next: '#nextport_product',
                                pagination: false,
                                swipe: true,
                                items: {visible:null},
                            });
                        }
                    setWidths();
                    initCarousel_product();
                    jQuery(window).on('debouncedresize', function( event ) {
                        $container.trigger('destroy');
                        setWidths();
                        initCarousel_product();
                    });
                    $wcontainer.animate({'opacity' : 1});
});
</script>
                    <div class='home-margin home-padding kad-animation' data-animation='fade-in' data-delay='0'>
                    <div class='home-widget-box'>
                                <div class='textwidget'>
                                <div class='btn btn-default'><a class='product_item_link' href= '#'>All Product Spotlights </a></div>
                                </div>
                    </div> <!--home widget box -->
                    </div>   


</div></div></div><!-- /.main -->";
            return $page;
        } else {
            return 'test';
        }
    }


    function getHomeSlider($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        $offset = 0;
        $this->db->select("*");
        $this->db->order_by("banner_id", $sorting);
        $this->db->where("banner_status", 'Enabled');
        $this->db->limit($limit, $offset);  
        $query = $this->db->get('banners');
     //echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            //print_r($results);die();
            $page = "<div id='carousel' class='carousel slide ' data-ride='carousel'>
        <ol class='carousel-indicators'>";
            
            $i=1;
            foreach ($results as $u)
            {
                 if($i==1)
                { 
                    $active = 'active';
                } else 
                {
                    $active = '';
                }
                $page .="
                    <li data-target='#carousel' data-slide-to='".$i."' class='".$active."'></li>
                   
                 ";
            }
            
            $page .= "</ol><div class='carousel-inner' role='listbox'>";
            $i=1;
            foreach ($results as $u) {
                $photo = $u['banner_image'];
                $banner_image = base_url().'uploads/banners/'.$u['banner_image'];
                $banner_title = $u['banner_title'];
                $bcheckbox = $u['bcheckbox'];
                //$contact_link = $u['contact_link'];
                $banner_content = $u['banner_content'];
                //$career_req = $u['career_req'];
                if($i==1)
                { 
                    $active = 'active';
                } else 
                {
                    $active = '';
                }

                if($bcheckbox == 'True')
                {
                    $page .= "<div class='item item".$i." ".$active."'>
                             
                <div class='col-md-5' style='text-align:right'><img src='$banner_image' alt='$photo' style='height:240px; width:350px'></div>  
                <div class='col-md-7' style='text-align:left'><h1 style='color:#fff;padding-right:25%; font-family: Archivo Black,sans-serif;    font-size: 64px !important;    line-height: 70px !important;    text-align: center !important;    text-shadow: 1px 2px 3px rgba(0, 0, 0, 0.9) !important;'>$banner_content</h1></div>
        </div>";
                    $i++;
                }
                else if($bcheckbox == 'False')
                {
                    $page .= "<div class='item item".$i." ".$active."'>
                             
                <div class='col-md-12' style='text-align:center'><img src='$banner_image' alt='$photo' style='height:240px; width:350px'></div></div>";
                    $i++;
                }  
            }
            $page .= "</div>
      <a class='left carousel-control' href='#carousel' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </a>
      <a class='right carousel-control' href='#carousel' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </a>
    </div>";
            return $page;
        } else {
            return 'test';
        }
      }


      function getNewsEvents($sorting = 'asc', $limit = '10') {
//        echo "h";
//        die;
        //$offset = 0;

        $this->load->library("pagination");
        $config["base_url"] = base_url() . "news-events/";
        $config["total_rows"] = $this->db->count_all("news_events");
        $config["per_page"] = $limit;
        $config["uri_segment"] = 2;
            
        $this->pagination->initialize($config);
            
        $offset = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 

        $this->db->select("*");
        $this->db->order_by("news_id", $sorting);
        $this->db->where("news_status", 'Enabled');
        $this->db->limit($limit, $offset);  
        $query = $this->db->get('news_events');
     //echo $this->db->last_query();die;
        if ($query->num_rows() > 0) {

            $results = $query->result_array();
            //print_r($results);die();
            $page = "<div class='col-md-12 col-sm-12' style='padding:0;'><div class='col-md-12 col-sm-12' style='padding:0;'>
            <div class='navigation pagination' style='display:block;  margin-top:0'>
            <div class='newsletter'>
            </div>
<div class='newsletter_btn'><a href='".BASE_URL."/newsletter'>Newsletter</a></div>
</div>
</div>";

            foreach ($results as $u) {
                $nid = $u['news_id'];
                $photo = $u['news_image'];
                $news_image = base_url().'uploads/news_events/'.$u['news_image'];
                $news_title = $u['news_title'];
                $short_content = $u['short_content'];
                $news_desc = $u['news_desc'];
                //$career_req = $u['career_req'];
                
                $page .= "<div class='col-sm-6 col-md-6 mrg-btm40'>


                    <div class='image-left'><img style='height:173px; width:200px' src='$news_image' class='attachment-medium size-medium wp-post-image' alt='$news_image' srcset='$news_image 165w, $news_image 143w' sizes='(max-width: 165px) 100vw, 165px'></div>
                      <div class='content-right'>
                        <a href='".BASE_URL."/news_events/".base64_encode($nid)."' title='$news_title'>
                         <h2 class='heading-inner-page'>$news_title</h2></a>
                            <p><p>$short_content</p></p>
                            <div class='read-more'> <a href='".BASE_URL."/news_events/".base64_encode($nid)."'>READ MORE</a></div></div>

                </div>";   
            }
            $plink = $this->pagination->create_links();            
            
            $page .= "</div><div class='wp-pagenavi'>$plink</div>";
                return $page;
            } else {
                return 'test';
            }
        }
      

}

?>