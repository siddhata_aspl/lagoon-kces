<?php

class Vendor_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }
    
    /*     * *************************** */
    /*     * ** Dash Querys ************ */
    /*     * *************************** */
    
    function getSiteName() {
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u): 
                return $u['siteTitle'];         
            endforeach; 
        }
        return array();
    }
    
    function getSiteFooterLine() {
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u): 
                return $u['siteFooter'];            
            endforeach; 
        }
        return array();
    }
    
    function checkMaintenance(){
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u): 
                return $u['siteMaintenance'];           
            endforeach; 
        }
        return array();
    }
    
    function getTheme() {
        // Get Theme
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u): 
                return $u['siteTheme'];         
            endforeach; 
        }
        return array();
    }
    
    function getLang() {
        // Get Theme
        $this->db->select("*");
        $this->db->where("siteID", 0);
        $query = $this->db->get('hoosk_settings');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            foreach ($results as $u): 
                return $u['siteLang'];          
            endforeach; 
        }
        return array();
    }
    
    
    /*     * *************************** */
    /*     * ** Login Querys ************ */
    /*     * *************************** */
    
    
    function company_login($email, $password) {
        $this->db->select("*");
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $this->db->where("company_status", "Enabled");
        $query = $this->db->get("hoosk_company");
        //print_r($this->db->last_query());        exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $data = array(
                    'companyId' => $rows->companyId,
                    'company_name' => $rows->company_name,
                    'first_name' => $rows->first_name,
                    'email' => $rows->email,
                    'logged_in' => TRUE,
                );
                $this->session->sess_expiration = '14400';
                $this->session->set_userdata('companyId', $rows->companyId);
                $this->session->set_userdata('company_name', $rows->company_name);
                $this->session->set_userdata('first_name', $rows->first_name);
                $this->session->set_userdata('email', $rows->email);
                $this->session->set_userdata('logged_in', TRUE);
                $this->session->set_userdata('is_master', TRUE);
                //$this->session->set_userdata($data);
                return true;
                //echo "hii";die();
            }
        } else {
            return false;
        }
    }
    
    
    function employee_login($email, $password) {
        
        $this->db->select("*");
        $this->db->where("emp_email", $email);
        $this->db->where("password", $password);
        $this->db->where("emp_status", "Enabled");
        $query = $this->db->get("employees");
//        print_r($this->db->last_query());        exit();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $data = array(
                    'emp_id' => $rows->emp_id,
                    'com_id' => $rows->com_id,
                    'emp_firstname' => $rows->emp_firstname,
                    'emp_email' => $rows->emp_email,
                    'logged_in' => TRUE,
                );
                $this->session->sess_expiration = '14400';
                $this->session->set_userdata('emp_id', $rows->emp_id);
                $this->session->set_userdata('companyId', $rows->com_id);
                $this->session->set_userdata('first_name', $rows->emp_firstname);
                $this->session->set_userdata('email', $rows->emp_email);
                $this->session->set_userdata('logged_in', TRUE);
                $this->session->set_userdata('is_master', FALSE);
                //$this->session->set_userdata($data);
                return true;  
            }
        } else {
            return false;
        }
    }
    
    // function update_user_for_forgotmail($data, $email)
    // {
    //     $this->db->where('email', $email);
    // $this->db->update('hoosk_company', $data);
    //     $this->db->select("*");
    //     $this->db->where('email', $email);
    //     $query = $this->db->get('hoosk_company');
    //     if ($query->num_rows() > 0) {
    //         return $query->result_array();
    //     }
    //     return array();
    // }


    function update_company_for_forgotmail($data, $email)
    {
        $this->db->where('email', $email);
        $this->db->update('hoosk_company', $data);
        $this->db->select("*");
        $this->db->where('email', $email);
        $query = $this->db->get('hoosk_company');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function update_employee_for_forgotmail($data, $email)
    {
        $this->db->where('emp_email', $email);
        $this->db->update('employees', $data);
        $this->db->select("*");
        $this->db->where('emp_email', $email);
        $query = $this->db->get('employees');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    
    
    /*     * *************************** */
    /*     * ** User Querys ************ */
    /*     * *************************** */
    
    function countUsers(){
        return $this->db->count_all('hoosk_company');
    }
    
//    function getUsers($limit, $offset=0) {
//        // Get a list of all user accounts
//        $this->db->select("User_firstname,User_lastname,userName, email, userID,user_status");
//        $this->db->order_by("userName", "desc");
//  $this->db->limit($limit, $offset);
//        $query = $this->db->get('hoosk_user');
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return array();
//    }
    
//    function resultperpage()
//    {
//        $this->db->select("resultperpage");
//        $this->db->where("siteID", 0);
//        $query = $this->db->get('hoosk_settings');
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return array();
//    }
    
//    function getUser($id) {
//        // Get the user details
//        $this->db->select("*");
//        $this->db->where("userID", $id);
//        $query = $this->db->get('hoosk_user');
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        }
//        return array();
//    }

    function getUserEmail($id) {
        // Get the user email address
        $this->db->select("email");
        $this->db->where("companyId", $id);
        $query = $this->db->get('hoosk_company');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                $email = $rows->email;
                return $email;
            }
        }
    }
//    function change_status($data, $id) {
//        
//        $this->db->where('userID', $id);
//        if($this->db->update('hoosk_user', $data))
//        {
//            return TRUE;
//        }
//        else
//        {
//            return FALSE;
//        }
//    }
//    function createUser($filename = '') {
//        // Create the user account
//        $data = array(
//            'User_firstname' => $this->input->post('firstname'),
//            'User_lastname' => $this->input->post('lastname'),
//            'userName' => $this->input->post('username'),
//            'email' => $this->input->post('email'),
//            'phone' => $this->input->post('phone'),
//            //'password' => md5($this->input->post('password').SALT),
//            //'password' => base64_encode($this->input->post('password')),
//            'user_status'=>'Disabled',
//            'user_img'=>$filename,
//            'created_date'=>date("Y-m-d H:i:s"),
//            'modified_date'=>date("Y-m-d H:i:s"),
//        );
//        $this->db->insert('hoosk_user', $data);
//    }

//    function updateUser($id,$filename = '') {
//        if($filename)
//        {
//            // update the user account
//            $data = array(
//                'User_firstname' => $this->input->post('firstname'),
//                'User_lastname' => $this->input->post('lastname'),
//                'phone' => $this->input->post('phone'),
//                //'password' => base64_encode($this->input->post('password')),
//                'user_img'=> $filename,
//                'modified_date'=>date("Y-m-d H:i:s"),
//            );
//        }
//        else
//        {
//            // update the user account
//            $data = array(
//                'User_firstname' => $this->input->post('firstname'),
//                'User_lastname' => $this->input->post('lastname'),
//                'phone' => $this->input->post('phone'),
//                //'password' => base64_encode($this->input->post('password')),
//                'modified_date'=>date("Y-m-d H:i:s"),
//            );
//        }
//        $this->db->where('userID', $id);
//        $this->db->update('hoosk_user', $data);
//    }

//    function removeUser($id) {
//        // Delete a user account
//        $this->db->delete('hoosk_user', array('userID' => $id));
//    }
//        
    function getmail_template($id)
    {
        $this->db->select("*");
        $this->db->where("email_template_id", $id);
        $query = $this->db->get('email_template');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
            
    
}