<?php

class News_events_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    /*     * *************************** */
    /*     * ** Our News Events Querys ************ */
    /*     * *************************** */
    
    function get_news_events_datatable_data($module,$requestData)
    {
         $columns = array( 
                0 =>'news_title'
            );
        $sql = "SELECT * ";
        $sql.=" FROM $module ";
        $query= $this->db->query($sql);
        $totalData = $query->num_rows();
        $totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
        
        
        $sql = "SELECT * ";
        $sql.=" FROM $module WHERE 1=1";
        if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
            $sql.=" AND ( news_title LIKE '".$requestData['search']['value']."%' ) ";    
            
//          $sql.=" OR banner_status LIKE '".$requestData['search']['value']."%' )";
        }
        $query=$this->db->query($sql);
        $totalFiltered = $query->num_rows(); 
        if( isset($requestData['order'][0]['column']) ) {
            $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  ";
        }
        else
        {
            $sql.=" ORDER BY news_id  desc ";
        }
        if(isset($requestData['length']))
        {
            $sql.=" LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
        }
        
        
        
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data = array();
        if ($query->num_rows() > 0) {
            foreach($rows as $row) {  
                
                    $nestedData=array();
                    $nestedData[] = $row["news_title"];
                    $nestedData[] = '<img src="'.BASE_URL.'/uploads/news_events/'.$row['news_image'].'" width="100px" height="100px" >';
                    $statusbutton = '<button style="color:white" data-id="'.$row["news_id"].'" data-val="'.$row["news_status"].'"';
                    if ($row['news_status'] == "Disabled") {
                        $statusbutton .= ' class="bg-orange mystatus btn">Disabled</button>';
                    } elseif ($row['news_status'] == "Enabled") {
                        $statusbutton .= ' class="bg-green mystatus btn">Enabled</button>';
                    } else {
                        $statusbutton .= 'class="bg-red mystatus btn ">Deleted</button>';
                    }
                    $nestedData[] = $statusbutton;
                    $nestedData[] = '<td class="td-actions"><a href="'.BASE_URL.'/admin/news_events/edit/'.base64_encode($row['news_id']).'" class="btn btn-small btn-success"><i class="fa fa-pencil"> </i></a> <a data-toggle="modal" data-target="#ajaxModal" class="btn btn-danger btn-small" href="'.BASE_URL.'/admin/news_events/delete/'.base64_encode($row['news_id']).'"><i class="fa fa-remove"> </i></a></td>';
                    $data[] = $nestedData;
            }
            
        }
        $json_data = array(
            "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
            "recordsTotal"    => intval( $totalData ),  // total number of records
            "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data"            => $data   // total data array
            );
        
         return json_encode($json_data);
    }
    
    function countnews_events()
    {
        return $this->db->count_all('news_events');
    }

    function count_news_events_search($search_text)
    {       
        $this->db->select('count(*) as allnewscount');
        
        //if($search_text != '') {

            $this->db->like('news_title', $search_text);
            $this->db->or_like('short_content', $search_text);
            $this->db->or_like('news_desc', $search_text);
        //}

        $query = $this->db->get('news_events');
        $result = $query->result_array();

        return $result[0]['allnewscount'];
    }
    
    function get_news_events_search($limit, $start, $search_text) 
    {       
        $this->db->select("*");

        //if($search_text != '') {
            
            $this->db->like('news_title', $search_text);
            $this->db->or_like('short_content', $search_text);
            $this->db->or_like('news_desc', $search_text);
        //}
            
        $this->db->limit($limit, $start);
        $query = $this->db->get('news_events');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }                 
        return array();        
    }
    
    function getnews_events($limit, $offset)
    {
         // Get a list of all news_events
        $this->db->select("*");
        $this->db->order_by("news_id", "desc");
        $this->db->limit($limit, $offset);
        $query = $this->db->get('news_events');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function getnews_eventss($id)
    {
        // Get the news_events details
        $this->db->select("*");
        $this->db->where("news_id", $id);
        $query = $this->db->get('news_events');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
    
    function removenews_events($id)
    {
        // Delete a news_events
        $this->db->delete('news_events', array('news_id' => $id));
        
    }
    
    function changenews_events_status($data, $id) {
        
        $this->db->where('news_id', $id);
        if($this->db->update('news_events', $data))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    function  createnews_events($filename = '')
    {
        // Create the news_events
        $data = array(
            'news_title' => strip_tags($this->input->post('news_title')),
            'short_content' => $this->input->post('short_content'),
            'news_status'=>'Enabled',
            'news_image'=> $filename,
            'news_desc' => $this->input->post('news_desc'),
            'created_date'=>date("Y-m-d H:i:s"),
            'modified_date'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('news_events', $data);
    }
    
    function updatenews_events($id,$filename)
    {
        if($filename)
        {
            // update the user account
            $data = array(
                'news_title' => strip_tags($this->input->post('news_title')),
                'short_content' => $this->input->post('short_content'),
                'news_image'=> $filename,
                'news_desc' => $this->input->post('news_desc'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        else
        {
            // update the user account
            $data = array(
                'news_title' => strip_tags($this->input->post('news_title')),
                'short_content' => $this->input->post('short_content'),
                'news_desc' => $this->input->post('news_desc'),
                'modified_date'=>date("Y-m-d H:i:s"),
            );
        }
        $this->db->where('news_id', $id);
        $this->db->update('news_events', $data);
    }
    
}
?>