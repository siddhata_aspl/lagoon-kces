<?php


class ImageConverter extends BaseConverter implements ConverterInterface
{
    public function toJson(\DOMElement $node)
    {
        return array(
            'type' => 'image',
            'data' => array(
                'file' => array(
                    'url' => $node->getAttribute('src')
                )
            )
        );
    }

    public function toHtml(array $data)
    {
        return '<img class="img-responsive '.$data['tagclass'].'" src="' . $data['file']['url'] . '" id="'.$data['tagid'].'" />' . "\n";
    }
}
